open String
open Netstring_pcre

module Utfstring = struct
  open Netconversion

  exception MalformedString of string;;
  
  let get_char_len = function
      '\000'..'\127' -> 1
    | '\128'..'\223' -> 2
    | '\224'..'\239' -> 3
    | '\240'..'\247' -> 4
    | _ -> raise Malformed_code 
	
  (** Get the character string beginng at i *)
  let utfget_char str i =
    let len = get_char_len (unsafe_get str i) in
      (String.sub str i len, len)
      
  let length str =
    let get i =
      unsafe_get str i
    in
    let len = String.length str in
    let i = ref 0 in
    let res = ref 0 in
      while !i < len do
	i := !i + (get_char_len str.[!i]);
	res := succ !res;
      done;
      !res;;

  let match_string str substr = 
    let lena = String.length str and lenb = String.length substr in
    let utflenb = length substr in
    let ri = ref 0 in
    let initi = ref 0 in
    let rj = ref 0 in
    let matched = ref 0 in
      if lena < lenb then raise Not_found;
      while not (!matched = utflenb) && !ri < lena && !rj < lenb do
	let (x, i) = utfget_char str !ri in
	let (y, j) = utfget_char substr !rj in
	  if x = y then (
	    matched := succ !matched;
	    ri := !ri + i;
	    rj := !rj + i;
	  ) else (
	    matched := 0;
	    let (x, newi) = utfget_char str !initi in
	      ri := !ri + newi;
	      initi := !ri;
	      rj := 0;
	  )
      done;
      if (!matched = utflenb) then 
	!initi 
      else 
	raise Not_found;;

  let index_of_nth_char str i =
    let j = ref 0 in
    let r = ref 0 in
      while !j != i do
	r := !r + (get_char_len str.[!r]);
	j := succ !j;
      done;
      !r

  let first_chars str i =
    let j = index_of_nth_char str i in
      first_chars str j;;

  let string_after str i =
    let j = index_of_nth_char str i in
      string_after str (j + (get_char_len str.[j]));;

  let sub str i j =
    let idx = index_of_nth_char str i and jdx = index_of_nth_char str j in
      String.sub str idx (jdx-idx)
	

end

let string_of_list func sep list = 
  match list with 
      [] -> ""
    | hd::[] -> func hd
    | hd::tl -> List.fold_left 
	(fun res x ->
	   res ^ sep ^ (func x)) (func hd) tl;;

let split_string str i = 
  (String.sub str 0 i, string_after str (i+1))

let trim str = 
  let whitespace = function
      ' ' | '\n' | '\t' | '\r' -> true
    | _ -> false
  in
  let length = String.length str in
  let i = ref 0 in    
    while !i < length && whitespace str.[!i] do
      incr i
    done;
    if !i = length then ""
    else 
      let str = String.sub str !i (length - !i) in
	i := length - !i - 1;
	while !i >= 0 && whitespace str.[!i] do
	  decr i
	done;
	String.sub str 0 (!i + 1)

let whitespace_split = 
  Netstring_pcre.split (Netstring_pcre.regexp "( |\n|\r|\t)+")

let list_filter_option f =
  let rec aux = function
    | hd :: tl -> 
       (match f hd with
          | Some v -> v :: aux tl
          | None -> aux tl)
    | [] -> []
  in aux
       
let list_first_option fn = 
  let rec aux = function
    | hd::tl -> 
        (match fn hd with
           | Some v -> v
           | None -> aux tl)
    | [] -> raise Not_found
  in aux
