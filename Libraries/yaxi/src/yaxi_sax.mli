class type sax_handler =
object
    method documentDeclaration : (string * string) list -> unit
    method startNSMapping : Yaxi_xml.ns_mapping -> unit
    method endNSMapping : Yaxi_xml.ns_mapping -> unit
    method startElement :
      Yaxi_xml.qname ->
      (Yaxi_xml.qname * Yaxi_xml.cdata) list -> unit
    method endElement : Yaxi_xml.qname -> unit
    method characters : Yaxi_xml.cdata -> unit
    method comment : Yaxi_xml.cdata -> unit
    method pi : Yaxi_xml.ncname -> Yaxi_xml.cdata -> unit
  end
