(* Yaxi - Yet Another X* Implementation
 * Copyright (C) 2003 Matthieu Sozeau
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * $Id: yaxi_xsltxpathext.mli,v 1.3 2003/10/20 00:17:18 mat Exp $
 * 
 *)

(** Yaxi XSLT XPath extension functions.
  It adds all the declared functions to the additional functions list of the
  XSLT processor at initialization time.

  @see <http://www.w3.org/TR/xslt#add-func> Additional functions.
*)

val document : Yaxi_xslt.xslt_extension_function  
val key : Yaxi_xslt.xslt_extension_function  
val current : Yaxi_xslt.xslt_extension_function  
val unparsed_entity_uri : Yaxi_xslt.xslt_extension_function  
val generate_id : Yaxi_xslt.xslt_extension_function  
val system_property : Yaxi_xslt.xslt_extension_function  
val element_available : Yaxi_xslt.xslt_extension_function  
val function_available : Yaxi_xslt.xslt_extension_function  
