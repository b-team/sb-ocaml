(* Yaxi - Yet Another X* Implementation
 * Copyright (C) 2003 Matthieu Sozeau
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * $Id: yaxi_xmllexer.mli,v 1.4 2003/09/28 22:42:58 mat Exp $
 * 
 * The xml lexer productions and functions
 *)

exception ParseError of (int * int) * string * string

val parse_error : Ulexing.lexbuf -> string -> 'a

type production =
    Comment of string
  | Pi of string * string
  | OpenElement of Yaxi_xml.qname * Yaxi_xml.ns_mappings * Yaxi_xml.attributes
  | EmptyElement of Yaxi_xml.qname * Yaxi_xml.ns_mappings * Yaxi_xml.attributes
  | CloseElement of Yaxi_xml.qname
  | Whitespace of string
  | Cdata of string
  | Text of string
  | ExternalParsedEntityRef of string * string * string (* name, public ID, system ID *)
  | Doctype of Yaxi_xml.dtd
  | DocumentEnd

val token : ?keep_refs:bool -> Yaxi_xml.ns_mappings -> Yaxi_xml.dtd -> Ulexing.lexbuf -> production

val parse_options : string -> (string * string) list
