(* Yaxi - Yet Another X* Implementation
 * Copyright (C) 2003 Matthieu Sozeau
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * $Id: yaxi_utils.mli,v 1.3 2003/10/19 23:58:41 mat Exp $ 
 * 
 * Utf-8 strings functions and little utility functions.
 *)

module Utfstring :
  sig
    exception MalformedString of string
    val get_char_len : char -> int
    val utfget_char : string -> int -> string * int
    val length : string -> int
    val match_string : string -> string -> int
    val index_of_nth_char : string -> int -> int
    val first_chars : string -> int -> string
    val string_after : string -> int -> string
    val sub : string -> int -> int -> string
  end
val string_of_list : ('a -> string) -> string -> 'a list -> string
val split_string : string -> int -> string * string

(** Removes whitespace at start and end of the string *)
val trim : string -> string

(** Splits the string using whitespace characters as separators *)
val whitespace_split : string -> string list

(** Keep only Some v values returned by function *)
val list_filter_option : ('a -> 'b option) -> 'a list -> 'b list

(** Returns the first v in Some v returned by f or raises Not_found *)
val list_first_option : ('a -> 'b option) -> 'a list -> 'b
