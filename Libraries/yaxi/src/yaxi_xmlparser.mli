(* Yaxi - Yet Another X* Implementation
 * Copyright (C) 2003 Matthieu Sozeau
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * $Id: yaxi_xmlparser.mli,v 1.8 2003/09/27 17:53:45 mat Exp $
 * 
 *)

(** XML document parser *)

(** Syntax error in the document *)
exception SyntaxError of string

(** Parse a file *)
val parse_file : string -> Yaxi_xml.document

(** Parse a string buffer *)
val parse_string : string -> Yaxi_xml.document

(** Parse a file using a sax handler *)
val sax_parse_file : Yaxi_sax.sax_handler -> string -> unit

(** Parse a string buffer using a sax handler *)
val sax_parse_string : Yaxi_sax.sax_handler -> string -> unit

