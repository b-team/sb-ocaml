(* Yaxi - Yet Another X* Implementation
 * Copyright (C) 2003 Matthieu Sozeau
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * $Id: yaxi_xpathlexer.ml,v 1.4 2004/02/18 22:42:37 mat Exp $
 * 
 * XPath lexer
 *)

open Yaxi_xpathparser

let regexp digit = xml_digit
let regexp digits = digit+
		    
let regexp number = digits (("." digits)?) | "." digits

let regexp nc_name_char = xml_letter | xml_digit | '.' | '-' | '_' | xml_combining_char | xml_extender

let regexp nc_name = (xml_letter | '_') (nc_name_char*)

let regexp name = (nc_name ':')? nc_name

let regexp axisname = ((xml_letter | '-') +)

let regexp space = ' ' | '\n' | '\t' | '\r'

let regexp s = (space+)

let enclosed lexbuf offset_begin offset_end = 
  let len = Ulexing.lexeme_length lexbuf in
    Ulexing.utf8_sub_lexeme lexbuf offset_begin (len - (offset_end + offset_begin))
      		  
let return lexbuf tok = (tok, Ulexing.loc lexbuf)
let return_loc i j tok = (tok, (i,j))
                           
let rec old_token = 
  lexer
  | "/" -> return lexbuf SLASH 
  | "[" -> return lexbuf LBRACKET 
  | "]" -> return lexbuf RBRACKET 
  | "(" -> return lexbuf LPAREN 
  | ")" -> return lexbuf RPAREN 
  | "::" -> return lexbuf COLONCOLON 
  | ":" -> return lexbuf COLON 
  | "*" -> return lexbuf STAR 
  | "," -> return lexbuf COMMA 
  | ".." -> return lexbuf DOTDOT 
  | "." -> return lexbuf DOT 
  | "+" -> return lexbuf PLUS 
  | "-" -> return lexbuf MINUS 
  | "!=" -> return lexbuf INEQUAL 
  | "=" -> return lexbuf EQUAL 
  | ">=" -> return lexbuf SUPEQUAL 
  | "<=" -> return lexbuf INFEQUAL 
  | ">" -> return lexbuf SUP 
  | "<" -> return lexbuf INF 
  | "|" -> return lexbuf PIPE 
  | "@" -> return lexbuf AT 
  | "$" -> 
      ignore(Ulexing.utf8_lexeme lexbuf);
      return lexbuf (VARIABLE (tname lexbuf))
  | "'" -> ignore(Ulexing.lexeme lexbuf); 
	  let str = tcdata_quote lexbuf in
	    get_quote lexbuf;
	    return lexbuf (LITERAL(str))
  | "\"" ->
      ignore(Ulexing.lexeme lexbuf); 
      let str = tcdata_dblquote lexbuf in
	get_dblquote lexbuf;
	return lexbuf (LITERAL(str))
  | axisname "::" -> return lexbuf (AXIS(enclosed lexbuf 0 2))
  | number -> return lexbuf (NUMBER(float_of_string (Ulexing.utf8_lexeme lexbuf)))
  | nc_name ":*" -> return lexbuf (NAME(Ulexing.utf8_lexeme lexbuf))
  | name "()" ->
      let app = Ulexing.utf8_lexeme lexbuf in
	(match app with
	     "text()" -> return lexbuf TEXTP
	   | "comment()" -> return lexbuf COMMENTP
	   | "processing-instruction()" -> return lexbuf PIP
	   | "node()" -> return lexbuf NODEP
	   | _ -> return lexbuf (FUNCTION(String.sub app 0 ((String.length app) - 2))))
  | name "(" -> 
      let app = Ulexing.utf8_lexeme lexbuf in
	(match app with
	   | "processing-instruction(" -> return lexbuf PIP
	   | _ -> return lexbuf (FUNCTION(String.sub app 0 ((String.length app) - 1))))
  | name -> 
      (match Ulexing.utf8_lexeme lexbuf with
	   "and" -> return lexbuf AND
	 | "or" -> return lexbuf OR
	 | "div" -> return lexbuf DIV
	 | "mod" -> return lexbuf MOD
	 | "not" -> return lexbuf NOT
	 | n -> return lexbuf (NAME(n)))
  | s -> ignore(Ulexing.lexeme lexbuf); old_token lexbuf
  | eof -> return lexbuf EOF
  | _ -> failwith ("Unknown token: " ^ (Ulexing.utf8_lexeme lexbuf)) 

and token =
  lexer
  | "/" | "[" | "]" | "(" | ")" | "::" | ":"
  | "*" | "," | ".." | "." | "+" | "-" | "!="
  | "=" | ">=" | "<=" | ">" | "<" | "|" | "@" ->
      return lexbuf ("", (Ulexing.utf8_lexeme lexbuf))
  | "$" -> 
      ignore(Ulexing.utf8_lexeme lexbuf);
      return lexbuf ("VARIABLE", (tname lexbuf))
  | "'" -> ignore(Ulexing.lexeme lexbuf); 
      let str = tcdata_quote lexbuf in
	get_quote lexbuf;
	return lexbuf ("LITERAL", str)
  | "\"" ->
      ignore(Ulexing.lexeme lexbuf); 
      let str = tcdata_dblquote lexbuf in
	get_dblquote lexbuf;
	return lexbuf ("LITERAL", str)
  | axisname "::" -> return lexbuf ("AXIS", enclosed lexbuf 0 2)
  | number -> return lexbuf ("NUMBER", Ulexing.utf8_lexeme lexbuf)
  | nc_name ":*" -> return lexbuf ("NAME", Ulexing.utf8_lexeme lexbuf)
  | name "()" ->
      let app = Ulexing.utf8_lexeme lexbuf in
	(match app with
	     "text()" -> return lexbuf ("TEXTP", "")
	   | "comment()" -> return lexbuf ("COMMENTP", "")
	   | "processing-instruction()" -> return lexbuf ("PIP", "")
	   | "node()" -> return lexbuf ("NODEP", "")
	   | _ -> return lexbuf ("FUNCTION", String.sub app 0 ((String.length app) - 2)))
  | name "(" -> 
      let app = Ulexing.utf8_lexeme lexbuf in
	(match app with
	   | "processing-instruction(" -> return lexbuf ("PIP", "")
	   | _ -> return lexbuf ("FUNCTION", String.sub app 0 ((String.length app) - 1)))
  | name -> 
      (match Ulexing.utf8_lexeme lexbuf with
	   "and" -> return lexbuf ("AND", "")
	 | "or" -> return lexbuf ("OR", "")
	 | "div" -> return lexbuf ("DIV", "")
	 | "mod" -> return lexbuf ("MOD", "")
	 | "not" -> return lexbuf ("NOT", "")
	 | n -> return lexbuf ("NAME", n))
  | s -> ignore(Ulexing.lexeme lexbuf); token lexbuf
  | eof -> return lexbuf ("EOI", "")
  | _ -> failwith ("Unknown token: " ^ (Ulexing.utf8_lexeme lexbuf)) 
      
and tname = lexer
| name -> Ulexing.utf8_lexeme lexbuf 

and tcdata_quote = lexer
| [^ "'"]* -> Ulexing.utf8_lexeme lexbuf 
    
and get_quote = lexer
| "'" -> Ulexing.utf8_lexeme lexbuf 
    
and tcdata_dblquote = lexer
| [^ '"']* -> Ulexing.utf8_lexeme lexbuf 
    
and get_dblquote = lexer
| "\"" -> Ulexing.utf8_lexeme lexbuf 

exception Lexbuf of Ulexing.lexbuf
let normal_token lexbuf =
  try 
    lexbuf.Lexing.refill_buff lexbuf;
    failwith "Don't use it I said!!!"
  with Lexbuf(l) -> 
    (*print_endline "Got normal_token raised lexbuf";*)
    match fst (old_token l) with
	EOF -> lexbuf.Lexing.lex_eof_reached <- true; EOF
      | other -> other          
(*
let keywords : (string,unit) Hashtbl.t = Hashtbl.create 17

exception Error of int * int * string

let error i j s = raise (Error (i,j,s))

let lexbuf = ref None
let last_tok = ref ("", "")
                 
let enc = ref Ulexing.Utf8

let tok_func cs =
  let lb = Ulexing.from_var_enc_stream enc cs in
    (lexer ("#!" [^ '\n']* "\n")? -> ()) lb;
  lexbuf := Some lb;
  let next () =
    let tok = 
      try token lb
      with
	| Ulexing.Error -> 
	    raise (Error (Ulexing.lexeme_end lb, Ulexing.lexeme_end lb,
			  "Unexpected character"))
	| Ulexing.InvalidCodepoint i ->
	    raise (Error (Ulexing.lexeme_end lb, Ulexing.lexeme_end lb,
			  "Code point invalid for the current encoding")) in
      last_tok := fst tok;
      tok
  in
    Token.make_stream_and_location next
      
let register_kw (s1,s2) =
  if s1 = "" then 
    match s2.[0] with 
      | 'a' .. 'z' when not (Hashtbl.mem keywords s2) -> 
	  Hashtbl.add keywords s2 ()      
      | _ -> ()

let lex =
  { 
    Token.tok_func = tok_func;
    Token.tok_using = register_kw;
    Token.tok_removing = (fun _ -> ()); 
    Token.tok_match = Token.default_match;
    Token.tok_text = Token.lexer_text;
    Token.tok_comm = None;
  }

*)
