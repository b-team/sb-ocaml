open Yaxi_xml

class type sax_handler = object
  
  method documentDeclaration : (string * string) list -> unit
    
  method startNSMapping : ns_mapping -> unit
    
  method endNSMapping : ns_mapping -> unit
    
  method characters : cdata -> unit
    
  method startElement : qname -> (qname * cdata) list -> unit
    
  method endElement : qname -> unit

  method comment : cdata -> unit

  method pi : ncname -> cdata -> unit
end
