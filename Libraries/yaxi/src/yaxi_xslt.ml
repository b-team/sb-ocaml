(* Yaxi - Yet Another X* Implementation
 * Copyright (C) 2003 Matthieu Sozeau
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * $Id: yaxi_xslt.ml,v 1.30 2004/05/02 11:45:21 mat Exp $
 * 
 *)

(** 
  XSLT recomendation implementation.
*)

open List
open Printf
open Yaxi_xml
open Yaxi_xmlparser
open Yaxi_xpath
open Yaxi_xpathxml

let xsl_ns = `URI("http://www.w3.org/1999/XSL/Transform")
let xsl_ns_mapping = ("xsl", xsl_ns)
                       
let properties : Yaxi_xpath.primary_type qname_assoclist = 
  [((xsl_ns_mapping, "version"), Number 1.0);
   ((xsl_ns_mapping, "vendor"), Literal "Matthieu Sozeau (mattam)");
   ((xsl_ns_mapping, "vendor-url"), Literal "http://mattam.ath.cx/soft/yaxi.html");
  ]
  
let get_property ?default p = 
  match default with
    | Some v -> qname_try_assoc p properties v
    | None -> qname_assoc p properties

 
type xsl_test = Yaxi_xpath.global_context -> Yaxi_xpath.eval_context -> bool
  
(** Type of xpath functions returing numbers *)
type number_expr = Yaxi_xpath.global_context -> Yaxi_xpath.eval_context -> float

type value_template = [ 
  `Text of string
| `Expr of xpath_expr
]

type uri_reference = string

type mode = string

type pattern = xpath_expr list (* | separated list of expressions *)

type nmtoken = string

type priority = float

type xsl_decimal_format = {
  name: qname;
  decimal_separator: char;
  grouping_separator: char;
  infinity: string;
  minus_sign: char;
  nan: string;
  percent: char;
  per_mille: char;
  zero_digit: char;
  digit: char;
  pattern_separator: char
}

type xsl_number = {
  level: [`Single | `Multiple | `Any ];
  count: pattern;
  from: pattern;
  value: number_expr;
  format: string;
  lang: nmtoken;
  letter_value: [`Alphabetic | `Traditional ];
  grouping_separator: char;
  grouping_size: float;
}

type xsl_output = {
  method_: [`XML | `Text | `HTML | `QName of string ];
  version: nmtoken;
  encoding: string;
  omit_xml_decl: bool;
  standalone: bool option;
  doctype_public: string;
  doctype_system: string;
  cdata_elements: qnames;
  indent: bool;
  media_type: string;		      
}

type xsl_sort = {
  select: Yaxi_xpath.xpath_expr;
  lang: nmtoken;
  data_type: [`Text | `Number | `QName of string];
  order: [ `Ascending | `Descending ];
  case_order: [ `Upper_first | `Lower_first ];
}

(** XSL control structures *)
type template =
    XSLIf of xsl_test * templates
  | XSLChoose of (xsl_test * templates) list * templates option   (* otherwise is optional *)
  | XSLValueOf of bool * xpath_expr                               (* disable-output-escaping, select="" *)
  | XSLCopy of qname list * templates
  | XSLCopyOf of xpath_expr
  | XSLApplyTemplates of xpath_expr * mode * xsl_params * (xsl_sort list)  (* select, mode, params, sort *)
  | XSLCallTemplate of qname * xsl_params
  | XSLApplyImports 
  | XSLElement of qname * templates
  | XSLOrigElement of node ref * qname * nodeset ref * nodeset ref * templates
  | XSLAttribute of qname * templates
  | XSLText of bool * cdata                                      (* Disable-output-escaping *)
  | XSLFallback of templates
  | XSLForEach of xpath_expr * templates * (xsl_sort list)
  | XSLMessage of bool * templates
  | XSLProcessingInstruction of ncname * templates
  | XSLVariable of xsl_param
  | XSLParam of xsl_param
  | XSLExtension of instruction
  | XSLNop (* No-operation *)
and templates = template list
and xsl_param = Yaxi_xml.qname * Yaxi_xpath.xpath_expr option * templates option
and xsl_params = xsl_param list
and instruction = Yaxi_xpath.global_context -> 
  Yaxi_xpath.eval_context -> 
  Yaxi_xml.nodeset option

type xsl_key = qname * (xpath_expr * xpath_expr)

(** Type of an xsl:template element *)
type xsl_template = {   
  match_expr: (priority * xpath_expr) list;
  name: qname option;
  parameters: xsl_params;
  instructions: templates ref;
  mutable compiled_fun: (Yaxi_xpath.global_context -> Yaxi_xpath.eval_context -> nodeset option) option
}

(** Type of xsl:stylesheet *)
type stylesheet = {
  
  mutable templates: xsl_template list;
  
  mutable params: xsl_param list;
  
  mutable variables: xsl_param list;   
  
  mutable namespaces: ns_mapping list;
  mutable namespace_aliases: (string * string) list;
  
  mutable attribute_sets: (qname * (qname * templates) list) list;
  
  mutable decimal_formats: xsl_decimal_format list;
  
  mutable preserve_space: Yaxi_xpath.xpath_expr list;
  mutable strip_space: Yaxi_xpath.xpath_expr list;
  
  mutable imports: (uri_reference * stylesheet) list;
  
  mutable keys: xsl_key list;
  mutable output: xsl_output option;
  
  mutable extension_prefixes: Yaxi_xml.ncname list;
  mutable extension_elements: extension_elements list;
  mutable extension_functions: extension_functions list;
}
  
and xslt_extension_element = 
   Yaxi_xml.node -> stylesheet -> Yaxi_xml.ns_mappings -> Yaxi_xml.ncnames -> instruction

and extension_element = Yaxi_xml.ncname * xslt_extension_element
and extension_elements = string * extension_element list

and xslt_extension_function = stylesheet -> Yaxi_xml.document -> Yaxi_xpath.xpath_function
and extension_function = ncname * xslt_extension_function
and extension_functions = string * extension_function list


(** Exception launched when the stylesheet does not conform to the spec *)
exception XSLError of string

(** Exception launched when an halting message is encountered *)
exception XSLStylesheetDirectedStop

(** Evaluates an XPath expression with the xsl prefix bound to the XSLT namespace *)
let xpath_eval str node =
  let parse = Yaxi_xpathxml.parse str in
  let expr = Yaxi_xpathxml.compile_expression parse in
  let gctx = Yaxi_xpathxml.default_context () in
  let ctx = new_context node 1 1 in
    context_add_ns gctx ("xsl", xsl_ns);
    expr gctx ctx    

let current_qname = (default_ns, "current")

let compile_expression str = 
  let expr = Yaxi_xpathxml.compile str in
    (fun gctx ctx ->
       gctx.library <- (current_qname, (fun _ _ _ -> Nodes [ctx.node])) :: gctx.library;
       let res = expr gctx ctx in
         gctx.library <- List.tl gctx.library;
         res)
    
(** Transform the xpath steps of a pattern using the rules of XSL. *)
let rec transform_steps ?(first_axis = `Self) steps =
  let rev_steps = List.rev steps in
    match rev_steps with
      | [] -> []
      | (axis, test, preds)::tl -> 
	  let tail =
	    List.map 
	      (fun (axis, test, preds) -> 
		 let new_axis = 
		   match axis with
		       `Child -> `Parent
		     | `Parent -> `Child
		     | `Descendant -> `Ancestor
		     | `Descendant_or_self -> `Ancestor_or_self
		     | `Ancestor -> `Descendant
		     | `Ancestor_or_self -> `Descendant_or_self
		     | _ -> axis
		 in
		   (new_axis, test, preds))
	      tl
	  in
            (first_axis, test, preds)::tail
            
(** Parses a pattern *)
let parse_pattern ?(first_axis = `Self) str = 
  let cases = Netstring_pcre.split (Netstring_pcre.regexp "\\|") str in    
    List.map 
      (fun x -> 
	 match Yaxi_xpathxml.parse x with
	   | `Steps (dir, list) -> `Steps (`Relative, (transform_steps ~first_axis:first_axis list))
	   | _ as expr -> expr) 
      cases

let compile_pattern_cases cases =
  let comp_cases = 
    List.map Yaxi_xpathxml.compile_expression cases
  in
    (fun gctx ctx ->
       let res = ref (Nodes []) in
       let i = ref comp_cases
       in
	 while !i <> [] && (boolean_of_object !res) = false do
	   res := (List.hd !i) gctx ctx;
	   i := List.tl !i;
	 done;
	 !res)
    
(** Compile an XSL pattern *)
let compile_pattern ?(first_axis = `Self) str =
  compile_pattern_cases (parse_pattern ~first_axis str)

let string_of_template tmpl =
  let namestr =
    (match tmpl.name with
	 Some n -> (string_of_name n)
       | None -> "None")
  in
    sprintf "Name: %s" namestr

let string_of_param (name, expr, nodes) =
  string_of_name name;;

let get_extension_prefixes st nss str =
  try 
    let nclist = Yaxi_utils.whitespace_split str in
      assert (List.for_all 
                (fun n -> 
                   (List.mem_assoc n st.namespaces) ||
                   (List.mem_assoc n st.namespace_aliases) ||
                   (List.mem_assoc n nss)) nclist);
      nclist
  with Not_found -> []
    
(** Parses and compiles an instruction with the "test" attribute *)
let rec parse_test_instruction st nss prefs instr =
  let test = 
    get_attr_required "test" (attribute_nodes instr) 
      (required_attr "test" (string_of_node instr)) 
  in
  let compiled = compile_expression test in
  let children = children instr in
    ((fun gctx ctx -> Yaxi_xpathxml.boolean_of_object (compiled gctx ctx)), 
     parse_instructions st nss prefs children)
    
(** Parses a parameter/variable element *)    
and parse_param st namespaces prefixes param =
  let attrs = attribute_nodes param in
    if not (attr_mem_assoc "name" attrs) then
      raise (SyntaxError (sprintf "Name required for parameter %s" (string_of_node param)))
    else
      let name = attr_assoc "name" attrs in
      let qname = Yaxi_xml.parse_name st.namespaces name in
	match try_get_attr "select" attrs "" with
	    "" -> (qname, None, 
                   Some (parse_instructions st namespaces prefixes (children param)))
	  | expr -> let comp = compile_expression expr in
	      (qname, Some comp, None)

and parse_sort = function
    Element (p, (ns, name), nss, attrs, children) ->
      let datatype = match try_get_attr "data-type" !attrs "text" with
	  "text" -> `Text
	| "number" -> `Number
	| qname -> `QName qname
      and order = match try_get_attr "order" !attrs "ascending" with
	  "ascending" -> `Ascending
	| "descending" -> `Descending
	| str -> failwith ("Invalid order value in xsl:sort: " ^ str)
      and caseorder = match try_get_attr "case-order" !attrs "upper-first" with
	  "upper-first" -> `Upper_first
	| "lower-first" -> `Lower_first
	| str -> failwith ("Invalid case-order value in xsl:sort: " ^ str)
      in
	{ select = compile_expression (try_get_attr "select" !attrs ".");
	  lang = try_get_attr "lang" !attrs "en";
	  data_type = datatype;
	  order = order;
	  case_order = caseorder }
  | _ -> failwith ("Unexpected node type for xsl:sort element")

(** Parses an XSLT template instruction element *)	      
and parse_instruction (st : stylesheet) (namespaces : ns_mappings)
  (prefixes : ncname list) (instr : node) =
  match instr with
     Element (p, ((pref, ns), name), nss, attrs, children) when ns = xsl_ns -> 
       (match name with
	   "if" -> 
	     let (test, sub) = parse_test_instruction st namespaces prefixes instr in
	       XSLIf (test, sub)

	  | "choose" -> 
	      let whens = 
		let nodes = 
		  try 
		    (match xpath_eval "xsl:when" instr with
		       | Nodes n -> n
		       | _ -> raise Not_found)
		  with Not_found -> required_elem "when" "choose" ()
		in
		  List.map (parse_test_instruction st namespaces prefixes) nodes				     
	      and otherwise = 
		try 
		  (match xpath_eval "xsl:otherwise" instr with
		     | Nodes (hd::[]) -> 
			 let instructions = 
                           List.map 
                             (parse_instruction st namespaces prefixes) 
                             (Yaxi_xml.children hd) 
                         in
			   Some instructions
		     | _ -> None)
		with Not_found -> None
	      in XSLChoose (whens, otherwise)
                  
	  | "value-of" ->
	      let select = get_attr_required "select" !attrs (required_attr "select" "value-of") in
		XSLValueOf (try_get_bool_attr "disable-output-escaping" !attrs false, 
			    compile_expression select)

	  | "apply-templates" ->
	      let select = 
		let attr = try_get_attr "select" !attrs "node()" in
		  compile_expression attr
	      and params = 
		try 
		  (match xpath_eval "xsl:with-param" instr with
		     | Nodes n -> List.map (parse_param st namespaces prefixes) n
		     | _ -> [])
		with Not_found -> []
	      and sorts = 
		try 
		  (match xpath_eval "xsl:sort" instr with
		     | Nodes n -> List.map parse_sort n
		     | _ -> [])
		with Not_found -> []
	      in
		XSLApplyTemplates (select, "", params, sorts)

	  | "for-each" ->
	      let select = get_attr_required "select" !attrs (required_attr "select" "for-each")
	      and sorts = 
		try 
		  (match xpath_eval "xsl:sort" instr with
		     | Nodes n -> List.map parse_sort n
		     | _ -> [])
		with Not_found -> []
	      in XSLForEach (compile_expression select, 
                             parse_instructions st namespaces prefixes !children, sorts)

	  | "copy" ->
	      XSLCopy ([], parse_instructions st namespaces prefixes !children)
              
	  | "copy-of" ->
	      let select = get_attr_required "select" !attrs (required_attr "select" "copy-of") in
		XSLCopyOf (compile_expression select)

	  | "text" -> 
	      let doe = try_get_bool_attr "disable-output-escaping" !attrs false in
		XSLText (doe, (string_value_of_nodes !children))

	  | "call-template" ->
	      let name = get_attr_required "name" !attrs (required_attr "name" "call-template")
	      and params = 
		try 
		  (match xpath_eval "xsl:with-param" instr with
		      Nodes n -> List.map (parse_param st namespaces prefixes) n
		     | _ -> [])
		with Not_found -> []
	      in
		XSLCallTemplate (Yaxi_xml.parse_name namespaces name, params)
                  
	  | "param" -> XSLParam (parse_param st namespaces prefixes instr)
	      
	  | "variable" -> XSLVariable (parse_param st namespaces prefixes instr)

	  | "attribute" -> 
              let name = get_attr_required "name" !attrs (required_attr "attribute" "name") in
	        XSLAttribute (Yaxi_xml.parse_name namespaces name, 
			      parse_instructions st namespaces prefixes !children)

	  | "element" -> 
              let name = get_attr_required "name" !attrs (required_attr "element" "name") in
	        XSLElement (Yaxi_xml.parse_name namespaces name,
			    parse_instructions st namespaces prefixes !children)
		  
          | "apply-imports" -> XSLApplyImports
              
	  | _ -> raise (XSLError ("Unimplemented tag " ^ name)))
       
    | Element (p, (((prefix, uri), lname) as name), nss, attrs, children) ->
        (match uri with
           | `URI uri ->
               let use_extension namespaces prefs =
                 try
                   let extension = List.assoc uri st.extension_elements in
                   let elext = List.assoc lname extension in
                     XSLExtension (elext instr st namespaces prefs)
                 with Not_found -> 
                   XSLOrigElement (p, name, nss, attrs, 
                                   parse_instructions st namespaces prefs !children)
               in 
               let namespaces = Yaxi_xml.namespaces instr @ namespaces in
               let list =
                 try attrns_assoc xsl_ns "extension-namespace-prefixes" !attrs
                 with Not_found -> "" 
               in
               let prefixes = (get_extension_prefixes st namespaces list) @ prefixes in
                 if List.mem prefix prefixes
                 then use_extension namespaces prefixes
                 else
                   XSLOrigElement (p, name, nss, attrs, 
                                   parse_instructions st namespaces prefixes !children)
           | `None -> 
               XSLOrigElement (p, name, nss, attrs, 
                               parse_instructions st namespaces prefixes !children))
        
    | Text (p, str) -> XSLText (false, str) (*Yaxi_utils.trim str)*)

    | Comment (p, str) -> XSLApplyImports

    | _ ->
	printf "Unknown element at top-level\n";
	XSLApplyImports
          
(*	  raise (XSLError "Element forbidden at top-level")*)

and parse_instructions st namespaces prefixes = 
  List.map (parse_instruction st namespaces prefixes)
                              
let get_priority = function
   `Steps (_, (axis, test, _) :: tl) ->
     if axis = `Attribute || axis = `Child then
       (match test with
          | `NameTest (pref, "*") -> -0.25
          | `NameTest _ | `ProcessingInstructionp (Some _) -> 0.0
          | `Commentp | `Textp | `Nodep 
          | `ProcessingInstructionp _ | `Anyp -> -0.5
          | _ -> 0.5)
     else 0.5
  | _ -> 0.5
     
(** Parse an xsl:template element *)
let parse_template st attrs ( children : nodeset ref ) = 
  let instrs, params = 
    List.partition 
      (fun instr -> 
	 match instr with
	     XSLParam _ -> false
	   | _ -> true)
      (parse_instructions st st.namespaces st.extension_prefixes !children)
  in
  let priority p = 
    match (try_get_attr "priority" attrs "") with
      | "" -> get_priority p
      | str -> float_of_string str
  in
  let expr =    
    match try_get_attr "match" attrs "" with
      | "" -> []
      | str ->
          List.map 
          (fun p -> (priority p, Yaxi_xpathxml.compile_expression p)) 
          (parse_pattern str)
  (*in let _ = printf "pattern %s has priority %f\n" (try_get_attr "match" attrs "(none)") (fst (List.hd expr)) *)
  in
    {
      compiled_fun = None;
      match_expr = expr;
      parameters = List.map
		     (fun x -> 
		        match x with 
			   XSLParam param -> param 
			  | _ -> failwith "BUG!!!") 
		     params;
      instructions = ref instrs;
      name = (match try_get_attr "name" attrs "" with
		 "" -> None
	        | _ as n -> Some (Yaxi_xml.parse_name st.namespaces n));
    };;

let bool_of_string default = function
    "false" -> false
  | "true" -> true
  | _ -> default

let parse_output element = 
  let attrs = attributes element in
  let meth = 
    match qname_try_assoc_nc "method" attrs "xml" with
	 "xml" -> `XML
       | "text" -> `Text
       | "html" -> `HTML
       | _ as name -> `QName name
  and standalone = 
    match qname_try_assoc_nc "standalone" attrs "" with
      "yes" -> Some true
    | "no" -> Some false
    | _ -> None
  and cdatasect = 
    Yaxi_utils.whitespace_split (qname_try_assoc_nc "cdata-section-elements" attrs "")
  in
    { method_ = meth;
      version = qname_try_assoc_nc "version" attrs "1.0";
      encoding = qname_try_assoc_nc "encoding" attrs "UTF-8";
      omit_xml_decl = (bool_of_string false (qname_try_assoc_nc "omit-xml-declaration" attrs "false"));
      standalone = standalone;
      doctype_public = qname_try_assoc_nc "doctype-public" attrs "";
      doctype_system = qname_try_assoc_nc "doctype-system" attrs "";
      media_type = qname_try_assoc_nc "media-type" attrs "";
      cdata_elements = [];
      indent = bool_of_string false (qname_try_assoc_nc "indent" attrs "false");
    }

let parse_key ns attrs =
  let get_attr name =
    get_attr_required name attrs (required_attr name "xsl:key")
  in
    (Yaxi_xml.parse_name ns (get_attr "name"), 
     (compile_pattern ~first_axis:`Descendant_or_self (get_attr "match"),
      Yaxi_xpathxml.compile (get_attr "use")))
     
(** Parses an xsl:stylesheet *)
let rec parse_stylesheet ?(extension_elements=[]) ?(extension_functions=[]) doc = 
  let st = 
    {
      templates = [];
      params = [];
      variables = [];
      attribute_sets = [];
      decimal_formats = [];
      preserve_space = [];
      strip_space = [];
      imports = [];
      namespace_aliases = [];
      keys = [];
      output = None;
      namespaces = [];
      extension_prefixes = [];
      extension_elements = extension_elements;
      extension_functions = extension_functions;
    }
  in
  let get_whitespace_elem tag attrs =
    let elements = get_attr_required "elements" attrs (required_attr tag "elements") in
      List.map Yaxi_xpathxml.compile (Yaxi_utils.whitespace_split elements)
  in
  let rec parse_toplevel x = 
    match x with
	Element (p, ((pref, ns) as map, name), nss, attrs, children) when ns = xsl_ns ->
	  (match name with
	       "template" ->
		 let template = parse_template st !attrs children in
		   st.templates <- template::(st.templates);
	     | "param" -> 
                 let p = parse_param st st.namespaces st.extension_prefixes x in
                   st.params <- p :: st.params;
	     | "variable" -> 
                 let p = parse_param st st.namespaces st.extension_prefixes x in
                   st.variables <- p :: st.variables;
	     | "include" -> 
		 let href = 
		   let orig = get_attr_required "href" !attrs (required_attr "href" "xsl:import") in
		     if doc.base_uri != "" then
		       let path = Filename.dirname (doc.base_uri) in
			 Filename.concat path orig
		     else
		       orig
		 in
		   (*printf "Parsing file %s\n" href;*)
		 let ist = Yaxi_xmlparser.parse_file href in
		   (match document_element ist with
			Some root ->
			  st.namespaces <- st.namespaces @ (namespaces root);
			  let ch = Yaxi_xml.children root in
			    List.iter parse_toplevel ch;
			    (*printf "Parsed file %s\n" href;*)
		      | None -> failwith (sprintf "Included stylesheet %s without document element" href))
	     | "import" -> 
		 let href = get_attr_required "href" !attrs (required_attr "href" "xsl:import") in
		 let stylesheet = parse_stylesheet_file href in
		   st.imports <- (href, stylesheet)::st.imports;
	     | "output" -> st.output <- Some (parse_output x)
	     | "preserve-space" ->
		 st.preserve_space <- (get_whitespace_elem name !attrs) @ st.preserve_space
	     | "strip-space" -> 
		 st.strip_space <- (get_whitespace_elem name !attrs) @ st.strip_space
	     | "decimal-format" -> ()
             | "number" -> ()
             | "key" ->
                 st.keys <- (parse_key st.namespaces !attrs) :: st.keys
             | _ -> raise (XSLError ("Unknow xsl tag: " ^ name)))
      | ProcessingInstruction (p, name, pi) ->
	  ();
      | _ -> 
	  ();
  in
  let root = document_element doc in
    (match root with 
	 Some n ->
	   (match n with
		Element (p, ((pref, ns), name), nss, attrs, sub) when ns = xsl_ns ->
		  if name = "stylesheet" or name = "transform" then (
		    st.namespaces <- namespaces n;
                    let attr = qname_try_assoc_nc  "extension-element-prefixes" (attributes n) "" in
                      st.extension_prefixes <- get_extension_prefixes st st.namespaces attr;
                      List.iter parse_toplevel !sub
		  ) else
		    raise (SyntaxError (sprintf "Root of stylesheet has wrong name %s" name))
	      | _ -> failwith "BUG: Found root node, but it is not an Element node.")
       | None -> 
	   printf "Root is not an element! %s\n" (string_of_nodes (children doc.root));
	   raise Not_found);
    st

(** Parse a stylesheet file
  @param f filename
  @return the parsed stylesheet
*)
and parse_stylesheet_file ?extension_elements ?extension_functions f = 
  parse_stylesheet ?extension_elements ?extension_functions (parse_file f);;

(** Find a matching template *)
let rec find_matches templates gctx ctx =
  List.fold_left 
    (fun res tmpl -> 
       match tmpl.match_expr with
         | [] -> res
	 | list -> 
	     let fn tmpls (pri, exp) =
               let res = ((tmpl, pri), exp gctx ctx) in
		 if Yaxi_xpathxml.boolean_of_object (snd res) then
                   res :: tmpls
                 else tmpls
             in List.fold_left fn res list)
    []
    (List.rev templates)
    
let rec find_matching_template st gctx ctx = 
  match find_matches st.templates gctx ctx with
    | [] -> 
        Yaxi_utils.list_first_option
        (fun (href, xsl) ->
	   try 
	     Some (find_matching_template xsl gctx ctx)
	   with Not_found -> None)
        st.imports
    | list ->
        (* Sort templates by priority *)	
        let list = 
	  List.sort
	    (fun ((_, xpri), _) ((_, ypri), _) ->
	       Pervasives.compare ypri xpri) (* descending order *)
	    list
        in
	  (match list with
	      ((y, _), nodes)::l -> y, nodes
 	     | [] -> raise Not_found)
          
(** Find a named template *)
let find_named_template templates name =
  List.find 
    (fun tmpl -> 
       match tmpl.name with
	 | Some n -> n = name
	 | None -> false)
    templates;;

(** Compiles an template of text and \{ \} enclosed XPath expressions into an xpath
  expression resulting in the aggregation of text and evaluated xpath expression nodes.
  An attribute template result is the 'string value' of this function's result.
  @param attr The template string containing xpath expressions inside curly braces.
*)
let compile_curly_template str = 
  let rec parse_str str =
    try 
      let i = String.index str '{' in
      let j = String.index_from str i '}' in
      let expr = `Expr (compile_expression (String.sub str (i+1) (j - i - 1))) in
	if i > 0 then
	  `Text (Netstring_pcre.string_before str i)::expr::(parse_str (Netstring_pcre.string_after str (j + 1)))
	else
	  expr::(parse_str (Netstring_pcre.string_after str (j + 1)))
    with Not_found -> 
      if String.length str > 0 then
	[`Text str]
      else
	[]
  in
  let exprs = parse_str str in
    (fun gctx ctx ->
       (List.flatten 
	  (List.map
	     (fun node ->
		match node with
		    `Text str -> [new_text str]
		  | `Expr e -> match (e gctx ctx) with
			Nodes n -> n
		      | obj -> [new_text (string_of_object obj)])
	     exprs)))
		
(** Compiles an attribute template 
  @param attr The attribute template string containing xpath expressions inside curly braces.
*)
let compile_attribute_template attr =
  let fn = compile_curly_template attr in
    (fun gctx ectx ->
       (string_value (Nodes (fn gctx ectx))))
    
(** Compiles the attribute templates for a list of attributes *)
let compile_attrs_templates attrs =
  List.map 
    (fun attr_tmpl ->
       match attr_tmpl with 
	   Attribute (p, name, value) ->
	     let tmpl = compile_attribute_template value in
	       (fun gctx ctx ->
		  let newvalue = tmpl gctx ctx in
		    new_attribute name newvalue)
	 | _ -> raise (XSLError "Cannot apply compile_attribute_template to non-attribute node"))
    attrs;;

let new_null_node_ref () = 
  ref (Root (ref []));;

module Comp = CamomileLibrary.UCol.Make(CamomileLibrary.CamomileDefaultConfig)(CamomileLibrary.UTF8)    

let map_context gctx fn list =
  match list with
    | [] -> []
    | hd::tl ->
        let ctx = new_context hd (List.length list) 1 in
          List.map
            (fun node ->
               ctx.node <- node;
               ctx.position <- ctx.position + 1;
               fn gctx ctx)
            list
            
let apply_sorts sorts gctx nodes = 
  let sort s list =
    let locale = s.lang in
    let unsorted =
      map_context
        gctx
        (fun gctx ctx ->
           let key = (s.select gctx ctx) in
             (key, ctx.node))
        nodes
    in 
    let sorted =       
      List.map snd
        (match s.data_type with
           | `Text -> 
               List.sort 
               (fun (a, va) (b, vb) ->
                  (* FIXME: upper-first/lower-first switch *)
                  Comp.compare ~locale:s.lang (string_of_object a) (string_of_object b))
               unsorted
           | `Number -> 
               List.sort 
               (fun (a, va) (b, vb) -> 
                  Pervasives.compare (number_of_object a) (number_of_object b))
               unsorted
           | `QName str -> 
               failwith (sprintf "Unimplemented %s sorting method" str))
    in      
      if s.order = `Descending then List.rev sorted
      else sorted
  in
    List.fold_left 
      (fun sorted s ->
         sort s sorted)
      nodes
      sorts

(** Apply a template to a given context 
  @param template the template compiled function
  @param context the xpath context
  @return the resulting document fragment
*)
let rec apply_template template gctx context =
  (*printf "Applying template to %s\n" (pretty_string_of_node "" !(context.node));*)
  template gctx context;

(** Apply the given templates in order to the context
  @return the resulting document fragments
 *)  
and apply_templates tmpls gctx context =
  let res = List.map 
	      (fun x -> 
		 match apply_template x gctx context with
		     Some nodes -> nodes
		   | None -> []) 
	      tmpls
  in
    List.flatten res

and compile_param st (name, expr, tmpls) =
  match expr with
      Some e -> (fun gctx ctx -> (name, e gctx ctx))
    | None -> 
	(match tmpls with
	     Some templates -> 
	       let tmpls = compile_templates st templates in
		 (fun gctx ctx -> 
		    (name, Nodes (apply_templates tmpls gctx ctx)))
	   | None -> (fun gctx ctx -> (name, Literal "")))
	
and compile_params st =
  List.map (compile_param st)
    
(** Compile the given template instruction as a function of an XPath context *)
and compile_instruction xsl = function         
  | XSLExtension instr -> instr
      
  | XSLIf (test, tmpls) -> 
      let templates = compile_templates xsl tmpls in
        (fun gctx ctx -> 
	   if test gctx ctx then
	     Some (apply_templates templates gctx ctx)
	   else
	     None)
  | XSLChoose (lst, other) ->
      (fun gctx ctx -> 
	 try
	   let (test, tmpls) = List.find (fun (test, tmpls) -> (test gctx ctx)) lst in
	     Some (apply_templates (compile_templates xsl tmpls) gctx ctx)
	 with Not_found -> 
	   match other with
	       Some tmpls -> 
		 Some (apply_templates (compile_templates xsl tmpls) gctx ctx)
	     | None -> None)
  | XSLValueOf (doe, sel) ->
      (fun gctx ctx ->
	 let str = string_of_object (sel gctx ctx) in
	   if str != "" then
	     Some [new_text str]
	   else None)	
  | XSLCopy (qnames, templates) ->
      let tmpls = compile_templates xsl templates in
	(fun gctx ctx ->
 	   match ctx.node with
	       Element (p, name, nss, attrs, ch) -> 
		 (*printf "Copy of element %s with attrs: %s\n" (string_of_name name) (string_of_attrs !attrs);
		 printf "Before apply templates\n";*)
		 let res = (apply_templates tmpls gctx ctx) in
		 let attributes, children = 
		   List.partition
		     (fun x -> match x with
			  Attribute _ -> true
			| Element _ -> false
			| Text _ -> false
			| Comment _ -> false
			| ProcessingInstruction _ -> false
			| Root _ -> failwith "BUG!!! Root can't be child of anything"
			| NS _ -> failwith "BUG!!! NS nodes can't be created that way")
		     res
		 in
		   (*printf "Apply-templates gives: %s\n" (string_of_attrs res);
		     printf "End for element %s\n" (string_of_name name);*)
		   (*printf "Element: %s Namespaces: %s\n" (string_of_name name) (Yaxi_utils.string_of_list string_of_namespace " " !nss);*)
		   Some [new_element name ~namespaces:!nss
			   ~children:children ~attributes:attributes ()]
	     | Attribute (p, name, value) -> 
		 (*printf "Copy of attr: %s\n" (string_of_attr ctx.node);*)
		 Some [new_attribute name value]
	     | Root (nodes) ->
		 let res = apply_templates tmpls gctx ctx
		 in (match res with 
			 [] -> None
		       | list -> Some list)
	     | Text (p, text) -> Some [new_text text]
	     | Comment (p, text) -> Some [new_comment text]
	     | ProcessingInstruction (p, target, text) -> Some [new_pi target text]
	     | NS (p, nss) -> Some [new_namespace nss])
  | XSLCopyOf (sel) ->
      (fun gctx ctx -> 
	 match sel gctx ctx with
	     Nodes n -> 
	       let nodes = List.map copy_node n in
		 if nodes <> [] then
		   Some nodes
		 else
		   None
	   | _ -> None)

  | XSLApplyTemplates (select, mode, params, sorts) ->
      let params_fns = compile_params xsl params in
      (fun gctx context ->	 
	 let nodes = select gctx context in	   
	 let params = List.map (fun x -> x gctx context) params_fns in
	 let gctx = global_context_mutate gctx params in
	 let res = 
	   List.map 
	     (fun node ->
		let ctx = new_context node 1 1 in
		  try 
		    let tmpl, nodes = find_matching_template xsl gctx ctx in                  
		    let compiled = get_template_fun xsl tmpl in
                      (match apply_template compiled gctx ctx with
			 | Some nodes -> nodes
			 | None -> [])
		  with Not_found -> [])		   
	     (apply_sorts sorts gctx
                (match nodes with
                   | Nodes n -> n
		   | _ -> []))
	 in
	   match res with
	     | [] -> None
	     | _ -> Some (List.flatten res))
      
  | XSLCallTemplate (qname, params) ->
      let template = 
	try find_named_template xsl.templates qname 
	with Not_found -> 
	  raise (XSLError (sprintf "template %s not found (in call-template)" 
			     (string_of_name qname)))
      in
      let compiled_tmpls = compile_templates xsl !(template.instructions) in
      let params_fns = compile_params xsl params in
	(fun gctx ctx -> 
	   let params = List.map (fun x -> x gctx ctx) params_fns in
	   let gctx = global_context_mutate gctx params in
	     match apply_templates compiled_tmpls gctx ctx with
	       | [] -> None
	       | _ as nodes -> Some nodes)

  | XSLApplyImports ->
      (fun gctx ctx -> 
	 try 
           let stylesheet, (tmpl, _) = 
             Yaxi_utils.list_first_option
               (fun (href, stylesheet) ->
                  try
                    Some (stylesheet, find_matching_template stylesheet gctx ctx)
                  with Not_found -> None)
               xsl.imports
           in
           let fn = get_template_fun stylesheet tmpl in
             apply_template fn gctx ctx
	 with Not_found -> None)
      
  | XSLElement (qname, templates) ->
      let tmpls = compile_templates xsl templates in
	(fun gctx ctx ->
	   let el = new_element qname () in
	     match apply_templates tmpls gctx ctx with
	       | [] -> Some [el]
	       | nodes -> reparent_nodes el nodes;		   
		   Some [el])

  | XSLOrigElement (p, qname, nss, attrs, templates) ->
      let tmpls = compile_templates xsl templates in
      let attr_tmpls = compile_attrs_templates !attrs in
	(fun gctx ctx ->
	   let el = new_element qname ~namespaces:!nss () in
	     set_element_attrs el (List.map (fun x -> x gctx ctx) attr_tmpls);
	     match apply_templates tmpls gctx ctx with
	       | [] ->  Some [el]
	       | nodes -> reparent_nodes el nodes;
		   Some [el])
        
  | XSLAttribute ((name, value), templates) ->
      let qname_tmpl = compile_attribute_template value and tmpls = compile_templates xsl templates in
	(fun gctx ctx ->
	   let qname = qname_tmpl gctx ctx 
	   and tmpls_str = 
	     match apply_templates tmpls gctx ctx with
	       | [] -> ""
	       | nodes -> string_value_of_nodes nodes
	   in
	     Some [(new_attribute (name, qname) tmpls_str)])

  | XSLText (doe, str) -> 
      (fun gctx ctx -> 
	 Some [(new_text str)])

  | XSLFallback (templates) -> 
      let tmpls = compile_templates xsl templates in
	(fun gctx ctx -> 
	   match apply_templates tmpls gctx ctx with
	     | [] -> None
	     | nodes -> Some nodes)

  | XSLForEach (expr, templates, sorts) ->
      let tmpls = compile_templates xsl templates in
	(fun gctx ctx ->
           try
	     (match expr gctx ctx with
	         Nodes n ->
		   (*printf "for-each selected %i nodes: %s\n" (List.length n) (string_of_nodes n);*)
		   let i = ref 0 and nodes_size = List.length n in 
		   let res = 
		     List.map
		       (fun node -> 
			  i := succ !i;
			  let context = new_context node nodes_size !i
			  in
			    (*printf "apply templates to %s\n" (string_of_node node);*)
			    apply_templates tmpls gctx context)
		       (apply_sorts sorts gctx n)
		   in
		   let nodes = List.flatten res in
		     Some nodes;
	        | _ -> None)
           with Not_found -> None
             | e -> raise (XSLError (sprintf "Got unexpected exception %s in for-each\n" (Printexc.to_string e))))

  | XSLMessage (halt, templates) ->
      let tmpls = compile_templates xsl templates in
	(fun gctx ctx -> 
	   (match apply_templates tmpls gctx ctx with
	      | [] -> ()
	      | nodes -> printf "xsl:message: \n%s\n" (string_of_nodes nodes));
	   if halt then
	     raise XSLStylesheetDirectedStop;
	   None)

  | XSLProcessingInstruction (ncname, templates) ->
      (fun gctx ctx -> None)

  | XSLVariable (qname, expr, templates) ->
      (match expr with
	 | Some expr ->
	     (fun gctx ctx -> 
		let res = expr gctx ctx in
		  gctx.Yaxi_xpath.variables <- (qname, res) :: gctx.Yaxi_xpath.variables;
		  None)
	 | None -> 
	     (match templates with
		  Some templates ->
		    let tmpls = compile_templates xsl templates in
		      (fun gctx ctx -> 
			 let res = Nodes [Root (ref (apply_templates tmpls gctx ctx))] in
			   gctx.Yaxi_xpath.variables <- 
			   (qname, res) :: gctx.Yaxi_xpath.variables;
			   None)
		| None -> (fun gctx ctx -> 
			     gctx.Yaxi_xpath.variables <- 
			     (qname, Literal "") :: gctx.Yaxi_xpath.variables;
			     None)))

  | XSLNop -> (fun _ _ -> None)

      (* Impossible *)
  | XSLParam _ -> (fun gctx ctx -> None)

(** Compiles the template instructions *)      
and compile_templates xsl templates =
  List.map (compile_instruction xsl) templates    
    
(** Compile a template *)
and compile_template xsl template =
  let tmpls = compile_templates xsl !(template.instructions)
  and params = List.map 
		 (fun param ->
		    let (name,_,_) = param in
		      (name, compile_param xsl param))
		 template.parameters 
  in let fn =
    (fun gctx ctx -> 
       List.iter  
       (fun (paramname, paramfn) -> 
	  if not (context_has_variable gctx paramname) then
	    context_add_variable gctx (paramfn gctx ctx)
	  (*else (
	    printf "Context already has variable %s : %s\n" (string_of_name paramname)
		  (string_of_object (context_get_variable gctx paramname));)*))
       params;
       match apply_templates tmpls gctx ctx with
	   [] -> None
	 | nodes -> Some nodes)
  in
    template.compiled_fun <- Some fn

(** Compile all the stylesheet templates *)
and compile_stylesheet xsl = 
  List.iter (compile_template xsl) xsl.templates

(** Get the compiled function for a template *)
and get_template_fun xsl template =
  match template.compiled_fun with
    | Some fn -> fn
    | None -> compile_template xsl template;
	get_template_fun xsl template
	  
(**
let strip_nodes preserve strip doc =
  let rec strip pres strip = function
      Element (xml
*)

(** Bind the variables and params to values *)
and bind_variable st gctx context (name, expr, tmpls) =
  match expr with
      Some e -> (name, e gctx context)
    | None -> (match tmpls with
		   Some templates -> 
		     let tmpls = compile_templates st templates in
		       (name, Nodes [ Root (ref (apply_templates tmpls gctx context))])
		 | None -> (name, Literal ""))
        
let string_of_boundparam (name, value) =
  string_of_name name ^ " = " ^ (desc_string_of_object value);;

let additional_functions : extension_function list ref = ref []
                            
let add_additional_functions exts =
  additional_functions := exts @ !additional_functions
    
let get_additional_functions xslt doc =
  List.map (fun (name, fn) -> ((default_ns, name), fn xslt doc)) !additional_functions
    

let transform_functions st doc fns = 
  List.flatten
    (List.map 
       (fun (ns, fns) ->
	  List.map
	  (fun (name, fn) -> 
	     ((("", `URI ns), name), fn st doc))
	  fns)
       fns)

(** Apply a stylesheet to a document.
  @param parameters A list a parameters to use.
  @return the result document 
*)
let apply_stylesheet ~stylesheet:st ~document:doc
  ?(ns_mappings = []) ?(parameters = []) 
  ?(extension_functions = []) () =
  let res = Yaxi_xml.new_document "" () in
    try
      let gctx = Yaxi_xpathxml.new_global_context [] [] 
		   ((transform_functions st doc extension_functions)
		    @ (transform_functions st doc st.extension_functions)
		    @ (get_additional_functions st doc))
      in
      let _ = List.iter (context_add_ns gctx) ns_mappings in
      let context = new_context doc.root 1 1 in
      let _ = List.iter (context_add_ns gctx) st.namespaces in
        (*let doc = strip_nodes st.preserve st.strip doc in*)
      let vars = List.map (bind_variable st gctx context) st.variables
      and params = List.map (bind_variable st gctx context) st.params in 
	context_set_variables gctx (vars@params);
	List.iter 
	  (fun (name, e) -> 
	     let param = (name, e gctx context) in	       
	       context_add_variable gctx param)
	  parameters;
	(*printf "variables: %s\n" (Yaxi_utils.string_of_list string_of_boundparam
	  ", " gctx.Yaxi_xpath.variables);
        printf "Got %i functions in the library\n" (List.length gctx.library);
        *)
	let template, nodes = find_matching_template st gctx context in
	let fn = get_template_fun st template in
	  (match apply_template fn gctx context with
	       None ->  res
	     | Some nodes -> 
		 let root = Root (ref []) in
		   reparent_nodes root nodes;
		   res.root <- root;
		   res)
    with Not_found -> 
      res

(*
  printf "Result is : %s\n"
  (match res with 
  Nodes [] -> "an empty nodeset"
  | Nodes l -> sprintf "some nodes %s" (string_of_nodes l)
  | Literal s -> sprintf "literal %s" s
  | Number n -> sprintf "number %f" n
  | Boolean b -> sprintf "the boolean %b" b);
*)
      
