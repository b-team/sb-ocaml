%{
  open Yaxi_xpath    
%}
  
%token SLASH RBRACKET LBRACKET RPAREN LPAREN
%token COLONCOLON COLON STAR QUOTE DOUBLEQUOTE 
%token PLUS MINUS AND OR NOT MOD DIV 
%token EQUAL INEQUAL SUPEQUAL INFEQUAL SUP INF
%token PIPE DOT DOTDOT AT COMMA
%token TEXTP COMMENTP PIP NODEP
%token <string> NAME VARIABLE LITERAL FUNCTION AXIS
%token <float> NUMBER
%token EOF

%type <Yaxi_xpath.expression> expr
%start expr

%%

location_path:
  absolute_location_path { `Absolute, $1 }
| relative_location_path { `Relative, $1 }
;;
  
absolute_location_path:
  SLASH                                    {  [(`Ancestor_or_self, `Rootp, [])] }
| SLASH relative_location_path             { (`Ancestor_or_self, `Rootp, [])::$2 }
| SLASH SLASH relative_location_path       { (`Descendant_or_self, `Anyp, [])::$3 }
;;

relative_location_path:
  step                                     { [$1] }
| relative_location_path SLASH step        { $1 @ [$3] }
| relative_location_path SLASH SLASH step  { $1 @ [(`Descendant_or_self, `Anyp, []);$4] }
;;

step:
  axis_spec node_test                      { ($1, $2, []) }
| axis_spec node_test predicates           { ($1, $2, $3) }
| node_test predicates                     { (`Child, $1, $2) }
| node_test                                { (`Child, $1, []) }
| DOTDOT                                   { (`Parent, `Anyp, []) }
| DOT                                      { (`Self, `Anyp, []) }
;;

axis_spec:
  AXIS                                     { Yaxi_xpath.axis_of_axisname $1 }
| AT                                       { `Attribute }
;;

node_test:
  STAR                                     { `Anyp }
| COMMENTP                                 { `Commentp }
| NODEP                                    { `Nodep }
| TEXTP                                    { `Textp }
| PIP                                      { `ProcessingInstructionp None }
| PIP LITERAL RPAREN                       { `ProcessingInstructionp (Some $2) }
| NAME                                     { `NameTest (Yaxi_xpath.parse_name $1) }
| DIV                                      { `NameTest ("", "div") }
| MOD                                      { `NameTest ("", "mod") }
| AND                                      { `NameTest ("", "and") }
| OR                                       { `NameTest ("", "or") }
| NOT                                      { `NameTest ("", "not") }
;;

predicates:
  predicate                                { [$1] }
| predicate predicates                     { $1::$2 }
;;

predicate:
  LBRACKET expr RBRACKET                   { $2 }
;;

expr: 
  or_expr                                  { $1 }
| primary_expr                             { $1 }
;;

primary_expr:
  VARIABLE                                 { `Variable (Yaxi_xpath.parse_name $1) }
| LPAREN expr RPAREN                       { $2 }
| LITERAL                                  { `Primary (Literal $1) }
| NUMBER                                   { `Primary (Number $1) }
| FUNCTION                                 { `Funcall ((Yaxi_xpath.parse_name $1), []) }
| FUNCTION args RPAREN                     { `Funcall ((Yaxi_xpath.parse_name $1), $2) }
;;

args: 
  or_expr                                  { [$1] }
| or_expr COMMA args                       { $1::$3 }
;;

or_expr:
  and_expr                                 { $1 }
| or_expr OR and_expr                      { `Operation (`Or, $1, $3) }
;;
    
and_expr:
  equality_expr                            { $1 }
| and_expr AND equality_expr               { `Operation (`And, $1, $3) }
;;

equality_expr:
  relational_expr                          { $1 }
| equality_expr EQUAL relational_expr      { `Operation (`Equal, $1, $3) }
| equality_expr INEQUAL relational_expr    { `Operation (`InEqual, $1, $3) }
;;

relational_expr:
  additive_expr                            { $1 }
| relational_expr INF additive_expr        { `Operation (`Inf, $1, $3) }
| relational_expr SUP additive_expr        { `Operation (`Sup, $1, $3) }
| relational_expr INFEQUAL additive_expr   { `Operation (`InfEqual, $1, $3) }
| relational_expr SUPEQUAL additive_expr   { `Operation (`SupEqual, $1, $3) }
;;
     
additive_expr:
  multiplicative_expr                      { $1 }
| additive_expr PLUS multiplicative_expr   { `Operation (`Add, $1, $3) }
| additive_expr MINUS multiplicative_expr  { `Operation (`Sub, $1, $3) }
;;

multiplicative_expr:
  unary_expr                               { $1 }
| multiplicative_expr STAR unary_expr      { `Operation (`Mul, $1, $3) }
| multiplicative_expr DIV unary_expr       { `Operation (`Div, $1, $3) }
| multiplicative_expr MOD unary_expr       { `Operation (`Mod, $1, $3) }
;;
      
unary_expr:
  union_expr                               { $1 }
| MINUS unary_expr                         { `UnaryOperation (`Minus, $2) }
;;

union_expr:
  path_expr                                { $1 }
| union_expr PIPE path_expr                { `Operation (`Union, $1, $3) }
;;

path_expr:
  location_path                            { `Steps $1 }
| filter_expr                              { $1 }
| filter_expr SLASH relative_location_path { $1 }
;;

filter_expr:
  primary_expr                             { $1 }
| filter_expr predicate                    { `Filter ($1, $2) }
;;    
