(* Yaxi - Yet Another X* Implementation
 * Copyright (C) 2003 Matthieu Sozeau
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * $Id: yaxi_xpathxml.mli,v 1.10 2003/09/27 17:56:24 mat Exp $
 * 
 *)

(** Implements basic XPath functions and operators 
  over a document tree
*)

(** Function name, minimal, maximal number of arguments *)
exception ArityError of string * int * int

(** *)
val string_value_of_node : Yaxi_xml.node -> Yaxi_xml.attribute_value
val string_value_of_nodes : Yaxi_xml.nodeset -> Yaxi_xml.attribute_value

(** Conversions *)

(** Return the string corresponding to the given float number *)
val string_of_number : float -> string

(** Float of a string*)
val number_of_string : string -> float

(** String value of an object *)
val string_value : Yaxi_xpath.primary_type -> Yaxi_xml.attribute_value

(** List all the parents of a node *)
val ancestors : Yaxi_xml.node -> Yaxi_xml.nodeset

(** List all the children of a node *)
val descendants : Yaxi_xml.node -> Yaxi_xml.nodeset

(** List the following nodes at the same level of a node *)
val following_siblings : Yaxi_xml.node -> Yaxi_xml.nodeset

(** List the following nodes at any level of a node *)
val followings : Yaxi_xml.node -> Yaxi_xml.node list

(** List the preceding nodes at the same level of a node *)
val preceding_siblings : Yaxi_xml.node -> Yaxi_xml.node list

(** List preceding nodes at any level of a node *)
val precedings : Yaxi_xml.node -> Yaxi_xml.node list

(** Return the nodes on a specific axis for the node *)
val nodes_of_axis : Yaxi_xpath.axis -> Yaxi_xml.node -> Yaxi_xml.nodeset

(** For axis::* *)
val starp : Yaxi_xpath.axis -> Yaxi_xml.node -> bool

(** Test the name of the node*)
val name_test : Yaxi_xpath.axis -> Yaxi_xml.qname -> Yaxi_xml.node -> bool

(** String conversion *)
val string_of_object : Yaxi_xpath.primary_type -> Yaxi_xml.cdata

(** Boolean conversion *)
val boolean_of_object : Yaxi_xpath.primary_type -> bool

(** Float (Number) conversion *)
val number_of_object : Yaxi_xpath.primary_type -> float

(** Nodes of an object. Raise an error if applied on something different from Nodes *)
val nodes_of_object : Yaxi_xpath.primary_type -> Yaxi_xml.nodeset

(** *)
val desc_string_of_object : Yaxi_xpath.primary_type -> string

(** Create a number from an integer *)
val number_of_int : int -> Yaxi_xpath.primary_type

(** Union of nodes, used from xslt extensions *)
val xpath_union : Yaxi_xpath.primary_type -> Yaxi_xpath.primary_type -> 
  Yaxi_xpath.primary_type

(** XPath core functions *)
val functions : Yaxi_xpath.library

(** XPath operators: = != <= >= mod div + - *)
val operations :
  (Yaxi_xpath.operator * (Yaxi_xpath.primary_type -> Yaxi_xpath.primary_type -> Yaxi_xpath.primary_type))
  list

(** Compile an expression into a function of the context *)
val compile_expression : Yaxi_xpath.expression -> Yaxi_xpath.xpath_expr

(** Parses a string into an xpath expression *)
val parse : string -> Yaxi_xpath.expression

(** Parse and compile an XPath expression *)
val compile : string -> Yaxi_xpath.xpath_expr

(** The default (global) context no variables, the default namespace, xpath core functions *)
val default_context : unit -> Yaxi_xpath.global_context

(** Default context plus some variables, namespace mappings and functions *)
val new_global_context : 
  Yaxi_xpath.variable_bindings -> 
  Yaxi_xml.ns_mapping list -> 
  Yaxi_xpath.library ->
  Yaxi_xpath.global_context

(** Evaluates an xpath expression given a context node *)
val xpath_eval : ?gctx:Yaxi_xpath.global_context ->
  string -> Yaxi_xml.node -> Yaxi_xpath.primary_type
