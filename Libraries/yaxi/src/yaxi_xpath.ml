(* Yaxi - Yet Another X* Implementation
 * Copyright (C) 2003 Matthieu Sozeau
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * $Id: yaxi_xpath.ml,v 1.13 2003/09/04 01:18:10 mat Exp $
 * 
 *)

(** XPath implementation *)

open Yaxi_xml;;

exception XPathError of string;;

type primary_type = 
    Literal of string
  | Number of float
  | Boolean of bool
  | Nodes of nodeset;;

type xpath_name = Yaxi_xml.ncname * Yaxi_xml.ncname;;

type variable_binding = Yaxi_xml.qname * primary_type;;

type variable_bindings = variable_binding list;;

(** XPath evaluation context *)
type eval_context = {
  mutable node: node;
  mutable size: int;
  mutable position: int;
}

type global_context = {
  mutable variables: variable_bindings;
  mutable namespaces: ns_mapping list;
  mutable library : library;      (** Function library *)
} 
and library = (Yaxi_xml.qname * xpath_function) list

(** Type of XPath functions *)
and xpath_function = primary_type list -> xpath_expr
and xpath_expr = global_context -> eval_context -> primary_type

(** Parsed expression components *)
type axis = [
  `Ancestor
| `Ancestor_or_self
| `Attribute
| `Child
| `Descendant
| `Descendant_or_self
| `Following
| `Following_sibling
| `Namespace
| `Parent
| `Preceding
| `Preceding_sibling
| `Self
];;

type axis_direction = [ `Forward | `Backward ];;

type node_test = [
  `Nodep
| `Rootp
| `Textp
| `Commentp
| `ProcessingInstructionp of string option
| `NameTest of xpath_name
| `Anyp ];;

type operator = [
  `Union 
| `Add
| `Sub
| `Mul 
| `Div
| `Mod
| `And
| `Or
| `Not
| `Equal
| `InEqual 
| `InfEqual
| `SupEqual
| `Sup
| `Inf 
| `Minus
]

type expression = [
  `Primary of primary_type
| `Variable of xpath_name
| `Funcall of xpath_name * expression list
| `Operation of operator * expression * expression
| `UnaryOperation of operator * expression
| `Steps of [`Relative | `Absolute] * steps
| `Filter of expression * expression
]
and step = axis * node_test * expression list
and steps = step list;;


(** Convenience functions *)
let booleanp = function 
    Boolean b -> true
  | _ -> false;;

let literalp = function
    Literal l -> true
  | _ -> false;;

let numberp = function
    Number n -> true
  | _ -> false;;

let nodesp = function
    Nodes n -> true
  | _ -> false;;

let axis_of_axisname = function
    "ancestor" -> `Ancestor
  | "ancestor-or-self" -> `Ancestor_or_self
  | "attribute" -> `Attribute
  | "child" -> `Child
  | "descendant" -> `Descendant
  | "descendant-or-self" -> `Descendant_or_self
  | "following" -> `Following
  | "following-sibling" -> `Following_sibling
  | "namespace" -> `Namespace
  | "parent" -> `Parent  
  | "preceding" -> `Preceding
  | "preceding-sibling" -> `Preceding_sibling
  | "self" -> `Self
  | _ as axis -> failwith ("Wrong axis name " ^ axis)


let direction_of_axis = function
    `Ancestor 
  | `Ancestor_or_self
  | `Preceding
  | `Preceding_sibling -> `Backward
  | _ -> `Forward

let nodetest_of_str = function
    "comment" -> `Commentp
  | "node" -> `Nodep
  | "text" -> `Textp
  | "processing-instruction" -> `ProcessingInstructionp None
  | _ as str -> failwith ("Invalid nodetype test " ^ str)
      
      
let parse_name str = 
  if String.contains str ':' then
    let idx = String.index str ':' in
    let nsstr = String.sub str 0 idx in
      (nsstr, (String.sub str (idx + 1) ((String.length str) - idx - 1)))
  else
    ("", str);;


(** Create a new context of only one node *)
let new_context node size pos =
  { node = node;
    size = size;
    position = pos;
  };;

(** Sets the variables of a context *)
let context_set_variables ctx vars =
  ctx.variables <- vars;;

let global_context_mutate ctx vars =
  {
    variables = vars @ ctx.variables;
    namespaces = ctx.namespaces;
    library = ctx.library
  }

let context_add_ns ctx map =
  ctx.namespaces <- map::(ctx.namespaces);;

let context_copy ctx = 
  { variables = ctx.variables;
    namespaces = ctx.namespaces;
    library = ctx.library }

let context_add_variable ctx binding =
  ctx.variables <- binding :: ctx.variables
    
let context_has_variable ctx qname = 
  qname_mem_assoc qname ctx.variables

let context_get_variable ctx qname = 
  qname_assoc qname ctx.variables

let string_of_operator = function
    `Union -> "|"
  | `Add -> "+"
  | `Sub -> "-"
  | `Mul -> "*"
  | `Div -> "div"
  | `Mod -> "mod"
  | `And -> "and"
  | `Or -> "or"
  | `Not -> "not"
  | `Equal -> "="
  | `InEqual -> "!="
  | `InfEqual -> "<="
  | `SupEqual -> ">="
  | `Sup -> ">"
  | `Inf -> "<"
  | `Minus -> "-"
