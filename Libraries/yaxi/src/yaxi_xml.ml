(* Yaxi - Yet Another X* Implementation
 * Copyright (C) 2003 Matthieu Sozeau
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * $Id: yaxi_xml.ml,v 1.23 2003/11/02 16:56:46 mat Exp $
 * 
 *)

open Yaxi_utils
open Printf

(** XML types and functions *)

type namespace = [ 
  `URI of string
| `None 
]

type ncname = string
type ncnames = ncname list

type prefix = ncname

type ns_mapping = prefix * namespace (* prefix, namespace *)
type ns_mappings = ns_mapping list

type nmtoken = string

type cdata = string 

type qname = ns_mapping * ncname
and qnames = qname list

type 'a qname_assoclist = (qname * 'a) list

type attribute_value = cdata

type attributes = attribute_value qname_assoclist
    
type node = 
    Root of nodeset ref      (* Root node, no parent *)
  | Element of node ref      (* parent *)
      * qname               (* name *)
      * nodeset ref         (* namespaces *)
      * nodeset ref         (* attributes *)
      * nodeset ref         (* children *)
  | Attribute of node ref * qname * attribute_value    (* parent, name, value *)
  | NS of node ref * ns_mapping   	                (* parent, mapping *)
  | Text of node ref * cdata                          (* parent, contents *)
  | Comment of node ref * cdata
  | ProcessingInstruction of node ref * ncname * cdata
and nodeset = node list

(** Element quantifier *)
type dtd_quantifier = [ `Zero | `NonZero | `One | `Any ]

(** DTD children spec *)
type dtd_children_el = dtd_quantifier * dtd_children
and dtd_children_els = dtd_children_el list
and dtd_children = 
    [ `Choice of dtd_children_els 
    | `Seq of dtd_children_els 
    | `Element of string ]

(** DTD element content spec *)
type dtd_content_spec =
    [ `Any | `Empty | `PCDATA 
    | `Mixed of string list 
    | `Children of dtd_children ]

(** DTD attribute type *)
type dtd_attribute_type = 
    [ `CDATA | `ID | `IDREF | `IDREFS | `NMTOKEN | `NMTOKENS 
    | `Notation of string list 
    | `Enumeration of string list ]

(** DTD attribute default value *)    
type dtd_attribute_default =  
    [ `Fixed of string 
    | `Implied 
    | `Required ]

(** DTD attribute definition *)    
type dtd_attrdef = string * dtd_attribute_type * dtd_attribute_default

(** DTD attribute definitions *)    
and dtd_attrdefs = dtd_attrdef list

type dtd_entity_value_template = [ `Text of string | `EntityRef of string ] list

(** DTD entity value *)    
type dtd_entity_value = 
    [ `ExternalID of string * string 
    | `UnparsedExternalID of string * string * string
    | `Value of dtd_entity_value_template ]

(** Parsed DTD entity value *)    
type dtd_parsed_entity_value = 
    [ `ExternalID of string * string 
    | `UnparsedExternalID of string * string * string
    | `Value of string ]

(** DTD declaration *)
type dtd_declaration = 
    [ `PEReference of string
    | `ElementDecl of string * dtd_content_spec
    | `AttributeDecl of string * dtd_attrdefs
    | `NotationDecl of string * (string * string)
    | `Pi of string * string
    | `Comment of string
    | `EntityDecl of string * dtd_entity_value
    ]
    
(** DTD declarations *)
and dtd_declarations = {
  mutable dtd_elements: (string * dtd_content_spec) list;
  mutable dtd_attributes: (string * dtd_attrdefs) list;
  mutable dtd_notations: (string * (string * string)) list;
  mutable dtd_pis: (string * string) list;
  mutable dtd_comments: string list;
  mutable dtd_entities: (string * dtd_entity_value) list;
  mutable dtd_parameter_entities: (string * dtd_parsed_entity_value) list;
}
    
type dtd = {
  name: ncname;
  system_id: string option;
  public_id: string option;
  mutable internal_subset: dtd_declarations option;
  external_subset: dtd_declarations; (** Default entities here *)
  mutable fetch_remote_entities: bool; (** Fetch http:// ftp:// entities if needed ? *)
}

type document = {
  mutable root: node;
  encoding: Netconversion.encoding;
  version: string;
  standalone: bool;
  mutable doctype: dtd;
  base_uri: string;
}

exception XMLError of string;;

exception UndefinedNamespace of string;;

let xml_uri = `URI ("http://www.w3.org/XML/1998/namespace")

let xml_ns = ("xml", xml_uri)

let xmlns_ns = ("xmlns", `None)

let default_ns = ("", `None)

let null_name = (default_ns, "")

let is_null_name = ((=) null_name)

let unqname n = (default_ns, n)

let new_dtd_declarations () =
  {  
    dtd_elements = [];
    dtd_attributes = [];
    dtd_notations = [];
    dtd_pis = [];
    dtd_comments = [];
    dtd_entities = [];
    dtd_parameter_entities = [];
  }

let default_dtd_declarations () =
  let decls = new_dtd_declarations () in
    decls.dtd_entities <- 
    [("lt", `Value [`Text "&#38;#60;"]); 
     ("gt", `Value [`Text "&#62;"]);
     ("amp", `Value [`Text "&#38;#38;"]); 
     ("apos", `Value [`Text "&#39;"]);
     ("quot", `Value [`Text "&#34;"])];
    decls

let add_dtd_declarations a b = 
  a.dtd_elements <- a.dtd_elements @ b.dtd_elements;
  a.dtd_attributes <- a.dtd_attributes @ b.dtd_attributes;
  a.dtd_notations <- a.dtd_notations @ b.dtd_notations;
  a.dtd_pis <- a.dtd_pis @ b.dtd_pis;
  a.dtd_comments <- a.dtd_comments @ b.dtd_comments;
  a.dtd_entities <- a.dtd_entities @ b.dtd_entities;
  a.dtd_parameter_entities <- a.dtd_parameter_entities @ b.dtd_parameter_entities;
  a

let rec parse_entity_value_template dtd =
  List.fold_left 
    (fun res el -> match el with
	 `Text str -> res ^ str
       | `EntityRef r -> 
	   (match get_entity_value dtd r with
		`Value v -> res ^ v
	      | _ -> raise (XMLError "Cannot concatenate entity value and external ID")))
    ""

and parse_entity_value dtd = function
    `Value tmpl -> `Value (parse_entity_value_template dtd tmpl)
  | (`ExternalID (pub, sys)) as e -> e
  | (`UnparsedExternalID _) as u -> u

and get_entity dtd eref = 
  try
    (match dtd.internal_subset with
	Some d -> List.assoc eref d.dtd_entities
       | None -> raise Not_found)
  with Not_found ->
    List.assoc eref dtd.external_subset.dtd_entities
    
and get_entity_value dtd eref =
  parse_entity_value dtd (get_entity dtd eref)

let get_parameter_entity_value dtd eref =
  try
    (match dtd.internal_subset with
	 Some d -> List.assoc eref d.dtd_parameter_entities
       | None -> raise Not_found)
  with Not_found ->
    List.assoc eref dtd.external_subset.dtd_parameter_entities
    
let get_element_def dtd el =
  try
    (match dtd.internal_subset with
	 Some d -> List.assoc el d.dtd_elements
       | None -> raise Not_found)
  with Not_found ->
    List.assoc el dtd.external_subset.dtd_elements

let get_attributes_def dtd attr =
  try
    (match dtd.internal_subset with
	 Some d -> List.assoc attr d.dtd_attributes
       | None -> raise Not_found)
  with Not_found ->
    List.assoc attr dtd.external_subset.dtd_attributes
      
let new_dtd name ?public_id ?system_id ?internal_subset ?external_subset () =
  let decls = 
    match external_subset with 
	None -> default_dtd_declarations ()
      | Some dtd -> add_dtd_declarations dtd (default_dtd_declarations ())
  in
    { name = name; public_id = public_id; system_id = system_id; 
      internal_subset = internal_subset;
      external_subset = decls;
      fetch_remote_entities = false;
    }

let default_dtd () = 
  new_dtd "" ()
  
let new_document uri ?(version = "1.0") ?(encoding = `Enc_utf8) ?(standalone = true) ?doctype () = 
  let dtd = 
    match doctype with
	Some d -> d
      | None -> default_dtd ()
  in
    { root = Root (ref []); encoding = encoding; version = version; standalone = standalone; 
      base_uri = uri; doctype = dtd };;

let nsprefix_assoc prefix list =
  List.find (fun (pref, ns) -> pref = prefix) list;;

let nsprefix_mem_assoc prefix list =
  List.exists (fun (pref, ns) -> pref = prefix) list;;

let parse_name env str =
  if String.contains str ':' then
    let idx = String.index str ':' in
    let nsstr = String.sub str 0 idx in
      if nsprefix_mem_assoc nsstr env then
	(nsprefix_assoc nsstr env, (String.sub str (idx + 1) ((String.length str) - idx - 1)))
      else
	raise (UndefinedNamespace nsstr)
  else
    (default_ns, str);;

let parse_ns_mapping env pref value =
  if pref <> "" then
    match value with
	"" -> 
	  failwith ("Namespace declaration without namespace uri: xmlns:" ^ pref)
      | str -> (pref, `URI str)
  else
    (* xmlns=".." *)
    match value with
	"" -> ("", `None)
      | str -> ("", `URI str)

let set_doctype doc dtd =
  doc.doctype <- dtd

let null_noderef () = ref (Root (ref []));;

(** Bind a child to its parent *)
let bind_child parent child =
  match child with
       Attribute (p, _, _)
     | Element (p, _, _, _, _) 
     | Text (p, _) 
     | Comment (p, _)
     | ProcessingInstruction (p, _, _) 
     | NS (p, _) ->
	 p := parent
    | Root (nodes) -> raise (XMLError "Root node cannot be bound to any parent")
;;

let bind_children parent children =
  List.iter (bind_child parent) children;;

let new_element name ?(namespaces = []) ?(attributes = []) ?(children = []) () =
  let el = Element (null_noderef (), name, ref namespaces, ref attributes, ref children) in
    match el with
	Element (newp, nname, nnss, nattrs, nch) -> 
	  bind_children el !nnss;
	  bind_children el !nattrs;
	  bind_children el !nch;
	  el;
      | _ -> el	

let new_attribute name value =
  Attribute (null_noderef (), name, value);;

let new_text str =
  Text (null_noderef (), str);;

let new_comment str =
  Comment (null_noderef (), str);;

let new_pi name str =
  ProcessingInstruction (null_noderef (), name, str);;

let new_namespace map =
  NS (null_noderef (), map);;

(** String representations *)
let string_of_name = 
  function
      (ns, n) when ns = default_ns -> n
    | ((pref,uri), n) -> 
	pref ^ ":" ^ n;;

let string_of_attr_value v =
  if not (String.contains v '"') then
    "\"" ^ v ^ "\""
  else "'" ^ v ^ "'"
    
let string_of_attribute attr = 
  match attr with
      Attribute (_, n, v) -> (string_of_name n) ^ "=" ^ (string_of_attr_value v)
    | _ -> raise (XMLError "node is not an attribute in string_of_attribute")

let string_of_ns_mapping (pref, uri) = 
  match uri with
      `URI u -> "xmlns:" ^ pref ^ "=\"" ^ u ^ "\""
    | `None -> 
	if pref <> "" then
	  failwith (sprintf "A mapping must contain an uri: prefix %s is bound to no uri" pref)
	else "xmlns=\"\""

let string_of_namespace = function
    NS (p, (prefix, ns)) -> 
      (match ns with 
	   `None -> ""	     
	 | `URI str ->
	     if prefix = "" then
	       "xmlns=\"" ^ str ^ "\""
	     else
	       "xmlns:" ^ prefix ^ "=\"" ^ str ^ "\"")
  | _ -> ""

let rec string_of_node =
  function
      Element (p, name, nss, attrs, children) ->
	let attrstr =
	  if List.length !attrs != 0 then
	    " " ^ (string_of_list string_of_attribute " " !attrs)
	  else
	    ""
	and childstr =
	  if List.length !children != 0 then
	    ">" ^ (string_of_list string_of_node "" !children) ^ "</" ^ (string_of_name name) ^ ">"
	  else
	    "/>"
	in
	  "<" ^ (string_of_name name) ^ attrstr ^ childstr
    | Text (p, str) -> str
    | Comment (p, str) -> "<!-- " ^ str ^ " -->"
    | ProcessingInstruction (p, pi, str) -> "<?" ^ pi ^ " " ^ str ^ "?>"
    | Root nodes -> string_of_list string_of_node "\n" !nodes
    | _ -> "";;

let string_of_nodes nodes = 
  string_of_list string_of_node " " nodes


let rec copy_node =
  function
      Element (p, name, nss, attrs, ch) -> 
	let mapcopy = List.map copy_node in
	  new_element name ~namespaces:(mapcopy !nss)
	    ~attributes:(mapcopy !attrs) ~children:(mapcopy !ch) ()
    | Attribute (p, name, value) -> new_attribute name value
    | Text (p, s) -> new_text s
    | Comment (p, s) -> new_comment s
    | ProcessingInstruction (p, n, str) -> new_pi n str
    | NS (p, map) -> new_namespace map
    | Root (nodes) -> 
	let node = Root (ref []) in
	  (match node with
	       Root nnodes -> nnodes := bind_copy_children node !nodes;
		 node;
	     | _ -> 
		 node;)

and bind_copy_children el =
  List.map (fun x -> let n = copy_node x in bind_child el n; n);;	

let reparent refel node =
  match refel with 
      Element (elemp, name, nss, attrs, children) ->
	bind_child refel node;
	(match node with
	     Attribute (p, name, value) ->
	       attrs := node::!attrs;
	   | Text (p, str) ->
	       children := node::!children;
	   | Element (p, name, nss, attrs, ch) ->
	       children := node::!children;
	   | Comment (p, str) ->
	       children := node::!children;
	   | ProcessingInstruction (p, pi, str) ->
	       children := node::!children;
	   | NS (p, ns) -> nss := node::!nss
	   | Root (nodes) -> raise (XMLError "Element node cannot have a root node in its children"));
    | Root (oldnodes) ->
	bind_child refel node;
	(match node with
	     Text (p, str) ->
	       oldnodes := node::!oldnodes
	   | Element (p, name, nss, attrs, ch) ->
	       oldnodes := node::!oldnodes
	   | Comment (p, str) ->
	       oldnodes := node::!oldnodes
	   | ProcessingInstruction (p, pi, str) ->
	       oldnodes := node::!oldnodes
	   | _ -> raise (XMLError "Cannot make node a children of root"))
    | _ -> raise (XMLError "Node cannot be a parent of any other");;

let reparent_nodes refel nodes =
  List.iter (reparent refel) (List.rev nodes)

let remove_child parent child =
  bind_child !(null_noderef ()) child;
  match parent with
      Root (nodes) ->
	nodes := List.filter ((!=) child) !nodes
    | Element (_,_,nss,attrs,nodes) -> 
	(match child with 
	     Attribute _ -> attrs := List.filter ((!=) child) !attrs
	   | NS _ -> nss := List.filter ((!=) child) !nss
	   | _ -> nodes := List.filter ((!=) child) !nodes)
    | _ -> raise (XMLError "Cannot remove any child from a non-element node")

let parent : node -> node option =
  function
      Root (nodes) -> None
    | Attribute (p, _, _)
    | Element (p, _, _, _, _) 
    | Text (p, _) 
    | Comment (p, _)
    | ProcessingInstruction (p, _, _) 
    | NS (p, _) -> Some (!p)
	
let unlink node =
  match parent node with
      Some p -> remove_child p node; node
    | None -> node

let replace node onode =
  let p, parent_childs = 
    match node with
	Element (p, _, _, _, _)  
      | Comment (p, _)
      | ProcessingInstruction (p, _, _) ->
	  (match !p with
	       Root (nodes) -> (!p, nodes)
	     | Element (_, _, _, _, ch) -> (!p, ch)
	     | _ -> raise (XMLError "Bad parent type in replace"))
      | Text (p, _) -> 
	  (match !p with
	       Element (_, _, _, _, ch) -> (!p, ch)
	     | _ -> raise (XMLError "Bad parent type for Text node in replace"))
      | NS (p, _) -> 
	  (match !p with
	       Element (_, _, nss, _, _) -> (!p, nss)
	     | _ -> raise (XMLError "Bad parent type for NS node in replace"))
      | Attribute (p, _, _) ->
	  (match !p with
	       Element (_, _, _, attrs, _) -> (!p, attrs)
	     | _ -> raise (XMLError "Bad parent type for Attribute node in replace"))	  
      | Root _ -> raise (XMLError "Root node has no parent (in replace)")
 in
    parent_childs := 
    List.map 
      (fun anode -> if anode == node then onode else anode) 
      !parent_childs;
    bind_child p onode

let replace_with_nodes node onodes =
  let p, parent_childs = 
    match node with
	Element (p, _, _, _, _)  
      | Comment (p, _)
      | ProcessingInstruction (p, _, _) ->
	  (match !p with
	       Root (nodes) -> (!p, nodes)
	     | Element (_, _, _, _, ch) -> (!p, ch)
	     | _ -> raise (XMLError "Bad parent type in replace"))
      | Text (p, _) -> 
	  (match !p with
	       Element (_, _, _, _, ch) -> (!p, ch)
	     | _ -> raise (XMLError "Bad parent type for Text node in replace"))
      | NS (p, _) -> 
	  (match !p with
	       Element (_, _, nss, _, _) -> (!p, nss)
	     | _ -> raise (XMLError "Bad parent type for NS node in replace"))
      | Attribute (p, _, _) ->
	  (match !p with
	       Element (_, _, _, attrs, _) -> (!p, attrs)
	     | _ -> raise (XMLError "Bad parent type for Attribute node in replace"))	  
      | Root _ -> raise (XMLError "Root node has no parent (in replace)")
 in
    parent_childs := 
    List.fold_left
      (fun nodes anode -> if anode == node then onodes @ nodes else anode :: nodes) 
      []
      (List.rev !parent_childs);
    bind_children p onodes


let not_an_attribute () = raise (XMLError "Node is not an attribute");;

(** Attribute functions *)
let name_of_attr =
  function 
      Attribute(p, n, v) -> n
    | _ -> not_an_attribute ();;

let value_of_attr =
  function 
      Attribute(p, n, v) -> v
    | _ -> not_an_attribute ();;

let set_element_attrs el attributes = 
  match el with
      Element (p, name, nss, attrs, ch) -> attrs := attributes
    | _ -> raise (XMLError ("Only element nodes can have attributes"))

(* Utility functions to find/retrieve attributes *) 
let attr_nameeq name =
  fun x ->
    match x with
	Attribute (p, ((pref, ns), oname), value) -> name = oname
      | _ -> not_an_attribute ();;


let attrns_nameeq ns name =
  fun x ->
    match x with
	Attribute (p, ((pref, ons), oname), value) -> name = oname && ns = ons
      | _ -> not_an_attribute ();;


let attr_mem_assoc name = 
  List.exists (attr_nameeq name);;

let attrns_mem_assoc ns name = 
  List.exists (attrns_nameeq ns name);;

let attr_assoc name attrs =
  let attr = (List.find (attr_nameeq name) attrs) in
    match attr with
	Attribute (p, n, value) -> value
      | _ -> not_an_attribute ();;


let attrns_assoc ns name attrs =
  let attr = List.find (attrns_nameeq ns name) attrs in
    match attr with
	Attribute (p, n, value) -> value
      | _ -> not_an_attribute ();;

let qname_eq ((pref, ns), name) =
  (fun ((apref, ans), aname) -> ans = ns && aname = name)

let qname_mem_assoc qname list =
  List.exists (fun (aname, x) -> qname_eq qname aname) list

let qname_assoc qname list =
  snd (List.find (fun (aname, x) -> qname_eq qname aname) list)

let qname_assoc_nc ncname = qname_assoc (unqname ncname)

let qname_try_assoc qname list default =
  try
    qname_assoc qname list
  with Not_found -> default

let qname_try_assoc_nc ncname = qname_try_assoc (unqname ncname)

let ns_assoc = qname_assoc  

let raiseelem () = raise (XMLError ("Only element nodes have attributes"))

let has_attribute ncname node =
  match node with
      Element (_, _, _, attr, _) -> List.exists (attr_nameeq ncname) !attr
    | _ -> raiseelem ()

let get_attribute ncname node =
  match node with
      Element (_, _, _, attr, _) -> List.find (attr_nameeq ncname) !attr
    | _ -> raiseelem ()
	
let has_attribute_ns (ns, ncname) node =
  match node with
      Element (_, _, _, attr, _) -> List.exists (attrns_nameeq ns ncname) !attr
    | _ -> raiseelem ()

let get_attribute_ns (ns, ncname) node =
  match node with
      Element (_, _, _, attr, _) -> List.find (attrns_nameeq ns ncname) !attr
    | _ -> raiseelem ()

let get_attr_required name attrs handler =
  try 
    attr_assoc name attrs
  with
      Not_found -> handler ();;

let try_get_attr name attrs default =
  if not (attr_mem_assoc name attrs) then
    default
  else
    attr_assoc name attrs;;

let try_get_int_attr name attrs default =
  match try_get_attr name attrs "" with
      "" -> default
    | _ as c -> 
	int_of_string c;;

let try_get_bool_attr name attrs default =
  match try_get_attr name attrs "" with
      "" -> default
    | "yes" -> true
    | "no" -> false
    | _ as value -> raise (XMLError ("Attribute " ^ name ^ " do not accept value " ^ value));;

let required_attr attr elem () =
  raise (XMLError ("Attribute " ^ attr ^ " required for element " ^ elem));;

let required_elem elem parent () =
  raise (XMLError ("Child element " ^ elem ^ " required for element " ^ parent));;

(** Type predicates *)
let elementp =
  function
      Element _ -> true
    | _ -> false;;

let attributep =
  function
      Attribute _ -> true
    | _ -> false;;

let namespacep =
  function
      NS _ -> true
    | _ -> false;;


let rootp =
  function
      Root (nodes) -> true
    | _ -> false;;

let root_or_elemp =
  function
      Element (p, name, nss, att, child) -> true
    | Root nodes -> true
    | _ -> false;;

let nodep =
  function
    | _ -> true;;

let commentp =
  function 
      Comment (p, str) -> true
    | _ -> false;;

let textp = 
  function
      Text (p, str) -> true
    | _ -> false;;

let processing_instructionp = function
      ProcessingInstruction (p, name, str) -> true
    | _ -> false;;

let name = function
    | Element (p, name, nss, att, child) -> name
    | ProcessingInstruction (p, target, str) -> default_ns, target
    | Attribute (p, name, value) -> name
    | NS (p, (pref, uri)) -> default_ns, pref
    | _ -> null_name

let name_opt = function
  | Element (p, name, nss, att, child) -> Some name
  | ProcessingInstruction (p, target, str) -> Some (default_ns, target)
  | Attribute (p, name, value) -> Some name
  | NS (p, (pref, uri)) -> Some (default_ns, pref)
  | _ -> None

(** Member accessing functions *)
let children = function
      Element (p, n, nss, att, children) ->
	!children
    | Root (nodes) ->
	!nodes
    | _ -> [];;

let namespace_nodes = function
      Element (p, name, nss, att, child) -> !nss
    | _ -> [];;

let namespaces n = 
  List.map 
    (fun el -> 
       match el with
	   NS (_, m) -> m
	 | _ -> 
	     raise (XMLError
		      (sprintf "non-namespace node in namespaces of node %s"
			 (string_of_name (name n)))))
    (namespace_nodes n)

let attribute_nodes = function
      Element (p, name, nss, att, child) -> !att
    | _ -> [];;

let attributes node = 
  List.map 
    (fun el -> 
       match el with
	   Attribute (_, n, v) -> (n,v)
	 | _ -> raise (XMLError "non-attribute node in attributes"))
    (attribute_nodes node)
 
let rec find_ns_mapping prefix = function
    Element (p, _, nss, _, _) -> 
      (try 
	 let ns = 
	   List.find 
	     (fun ns -> 
		match ns with
		    NS (p, (pref, uri)) -> pref = prefix
		  | _ -> false)
	     !nss
	 in match ns with
	     NS (p, mapping) -> mapping
	   | _ -> raise Not_found
       with Not_found -> find_ns_mapping prefix !p)
  | _ -> raise Not_found

let rec find_ns_mappings uri = function
    Element (p, _, nss, _, ch) -> 
      let nss = 
	List.find_all
	  (fun ns -> 
	     match ns with
		 NS (p, (pref, `URI u)) -> u = uri
	       | _ -> false)
	  !nss
	 in 
	(List.map (fun ns -> match ns with NS (p, mapping) -> mapping
		     | _ -> raise Not_found) nss) @ 
	(List.flatten (List.map (find_ns_mappings uri) !ch))	
  | Root (nodes) -> List.flatten (List.map (find_ns_mappings uri) !nodes)
  | _ -> []

let name_exc n = 
  let qname = name n in
    if qname = null_name then raise Not_found
    else qname

let root_node doc = doc.root 

let document_element doc : node option =
  try
    Some (List.find elementp (children doc.root))
  with Not_found -> None;;


(** Serializing functions *)

let rec pretty_string_of_node str (n: node) =
  match n with
      Element (parent, n, nss, attrs, children) ->
	let attrstr =
	  if List.length !attrs != 0 then
	    " " ^ (string_of_list string_of_attribute " " !attrs)
	  else ""
	and childstr =
	  if List.length !children != 0 then
	    ">\n" ^ (string_of_list (pretty_string_of_node ("  " ^ str)) "" !children) ^ str ^ "</" ^ (string_of_name n) ^ ">\n"
	  else "/>\n"
	and nsstr =
	  if List.length !nss != 0 then
	    " " ^ (string_of_list string_of_namespace " " !nss)
	  else ""
	in
	  str ^ "<" ^ (string_of_name n) ^ attrstr ^ nsstr ^ childstr
    | Text (p, txt) -> str ^ txt ^ "\n"
    | Comment (p, comm) -> str ^ "<!-- " ^ comm ^ " -->\n"
    | ProcessingInstruction (p, n, pi) -> str ^ "<?" ^ n ^ " " ^ pi ^ "?>\n"
    | Root (nodes) -> pretty_string_of_nodes !nodes;
    | _ -> ""

and pretty_string_of_nodes nodes =
  String.concat "" (List.map (pretty_string_of_node "") nodes)

(** FIXME 
let string_of_doctype_decl = function
*)
let string_of_doctype_decls decls = 
  ""


let pretty_string_of_doctype d = 
  let extid =
    match d.public_id with
	Some p -> 
	  let syslit = 
	    match d.system_id with 
		None -> raise (XMLError "DTD declaration has public id but no system id!!")
	      | Some s -> string_of_attr_value s
	  in
	    " PUBLIC " ^ (string_of_attr_value p) ^ " " ^ syslit
      | None -> 
	  (match d.system_id with 
	       None -> ""
	     | Some s -> " SYSTEM " ^ (string_of_attr_value s))
  in
    if d.name <> "" then
      Printf.sprintf "<!DOCTYPE %s%s%s>"
	d.name extid 
	(match d.internal_subset with 
	     None -> ""
	   | Some d -> " [" ^ (string_of_doctype_decls d) ^ "]")
    else ""
	
let pretty_string_of_doc doc = 
  let decl = Printf.sprintf "<?xml version=\"1.0\" encoding=\"%s\"?>\n" (Netconversion.string_of_encoding doc.encoding) in
  let dt = pretty_string_of_doctype doc.doctype in
    decl ^ dt ^ "\n" ^ (pretty_string_of_nodes (children doc.root))

let pretty_print_doc doc = 
  print_string (pretty_string_of_doc doc)

let serialize_doctype write d =
  write (pretty_string_of_doctype d)

let rec serialize_doc write ?(indent = false) ?(omit_decl = false) ?(omit_doctype = false) doc =
  if not omit_decl then 
    write (sprintf "<?xml version=\"1.0\" encoding=\"%s\"?>" 
	     (Netconversion.string_of_encoding doc.encoding));
  if not omit_doctype then 
    serialize_doctype write doc.doctype;
  if indent then (
    write "\n";
    serialize_node_indent write "" doc.root
  ) else serialize_node write doc.root

and serialize_node write = function
    Element (parent, n, nss, attrs, children) ->
      let attrstr =
	if List.length !attrs != 0 then
	  " " ^ (string_of_list string_of_attribute " " !attrs)
	else ""
      and nsstr =
	if List.length !nss != 0 then
	  " " ^ (string_of_list string_of_namespace " " !nss)
	else ""
      in
	write ("<" ^ (string_of_name n) ^ attrstr ^ nsstr);
	if !children = [] then write "/>" 
	else (
	  write ">";
	  List.iter (serialize_node write) !children;
	  write (sprintf "</%s>" (string_of_name n))
	)
  | Text (p, txt) -> write txt
  | Comment (p, comm) -> write "<!--"; write comm; write "-->"
  | ProcessingInstruction (p, n, pi) -> write (sprintf "<?%s %s?>" n pi)
  | Root (nodes) -> List.iter (serialize_node write) !nodes;
  | _ -> ()
      
and serialize_node_indent write str = function
    Element (parent, n, nss, attrs, children) ->
      let attrstr =
	if List.length !attrs != 0 then
	  " " ^ (string_of_list string_of_attribute " " !attrs)
	else
	  ""
      and nsstr =
	if List.length !nss != 0 then
	  " " ^ (string_of_list string_of_namespace " " !nss)
	else ""
      in
	write (str ^ "<" ^ (string_of_name n) ^ attrstr ^ nsstr);
	if !children = [] then write "/>\n" 
	else (
	  write ">\n";
	  List.iter (serialize_node_indent write (str ^ "  ")) !children;
	  write (sprintf "%s</%s>\n" str (string_of_name n))
	)
  | Text (p, txt) -> write (Yaxi_utils.trim txt); write "\n"
  | Comment (p, comm) -> write (str ^ "<!--"); write str; write comm; write str; write "-->\n"
  | ProcessingInstruction (p, n, pi) -> write (sprintf "%s<?%s %s?>\n" str n pi)
  | Root (nodes) -> List.iter (serialize_node_indent write "") !nodes;
  | _ -> ()

let rec find_root = function
    Root (nodes) as n -> n
  | Element (p, name, nss, attrs, chil) -> find_root !p
  | Comment (p, cont) -> find_root (!p)
  | Text (p, cont) -> find_root (!p)
  | ProcessingInstruction (p, name, cont) -> find_root !p
  | Attribute (p, n, v) -> find_root !p
  | _ -> failwith ("Node has no parent")
