(* Yaxi - Yet Another X* Implementation
 * Copyright (C) 2003 Matthieu Sozeau
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * $Id: yaxi_xmlparser.ml,v 1.27 2003/09/28 22:43:12 mat Exp $
 * 
 *)

open Printf

exception SyntaxError of string
exception EOF

let default_namespaces = [Yaxi_xml.default_ns;Yaxi_xml.xml_ns;Yaxi_xml.xmlns_ns]
let xmlspace_attribute = (Yaxi_xml.xml_ns, "space")
			   
let sax_parse_doc (handler : Yaxi_sax.sax_handler) uri options use_dtd keep_references lexbuf =
  let doctype = ref (Yaxi_xml.new_dtd "" ()) in
  let next env = Yaxi_xmllexer.token ~keep_refs:keep_references env !doctype lexbuf in
  let rec get_node env strip = function
      Yaxi_xmllexer.Comment str -> handler # comment str
    | Yaxi_xmllexer.Pi (target, data) -> handler # pi target data
    | Yaxi_xmllexer.Whitespace str -> 
	if not strip then handler # characters str 
	else get_node env strip (next env)
    | Yaxi_xmllexer.Text str -> handler # characters str
    | Yaxi_xmllexer.OpenElement (qname, namespaces, attributes) ->
	let strip = 
	  if List.mem_assoc xmlspace_attribute attributes then
	    (List.assoc xmlspace_attribute attributes) <> "preserve"
	  else strip
	in
	let rec parse_next env = 
	  match next env with
	      Yaxi_xmllexer.CloseElement qname -> 
		handler # endElement qname
	    | Yaxi_xmllexer.Whitespace c ->
		if not strip then handler # characters c;
		parse_next env
	    | other -> get_node env strip other;
		parse_next env
	in
	let env = namespaces @ env in
	  if namespaces <> [] then (
	    List.iter (handler # startNSMapping) namespaces;
	    handler # startElement qname attributes;
	    parse_next env;
	    List.iter (handler # endNSMapping) namespaces)
	  else (
	    handler # startElement qname attributes;
	    parse_next env;
	  )   
    | Yaxi_xmllexer.EmptyElement (qname, namespaces, attributes) ->
	if namespaces <> [] then (
	  List.iter (handler # startNSMapping) namespaces;
	  handler # startElement qname attributes;
	  handler # endElement qname;
	  List.iter (handler # endNSMapping) namespaces)
	else (
	  handler # startElement qname attributes;
	  handler # endElement qname;
	)
    | Yaxi_xmllexer.Cdata str -> handler # characters str
    | Yaxi_xmllexer.ExternalParsedEntityRef (name, pub, sys) ->
	handler # characters ("&" ^ name ^ ";")
    | Yaxi_xmllexer.Doctype dtd -> 
	failwith "Doctype declaration inside of element"
    | Yaxi_xmllexer.CloseElement qname ->
	failwith "Error: close element not expected (no open element)"
    | Yaxi_xmllexer.DocumentEnd ->
	failwith "Error: document end not expected while parsing a node's content"	
  in
  let rec get_nodes () =
    match next default_namespaces with
      | Yaxi_xmllexer.Doctype (dtd) ->   
	  doctype := dtd;
	  get_nodes ()
      | Yaxi_xmllexer.Comment data -> 
	  handler # comment data;
	  get_nodes ()
      | Yaxi_xmllexer.Pi (target, data) ->
	  (match target with
	       "xml" -> get_nodes ()
	     | other -> handler # pi target data)
      | Yaxi_xmllexer.Text str -> failwith "Text not allowed at top level"
      | Yaxi_xmllexer.DocumentEnd -> ()
      | other -> let n = get_node default_namespaces true other in
	  get_nodes ()
  in
    handler # documentDeclaration options;    
    get_nodes ()
      
open Yaxi_xml

let curry f =
  fun (n, v) -> f n v

let dom_parse_doc uri options use_dtd keep_references lexbuf =
  let el = Root(ref []) in
  let doctype = ref (new_dtd  "" ()) in
  let parse_attributes = List.map (curry new_attribute) in
  let parse_namespaces = List.map new_namespace in
  let next env = 
    try
      Yaxi_xmllexer.token ~keep_refs:keep_references env !doctype lexbuf 
    with Ulexing.Error ->
      Yaxi_xmllexer.parse_error lexbuf "Unknown context!"
  in
  let rec get_node env strip = function
      Yaxi_xmllexer.Comment str -> new_comment str
    | Yaxi_xmllexer.Pi (target, data) -> new_pi target data
    | Yaxi_xmllexer.Whitespace str -> 
	if strip then get_node env strip (next env)
	else new_text str
    | Yaxi_xmllexer.Text str -> new_text str
    | Yaxi_xmllexer.OpenElement (qname, namespaces, attributes) ->
	let strip = 
	  if List.mem_assoc xmlspace_attribute attributes then
	    (List.assoc xmlspace_attribute attributes) <> "preserve"
	  else strip
	in
	let rec parse_next env = 
	  match next env with
	      Yaxi_xmllexer.CloseElement str -> 
		if qname = str then []
		else failwith 
		  (sprintf "Unmatched tags %s and %s" 
		     (string_of_name qname) (string_of_name str))
	    | Yaxi_xmllexer.Whitespace str ->
		if strip then parse_next env
		else (new_text str) :: parse_next env
	    | other -> 
		let n = get_node env strip other in
		  n :: parse_next env
	in
	let env = namespaces @ env in
	  (*printf "Got namespaces: %s\n" (Utils.string_of_list (fun (pref,uri) -> pref ^ ":" ^ (match uri with `None -> "" | `URI s -> s)) " " namespaces);*)
	  new_element qname ~namespaces:(parse_namespaces namespaces) 
	    ~attributes:(parse_attributes attributes) ~children:(parse_next env) ()
    | Yaxi_xmllexer.EmptyElement (qname, namespaces, attributes) ->
	new_element qname ~namespaces:(parse_namespaces namespaces) 
	~attributes:(parse_attributes attributes) ~children:[] ()	
    | Yaxi_xmllexer.Cdata str -> new_text str
    | Yaxi_xmllexer.ExternalParsedEntityRef (name, pub, sys) ->
	new_text ("&" ^ name ^ ";")
    | Yaxi_xmllexer.Doctype (dtd) -> 
	failwith "Doctype declaration inside of element"
    | Yaxi_xmllexer.CloseElement qname ->
	failwith "Error: close element not expected"
    | Yaxi_xmllexer.DocumentEnd ->
	failwith "Error: document end not expected while parsing a node's content"	
  in
  let rec get_nodes () =
    match next default_namespaces with
      | Yaxi_xmllexer.Doctype (dtd) -> 
	  doctype := dtd;
	  get_nodes ()
      | Yaxi_xmllexer.Comment data -> 
	  (new_comment data) :: (get_nodes ())
      | Yaxi_xmllexer.Pi (target, data) ->
	  (match target with
	       "xml" -> get_nodes ()
	     | other -> (new_pi target data) :: (get_nodes ()))
      | Yaxi_xmllexer.Whitespace _ -> get_nodes ()
      | Yaxi_xmllexer.ExternalParsedEntityRef _ -> 
	  failwith "Entity references not allowed at top level"
      | Yaxi_xmllexer.Cdata str -> failwith "CDATA not allowed at top level"
      | Yaxi_xmllexer.Text str -> failwith "Text not allowed at top level"
      | Yaxi_xmllexer.DocumentEnd -> []
      | other -> let n = get_node default_namespaces true other in
	  n :: get_nodes ()
  in
  let nodes = get_nodes () in
  let doc = 
    new_document uri ~doctype:!doctype ()
  in
    reparent_nodes el nodes;
    doc.root <- el;
    doc;;

let try_assoc name list default =
  try
    List.assoc name list
  with Not_found -> default

let parse_document parse_doc_fn uri ?(use_dtd = true) ?(channel = None) ?(str = None) () =
  let dtd = Yaxi_xml.new_dtd "" () in
  let lexbuf = 
    match channel with
	Some chan -> Ulexing.from_utf8_channel chan 
      | None -> 
	  (match str with 
	       Some str -> Ulexing.from_utf8_string str
	     | None -> failwith ("Yaxi_xmlparser.parse_document needs either a channel or a string"))
  in
    match Yaxi_xmllexer.token ~keep_refs:true [] dtd lexbuf with
	Yaxi_xmllexer.Pi (target, data) ->
	  let options = Yaxi_xmllexer.parse_options data in 
	  let encoding = try_assoc "encoding" options "UTF-8"
	  and version = try_assoc "version" options "1.0" in
	  let lexbuf =
	    match Netconversion.encoding_of_string encoding with
		`Enc_utf8 ->
		  (match channel with 
		       Some chan -> 
			 seek_in chan 0;
			 Ulexing.from_utf8_channel chan
		     | None -> 
			 (match str with
			      Some str -> Ulexing.from_utf8_string str
			    | None -> failwith ("Yaxi_xmlparser.parse_document needs either a channel or a string")))
	      | in_encoding ->
		  let pipe = new Netconversion.recoding_pipe ~in_enc:in_encoding ~out_enc:`Enc_utf8 () in 
		  let chan =
		    match channel with 
			Some chan -> 
			  seek_in chan 0;			    
			  new Netchannels.input_channel chan
		      | None -> (match str with
				     Some str -> new Netchannels.input_string str
				   | None -> failwith ("Yaxi_xmlparser.parse_document needs either a channel or a string"))
		  in
		  let filter = new Netchannels.input_filter chan pipe in
		  let stream i = 
		    try Some (filter # input_char ())
		    with End_of_file -> None
		  in
		    Ulexing.from_utf8_stream (Stream.from stream)
	  in		
	    parse_doc_fn uri options use_dtd true lexbuf
      | tok -> failwith ("The xml declaration must be first in the document, got token");;

let parse_file filename = 
  let channel = open_in filename in
  let doc = parse_document dom_parse_doc filename ~channel:(Some channel) () in
    close_in channel;
    doc

let parse_string str =  
  parse_document dom_parse_doc "" ~str:(Some str) ();;


let sax_parse_file handler filename =
  let channel = open_in filename in
    parse_document (sax_parse_doc handler) filename ~channel:(Some channel) ();
    close_in channel

let sax_parse_string handler str =
  parse_document (sax_parse_doc handler) "" ~str:(Some str) ();;
