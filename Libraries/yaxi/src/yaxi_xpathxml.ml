(* Yaxi - Yet Another X* Implementation
 * Copyright (C) 2003 Matthieu Sozeau
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * $Id: yaxi_xpathxml.ml,v 1.19 2004/02/18 22:43:08 mat Exp $
 * 
 *)

(** Implements basic XPath functions and operators 
  over a document tree
*)

open Yaxi_utils
open Yaxi_xml
open Yaxi_xpath
open Printf

(** function name, minimal, maximal number of arguments *)
exception ArityError of string * int * int;; 

exception XPathError of string;;

let bad_arity fn min max = raise (ArityError (fn, min, max));;

let rec string_value_of_node : node -> string =
  function
      Root (children) -> string_value_of_nodes !children
    | Element (p, name, nss, attrs, children) -> 
	string_value_of_nodes !children
    | Text (p, str) -> str
    | Comment (p, str) -> str
    | ProcessingInstruction (p, n, str) -> ""
    | Attribute (p, attr, value) -> value
    | NS (p, (pref, ns)) -> 
	(match ns with
	     `URI str -> str
	   | `None -> "")
	
and string_value_of_nodes : nodeset -> string = 
  function
      [] -> ""
    | hd::tl -> 
	(string_value_of_node hd) ^ (string_value_of_nodes tl);;

let string_of_number n =
  try string_of_float n
  with Failure _ -> ""

let number_of_string s =
  try float_of_string s
  with Failure _ -> 0.

let rec string_value : primary_type -> string =
  function
      Literal str -> str
    | Boolean b -> if b then "true" else "false"
    | Number f -> string_of_number f
    | Nodes nodes -> string_value_of_nodes nodes;;

let rec ancestors (n : node) : nodeset =
  match parent n with
      Some p -> p :: (ancestors p)
    | None -> [];;

let rec descendants : node -> nodeset =
  function
      Element (parent, name, nss, att, children) ->
	(!children) @ (List.flatten (List.map descendants !children))
    | Root (nodes) ->
	!nodes @ (List.flatten (List.map descendants !nodes))
    | _ -> [];;

let following_siblings (node : node) : nodeset = 
  let rec aux =
    function 
	[] -> []
      | hd::tl ->
	  if hd = node then
	    tl
	  else
	    aux tl
  in
    match node with
	Root (nodes) -> []
      | Attribute (p, name, value) -> []
      | NS (p, mapping) -> []
      | _ ->
	  let nodes = 
	    match parent node with 
		Some p ->
		  children p
	      | None -> raise (Failure "Node without parent set!!!")
	  in
	    aux nodes;;

let followings n = 
  List.flatten (List.map following_siblings (n :: (ancestors n)));;

let preceding_siblings node = 
  let nodes = 
    match parent node with
	Some p -> 
	  children p
      | None -> []
  in
  let rec aux = 
    function 
	[] -> []
      | hd::tl ->
	  if hd = node then
	    []
	  else
	    hd :: (aux tl)
  in
    aux nodes;;

let precedings n = 
  List.flatten (List.map preceding_siblings (n :: (ancestors n)));;

let nodes_of_axis : axis -> (node -> nodeset) =
  let make_fun fn =
    (fun x -> fn x)
  in
    function
	`Self -> (fun x -> [x])
      | `Parent -> (fun x -> match parent x with None -> [] | Some n -> [n])
      | `Ancestor -> make_fun ancestors
      | `Ancestor_or_self -> (fun x -> x :: (ancestors x))
      | `Attribute -> make_fun attribute_nodes
      | `Namespace -> make_fun namespace_nodes
      | `Child -> make_fun children
      | `Descendant -> make_fun descendants
      | `Descendant_or_self -> (fun x -> x :: (descendants x))
      | `Following -> make_fun followings
      | `Following_sibling -> make_fun following_siblings
      | `Preceding -> make_fun precedings
      | `Preceding_sibling -> make_fun preceding_siblings

(* axis::* *)
let starp : axis -> node -> bool = function 
    `Attribute -> attributep
  | `Namespace -> namespacep
  | _ -> elementp

let name_test axis (n2 : qname) node : bool = 
  if not (starp axis node) then
    false
  else
    match name_opt node with
	Some (((pref, uri), name) as n) -> 
	  (match n2 with 
	       ((pref2, uri2), name2) ->
		 if name2 = "*" then
		   uri = uri2
		 else
		   uri = uri2 && name = name2)
      | None -> false

(* Conversions *)
	
let string_of_object = function
    Literal str -> str
  | Nodes nodes ->
      (match nodes with
	   [] -> ""
	 | hd::tl -> string_value_of_node hd)
  | Number n ->
      string_of_number n
  | Boolean b ->
      if b then "true" else "false"
	
let boolean_of_object = function
    Boolean b -> b
  | Literal l ->  (String.length l != 0)
  | Nodes n -> List.length n != 0
  | Number f -> 
      (match classify_float f with
	   FP_zero | FP_nan -> false
	 | _ -> true)
      
let rec number_of_object obj =
  match obj with
      Boolean b -> if b then 1. else 0.
    | Literal l -> number_of_string l
    | Nodes n -> number_of_object (Literal (string_of_object obj))
    | Number b -> b
	
let desc_string_of_object = function
    Literal str -> "'" ^ str ^ "'"
  | Nodes nodes ->
      string_of_list (string_of_node) "," nodes
  | Number n ->
      string_of_number n
  | Boolean b ->
      if b then "true" else "false";;   

let number_of_int n = Number (float_of_int n);;

let nodes_of_object = function
    Nodes n -> n
  | _ -> raise (Failure "Object is not a node-set");;

(* Node-set functions *)
let xpath_union a b =
  (match a with
       Nodes n -> 
	 (match b with 
	      Nodes n2 -> Nodes (n @ n2)
	    | _ -> raise (XPathError ("Invalid argument for union, all must be node-sets")))
     | _ -> raise (XPathError ("Invalid argument for union, all must be node-sets")))
  

let xpath_last args ctxt = match args with 
    [] -> number_of_int ctxt.size
  | _ -> bad_arity "last" 0 0;;

let xpath_position args ctxt = 
  match args with 
      [] -> number_of_int ctxt.position
    | _ -> bad_arity "position" 0 0;;

let xpath_count args ctxt = 
  match args with
      hd::[] -> number_of_int (List.length (nodes_of_object hd))
    | _ -> bad_arity "count" 1 1;;

let rec id obj =
  match obj with
      Nodes nodes -> 
	List.flatten (List.map (fun x -> id (Literal (string_value_of_node x))) nodes)
    | Literal str ->
	[]
    | _ -> [];;

let xpath_id args ctx =
  match args with
      hd::[] -> Nodes (id hd)
    | _ -> bad_arity "id" 1 1;;

let local_name =
  function
      [] -> ""
    | hd::tl -> snd (name hd)

let xpath_local_name args ctx =
  match args with
      hd::[] -> Literal (local_name (nodes_of_object hd))
    | [] -> Literal (local_name [ctx.node])
    | _ -> bad_arity "count" 0 1;;

let namespace_uri =
  function 
      [] -> ""
    | hd::tl ->
	let (_, uri), _ = name hd in
	  (match uri with 
	       `URI str -> str
	     | `None -> "")

let xpath_namespace_uri args ctx =
  match args with
      hd::[] -> Literal (namespace_uri (nodes_of_object hd))
    | [] -> Literal (namespace_uri [ctx.node])
    | _ -> bad_arity "namespace-uri" 0 1;;


let xname =
  function
      [] -> ""
    | hd::tl -> string_of_name (name hd)

let xpath_name args ctx =
  match args with
      hd::[] -> Literal (xname (nodes_of_object hd))
    | [] -> Literal (xname [ctx.node])
    | _ -> bad_arity "name" 0 1;;


(* String functions *)

let string obj = 
  Literal (string_of_object obj);;

let xpath_string args ctx = 
  match args with
      [] -> string (Nodes [ctx.node])
    | hd :: [] -> string hd
    | _ -> bad_arity "string" 0 1
	
let rec concat_ = 
  function
      [] -> ""
    | hd::tl -> 
	(string_of_object hd) ^ (concat_ tl);;

let concat strs = 
  Literal (concat_ strs);;

let xpath_concat args ctxt =
  concat args;;

(* Should work with UTF8, if composed and decomposed forms are not used together *)
let starts_with str prefix =
  let l = String.length prefix in
    if String.length str < l then Boolean false
    else
      Boolean (prefix = (Netstring_pcre.first_chars str l));;

let make_str_fn2args fname fn args ctx =
  match args with 
      hd::tl::[] -> fn (string_of_object hd) (string_of_object tl)
    | _ -> bad_arity fname 2 2;;

let xpath_starts_with =
  make_str_fn2args "starts-with" starts_with;;

let idx_of_substring str substr = 
  try
    Yaxi_utils.Utfstring.match_string str substr
  with Not_found ->
    -1;;

let contains str substr = 
  Boolean ((idx_of_substring str substr) != -1);;

let xpath_contains =
  make_str_fn2args "contains" contains;;

let substring_before str substr =
  match idx_of_substring str substr with
      -1 -> Literal ""
    | idx -> Literal (Yaxi_utils.Utfstring.first_chars str idx);;

let xpath_substring_before =
  make_str_fn2args "substring-before" substring_before;;

let substring_after str substr =
  match idx_of_substring str substr with
      -1 -> Literal ""
    | idx -> Literal (Yaxi_utils.Utfstring.string_after str (idx + (Yaxi_utils.Utfstring.length substr)));;

let xpath_substring_after =
  make_str_fn2args "substring-after" substring_after;;

let substring str beg end_ = 
  try
    Literal (Yaxi_utils.Utfstring.sub str (int_of_float beg) (int_of_float end_))
  with
      Failure str -> Literal ("");;

let xpath_substring args ctx =
  match args with
      a::b::c::[] -> substring (string_of_object a) (number_of_object b) (number_of_object c)
    | a::b::[] -> 
	let str = string_of_object a in
	  substring str (number_of_object b) (float_of_int (Yaxi_utils.Utfstring.length str))
    | _ -> bad_arity "substring" 2 3;;

let string_length str = 
  Number (float_of_int (Yaxi_utils.Utfstring.length str));;

let xpath_string_length args ctx =
  match args with 
      hd::[] -> string_length (string_of_object hd)
    | _ -> bad_arity "string-length" 1 1;;

let normalize_space str =
  let strings = Netstring_pcre.split_delim (Netstring_pcre.regexp "[ \t\n\r]+") str in
    Literal (String.concat " " strings);;

let xpath_normalize_space args ctx =
  match args with
      hd::[] -> normalize_space (string_of_object hd)
    | _ -> bad_arity "normalize" 1 1;;

let translate str from to_ = 
  let lento = String.length to_ in
  let subst c =
    try 
      let i = String.index from c in
	if i > lento then 
	  ""
	else
	  String.make 1 to_.[i]
    with
	Not_found -> (String.make 1 c)
  in
  let res = ref "" in
    String.iter (fun x -> res := !res ^ (subst x)) str;
    Literal !res;;

let xpath_translate args ctx =
  match args with
      str::from::to_::[] -> translate (string_of_object str) (string_of_object from) (string_of_object to_)
    | _ -> bad_arity "translate" 3 3;;

(* Boolean functions *)

let boolean obj =
  Boolean (boolean_of_object obj);;

let xpath_boolean args ctx =
  match args with
      hd::[] -> boolean hd
    | _ -> bad_arity "boolean" 1 1;;

let xpath_not args ctx =
  match args with
      hd::[] -> Boolean (not (boolean_of_object hd))
    | _ -> bad_arity "not" 1 1;;

let xpath_true args ctx =
  match args with
      [] -> Boolean true
    | _ -> bad_arity "true" 0 0;;

let xpath_false args ctx =
  match args with
      [] -> Boolean false
    | _ -> bad_arity "false" 0 0;;


(* Number functions *)

let xpath_number args ctx =
  match args with
      hd::[] -> Number (number_of_object hd)
    | _ -> bad_arity "number" 1 1;;

let rec sum_of_nodes =
  function
      [] -> 0.
    | hd::tl ->
	(number_of_string (string_value_of_node hd)) +. (sum_of_nodes tl);;

let sum nodes =
  Number (sum_of_nodes nodes);;

let xpath_sum args ctx =
  match args with
      hd::[] -> sum (nodes_of_object hd)
    | _ -> bad_arity "sum" 1 1;;

let xpath_floor args ctx =
  match args with
      hd::[] -> Number (floor (number_of_object hd))
    | _ -> bad_arity "floor" 1 1;;

let xpath_ceil args ctx =
  match args with
      hd::[] -> Number (ceil (number_of_object hd))
    | _ -> bad_arity "ceil" 1 1;;

let round_ n =
  match classify_float n with
      FP_normal | FP_subnormal ->
	Number (ceil n)
    | _ -> Number n;;   

let xpath_round args ctx =
  match args with
      hd::[] -> round_ (number_of_object hd)
    | _ -> bad_arity "floor" 1 1;;

let equals obj other =
  if nodesp obj || nodesp other then (
    if nodesp obj && nodesp other then 
      List.exists (fun x -> 
		     List.exists (fun y -> (string_value_of_node x) = (string_value_of_node y)) (nodes_of_object other)) 
	(nodes_of_object obj)
    else (
      let nodes, obj = if nodesp obj then (obj, other) else (other, obj) in
	if numberp obj then (
	  List.exists (fun x -> 
			 try 
			   let x = float_of_string (string_value_of_node x) in
			     x = (number_of_object obj)
			 with Failure str -> false
		      ) (nodes_of_object nodes)
	) else if literalp obj then
	  List.exists (fun x -> string_value_of_node x = string_of_object obj) (nodes_of_object nodes)
	else
	  (boolean_of_object obj) = (boolean_of_object nodes)
    )
  ) else (
    if booleanp obj || booleanp other then
      (boolean_of_object obj) = (boolean_of_object other)
    else if (numberp obj) || (numberp other) then
      (number_of_object obj) = (number_of_object other)
    else 
      (string_of_object obj) = (string_of_object other));;

(* Comparisons use number values *)
let compare obj other : float =
  (number_of_object obj) -. (number_of_object other);;

let xpath_or a b =
  Boolean ((boolean_of_object a) || (boolean_of_object b));;

let xpath_and a b =
  Boolean ((boolean_of_object a) && (boolean_of_object b));;

let xpath_equal a b =
  Boolean (equals a b);;

let xpath_inequal a b =
  Boolean (not (equals a b));;

let xpath_add a b =
  Number ((number_of_object a) +. (number_of_object b));;

let xpath_sub a b =
  Number ((number_of_object a) -. (number_of_object b));;

let xpath_modulo a b =
  Number (mod_float (number_of_object a) (number_of_object b));;

let xpath_mult a b =
  Number ((number_of_object a) *. (number_of_object b))

let xpath_division a b =
  Number ((number_of_object a) /. (number_of_object b))

let xpath_superior a b =
  Boolean ((compare a b) > 0.);;

let xpath_inferior a b =
  Boolean ((compare a b) < 0.);;

let xpath_superior_or_equal a b =
  Boolean ((compare a b) >= 0.);;

let xpath_inferior_or_equal a b =
  Boolean ((compare a b) <= 0.);;

let xpath_minus a =
  Number (-.(number_of_object a));;

let functions = 
  let list =
    [
      (* Node sets *)
      ("last", xpath_last);
      ("position", xpath_position);
      ("count", xpath_count);
      ("id", xpath_id);
      ("local-name", xpath_local_name);
      ("namespace-uri", xpath_namespace_uri);
      ("name", xpath_name);
      (* Strings *)
      ("string", xpath_string);
      ("concat", xpath_concat);
      ("starts_with", xpath_starts_with);
      ("contains", xpath_contains);
      ("substring_before", xpath_substring_before);
      ("substring_after", xpath_substring_after);
      ("substring", xpath_substring);
      ("string-length", xpath_string_length);
      ("normalize-space", xpath_normalize_space);
      ("translate", xpath_translate);
      (* Booleans *)
      ("boolean", xpath_boolean);
      ("not", xpath_not);
      ("true", xpath_true);
      ("false", xpath_false);
      (* Numbers *)
      ("number", xpath_number);
      ("sum", xpath_sum);
      ("floor", xpath_floor);
      ("ceil", xpath_ceil);
      ("round", xpath_round);
    ]
  in
    List.map (fun (x,y) -> ((default_ns, x), (fun args gctx ctx -> y args ctx))) list
      
let operations = [
  (`Union, xpath_union);
  (`Equal, xpath_equal);
  (`InEqual, xpath_inequal);
  (`And, xpath_and);
  (`Or, xpath_or);
  (`Sup, xpath_superior);
  (`Inf, xpath_inferior);
  (`SupEqual, xpath_superior_or_equal);
  (`InfEqual, xpath_inferior_or_equal);
  (`Mod, xpath_modulo);
  (`Div, xpath_division);
  (`Add, xpath_add);
  (`Sub, xpath_sub);
  (`Mul, xpath_mult);
]

let unary_operations = [
  (`Minus, xpath_minus)
]

let make_xpath_step_fun (axis, test, predicates) =
  let fun_node = nodes_of_axis axis in
    (fun gctx ctx -> 
       let nodes = fun_node ctx.node in
       let rest = List.filter (test gctx ctx) nodes in
       let i = ref 0 in
	 Nodes 
	   (List.filter 
	      (fun node -> 
		 i := !i + 1;
		 let ctx = { node = node;
			     size = List.length nodes;
			     position = !i; }
		 in
		   List.for_all (fun predicate -> boolean_of_object (predicate gctx ctx)) predicates) 
	      rest));;

let xpath_fun_compose fn nextfn =
  (fun gctx ctx -> 
     match fn gctx ctx with
	 Nodes n -> 
	   let i = ref 0 in
	     Nodes
	       (List.flatten
		  (List.map 
		     (fun node -> 
			i := !i + 1;
			let ctx = { 
			  node = node;
			  size = List.length n;
			  position = !i; }
			in
			  match nextfn gctx ctx with
			      Nodes n -> n
			    | _ -> [])
		     n))
       | _ -> Nodes []);;


let rec make_xpath_fun steps =
  match steps with 
      hd::next::[] -> 
	let fn = make_xpath_step_fun hd and nextfn = make_xpath_step_fun next in
	  xpath_fun_compose fn nextfn
    | hd::next::tl ->
	let fn = make_xpath_fun [hd;next] and nextfn = make_xpath_fun tl in
	  xpath_fun_compose fn nextfn
    | hd::[] ->
	make_xpath_step_fun hd
    | _ -> 
	fun gctx ctx -> Nodes [];;


let rec compile_xpath_step (axis, test, preds) =
  let test_fun = 
    match test with
	`Nodep -> (fun gctx ctx -> nodep)
      | `Anyp -> (fun gctx ctx -> starp axis)
      | `NameTest name -> 
	  (fun gctx ctx ->
	     let qname = get_context_assoc gctx name in
	       name_test axis qname)
      | `Textp -> (fun gctx ctx -> textp)
      | `Rootp -> (fun gctx ctx -> rootp)
      | `Commentp -> (fun gctx ctx -> commentp)
      | `ProcessingInstructionp target -> 
	  (match target with
	       None -> (fun gctx ctx -> processing_instructionp)
	     | Some target -> (fun gctx ctx -> 
				 (fun node -> 
				    match node with 
					ProcessingInstruction (p, t, content) -> t = target
				      | _ -> false)))
  in
    (axis, test_fun, List.map compile_xpath_expr preds)
    
and get_context_assoc ctx name =
  match name with
      "", name -> (default_ns, name)
    | pref, name -> 
	try
	  let namespace = List.assoc pref ctx.namespaces in
	    ((pref, namespace), name)
	with Not_found -> 
	  raise (XPathError 
		   (Printf.sprintf "Unbound namespace prefix %s on element %s, declared namespaces: %s"
		      pref name (string_of_list string_of_ns_mapping " " ctx.namespaces)))

and compile_xpath_expr : Yaxi_xpath.expression -> xpath_expr = 
  function
      `Primary p -> (fun gctx ctx -> p)
    | `Variable name -> 
	(fun gctx ctx -> 
	   try
	     let qname = get_context_assoc gctx name in
	       Yaxi_xml.ns_assoc qname gctx.variables 
	   with Not_found -> Literal "")
    | `Funcall (name, args) ->
	let newargs = List.map compile_xpath_expr args in
	  (fun gctx ctx -> 
             try
	       let qname = get_context_assoc gctx name in
	       let fn = (Yaxi_xml.ns_assoc qname gctx.library) in
	         fn (List.map (fun x -> x gctx ctx) newargs) gctx ctx
             with e ->
               raise (XPathError (sprintf "Reference to undefined function %s:%s (%s)"
                                    (fst name) (snd name) (Printexc.to_string e)))
          )
    | `Operation (op, x, y) ->
	(try
	   let op = List.assoc op operations 
	   and newx = (compile_xpath_expr x) and newy = (compile_xpath_expr y)
	   in
	     (fun gctx ctx -> op (newx gctx ctx) (newy gctx ctx))
	 with Not_found ->
	   raise (XPathError (sprintf "Unimplemented operator %s" (string_of_operator op))))
    | `UnaryOperation (op, x) -> 
	(try
	   let op = List.assoc op unary_operations 
	   and newx = (compile_xpath_expr x)
	   in
	     (fun gctx ctx -> op (newx gctx ctx))
	 with Not_found -> raise (XPathError (sprintf "Unimplemented operator %s" (string_of_operator op))))
    | `Steps (pos, list) ->
	let fn = make_xpath_fun (List.map compile_xpath_step list) in
	  (match pos with
	       `Absolute -> 
		 (fun gctx ctx -> 
		    let root = Yaxi_xml.find_root ctx.node in
		    let c = { node = root; position = 1; size = 1 } in
		      fn gctx c)
	     | `Relative -> fn)
    | `Filter (expr, pred) -> 
	let fn = compile_xpath_expr expr in
	let predfn = compile_xpath_expr pred in
	  xpath_fun_compose fn predfn
;;

let compile_expression = 
  compile_xpath_expr

open Lexing

let parse str =
  let normal_lexbuf l = 
    { (Lexing.from_string "some") 
      with refill_buff = 
	(fun buf ->
	   (*printf "Refill buff called\n";*)
	   raise (Yaxi_xpathlexer.Lexbuf l)) }
  in
  let old = Yaxi_xpathparser.expr Yaxi_xpathlexer.normal_token (normal_lexbuf (Ulexing.from_utf8_string str))
  (*and newp = Yaxi_newxpathparser.expr Yaxi_xpathlexer.token (Ulexing.from_utf8_string str)
  in 
    if old <> newp then
      printf "Different results";*)
    in old
    
let compile str =
  let parsed =
    (*Printf.printf "Parsing: %s\n" str;*)
    parse str
  in
    compile_xpath_expr parsed;;

let default_context () = 
  {
    variables = [];
    namespaces = [default_ns];
    library = functions
  };;

let new_global_context vars nss lib =
  { variables = vars; namespaces = default_ns::nss; library = lib @ functions }

let xpath_eval ?(gctx = default_context ()) str node =
  let expr = compile str in
    expr gctx (new_context node 1 1)
