(* Yaxi - Yet Another X* Implementation
 * Copyright (C) 2003 Matthieu Sozeau
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * $Id: yaxi_xmllexer.ml,v 1.5 2003/09/28 22:42:33 mat Exp $
 * 
 * The xml lexer
 *)

open Printf

let regexp space = ' ' | '\n' | '\t' | '\r'

let regexp s = (space+)

let regexp digit = xml_digit

let regexp digits = (digit+)
		      
let regexp hexa = xml_digit | 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'a' | 'b' | 'c' | 'd' | 'e' | 'f'
    
let regexp hexas = (hexa+)
		     
let regexp letter = xml_letter

let regexp any = letter | digit | xml_extender | xml_base_char | xml_ideographic | xml_combining_char | xml_blank
    
let regexp name_char = letter | digit | '.' | '-' | '_' | ':' | xml_combining_char | xml_extender
let regexp name = (letter | '_' | ':') (name_char*)

let regexp nmtoken = (name_char+)
		    
let regexp entityref = '&' name ';'
			 
let regexp pereference = '%' name ';'

let regexp charref = "&#x" hexas ';' | "&#" digits ';'

let regexp reference = entityref | charref

let regexp pubidchar_noquote = ' ' | '\r' | '\n' | ['a'-'z'] | ['A'-'Z'] | ['0'-'9'] | ["-()+,./:=?;!*#@$_%"]
    
let regexp pubidchar = pubidchar_noquote | "'"
    
let regexp cdata = ([^ '<']*)


exception ParseError of (int * int) * string * string
  
let parse_error lexbuf str = 
  raise (ParseError (Ulexing.loc lexbuf, Ulexing.utf8_lexeme lexbuf, str))

let parse_name = Yaxi_xml.parse_name

let parse_hexa lexbuf str = 
  let hexa_digit = function
    | '0'..'9' as c -> (Char.code c) - (Char.code '0')
    | 'a'..'f' as c -> (Char.code c) - (Char.code 'a') + 10
    | 'A'..'F' as c -> (Char.code c) - (Char.code 'A') + 10
    | _ -> parse_error lexbuf (sprintf "Invalid hexadecimal digit %s" str)
  in
  let len = String.length str in
  let rec aux i accu =
    if i = (len - 1) then accu + hexa_digit str.[i]
    else aux (succ i) (accu * 16 + hexa_digit str.[i])
  in
    aux 0 0
	
let parse_decimal lexbuf str =
  try int_of_string str 
  with _ -> parse_error lexbuf (sprintf "Invalid decimal number %s" str)
	     
type production = 
    Comment of string
  | Pi of string * string
  | OpenElement of Yaxi_xml.qname * Yaxi_xml.ns_mappings * Yaxi_xml.attributes
  | EmptyElement of Yaxi_xml.qname * Yaxi_xml.ns_mappings * Yaxi_xml.attributes
  | CloseElement of Yaxi_xml.qname
  | Whitespace of string
  | Cdata of string
  | Text of string
  | ExternalParsedEntityRef of string * string * string
  | Doctype of Yaxi_xml.dtd
  | DocumentEnd

let nsprefix_assoc prefix list =
  List.find (fun (pref, ns) -> pref = prefix) list;;

let nsprefix_mem_assoc prefix list =
  List.exists (fun (pref, ns) -> pref = prefix) list;;

let enclosed lexbuf offset_begin offset_end = 
  let len = Ulexing.lexeme_length lexbuf in
    Ulexing.utf8_sub_lexeme lexbuf offset_begin (len - (offset_end + offset_begin))
      
(** FIXME *)
let add_ns_mappings nss newnss =
  newnss @ nss

(** XML Lexer *)
let rec token ?(keep_refs = true) nss dtd = lexer
  | eof -> DocumentEnd
  | s -> Whitespace (Ulexing.utf8_lexeme lexbuf)
  | [^'<'] ->
      let txt = Ulexing.utf8_lexeme lexbuf in
      let txt = txt ^ text lexbuf in	
	Text (txt)
  | "&#" -> 
      if keep_refs then
	Text ("&#" ^ (char_ref true lexbuf))
      else
	Text (char_ref keep_refs lexbuf)
  | "&" name ";" -> 
      let name = enclosed lexbuf 1 1 in
	(try
	   (match Yaxi_xml.get_entity_value dtd name with
		`Value str -> Text (str)
	      | `ExternalID (pub, sys) -> ExternalParsedEntityRef (name, pub, sys)
	      | `UnparsedExternalID (pub, sys, ndata) -> 
		  parse_error lexbuf
 		  (sprintf "Reference to unparsed external entity %s in content" name))
	 with Not_found ->
	   parse_error lexbuf (sprintf "Undefined entity reference: %s" name))	
  | "<!--" -> Comment (comment lexbuf)  
  | "<![CDATA[" -> Cdata (cdata lexbuf)
  | "<!DOCTYPE" s -> 
      let name = aname lexbuf in
      let b = skip_spaces_end lexbuf in
	if b then
	  Doctype (Yaxi_xml.new_dtd name ())
	else
	  (match external_id lexbuf with
	       None -> 
		 let decls = Yaxi_xml.new_dtd_declarations () in
		 let dtd = Yaxi_xml.new_dtd name ~internal_subset:decls ()
		 in		   
		   Doctype (dtd_decls dtd decls lexbuf)
	     | Some (pub, sys) ->
		 let decls = Yaxi_xml.new_dtd_declarations () in
		 let dtd = Yaxi_xml.new_dtd name ~public_id:pub ~system_id:sys
			     ~internal_subset:decls () 
		 in
		   Doctype (dtd_decls dtd decls lexbuf))
  | "<?" -> 
      let target = aname lexbuf in
	Pi (target, pi lexbuf)
  | "</" name -> 
      let n = enclosed lexbuf 2 0 in
	assert (skip_spaces_end lexbuf);
	CloseElement (parse_name nss n)
  | "<" -> 
      let name = aname lexbuf in
      let empty, namespaces, attributes = attributes [] [] dtd lexbuf in
      let nss = add_ns_mappings nss namespaces in
      let newname = parse_name nss name 		     
      and newattributes = List.map (fun (n,v) -> (parse_name nss n, v)) attributes in
	if empty then
	  EmptyElement (newname, namespaces, newattributes)
	else 
	  OpenElement (newname, namespaces, newattributes)
  | _ -> failwith (Printf.sprintf "Unknow token: %s" (Ulexing.utf8_lexeme lexbuf))      

and parse_char = lexer
| _ -> Ulexing.utf8_lexeme lexbuf

and text = lexer
| [^"<&"]* -> Ulexing.utf8_lexeme lexbuf
| any -> parse_error lexbuf "Expecting text"
    
and comment = lexer
| [^'-']* -> let txt = Ulexing.utf8_lexeme lexbuf in
    txt ^ (comment' lexbuf)
| any -> parse_error lexbuf "Expecting comment"

and char_ref keep = lexer
| "x" hexas ';' ->
    if keep then
      Ulexing.utf8_lexeme lexbuf
    else
      let n = enclosed lexbuf 1 1 in
      let num = parse_hexa lexbuf n in
	parse_char (Ulexing.from_stream (Stream.of_list [num]))
| digits ';' ->
    if keep then
      Ulexing.utf8_lexeme lexbuf
    else
      let n = enclosed lexbuf 0 1 in
      let num = parse_decimal lexbuf n in      
	parse_char (Ulexing.from_stream (Stream.of_list [num]))
	
and comment' = lexer
| "-->" -> ""
| "--" -> parse_error lexbuf "'--' not allowed inside comments"
| "-" -> "-" ^ (comment lexbuf)
| any -> parse_error lexbuf "Expecting comment end"

and cdata = lexer
| [^']']* -> let txt = Ulexing.utf8_lexeme lexbuf in
    txt ^ (cdata' lexbuf)
| any -> parse_error lexbuf "Expecting cdata"

and cdata' = lexer
| "]]>" -> ""
| "]" -> "]" ^ (cdata lexbuf)
| any -> parse_error lexbuf "Expecting cdata end"    

and skip_spaces_end = lexer
| s? ">" -> true
| s -> false
| _ -> parse_error lexbuf "Expected whitespace"

and attributes nss attrs dtd = lexer
| s? "/>" -> (true, nss, attrs)
| s? ">" -> (false, nss, attrs)
| s -> (** Stops at end of spaces *)
    let ns, attr, value = attribute dtd lexbuf in
      if ns then 
	attributes ((attr, `URI value) :: nss) attrs dtd lexbuf
      else
	attributes nss ((attr, value) :: attrs) dtd lexbuf
| _ -> parse_error lexbuf "Expecting space"

and attribute dtd = lexer
| "xmlns:" name '=' ->
    let pref = enclosed lexbuf 6 1 in
      (true, pref, attr_value dtd lexbuf)
| "xmlns=" -> (true, "", attr_value dtd lexbuf)
| name '=' ->
    let n = enclosed lexbuf 0 1 in
      (false, n, attr_value dtd lexbuf)
| _ -> parse_error lexbuf "Expecting attribute"
    
and attr_value dtd = lexer
| "'" -> attr_value_apos dtd "" lexbuf
| "\"" -> attr_value_doublequote dtd "" lexbuf
| _ -> parse_error lexbuf "Expecting attribute value"

and attr_value_apos dtd accu = lexer
| ([^"<&'"])* -> let txt = accu ^ (Ulexing.utf8_lexeme lexbuf) in
    attr_value_apos dtd txt lexbuf
| "&#" -> let char = (char_ref false lexbuf) in
    attr_value_apos dtd (accu ^ char) lexbuf
| "&" name ";" -> 
    let name = enclosed lexbuf 1 1 in
    let text = 
      (try
	 (match Yaxi_xml.get_entity_value dtd name with
	      `Value str -> str
	    | _ ->
		parse_error lexbuf
		(sprintf "Reference to external entity %s in attribute value forbidden"
		   name))
       with Not_found ->
	 parse_error lexbuf 
	 (sprintf "Undefined entity reference in attribute value: %s" name))
    in
      attr_value_apos dtd (accu ^ text) lexbuf
| "'" -> accu
| any -> parse_error lexbuf "Expecting attribute value"

and attr_value_doublequote dtd accu = lexer
| ([^"<&\""])* -> let txt = accu ^ (Ulexing.utf8_lexeme lexbuf) in
    attr_value_doublequote dtd txt lexbuf
| "&#" -> let char = (char_ref false lexbuf) in
    attr_value_doublequote dtd (accu ^ char) lexbuf
| "&" name ";" -> 
    let name = enclosed lexbuf 1 1 in
    let text = 
      (try
	 (match Yaxi_xml.get_entity_value dtd name with
	      `Value str -> str
	    | _ ->
		parse_error lexbuf
		(sprintf "Reference to external entity %s in attribute value forbidden"
		   name))
       with Not_found ->
	 parse_error lexbuf 
	 (sprintf "Undefined entity reference in attribute value: %s" name))
    in
      attr_value_doublequote dtd (accu ^ text) lexbuf
| '"' -> accu
| any -> parse_error lexbuf "Expecting attribute value"

and aname = lexer
| name -> Ulexing.utf8_lexeme lexbuf
| _ -> parse_error lexbuf "Expecting name"
    
and pi = lexer
| (([^'?''>'])*) "?>" -> enclosed lexbuf 0 2
| _ -> parse_error lexbuf "Expecting pi data"

and external_id = lexer
| "SYSTEM" s -> 
    let sysid = system_id_literal lexbuf in
      Some ("", sysid)
| "PUBLIC" s -> 
    let pub = public_id_literal lexbuf in
    let b = skip_spaces_end lexbuf in
      if b then
	parse_error lexbuf "Expecting system id literal" 
      else
	let sys = system_id_literal lexbuf in
	  Some (pub, sys)
| s? -> None

and get_param_entity lexbuf name dtd =
    try
      Yaxi_xml.get_parameter_entity_value dtd name
    with Not_found ->
      parse_error lexbuf 
      (sprintf "Undefined parameter entity reference in entity value: %s" name)

and entity_value dtd = lexer
| "'" -> entity_value_apos dtd lexbuf
| "\"" -> entity_value_doublequote dtd lexbuf
| _ -> parse_error lexbuf "Expecting entity value"

and entity_value_apos dtd = lexer
| ([^"%&'"])+ -> let txt = Ulexing.utf8_lexeme lexbuf in
    `Text txt :: (entity_value_apos dtd lexbuf)
| "&#" -> let char = (char_ref false lexbuf) in
    `Text char :: (entity_value_apos dtd lexbuf)
| "&" name ";" -> 
    let name = enclosed lexbuf 1 1 in
      `EntityRef name :: (entity_value_apos dtd lexbuf)
| "%" name ";" ->  
    let name = enclosed lexbuf 1 1 in
    let txt = get_param_entity lexbuf name dtd in
      (match txt with
	   `Value v ->`Text v :: (entity_value_apos dtd lexbuf)
	 | _ -> 
	     parse_error lexbuf
	     (sprintf "Cannot concatenate external id parameter entity %s and general entity"
		name))
| "'" -> []
| any -> parse_error lexbuf "Expecting entity value"

and entity_value_doublequote dtd = lexer
| ([^"%&\""])+ -> let txt = Ulexing.utf8_lexeme lexbuf in
    `Text txt :: (entity_value_doublequote dtd lexbuf)
| "&#" -> let char = (char_ref false lexbuf) in
    `Text char :: (entity_value_doublequote dtd lexbuf)
| "&" name ";" -> 
    let name = enclosed lexbuf 1 1 in
      `EntityRef name :: (entity_value_doublequote dtd lexbuf)
| "%" name ";" ->   
    let name = enclosed lexbuf 1 1 in
    let txt = get_param_entity lexbuf name dtd in
      (match txt with
	   `Value v -> `Text v :: (entity_value_doublequote dtd lexbuf)
	 | _ -> 
	     parse_error lexbuf
	     (sprintf "Cannot concatenate external id parameter entity %s and general entity"
		name))
| "\"" -> []
| any -> parse_error lexbuf "Expecting entity value"

and system_id_literal = lexer
| "'" ([^"'"])* "'" -> enclosed lexbuf 1 1
| "\"" ([^"\""])* "\"" -> enclosed lexbuf 1 1
| any any any any any any -> parse_error lexbuf "Expecting system id literal"
    
and public_id_literal = lexer
| ('"' pubidchar* '"') | ("'" pubidchar_noquote* "'") ->
    enclosed lexbuf 1 1
| any any any any any -> parse_error lexbuf "Expecting public id literal"

and dtd_decls dtd subset = lexer
| s? "[" -> parse_decls dtd subset lexbuf
| s? ">" -> dtd
| any -> parse_error lexbuf "Expecting dtd declarations"

and parse_decls dtd subset = lexer
| s? "]" s? '>' -> dtd
| s? -> let decl = parse_decl dtd subset lexbuf in
    parse_decls dtd subset lexbuf
      
and parse_decl dtd subset = lexer
| "%" name ";" -> let eref = enclosed lexbuf 1 1 in
    (try
       let txt = Yaxi_xml.get_parameter_entity_value dtd eref in
	 (match txt with
	      `Value v -> parse_decl dtd subset (Ulexing.from_utf8_string v)
	    | _ -> failwith "Unimplemented external inclusion")
     with Not_found ->
       parse_error lexbuf (sprintf "Undefined parameter entity reference %s" eref))
| "<!ELEMENT" s ->
    let name = aname lexbuf in	  
      if skip_spaces_end lexbuf then
	parse_error lexbuf "Expected content specification"
      else
	let content = content_spec lexbuf in
	  subset.Yaxi_xml.dtd_elements <- (name, content) :: subset.Yaxi_xml.dtd_elements
| "<!ATTLIST" s ->
    let name = aname lexbuf in
    let rec parse_attrs lexbuf = 
      if skip_spaces_end lexbuf then 
	failwith (sprintf "Attributes expected in <!ATTLIST %s" name)
      else
	let name = aname lexbuf in
	let s = skip_spaces_end lexbuf in
	let type_ = attr_type lexbuf in
	let s = skip_spaces_end lexbuf in
	let default = attr_default dtd lexbuf
	in (name, type_, default) :: parse_attrs lexbuf
    in
      subset.Yaxi_xml.dtd_attributes <- (name, parse_attrs lexbuf) :: subset.Yaxi_xml.dtd_attributes
| "<!ENTITY" s -> entity dtd subset lexbuf
| "<!NOTATION" s ->
    let name = aname lexbuf in
    let s = skip_spaces_end lexbuf in	  
      assert (not (skip_spaces_end lexbuf));
      subset.Yaxi_xml.dtd_notations <- (name, notation_id lexbuf) :: subset.Yaxi_xml.dtd_notations
| "<?" -> 
    let name = aname lexbuf in
    let s = skip_spaces_end lexbuf in
      subset.Yaxi_xml.dtd_pis <- (name, pi lexbuf) :: subset.Yaxi_xml.dtd_pis
| "<!--" (([^'-'][^'-'][^'>'])*) "-->" ->
    subset.Yaxi_xml.dtd_comments <- (enclosed lexbuf 4 3) :: subset.Yaxi_xml.dtd_comments
| any -> parse_error lexbuf "Expecting dtd declaration"

(** Element declarations *)
and content_spec = lexer
| "EMPTY" s? '>' -> `Empty
| "ANY" s? '>' -> `Any
| '(' s? "#PCDATA" s? ')' s? '>' -> `PCDATA
| '(' s? "#PCDATA" -> `Mixed (mixed lexbuf)
| '(' s? -> `Children (children [] lexbuf)

and mixed = lexer
| name -> let n = Ulexing.utf8_lexeme lexbuf in
    n :: (mixed lexbuf)
| s? '|' s? -> mixed lexbuf
| s? ")*" s? ">" -> []

and children n = lexer
| name -> 
    let n = Ulexing.utf8_lexeme lexbuf in
    let q = quantifier lexbuf in
      children [(q, `Element n)] lexbuf
| s? '|' s? -> `Choice (n @ (children_or lexbuf))
| s? ',' s? -> `Seq (n @ (children_and lexbuf))
| s? ')' -> 
    (match n with
	 [] -> parse_error lexbuf "Expected at least one children in content specification"
       | list -> `Seq list)
    
and children_or = lexer
| name -> 
    let n = Ulexing.utf8_lexeme lexbuf in
    let q = quantifier lexbuf in
      (q, `Element n) :: (children_and lexbuf)
| s? '(' s? -> 
    let ch = children [] lexbuf in
    let q = quantifier lexbuf in
      (q, ch) :: children_or lexbuf
| s? '|' s? -> children_or lexbuf
| s? ')' -> []
| s? '>' -> []
    
and children_and = lexer
| name -> 
    let name = Ulexing.utf8_lexeme lexbuf in
    let q = quantifier lexbuf in
      (q, `Element name) :: (children_and lexbuf)
| s? '(' s? -> 
    let ch = children [] lexbuf in
    let q = quantifier lexbuf in
      (q, ch) :: children_and lexbuf
| s? ',' s? -> children_and lexbuf
| s? ')' -> []
| s? '>' -> []
    
and quantifier = lexer
| "?" -> `Zero
| "+" -> `NonZero
| "*" -> `Any
| "" -> `One

(** Attribute lists *)
and attr_type = lexer
| "CDATA" -> `CDATA
| "ID" -> `ID
| "IDREF" -> `IDREF
| "IDREFS" -> `IDREFS 
| "NMTOKEN" -> `NMTOKEN
| "NMTOKENS" -> `NMTOKENS
| "NOTATION" s '(' s? -> `Notation (notation lexbuf)
| "(" s? -> `Enumeration (enumeration lexbuf)

and notation = lexer
| name -> let el = Ulexing.utf8_lexeme lexbuf in
    el :: notation lexbuf
| s? '|' s? -> notation lexbuf
| s? ')' s? -> []

and enumeration = lexer
| nmtoken -> 
    let el = Ulexing.utf8_lexeme lexbuf in
      el :: enumeration lexbuf
| s? '|' s? -> enumeration lexbuf
| s? ')' s? -> []
    
and attr_default dtd = lexer
| "#REQUIRED" -> `Required
| "#IMPLIED" -> `Implied
| ("#FIXED" s)? -> `Fixed (attr_value dtd lexbuf)

(** Entities *)
and entity dtd subset = lexer
| '%' s -> 
    let name = aname lexbuf in
    let s = skip_spaces_end lexbuf in
    let ent = 
      match external_id lexbuf with
	  None -> (name, `Value (Yaxi_xml.parse_entity_value_template dtd (entity_value dtd lexbuf)))
	| Some (pub,sys) -> (name, `ExternalID (pub, sys))
    in
      assert (skip_spaces_end lexbuf);
      subset.Yaxi_xml.dtd_parameter_entities <- ent :: subset.Yaxi_xml.dtd_parameter_entities
	
| name ->
    let name = Ulexing.utf8_lexeme lexbuf in
    let s = skip_spaces_end lexbuf in
    let ent =
      (match external_id lexbuf with
	   None -> (name, `Value (entity_value dtd lexbuf))
	 | Some (pub,sys) -> 
	     (match ndata lexbuf with
		  Some ndataname -> (name, `UnparsedExternalID (pub, sys, ndataname))
		| None -> (name, `ExternalID (pub, sys))))	  
    in
      assert (skip_spaces_end lexbuf);
      subset.Yaxi_xml.dtd_entities <- ent :: subset.Yaxi_xml.dtd_entities

and ndata = lexer
| s "NDATA" s -> 
    let name = aname lexbuf in 
      Some name
| s? -> None	
    
(** Notations *)
and notation_id = lexer
| "SYSTEM" s -> 
    let sysid = system_id_literal lexbuf in
      assert (skip_spaces_end lexbuf);
      ("", sysid)
| "PUBLIC" s -> 
    let pub = public_id_literal lexbuf in
    let b = skip_spaces_end lexbuf in
      if b then
	(pub, "")
      else
	let sys = system_id_literal lexbuf in
	  (pub, sys)
    
let parse_options string =
  let _, _, attrs = 
    attributes [] [] (Yaxi_xml.new_dtd "" ()) (Ulexing.from_utf8_string (" " ^ string ^ ">"))
  in attrs
