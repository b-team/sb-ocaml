(* Yaxi - Yet Another X* Implementation
 * Copyright (C) 2003 Matthieu Sozeau
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * $Id: yaxsltproc.ml,v 1.10 2003/11/02 17:10:16 mat Exp $
 * 
 *)

open Printf

type param = Lit of string | Expr of string

let usage = "yaxsltproc: an xsltproc clone\nOptions:"
let stylesheet = ref ""
let document = ref ""
let output = ref ""
let verbose = ref false
let repeat = ref 1
let noout = ref false
let omit_decl = ref false
let pretty = ref false
let params = ref []
let namespaces = ref []

let spec = ref []

let l = 
  List.map (fun (a, b, c) -> (a, b, "\t" ^ c)) [
    ("--version", Arg.Unit (fun _ -> print_endline Yaxi.version; exit 0), "display program version");
    ("--stylesheet", Arg.String ((:=) stylesheet), "stylesheet filename");
    ("--document",  Arg.String ((:=) document), "document filename");
    ("--output", Arg.String ((:=) output), "output file");
    ("--repeat", Arg.Int ((:=) repeat), "repeat n times");
    ("--noout", Arg.Set noout, "do not dump the result");
    ("--omit-decl", Arg.Set omit_decl, "Omit the ?xml? declaration in the output document");
    ("--pretty", Arg.Set pretty, "output a pretty representation of the result");
    ("--verbose", Arg.Set verbose, "be verbose");
    ("--namespace", Arg.String
       (fun str ->
	  let idx = String.index str '=' in
	  let name, value = Yaxi_utils.split_string str idx in
	    namespaces := (name, `URI value) :: !namespaces),
       "bind a prefix to a namespace in the environment (e.g: i=\"http://my_namespace_uri\")");
    ("--param", Arg.String
       (fun str ->
	  Arg.current := succ (!Arg.current);
	  if Array.length Sys.argv < (succ !Arg.current) then
	    raise (Arg.Bad (sprintf "missing value argument for parameter %s" str))
	  else (
	    let value = Sys.argv.(succ (!Arg.current)) in
	      params := (str, Expr value)::!params)),
       "pass a (parameter,value) pair, with value being an XPath expression");
    ("--litparam", Arg.String
       (fun str ->
	  Arg.current := succ (!Arg.current);
	  if Array.length Sys.argv < !Arg.current then
	    raise (Arg.Bad (sprintf "missing value argument for parameter %s" str))
	  else (
	    let value = Sys.argv.(succ (!Arg.current)) in
	      params := (str, Lit value)::!params)),
       "pass a (parameter name, literal) pair");    
    ("--help", Arg.Unit (fun () -> Arg.usage !spec usage; exit 0),
     "display this list of options");
    ("-help", Arg.Unit (fun () -> Arg.usage !spec usage; exit 0),
     "display this list of options")
  ]
    
    
let _ = 
  spec := l;
  Arg.parse !spec   
    (fun str ->
       if !stylesheet = "" then stylesheet := str
       else if !document = "" then document := str
       else eprintf "Ignoring argument %s" str) usage;
  try 
    if !verbose then
      printf "namespaces: %s\n" (Yaxi_utils.string_of_list Yaxi_xml.string_of_ns_mapping " " !namespaces);
    let parameters = 
      List.map 
	(fun (name, value) ->
	   let qname = Yaxi_xml.parse_name !namespaces name in
	   let expr =
	     match value with
		 Lit str -> (fun _ _ -> Yaxi_xpath.Literal str)
	       | Expr str -> Yaxi_xpathxml.compile str
	   in (qname, expr))
	!params
    in
      if !stylesheet = "" then (
	eprintf "Error: you must specify a stylesheet filename\n";
	Arg.usage !spec usage;
	exit 2);
      if !document = "" then (
	eprintf "Error: you must specify a document filename\n";
	Arg.usage !spec usage;
	exit 2);
      let st = Yaxi_xslt.parse_stylesheet_file (!stylesheet) in
	if !verbose then printf "Stylesheet parsed\n";
	let doc = Yaxi_xmlparser.parse_file (!document) in	
	  if !verbose then printf "Document parsed\n";
	  for i = 1 to !repeat do
	    let res = Yaxi_xslt.apply_stylesheet ~ns_mappings:!namespaces ~parameters:parameters
                        ~stylesheet:st ~document:doc ()  
	    in
	      if not (!noout) then (
		Yaxi_xml.serialize_doc (output_string stdout) ~indent:!pretty
				      ~omit_decl:!omit_decl res;
		printf "\n");
	      if !output <> "" then
		let out = open_out !output in
		  Yaxi_xml.serialize_doc (output_string out) ~indent:!pretty
		    ~omit_decl:!omit_decl res;
	  done;
	  exit 0
  with 
      Sys_error s -> eprintf "System error occured %s\n" s; exit 2
    | Yaxi_xmllexer.ParseError ((begloc, endloc), environ, reason) ->
	eprintf "Parse error at char %i-%i, around '%s' : %s\n" begloc endloc environ reason
    | Yaxi_xml.XMLError str -> eprintf "XML error: %s\n" str; exit 2
    | Yaxi_xml.UndefinedNamespace(i) -> eprintf "Undefined namespace error %s\n" i; exit 2
    | Yaxi_xslt.XSLError str -> eprintf "XSL error: %s\n" str; exit 2
    | e -> eprintf "Unknown exception caught: %s\n" (Printexc.to_string e); 
	exit 2
