(* Yaxi - Yet Another X* Implementation
 * Copyright (C) 2003 Matthieu Sozeau
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 * $Id: yaxi_xsltxpathext.ml,v 1.6 2004/02/18 22:44:35 mat Exp $
 * 
 * xpath functions added by the xslt rec.
 *)

open Yaxi_xml
open Yaxi_xpath
open Yaxi_xpathxml

let get_doc base_uri uri =
  try
    let scheme = try Neturl.extract_url_scheme uri with Neturl.Malformed_URL -> "file" in
      (match scheme with
	   "file" ->
	     let syntax = { (Hashtbl.find Neturl.common_url_syntax "file") 
			    with Neturl.url_enable_scheme = Neturl.Url_part_allowed }
	     in 
	     let url = Neturl.url_of_string syntax uri
	     in 
	     let doc =
	       match Neturl.url_path url with
		   [] -> Yaxi_xmlparser.parse_file base_uri
		 | (""::tl) as path -> Yaxi_xmlparser.parse_file (Neturl.join_path path)
		 | _ as path -> 
		     let baseurl = Neturl.url_of_string syntax base_uri in
		     let url = Neturl.apply_relative_url baseurl url in
		       Yaxi_xmlparser.parse_file (Neturl.join_path (Neturl.url_path url))
	     in
	       (match document_element doc with
		    Some n -> Nodes [n]
		  | None -> Nodes [])
	 | _ -> Nodes [])
  with Neturl.Malformed_URL -> Nodes []

(* document(object, node-set?) *)    
let rec document xslt doc args gctx ectx : Yaxi_xpath.primary_type =
  (*Printf.printf "In document: args = %s!!!\n"
    (Utils.string_of_list string_of_object " " args);*)
  let base_uri = doc.base_uri in
    match args with
       (Nodes n) :: [] -> 
	 List.fold_left
	 (fun nodes node ->
	    let doc = document xslt doc [Literal (string_value_of_node node); Nodes [node]] gctx ectx
	    in xpath_union nodes doc)
	 (Nodes []) n
      | (Nodes n) :: ((Nodes _) as n') :: [] ->
	  List.fold_left
	  (fun nodes node ->
	     let doc = document xslt doc [Literal (string_value_of_node node); n']  gctx ectx
	     in xpath_union nodes doc)
	  (Nodes []) n
      | obj :: nodeset :: [] ->
	  let uri = string_of_object obj in
	    get_doc base_uri uri
      | obj :: [] ->
	  let uri = string_of_object obj in
	    get_doc base_uri uri
      | _ -> raise (ArityError ("document", 1, 2))
            
let key xsl doc args gctx ctx : Yaxi_xpath.primary_type =
  match args with
    | name :: obj :: [] -> 
        let name = Yaxi_xml.parse_name gctx.namespaces (string_of_object name) in
        let aux' str =
          let (matches, use) = List.assoc name xsl.Yaxi_xslt.keys in
            match matches gctx ctx with
              | Nodes nodes when nodes <> [] ->
                  let list = 
                    Yaxi_utils.list_filter_option
                      (fun node -> 
                         try 
                           let v = use gctx (new_context node 1 1) in
                             if string_of_object v = str then
                               Some (children node)
                             else None
                         with Not_found -> None)
                      nodes 
                  in List.flatten list                      
              | _ -> []
        in
        let aux = function
          | Nodes nodes -> 
              List.flatten (List.map (fun n -> aux' (string_value_of_node n)) nodes)
          | obj -> aux' (string_of_object obj)
        in
          (try Nodes (aux obj)
           with
             | Not_found -> Nodes []
             | e ->
                 raise (Yaxi_xslt.XSLError (Printf.sprintf "Unexpected exception in key(string, object): %s\n" (Printexc.to_string e))))
    | _ -> raise (ArityError ("key", 2, 2));;

let current xsl doc args gctx ctx : Yaxi_xpath.primary_type =
  match args with
    | [] -> Nodes [ctx.node]
    | _ -> raise (ArityError ("current", 0, 0))

let unparsed_entity_uri xsl doc args gctx ctx : Yaxi_xpath.primary_type =
  match args with
    | (Literal l) :: [] -> 
        Literal
        (try
           (match get_entity_value doc.doctype l with
              | `UnparsedExternalID (pub, sys, nd) -> pub
              | _ -> "")
         with Not_found -> "")
    | _ -> raise (ArityError ("unparsed-entity-uri", 1, 1))
        
let generate_id xsl doc : Yaxi_xpath.primary_type list -> Yaxi_xpath.xpath_expr =
  let hashtbl = Hashtbl.create 41 in
  let get_id h n = "id-" ^ string_of_int h ^ "-" ^ (string_of_int n) in
  let generate node =
    let h = Hashtbl.hash node in
      if Hashtbl.mem hashtbl h then 
        let nodes = Hashtbl.find_all hashtbl h in
          if List.mem_assoc node nodes then
            List.assoc node nodes
          else (
            let id = get_id h (List.length nodes) in
              Hashtbl.add hashtbl h (node,id);
              id
          )
      else (
        let id = get_id h 0 in
          Hashtbl.add hashtbl h (node, id);
          id)
  in 
    function
      | (Nodes (hd::tl)) :: [] -> (fun _ _ -> Literal (generate hd))
      | (Nodes []) :: [] -> (fun _ _ -> Literal "")
      | [] -> (fun _ ctx -> Literal (generate ctx.node))
      | _ -> raise (ArityError ("generate-id", 0, 1))

let system_property xsl doc args gctx ctx : Yaxi_xpath.primary_type =
  match args with
    | (Literal l) :: [] ->
        (try Yaxi_xslt.get_property (Yaxi_xml.parse_name gctx.namespaces l)
         with Not_found -> Literal "")
    | _ -> raise (ArityError ("system-property", 1, 1))

let extension_available name list args gctx  =
  match args with
    | (Literal l) :: [] ->
        Boolean 
        (try 
           let ((pref, ns), name) = Yaxi_xml.parse_name gctx.namespaces l in
             (match ns with
                 `URI ns -> List.mem_assoc name (List.assoc ns list)
                | _ -> false)
         with _ -> false)
    | _ -> raise (ArityError (name, 1, 1))

let element_available xsl doc args gctx ctx : Yaxi_xpath.primary_type =
  extension_available "element-available" xsl.Yaxi_xslt.extension_elements args gctx

let function_available xsl doc args gctx ctx : Yaxi_xpath.primary_type =
  extension_available "function-available" xsl.Yaxi_xslt.extension_functions args gctx

let _ =
  Yaxi_xslt.add_additional_functions
    [("document", document);
     ("key", key);
     ("current", current);
     ("unparsed-entity-uri", unparsed_entity_uri);
     ("generate-id", generate_id);
     ("system-property", system_property);
     ("element-available", element_available);
     ("function-available", function_available)
    ]
    
