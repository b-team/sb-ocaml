<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:i="http://apache.org/i18n/2.0"
  version="1.0">
 
  <xsl:template match="program">
    <html>
      <title><xsl:value-of select="@name"/></title>
      <head>
        <link rel="stylesheet" href="../stylesheets/prog.css"/>
      </head>
      <body>
        <table class="prog">
          <tr class="progtitle">
            <td class="progtitle">
              <xsl:value-of select="@name"/>
            </td>
          </tr>
          <tr>
            <td>
              <xsl:apply-templates/>
              <xsl:if test="@hasCVS='true'">
                <xsl:call-template name="cvs"/>
              </xsl:if>
            </td>
          </tr>
        </table>
      </body>
    </html>    
  </xsl:template>
  
  <xsl:template name="cvs">
    <table class="progsection">
      <tr class="progsectiontitle">
        <td class="progsectiontitle">
          <i:choose>
            <i:when locale="fr">
              CVS
            </i:when>
            <i:otherwise>
              CVS - Getting the up-to-date version
            </i:otherwise>
          </i:choose>
        </td>
      </tr>
      <tr class="progdesc">
        <td class="progdesc">
          <div class="progsection">
            <i:choose>
              <i:when locale="fr">
                J'ai install� un serveur CVS accessible de l'ext�rieur. 
                
                Pour obtenir les sources tapez: 
                <pre>              
$ setenv/[export] CVSROOT[=]":pserver:anonymous@mattam.ath.cx:/cvsroot"
$ cvs login
                </pre>
Le mot de passe est vide, tapez simplement sur entr�e, puis:
                <pre>
$ cvs co <xsl:value-of select="/program/@name"/>
              </pre>
              <a href="/cvsview/{/program/@name}/">CVSView</a> est aussi disponible.
              
            </i:when>
            <i:otherwise>
              I've setup a CVS server on my box, accessible from the internet.
                
              To get the sources type: 
              <pre>              
$ setenv/[export] CVSROOT[=]":pserver:anonymous@mattam.ath.cx:/cvsroot"
$ cvs login
                </pre>
                There's no password, so just type return, then
                <pre>
$ cvs co <xsl:value-of select="/program/@name"/>
                </pre>
                <a href="/cvsview/{/program/@name}/">CVSView</a> is accessible too.
              </i:otherwise>
            </i:choose>
          </div>
        </td>
      </tr>
    </table>
  
  </xsl:template>

  <xsl:template match="overview">
    <div class="progdesc">
      <table class="progsection">
        <tr class="progsectiontitle">
          <td class="progsectiontitle">
            <i:choose>
              <i:when locale="fr">Sujet</i:when>
              <i:otherwise>Overview</i:otherwise>
            </i:choose>
          </td>
        </tr>
        <tr class="progdesc">
          <td class="progdesc">
            <div class="progintro">
              <xsl:apply-templates/>
            </div>
          </td>
        </tr>
      </table>
    </div>
  </xsl:template>
  
  <xsl:template match="news">
    <div class="progdesc">
      <table class="progsection">
        <tr class="progsectiontitle">
          <td class="progsectiontitle">
            <i:choose>
              <i:when locale="fr">Nouvelles</i:when>
              <i:otherwise>News</i:otherwise>
            </i:choose>
          </td>
        </tr>
        <tr class="progdesc">
          <td class="progdesc">
            <div class="prognews">
              <ul>
                <xsl:for-each select="*">
                  <li>
                    <i:date src-pattern="dd/MM/yyyy" pattern="full">
                      <xsl:value-of select="@date"/>
                    </i:date>
                    <xsl:text>:&#xA;</xsl:text>
                    <p>
                      <div class="prognewsinfo">
                        <xsl:apply-templates/>
                      </div>
                    </p>
                  </li>
                </xsl:for-each>
              </ul>
            </div>
          </td>
        </tr>
      </table>
    </div>
  </xsl:template>

  <xsl:template match="files">
    <table class="progsection">
      <tr class="progsectiontitle">
        <td class="progsectiontitle">
          <i:choose>
            <i:when locale="fr">T�l�chargements</i:when>
            <i:otherwise>Download</i:otherwise>
          </i:choose>
        </td>
      </tr>
      <xsl:for-each select="file">
        <tr class="progdownloaditem">
          <xsl:apply-templates select="."/>
        </tr>
      </xsl:for-each>
    </table>
  </xsl:template>
  
  <xsl:template name="lowercase">
    <xsl:param name="str"/>
    <xsl:value-of select="translate($str, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')"/>
  </xsl:template>
  
  <xsl:template match="file">
    <xsl:variable name="directory">
      <xsl:call-template name="lowercase">
        <xsl:with-param name="str"><xsl:value-of select="/program/@name"/></xsl:with-param>
      </xsl:call-template>
    </xsl:variable>
    <xsl:variable name="filename">
      <xsl:call-template name="lowercase">
        <xsl:with-param name="str"><xsl:value-of select="@name"/></xsl:with-param>
      </xsl:call-template>
    </xsl:variable>
    <td class="progdownloaditem">
      <a href="../progs/releases/{$directory}/{$filename}-{@version}.tar.gz">
        <b><xsl:value-of select="@name"/></b>
      </a>
      v<xsl:value-of select="@version"/>
      <xsl:text> (</xsl:text>
      <i:date src-pattern="dd/MM/yyyy">
        <xsl:value-of select="@date"/>
      </i:date>
      <xsl:text>)</xsl:text>
      <xsl:if test="@signed = 'true'">
        <xsl:text>&#xa;</xsl:text>
        <a href="../progs/releases/{$directory}/{$filename}-{@version}.tar.gz.asc">Sig.</a>
        <xsl:text>&#xa;</xsl:text>
      </xsl:if>
      <xsl:if test="*|text()!=''">
        <xsl:text> - </xsl:text>
        <xsl:apply-templates/>
      </xsl:if>
    </td>
  </xsl:template>
    
  <xsl:template match="changelog">
    <i:translate>
      <i:text i:key="prog_changelog">
        See the {0}
      </i:text>
      <i:param>
        <a href="{@url}">
          <i:text>changelog</i:text>
        </a>
      </i:param>
    </i:translate>
  </xsl:template>
  
  <xsl:template match="@*|node()" priority="-1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>


</xsl:stylesheet>
