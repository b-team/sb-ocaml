open Yaxi_xml
open Yaxi_sax
open Yaxi_xmlparser
open Printf
open Utils

let attrs_string = 
  string_of_list (fun (x, y) -> (string_of_name x) ^ "=" ^ y) ", "

class myhandler = object(self : #sax_handler)

  method documentDeclaration options =
    printf "decl: %s\n" (string_of_list (fun (x,y) -> x ^ "=" ^ y) ", " options)
      
  method startNSMapping mapping = ()

  method endNSMapping mapping = ()
    
  method characters cdata =
    printf "chars: %s\n" cdata

  method startElement qname attributes =
    printf "elem: %s %s\n" (string_of_name qname) (attrs_string attributes)
      
  method endElement qname =
    printf "endel: %s\n" (string_of_name qname)

  method comment cdata =
    printf "comm: %s\n" cdata

  method pi name cdata =
    printf "pi: %s=%s" name cdata

end

let _ =
  let doc = ref "" in
  Arg.parse [] (fun s -> doc := s) Sys.argv.(0);
  if !doc = "" then
    prerr_endline (sprintf "Usage: %s filename" Sys.argv.(0))
  else (sax_parse_file (new myhandler) !doc)
