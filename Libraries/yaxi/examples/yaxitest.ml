open Yaxi_xml

let docs = ref []

let args = []

let update_docs anon_string = 
  docs := anon_string :: !docs

let usage = Sys.argv.(0)

let just_parse file = 
  let document = Yaxi_xmlparser.parse_file file in 
    ()

let _ = 
  Arg.parse args update_docs usage;
  List.iter just_parse !docs

