open Printf;;
open Yaxi_xml;;
open Yaxi_xpath;;
open Yaxi_xpathxml;;

let doc = ref ""
let expr = ref "/"

let args = [
  ("--doc", Arg.String ((:=) doc),
  "Document on which to run the xpath expression");
  ("--expr", Arg.String ((:=) expr),
  "Expression to evaluate (defaults to \"/\"")
]

let usage = Sys.argv.(0)

let run () = 
  let document = Yaxi_xmlparser.parse_file !doc in
  try
    (match xpath_eval !expr document.root with
	 Nodes n when n <> [] -> output_string stdout (string_of_nodes n);
       | Nodes [] -> prerr_endline "No nodes found";
       | obj -> output_string stdout (string_of_object obj));
    print_newline ();
    exit 0
  with Not_found -> prerr_endline "Nothing found.";
    exit 1

let _ = 
  Arg.parse args (fun _ -> ()) usage;
  if !doc = "" then (
    Arg.usage args usage;
    exit 2)
  else run ()
