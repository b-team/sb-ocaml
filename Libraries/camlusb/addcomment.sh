#!/bin/bash

#add a string to the start of file
if [ $# != 3 ]; then
 echo usage: addcomment file comment numberline
else
ed $1 <<EOF
$3
i
$2
.
w
q
EOF
fi
