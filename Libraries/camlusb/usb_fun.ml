(*
    This file is part of the camlusb binding to libusb for ocaml.

    Camlusb is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation (version 2 of the License).

    Camlusb is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with camlusb.  See the file LICENSE.  If you haven't received
    a copy of the GNU General Public License, write to:

        Free Software Foundation, Inc.,
        59 Temple Place, Suite 330, Boston, MA
        02111-1307  USA

  beerfun@users.sourceforge.net

*)

(** {b Higher level interface to libusb} *)

open Usb

(** {2 Type Definitions} *)

type write_endpoint = int
type read_endpoint = int

type endpoint = ReadEp of read_endpoint | WriteEp of write_endpoint

(** Record that rapresents a {! Usb.usb_device}:
  - [filename]: as returned by {! Usb.usb_device_filename}
  - [bus]: a function that returns a record of type {! Usb_fun.bus }, corresponding to the opaque {! Usb.usb_bus },
  as returned by {! Usb.usb_device_bus }
  - [descriptor]: as returned by {! Usb.usb_device_descriptor}
  - [ep]: array of the endpoints of the device
  - [usb_device]: the opaque {! Usb.usb_device} which the record corresponds to
*)
type device = { filename : string;
		bus : unit -> bus;
		descriptor : usb_device_descriptor;
		ep : endpoint array;
		usb_device : usb_device Com.opaque }
   

(** Record that rapresents a {! Usb.usb_bus}:
  - [dirname]: as returned by {! Usb.usb_bus_dirname}
  - [devices]: list of values of type {! Usb_fun.device} corresponding to the {! Usb.usb_device} list (a C-style list implemented by a record with "prev" and "next" fields) as returned by {! Usb.usb_bus_devices}
*)
and bus = { dirname : string;
	    devices : device list }

(** {2 Utility functions} *)

(**@return [true] if the endpoint is of type {! Usb_fun.read_endpoint}, [false] if it is of type {! Usb_fun.write_endpoint} *)
let is_readable_ep = function
    ReadEp _ -> true
  | WriteEp _ -> false

(** [bus_of_usb_bus b]
@return a {! Usb_fun.bus} record corresponding to b*)
let bus_of_usb_bus b =
  let rec aux dev =
    if is_null dev
    then []
    else { filename = usb_device_filename dev;
	   bus = get_bus;
	   descriptor = usb_device_descriptor dev;
	   ep = Array.map (fun x -> 
			     let addr = x.bEndpointAddress in
			       if addr land 0x80 <> 0
			       then ReadEp addr
			       else WriteEp addr) ((usb_device_config dev).interf.altsetting.endpoint);
	   usb_device = dev } :: (aux (usb_device_next dev)) 
  and get_bus () = { dirname = usb_bus_dirname b;
		     devices = aux (usb_bus_devices b) } 
  in get_bus ()

(** @return an ocaml list of {! Usb_fun.bus} values corresponding to the C-style list returned by {! Usb.usb_get_busses} *)
let get_busses_list () = 
  let rec aux b =
    if is_null b 
    then []
    else (bus_of_usb_bus b) :: (aux (usb_bus_next b))
  in   
  let i=usb_find_busses () in (*verificare che sia veramente necessario farlo tutte le volte*)
  let j=usb_find_devices () in
    aux (usb_get_busses ())

(** [open_dev dev]
@return (Some {! Usb.usb_dev_handle}) returned by {! Usb.usb_open} [dev] if it is not NULL, or else it returns [None]  *)
let open_dev dev = 
  let (x,d) = usb_open dev.usb_device in
    if is_null x
    then None
    else Some x

