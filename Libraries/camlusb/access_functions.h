/*

    This file is part of the camlusb binding to libusb for ocaml.

    Camlusb is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation (version 2 of the License).

    Camlusb is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with camlusb.  See the file LICENSE.  If you haven't received
    a copy of the GNU General Public License, write to:

        Free Software Foundation, Inc.,
        59 Temple Place, Suite 330, Boston, MA
        02111-1307  USA

  beerfun@users.sourceforge.net

*/

/* usb_bus related access functions */
struct usb_bus * usb_bus_prev (struct usb_bus * bus);
struct usb_bus * usb_bus_next (struct usb_bus * bus);
char * usb_bus_dirname (struct usb_bus * bus);
struct usb_device * usb_bus_devices (struct usb_bus * bus);

/* usb_device related access functions */
struct usb_device * usb_device_prev (struct usb_device * dev);
struct usb_device * usb_device_next (struct usb_device * dev);
char * usb_device_filename (struct usb_device * dev);
struct usb_bus * usb_device_bus (struct usb_device * dev);
struct usb_device_descriptor usb_device_descriptor (struct usb_device * dev);
struct usb_config_descriptor *usb_device_config (struct usb_device * dev);



void * niente();
