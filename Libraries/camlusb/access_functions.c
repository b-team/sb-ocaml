/*

    This file is part of the camlusb binding to libusb for ocaml.

    Camlusb is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation (version 2 of the License).

    Camlusb is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with camlusb.  See the file LICENSE.  If you haven't received
    a copy of the GNU General Public License, write to:

        Free Software Foundation, Inc.,
        59 Temple Place, Suite 330, Boston, MA
        02111-1307  USA

  beerfun@users.sourceforge.net

*/


#include <usb.h>
#include "access_functions.h"

#include <stddef.h>
#include <string.h>
#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/fail.h>
#include <caml/callback.h>
#ifdef Custom_tag
#include <caml/custom.h>
#include <caml/bigarray.h>
#endif
#include <caml/camlidlruntime.h>

#undef interface

value is_null(value _v_v)
{
  void *v; /*in*/
  value _vres;

  v = (void *) Field(_v_v, 0);
  _vres = Val_int(NULL==v);
  return _vres;
}

struct usb_bus * usb_bus_prev (struct usb_bus * bus)
{
  return bus->prev;
}
struct usb_bus * usb_bus_next (struct usb_bus * bus)
{
  return bus->next;
}
char * usb_bus_dirname (struct usb_bus * bus)
{
  return bus->dirname;
}
struct usb_device * usb_bus_devices (struct usb_bus * bus)
{
  return bus->devices;
}

struct usb_device * usb_device_prev (struct usb_device * dev)
{
  return dev->prev;
}
struct usb_device * usb_device_next (struct usb_device * dev)
{
  return dev->next;
}
char * usb_device_filename (struct usb_device * dev)
{
  return dev->filename;
}
struct usb_bus * usb_device_bus (struct usb_device * dev)
{
  return dev->bus;
}

struct usb_device_descriptor usb_device_descriptor (struct usb_device * dev)
{
  return dev->descriptor;
}

struct usb_config_descriptor *usb_device_config (struct usb_device * dev)
{
  return dev->config;
}

