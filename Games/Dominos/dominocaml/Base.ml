(*
 Utilities for Domino games

  TODO
  XML version of log game to file (in progress )

  history
  2.2 - log game to file
  2.1 - sorting and sequencing

  version 2.2
*)
module Base =

struct

  let version = "2.2"

  (* first all low then high *)
  let rec make_dominos d_set d_order high low =
    if high > d_order then
      d_set
    else
      if low > high then
        make_dominos d_set d_order (1 + high) 0
      else
        let new_set = (high, low) :: d_set in
          make_dominos new_set d_order high (1 + low)


  (* build ${\cal D}_N$ *)
  let calD_N n = (make_dominos [] n 0 0)

  (* build a list using function f *)
  let build_list_using g p =
    let rec list_with f n position state = 
      if position > n then
        state
      else
        let new_state = (f position n) :: state in
          list_with f n (position + 1) new_state
    in
      list_with g p 1 []


  (* list to array *)
  let list2array lst =
    (* sorry
       let max_n = (List.length lst) in
       let arr = (Array.make max_n 0) in
       for i = 0 to (max_n -1) do
       arr.(i) <- (List.nth lst i);
       done;
       arr
    *)
    Array.of_list lst

  (* array to list *)
  let array2list arr = 
    (* sorry
       let rec m_list lst arr idx size =
       if idx <= size then
       m_list ( arr.(size - idx) :: lst) arr (1+ idx) size
       else
       lst
       in
       m_list [] arr 0 ((Array.length arr) -1)
    *)
    Array.to_list arr
  


  (* sort a list of interger and return the associated index 
     I know that there is a recursive way to do it but I have this one on my fingers and I'm in a hurry
  *)
  let sort_and_index ns_list = 
    let max_n = (List.length ns_list) in
    let add_this pos m = (m - pos) in
    let idx = (list2array (build_list_using add_this max_n)) in
      for i=0 to (max_n -1) do
        for j=(i+1) to (max_n -1) do
          let this_u = (List.nth ns_list idx.(i)) 
          and this_v = (List.nth ns_list idx.(j)) in
            if this_v < this_u then
              let tmp = idx.(i) in
                idx.(i) <- idx.(j);
                idx.(j) <- tmp;
        done;
      done;
      idx


  (* randomized list of n integers *)
  let rand_list p =
    let rand_this pos m = (Random.int (4 * m)) in
    let list_of_rands = ( build_list_using rand_this p) in
      sort_and_index list_of_rands


  (* random subset of n dominos from d *)
  let rand_subset ds n =
    let rand_order = (Array.sub (rand_list (List.length ds)) 0 n)
    and build_subset state elt =
      (List.nth ds elt) :: state
    in
      Array.fold_left build_subset [] rand_order


  (* split a list at a given position *)
  let split_list lst n =
    let rec eat_untill c_lst state nb nb_max =
      if nb < nb_max then
        match c_lst with
            [] -> [], []
          | head :: tail -> 
              eat_untill tail (state @ [head]) (nb+1) nb_max;
      else
        state, c_lst
    in
      eat_untill lst [] 0 n


  (* function to split a set of dominos according to 
     a given property 
     to be rewritten using List.partition
  *)
  let split_according_to prop lst =
    let separation_according_to state elt =
      match state with
          (prop_ok, prop_no) ->
            if (prop elt) then
              (prop_ok @ [ elt], prop_no)
            else
              (prop_ok , prop_no @ [ elt])
    in
    let z = List.fold_left separation_according_to ([], []) lst in
      match z with
          (yes, no) -> yes, no
  

  (* properties *)
            
  (* is the domino a double one *)
  let is_double d =
    match d with
        (high, low) -> high == low


  (* max value of double in the set *)
  let max_double_value d_set =
    let keep_max_double state v =
      match v with
          (high, low) -> 
            if (high == low) && (high > state) then
              high
            else
              state
    in
      List.fold_left keep_max_double 0 d_set


  (* return the extremes of an ordered sequence *)
  let get_extremes lst =
    let a = List.hd  lst 
    and b = List.nth lst ((List.length lst) -1) 
    in
      match (a,b) with
          ((f,_),(_,l)) -> f, l


  (* build the set of marks *)
  let set_of_marks d_set =
    let build_marks state d =
      match d with 
          (high, low) -> [high; low] @ state
    in
      List.fold_left build_marks [] d_set
  
  
  (* ranodm element of a list *)
  let rand_element lst =
    List.nth lst (Random.int (List.length lst))
      

  exception Impossible;;

  (* check that the set of marks into the dominos set does not contain x1 neither x2 
     raise Impossible if it's not true
  *)
  let check_no_mark d_set x1 x2 =
    let marks = set_of_marks d_set
    and find_x m = (m = x1) || (m = x2)
    in 
      try
        List.find find_x marks;
        raise Impossible
      with Not_found -> 1


  (* set minus 
     raise Impossible
  *)
  let set_minus d_set dom =
    let rec build_minus state ds d =
      match ds with
          [] -> raise Impossible
        | head :: tail ->
            if head = d then
              state @ tail
            else
              build_minus (head :: state) tail d
    in
      build_minus [] d_set dom
  

  (* re ord 
     raise Impossible
  *)
  let re_ord d x1 x2 =
    match d with 
        (high, low) -> 
          if (high == x1) || (low == x2) then
            (low, high), if high == x1 then 1 else 2
          else
            if (high == x2) || (low == x1) then
              (high, low), if low == x1 then 1 else 2
            else
              raise Impossible
  
  
  (* find substring *)
  let has_substring str sub_str =
    let ss_length = String.length sub_str 
    and ss_first  = String.get    sub_str 0 in
    let rec find_from s ss i =
      try
        let idx = String.index_from s i ss_first in
        let cs  = 
          try
            String.sub s idx ss_length 
          with Invalid_argument e -> ""
        in
          if cs = ss then
            begin
              (* Printf.printf "comp (%s) and (%s) %d\n" cs ss i; *)
              true
            end
          else
            if (i+ss_length) > (String.length str) then
              false
            else
              find_from s ss (i+1)
      with 
          Not_found ->
            if (i+ss_length) > (String.length str) then
              false
            else
              find_from s ss (i+1)
    in
      find_from str sub_str 0


  (* find substring between two tags *)
  let str_find_between str b e =
    try 
      let beg_  = 1 + (String.index str b) in
      let end_  = String.index_from str beg_ e in
      let sub_l = end_ - beg_ in
        String.sub str beg_ sub_l
    with Not_found -> ""

  (* a log game is :
        { - player nb; current players names list
        { - L1 L2 L3
        { - ext1, ext2
  info  { - nb_domino player_name
        { - ...
  game  [ - dh dl player_name      <--- L1 (first=0)
  log   [ - ...
  my    { - dh dl                  <--- L2
  hand  { - ...
        [ - dh dl                  <--- L3
  sek   [ - ...

  *)
  let log_game_to_file pname pn plist my_dominos x1 x2 sek log nd smode fname =
    match smode with
        
        (* plain txt/ASCII mode *)
        "txt" ->
          let chano = open_out fname in
            Printf.fprintf chano "%d" pn;
            let print_pnames p_name =
              Printf.fprintf chano ";%s" p_name
            and print_nb_doms item  =
              match item with
                  (n, p_name) -> Printf.fprintf chano "%d %s\n" n p_name
            and print_log_item item =
              match item with
                  (p_name, (high, low)) -> Printf.fprintf chano "%d %d %s\n" high low p_name
            and print_hand_item item =
              match item with
                  (high, low) -> Printf.fprintf chano "%d %d\n" high low
            and l1 =  3 + (List.length nd)  in
            let l2 = l1 + (List.length log) in
            let l3 = l2 + (List.length my_dominos)
            in
              List.iter print_pnames plist;
              Printf.fprintf chano "\n%d %d %d\n%d %d\n" l1 l2 l3 x1 x2;
              List.iter print_nb_doms nd; 
              List.iter print_log_item log;
              List.iter print_hand_item my_dominos;
              List.iter print_hand_item sek;
              
              close_out chano
                
      (* XML mode *)
      | "xml" ->
          let chano = open_out fname in
            (* header ... *)
            Printf.fprintf chano "<INFO>\n<PLAYER NB=\"%d\"/>\n<PLAYERS>\n" pn;
            let print_pnames p_name =
              Printf.fprintf chano "<NAME VALUE=\"%s\"/>\n" p_name
            and print_nb_doms item  =
              match item with
                  (n, p_name) -> Printf.fprintf chano "%d %s\n" n p_name
            and print_log_item item =
              match item with
                  (p_name, (high, low)) -> Printf.fprintf chano "%d %d %s\n" high low p_name
            and print_hand_item item =
              match item with
                  (high, low) -> Printf.fprintf chano "%d %d\n" high low
            and l1 =  3 + (List.length nd)  in
            let l2 = l1 + (List.length log) in
            let l3 = l2 + (List.length my_dominos)
            in
              List.iter print_pnames plist;
              Printf.fprintf chano "</PLAYERS>\n</INFO>\n";
              Printf.fprintf chano "<GAME>\n<EXTREMITIES X1=\"%d\" X2=\"%d\"/>\n" x1 x2;
              (* to be continued ... *)
              List.iter print_nb_doms nd; 
              List.iter print_log_item log;
              List.iter print_hand_item my_dominos;
              List.iter print_hand_item sek;
              
              close_out chano

      (* default : error *)
      | _ ->
          Printf.printf "mode <%s> undefined yet...\n" smode

 (* string with "n1 n2 " returns int n1 int n2 *)
  let parse_2ints str =
    let deb = String.index str  ' '
    and fin = String.rindex str ' '
    in
      if deb != fin then
        begin
          Printf.printf "problem because <%s> is not compliant with <int int>\n" str;
          -1, -1
        end
      else
        try
          let part1 = String.sub str 0 deb 
          and part2 = String.sub str (deb +1) ((String.length str) - deb-1)
          in
            int_of_string part1, int_of_string part2
        with Failure s | Invalid_argument s ->
          Printf.printf "problem because <%s> is not compliant with <int int> (conversion phase --> %s)\n" str s;
          -1, -1

  (* pretty print and GUI part *)
  let string_of_int_with_blank i =
    if i = 0 then
      " "
    else
      string_of_int i

        
  let string_of_domino d =
    match d with 
        (h, l) -> ("[" ^ (string_of_int_with_blank h) ^ "|" ^ (string_of_int_with_blank l) ^ "]")
  

  (* pretty print of a set of dominos *)
  let string_of_dominos d_set =
    let build_sod state d =
      state ^ (string_of_domino d)
    in
      List.fold_left build_sod "" d_set
        

  (* pretty print of labels for a set of dominos *)
  let label_of_dominos d_set =
    let d_length = List.length d_set 
    and z = ref "" in
      for i=1 to d_length do 
        z := Printf.sprintf "%s%3d  " !z i;
      done;
      !z


  (* interactive choice of a domino 
     (-1,-1) is the code for "no domino"
  *)
  let choose_domino d_set =
    Printf.printf "%s\n%s\n"(string_of_dominos d_set) (label_of_dominos d_set);
    let d_n = int_of_string (read_line ()) in
      try
        List.nth d_set (d_n -1)
      with Invalid_argument e | Failure e ->
        (-1, -1)
  
  (* return a list with the keys of an hashtable *)
  let keys_of h =
    let get_only_keys k d state =
      k :: state
    in
      Hashtbl.fold get_only_keys h []

  (* extract an element of a list *)
  let extract_element lst n =
    let lst_size = (List.length lst) -1 in
    let z = ref [] in
      for i=0 to lst_size do
        if not (i = n) then
          z := !z @ [List.nth lst i]
      done;
      (List.nth lst n), !z

  (* find all the combinations compliants with two extremities of a given set of dominos 
     I know that I should organize this into a graph structure...
     One point is that if I can continue the sequence at two extremes, 
     I will have 2 times the associated solutions
  *)
  let find_combinations d_set x1 x2 =
    let z = ref [] in
    let rec one_step dset xa xb path =
      if dset = [] then
        z := path :: !z 
      else
        let at_least_one = ref false in
          for i=0 to (List.length dset) -1 do
            let base, dsubset = extract_element dset i in
              match base with
                  (high, low) -> 
                    let tmpa = 
                      (* can I continue the sequence with xa ? *)
                      if high = xa then
                        begin
                          at_least_one := true;
                          one_step dsubset low xb (base :: path)
                        end
                      else
                        if low = xa then
                          begin
                            at_least_one := true;
                            one_step dsubset high xb (base :: path)
                          end
                    and tmpb =
                      (* can I continue the sequence with xb ?... *)
                      if high = xb then
                        begin
                          at_least_one := true;
                          one_step dsubset xa low (path @ [base])
                        end
                      else
                        if low = xb then
                          begin
                            at_least_one := true;
                            one_step dsubset xa high (path @ [base])
                          end
                    in ()
          done;
          if not !at_least_one then
            z := path :: !z;
          ()
    in
      one_step d_set x1 x2 [];
      !z

  (* build all sequences *)
  let all_sequences d_set =
    let z = ref [] in
    let set_length = (List.length d_set) -1 in
      for i=0 to set_length do
        let base, d_subset = extract_element d_set i in
          match base with
              (x1, x2) -> 
                (* I need to inject the base to the combinations *)
                let insert_base elt =
                  base :: elt in
                let t = List.map insert_base (find_combinations d_subset x1 x2) in
                  (*
                    Printf.printf "(%d,%d) %s " x1 x2 (string_of_dominos d_subset);
                  let print_d_set ds =
                    Printf.printf "%s " (string_of_dominos ds)
                  in
                    List.iter print_d_set t;
                    Printf.printf "\n";
                  *)
                  if not (t = []) then
                    z := t @ !z
      done;
      !z

  (* return true or false relying on the sequenciability of the set of dominos *)
  let is_sequenciable d_set =
    let z = all_sequences d_set in
    let d_length = List.length d_set in
    let keep_max state s =
      state || (d_length = List.length s)
    in
      List.fold_left keep_max false z


  (* a simple signature for sequences ... *)
  let seq_cumsum d_set =
    let cadd state d =
      match d with 
          (h,l) -> state + h + l
    in
      List.fold_left cadd 0 d_set

  (* to sort sequences, use:
     List.stable_sort Base.compare_sequences w;;
  *)
  let compare_sequences sa sb =
    if sa = sb then 0
    else
      let lengtha = List.length sa
      and lengthb = List.length sb
      in
        if lengtha < lengthb then -1
        else
          if lengtha > lengthb then 1
          else
            let csa = seq_cumsum sa
            and csb = seq_cumsum sb
            in
              if csa < csb then -1
              else
                if csa > csb then 1 else 0
            

end;;
