DOMINOCAML

DOMINOCAML is an ocaml GPL software aimed to explore aspects of the dominoes game.
Actually, it can :
- compute statistics on domino gaming
- simulate games between automated players
- play games between humans and automated players

Dominocaml is written by Charles-Albert Lehalle (charles@lehalle.net).
Informations on it (and last version) can be found at http://phimatex.sf.net/dominocaml.html
If new developpers want to join its developpment, dominocaml will become a sourceforge project on its own.

I call a N-domino game a game where numbers on dominoes go from 'null' to N (included),
the classical dominoes game is a 6-domino game. I call N the 'order' of the domono game.

USE - statistics computations

1. ordering subsets of dominoes
This use of dominocaml try to 'order' a lot (V) of subsets of K randomly chosen dominoes in a N-domino game.

USE: dominocaml N subset-size(K) number-of-tests(V) 'subsets' verbose-mode
for instance: dominocaml 6 7 100 subsets Off
returns:
# DOMINOCAML version-3.3/B:2.2/S:2.2
# please press enter (seeding random generator)
# called with: dominocal 6 7 100 subsets
# +....+...++.....................+.....+......+..+...+.....+..+.++...............
# .+......+....++....+
# FULL STATS:
# statistics on sequenciable subsets of 100 6-dominos:
# 18 subset ended
# Total CPU time used was 1.302000 seconds

2. number of ended games
This use of dominocaml simulates a lot (V) of N-domino games between K simple-strategy players and returns the number 
of games with a winner.

USE: dominocaml N number-of-players(K) number-of-games(V) 'games' verbose-mode
for instance: dominocaml 6 4 100 games Off
# DOMINOCAML version-3.3/B:2.2/S:2.2
# please press enter (seeding random generator)
# called with: dominocal 6 4 100 games
# .+.+...+++.+.+++..++++++++...++.+..+.++++++..+...++.+......+..+.+..++.++++.+.+.+
# +...+.++..++++++.+..
#  100 games have been simulated
#   54 games have been won (  46 lost by all players)
#   19 games have been won by first player to play
# Total CPU time used was 0.621000 seconds

USE - game playing

3. games between players 
This use of dominocaml can simulate games between players.
Players can be :
- human
- automated

Keywords bewteen parenthesis in the names of players specify their nature.
Actually : 
- (human)  means interactive player
- (extern) means external automated player
- (simple) means a 'simple strategy' players
it is very simple to add new strategies (see sections 6 and 7)

USE: dominocaml N names-of-players verbose-mode number-of-games
for instance: dominocaml 6 alpha beta games Off 2
# DOMINOCAML version-3.3/B:2.2/S:2.2
# please press enter (seeding random generator)
# garzol
# called with: 7 arguments
# 
# GAME NUMBER 1: Number of turns  20 (~ 6x3+2)
# W                            <alpha> :
#                               <beta> : [ | ]
#                              <games> : [5|3][3|1][3| ]
# 
# GAME NUMBER 2: Number of turns  21 (~ 7x3+0)
# L                            <alpha> : [6|6][5|3][3|3][6|5]
# L                             <beta> : [1|1]
# L                            <games> : [5|5][2|2]
# 
# FULL STATS:
#                                          T   L   W  1st    S
#                              <alpha> :   1   0   1   0 |   2
#                              <games> :   1   1   0   1 |   2
#                               <beta> :   1   1   0   1 |   2

For external softwares, the name of the player must be : 'A(extern)B' 
where B is the software path and A will be the prefix of the exchange files.
for instance :
in: dominocaml 6 alpha beta good(extern)basic_strategy ga Off 1
# DOMINOCAML version-3.3/B:2.2/S:2.2
# please press enter (seeding random generator)
# garzol
# called with: 8 arguments
# 
# GAME NUMBER 1: Number of turns  21 (~ 5x4+0)
# L                            <alpha> : [3|3][5|1]
# W                             <beta> :
# L       <good(extern)basic_strategy> : [3|1]
# L                               <ga> : [4|2][5|3][4| ]
# 
# FULL STATS:
#                                          T   L   W  1st    S
#         <good(extern)basic_strategy> :   0   1   0   0 |   1
#                              <alpha> :   0   1   0   1 |   1
#                                 <ga> :   0   1   0   0 |   1
#                               <beta> :   0   0   1   0 |   1

good(extern)basic_strategy means that the software called is basic_strategy
and the way it is called is: basic_strategy good.in good.out
the file good.in contains informations about the current game and the software has to output its play 
in the file good.out

COMPILATION

4. Included files
readme.txt       : this file
Base.ml          : sources of the Base module, used by the main file
Strategy.ml      : sources of the Strategy module, used by the main file
dominocaml.ml    : main file
make.bat         : simple makefile for DOS
basic_strategy.m : MATLAB sample strategy file (to be used as external strategy)

5. compile

ocamlc -c Base.ml
ocamlc -c Strategy.ml
ocamlc Base.cmo Strategy.cmo -o dominocaml.exe dominocaml.ml

ADD STRATEGY

6. In ocaml

The process is very simple. All modifications can be made in the Strategy.ml file (containing the Strategy module).
1. implement a function with the name you want at the type t_strategy like :
>  let name_of_my_strategy player-name player-number players-list players-dominos extremity-1 extremity-2 current-sequence log-of-game number-of-dominos-for-each-player verbose-mode -> (int, int)
with:
- player-name:       is a string containing the name of the player
- player-number:     an int that is the order of the player in the turns
- players-list:      a list of strings containing the names of all the players
- players-dominos:   the list of the dominos of the player, like in [ (1,2) ; (5,6) ; ... ]
- the 2 extremities: the number that have to be matched by the players (extreminties of the sequence of the game)
- current-sequence:  the sequence of the game
- log-of-game:       the dominos played associated with players names like in [ ( 'player-one' , (1,2) ) ; ... ]
- number-of-...:     the number of dominoes of each player like in [ (nb, 'player-one') ; ... ]
- verbose-mode:      On or Off
and must return a pair of int which is the domino played.
Pass is (-1,-1) ; you have to pass only if you cannot produce a domino matching the extremities of the sequence of the game.

7. Using external software

It is also possible to use an external software to play with dominocaml.
The software has to be invoked this way : soft-name filename.in filename.out
The filename.in file has the following format (see the strategy.ml file):
#        { - player nb; current players names list
#        { - L1 L2 L3
#        { - ext1, ext2
#  info  { - nb_domino player_name
#        { - ...
#  game  [ - dh dl player_name      <--- L1 (first=0)
#  log   [ - ...
#  my    { - dh dl                  <--- L2
#  hand  { - ...
#        [ - dh dl                  <--- L3
#  sek   [ - ...

and the filename.out file (produced by the software) has this format:
# int int
for the marks on the domino to play.

TODO LIST

8. New interfaces

8.1. A pretty GUI has to be added.
The file DominoGUI.ml is a first version of that I could make in ocaml.
May be it should be easier to write it in Java, using the external software Strategy way.

8.2. A networked version of dominocaml has to be added.
First it is 'easy' to do it this way :
- dominocaml is the server
- players are clients, as new strategies

If someone write an ocaml class that is instancied in the strategy.ml file.
It has to be empty when created and mount a server at its first call (when it is empty).
It has to publish (may be xml-ed version) of the filename.out file and wait answers.
