open Base;;

(*
  TODO
  VC strategy
  Networked games

  history:
  2.2 - external strategy
  2.1 - got lot more informations to the players
  2.0 - packaged into an external module
        (problems encoutered were resolved using the module type definition -STRAT-)

  version 2.2
*)

module type STRAT =
  sig
    val version     : string

    type t_strategy = string -> int -> string list -> (int * int) list -> int -> int -> (int * int) list -> (string * (int * int)) list ->
      (int * string) list -> bool -> int * int

    val interactive : t_strategy
    val simple      : t_strategy
    val extern      : t_strategy
      (* add new strategy stype declaration here *)

    val strategies_table        : (string, t_strategy) Hashtbl.t
    val get_associated_strategy :  string -> t_strategy
  end
;;

module Strategy: STRAT =
    (* to implement a new strategy, 

       1. build a function with this interface :
       - inputs : dominos-of-player extreme-1 extreme-2 sequence
       - output:  domino,
       can be (-1, -1) if no domino is playable

       2. add the signature declaration in the type STRAT

       3. associated the strategy with a string into the |strategies_table|

       4. update minor version (add one)
    *)    
struct
  let version = "2.2"

  type t_strategy = string -> int -> string list -> (int * int) list -> int -> int -> (int * int) list -> (string * (int * int)) list ->
    (int * string) list -> bool -> int * int

    (* interactive :
       INTERFACE:
       - inputs : dominos-of-player players_nb current_players_list extreme-1 extreme-2 sequence log nb-doms
       - output:  domino,
       can be (-1, -1) if no domino is playable

       the name of this kind of player is:
       real_name(extern)file_name
       dominocaml call |filename real_name.in real_name.out| at each turn
       
  a log game is :
        { - player nb; current players names list
        { - L1 L2 L3
        { - ext1, ext2
  info  { - nb_domino player_name
        { - ...
  game  [ - dh dl player_name      <--- L1 (first=0)
  log   [ - ...
  my    { - dh dl                  <--- L2
  hand  { - ...
        [ - dh dl                  <--- L3
  sek   [ - ...

       the file simple_strategy.m is a MATLAB file using this interface
    *)    
  let extern pname pn plist my_dominos x1 x2 sek log nd verb = 
    let exec_name =
      try 
        let beg_  = 1 + (String.index pname ')') in
        let end_  = (String.length pname) in
        let sub_l = end_ - beg_ in
          String.sub pname beg_ sub_l
      with Not_found -> ""
    and basefile_name =
      try 
        let beg_  = 0 in
        let end_  = (String.index pname '(') in
        let sub_l = end_ - beg_ in
          String.sub pname beg_ sub_l
      with Not_found -> ""
    in
    let call_string = Printf.sprintf "%s %s.in %s.out" exec_name basefile_name basefile_name
    and out_file    = Printf.sprintf "%s.out" basefile_name
    in
      Base.log_game_to_file pname pn plist my_dominos x1 x2 sek log nd "txt" (Printf.sprintf "%s.in" basefile_name) ;
      
      (* this is the calling line *)
      let x = (Sys.command call_string) in
        if verb then
          Printf.printf "> call the command [%s]\n> ...\n> with exit code %d\n" call_string x;
        
        let chanin    = open_in out_file in
          if verb then
            Printf.printf "> read result in file <%s>\n" out_file;
          let move_line = input_line chanin in
            if verb then
              Printf.printf "> line is <%s>\n" move_line;
            close_in chanin;
        
            let move_high, move_low = Base.parse_2ints move_line in
              (move_high, move_low)


    (* interactive :
       INTERFACE:
       - inputs : dominos-of-player players_nb current_players_list extreme-1 extreme-2 sequence log
       - output:  domino,
       can be (-1, -1) if no domino is playable
    *)    
  let interactive pname pn plist my_dominos x1 x2 sek log nd verb = 
    Printf.printf "Sequence is:\n%s\n" (Base.string_of_dominos sek);
    Printf.printf "Player <%s> is an interactive one, let's choose a domino to play:\n" pname;
    Base.choose_domino my_dominos
    

    (* simple strategie :
       - get  compatible with extr1
       - get  compatible with extr2
       - (alt: play double  ...)
       - if no double: play at random
       -          alt: play the one with opposite mark minus presents in sequence
       
       INTERFACE:
       - inputs : dominos-of-player extreme-1 extreme-2 sequence
       - output:  domino,
       can be (-1, -1) if no domino is playable
    *)    
  let simple pname pn plist my_dominos x1 x2 sek log nd verb = 
    let comp_x1 d =
      match d with 
          (h,l) -> (h = x1) || (l == x1)
    and comp_x2 d =
      match d with 
          (h,l) -> (h = x2) || (l == x2)
    in 
    let c_x1, c_s1 = (Base.split_according_to comp_x1 my_dominos) 
    and c_x2, c_s2 = (Base.split_according_to comp_x2 my_dominos) 
    in
    let size1 = (List.length c_x1) 
    and size2 = (List.length c_x2) 
    in
      if (size1 + size2) = 0 then
        (-1,-1)
      else
        (* double chek ... *)
        (* randomization using |rand_element| *)
        if size1 > size2 then
          Base.rand_element c_x1
        else
          Base.rand_element c_x2
            
            
  (* hastable of strategies *)
  let strategies_table = 
    let tmp = Hashtbl.create 2 in
      Hashtbl.add tmp "simple" simple;
      Hashtbl.add tmp "human" interactive;
      Hashtbl.add tmp "extern" extern;
      (* add the association of the new strategy with a keyword *)
      tmp
        
    (* return the strategy associated with a key *)
  let get_associated_strategy pname =
    let pot = String.lowercase (Base.str_find_between pname '(' ')') in
      try
        Hashtbl.find strategies_table pot
      with Not_found -> simple
          
end;;
