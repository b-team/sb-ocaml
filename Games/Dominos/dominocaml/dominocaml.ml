(* DOMINOS GAMES SIMULATIONS 

   TODO
   log for statistics
   graphical aspects (DominoGUI module, under construction)

   history:
   3.3 - minor modifications to allow plug of external players (see Strategy.ml)
   3.2 - batch games for named players (to compare strategies)
   3.1 - statistics on subsets (option 'subset' on command line)
   3.0 - external modules
   2.8 - strategies into a module
   2.7 - add a strategy according to a table
   2.6 - clean seeding of random generator
   2.5 - interactive player named by 'human' in its name
   2.4 - class dedicated to statistics
   2.3 - additionnal checkings

   version: 3.3
*)
let version = "3.3";;

(* to be able to compile from an interactive session *)
let compile _ =
  Sys.command( "ocamlc Base.cmo Strategy.cmo -o dominocaml.exe dominocaml.ml")
;;
let compile_all _ =
  ignore (Sys.command( "ocamlc -c Base.ml"));
  ignore (Sys.command( "ocamlc -c Strategy.ml"));
  Sys.command( "ocamlc Base.cmo Strategy.cmo -o dominocaml.exe dominocaml.ml")
;;

open Base;; 
open Strategy;;

(* usefull types *)
type t_game_state = None | Initialized | In_game | End_game;;

type on_off       = On | Off;;

type player_designation = Nb of int | Names of string list;;

exception Lost;;
exception Win;;

(* usefull objects *)
class o_stats o plist max_cp =
object (this)

  val order                             = o;
  val players                           = plist;
  (* max chars in a name is stored into stats for pretty printing *)
  val p_max_length                      = max_cp;

  val mutable ended_games               = 0;
  val mutable total_games               = 0;
  val mutable last_game_status          = "none";
  (* -1: none, 0: not sequenciable, 1: at least one combination of sequences found *)
  val mutable last_subset_status        = -1;
  val mutable games_won_by_first_player = 0;
  val mutable current_first_player      = "none";
  val mutable ended_subsets             = 0;
  (* for each player, an array with : Lost(no more dominos), Lost(other winner), Win, 'is first player'
  *)
  val mutable players_stats             = 
   let tmp = Hashtbl.create 6 in
   let add_one_pstat pn =
     Hashtbl.replace tmp pn [| 0;0;0;0 |]
   in
     List.iter add_one_pstat plist;
     tmp;

  (* get methods *)
  method get_p_max_length = p_max_length;
  method get_order = order;
  method get_eg    = ended_games;
  method get_lgs   = last_game_status;
  method get_tg    = total_games;
  method get_gwbfp = games_won_by_first_player;
  method get_cfp   = current_first_player;
  method get_lss   = last_subset_status;
  method get_ended_subsets = ended_subsets;
  method get_players_stats = players_stats;
  method get_players_caracts pname = 
    try
      Hashtbl.find this#get_players_stats pname
    with Not_found -> [| 0;0;0;0 |]

  (* set methods *)
  (* method set_players p = players <- p; *)
  method set_first_player pname = 
    current_first_player <- pname ;
    let current_value =  this#get_players_caracts pname
    in
      current_value.(3) <- current_value.(3) +1
      ;

  method set_subset_status s = last_subset_status <- s;

  method set_last_subset   s =
    last_subset_status <- s;
    if s>0 then
      ended_subsets <- this#get_ended_subsets +1;
    total_games <- this#get_tg +1;
    
  (* I know that keywords have to be known externally ... *)
  method set_player_stat pname type_of_stat =
    let current_value = this#get_players_caracts pname
    in
    let idx =
      match type_of_stat with
          "lost"      -> 0
        | "other won" -> 1
        | "win"       -> 2
        | _     -> raise Base.Impossible
    in
      current_value.(idx) <- current_value.(idx) +1;
      Hashtbl.replace this#get_players_stats pname current_value

  (* usefull methods *)

  (* called at the end of the game, used to update stats *)
  method end_game pname =
    total_games <- this#get_tg +1;
    (* I know that's no very clean: 
       none has to be known by 2 different classes... *)
    if pname = "none" then
      let no_one pn st =
        st.(0) <- st.(0) +1;
        Hashtbl.replace this#get_players_stats pn st
      in
        Hashtbl.iter no_one this#get_players_stats;
        last_game_status <- "lost"
    else
      let just_one pn st =
        if pn = pname then
          st.(2) <- st.(2) +1
        else
          st.(1) <- st.(1) +1;
        Hashtbl.replace this#get_players_stats pn st
      in
        Hashtbl.iter just_one this#get_players_stats;

        last_game_status <- "won";
        ended_games <- this#get_eg +1;
        if pname = this#get_cfp then
          games_won_by_first_player <- this#get_gwbfp +1;


  method show_players_stats =
    let maxl = this#get_p_max_length in
    let show_one_player pname st =
      let no_neg_length = maxl - (String.length pname) in
      let spaces = (String.make (if no_neg_length < 0 then 0 else no_neg_length) ' ') in
        Printf.printf "  %s<%s> : %3d %3d %3d %3d | %3d\n" spaces pname st.(0) st.(1) st.(2) st.(3) (st.(0)+st.(1)+st.(2))
    in
      Printf.printf "\nFULL STATS:\n %s        T   L   W  1st    S\n" (String.make maxl ' ');
      Hashtbl.iter show_one_player this#get_players_stats;
    
  (* called to show stats *)
  method show =
    Printf.printf "%4d games have been simulated\n"  this#get_tg;
    Printf.printf "%4d games have been won (%4d lost by all players)\n" this#get_eg (this#get_tg - this#get_eg);
    Printf.printf "%4d games have been won by first player to play\n" this#get_gwbfp;

  (* to know if the last ended game was a dead end *)
  method is_dead_end = (this#get_lgs = "lost");

  (* to know if the last subset tested was sequenciable *)
  method is_sequenciable =
    this#get_lss = 1;

  (* to have stats on last subsets tests *)
  method show_subset_stats = 
    Printf.printf "\nstatistics on sequenciable subsets of %d %d-dominos:\n" this#get_tg this#get_order;
    Printf.printf "%d subset ended\n" this#get_ended_subsets

end;;

(* the object part : a game 
   parametrized by 
   - the order of the dominoes (default=6) 
   - the number of players
*)
class dominos_game o players_d =
  let rec make_names state c p_ =
    if c > p_ then
      state
    else
      make_names (state @ [("player " ^ (string_of_int c))]) (c+1) p_
  in
  let rand_init = Random.init (int_of_float((Sys.time ()) *. 1000.0)) 
  and p, p_list = match players_d with
      Nb( n) -> n, (make_names [] 1 n)
    | Names( lst) -> (List.length lst), lst
  in 
object (this)
  (* hard part of the configuration of a game *)
  val order       = o;
  val dominos_nb  = (o+1)*(o+2)/2;
  val players_nb  = p;
  val players_names = p_list;

  val dominos_all = Base.calD_N o;
  (* player strategies *)
  val mutable strategies = Hashtbl.create o;
                     

  (* the state of the game *)
  val mutable game_state = None;
  (* part of a game parametrized at each game (soft part) *)
  val mutable rand_dominos = [];
  (* I need an entry for each player + the sequence + the 'dead dominos' *)
  val mutable players_dominos = Hashtbl.create (o+2);
  (* order of play : element n is the name of the nth player *)
  val mutable players_order   = [];
  (* current turn *)
  val mutable turn = 0;
  (* current subturn : i.e. turn modulo players_nb *)
  val mutable subturn = 0;
  (* informations level *)
  val mutable verbose = On;
  (* current number of players *)
  val mutable players_c_nb = p;
  (* winner name *)
  val mutable winner_name = "nobody";
  (* object for stats *)
  val mutable stats = new o_stats o p_list 32;
  (* log of a game *)
  val mutable log_game = [];
    

  (* get methods *)
  method get_order         = order;
  method get_players_nb    = players_nb;
  method get_players_c_nb  = players_c_nb;
  method get_dominos_nb    = dominos_nb;
  method get_dominos       = rand_dominos;
  method get_distrib       = players_dominos;
  method get_names         = players_names;
  method player_name n     = (List.nth (this#get_names) (n-1));
  method get_game_state    = game_state;    
  method get_players_order = players_order;
  method get_turn          = turn;
  method get_subturn       = subturn;
  method get_verbose       = verbose;
  method set_verbose v     = verbose <- v;
  method is_verbose        = (verbose = On);
  method get_strategies    = strategies;
  method strategy p_name   = Hashtbl.find strategies p_name;
  method get_winner_name   = winner_name;
  method get_log_game      = log_game;
  method add_to_log_game d =
    let pname = (List.nth this#get_players_order this#get_subturn) in
      log_game <- (this#get_log_game @ [(pname, d)]);
  method clear_log_game    = log_game <- [];

  (* to know if a player is an 'interactive' one :
     just search for the 'human' string in it *)
  method set_strategies =
    let mark_strategy p_name =
      Hashtbl.add this#get_strategies p_name (Strategy.get_associated_strategy p_name)
    in
      List.iter mark_strategy this#get_names
      ;

  (* *)
  method test_sequenciable sz =
    rand_dominos <- (Base.rand_subset dominos_all this#get_dominos_nb);
    let d_set, tmp = Base.split_list rand_dominos sz in
    let seq_status = if Base.is_sequenciable d_set then 1 else 0 in
      stats#set_last_subset seq_status
    ;

  (* batch statistics on subsets *)
  method batch_subsets sz nb vmode = 
    verbose <- vmode;
    let rec one_subset c =
      this#test_sequenciable sz;
      Printf.printf "%s" (if stats#is_sequenciable then "+" else ".");
      if c < nb then
        one_subset (c+1)
    in
      if nb > 1 then
        verbose <- Off;
      one_subset 1;
      Printf.printf "\nFULL STATS:";
      stats#show_subset_stats


  (* start a game
     init the soft part *)
  method init_game =
    this#clear_log_game;
    this#set_strategies;
    winner_name  <- "nobody";
    players_c_nb <- this#get_players_nb;
    (* repartition of the dominos among players, sequence and rest *)
    rand_dominos <- (Base.rand_subset dominos_all this#get_dominos_nb);
    Hashtbl.add (this#get_distrib) "rest" rand_dominos;
    Hashtbl.add (this#get_distrib) "sequence" [];
    let max_doubles = ref [] in
      for i = 1 to (this#get_players_nb) do
        let for_player, for_rest = Base.split_list (Hashtbl.find (this#get_distrib) "rest") (this#get_order +1) in
          Hashtbl.add     (this#get_distrib) (this#player_name i) for_player;
          Hashtbl.replace (this#get_distrib) "rest" for_rest;
          max_doubles := !max_doubles @ [(Base.max_double_value for_player)];
      done;
    (* ordering of players 
       warning: in index first is 0, and in names it's 1
    *)
      let nb_of_first_player = (Array.get (Base.sort_and_index !max_doubles) (this#get_players_nb -1)) in
      let rec build_players_order state c =
        if (List.length state) == (this#get_players_nb) then
          state
        else
          let real_c = if c < (this#get_players_nb) then c else 0
          in
            (* print_int real_c; print_endline ""; *)
            build_players_order (state @ [ (this#player_name (real_c +1))]) (real_c+1)
      in
        players_order <- (build_players_order [] nb_of_first_player);
        if this#is_verbose then
          begin
            let show_order pname =
              Printf.printf "<%s> -> " pname
            in
              List.iter show_order this#get_players_order;
              Printf.printf "\n";
          end;
    (* state of the game *)
          game_state <- Initialized;
    (* informations for stats *)
          stats#set_first_player (List.hd this#get_players_order);

  (* get a player dominos (player is a string or an int from 1 to nb of players) *)
  method dominos p =
    try
      Hashtbl.find (this#get_distrib) p
    with Not_found ->
      let get_keys = Base.keys_of (this#get_distrib) 
      and pretty_string state str =
        ("<" ^ str ^ "> " ^ state)
      in
        print_endline ("The only keys avalables are " ^ (List.fold_left pretty_string "" get_keys));
        []
        ;
  (* get dominos of player to play at subturn t *)        
  method player_at_subturn t  = 
    this#dominos (List.nth this#get_players_order t)
    

  (* return a list with dominos number of each player *)
  method dominos_nb_list n_list =
    let build_nb state pname =
      let d_set = this#dominos pname in
        state @ [( List.length d_set, pname)]
    in
      List.fold_left build_nb [] n_list;

  (* show players state *)
  method show_players =
    let show_one pname =
      let till_in_game x =
        x = pname
      (* this call to stats need that storing of max chars in a name is stored into stats *)
      and no_neg_length = stats#get_p_max_length - (String.length pname)
      in
      let tag_str = 
        if pname = this#get_winner_name then
          "W"
        else
          if (List.exists till_in_game this#get_players_order) then
            " "
          else
            "L"
      and spaces = (String.make (if no_neg_length < 0 then 0 else no_neg_length) ' ')
      in
        Printf.printf "%s %s<%s> : %s\n" tag_str spaces pname (Base.string_of_dominos (this#dominos pname))
    in
    let t = this#get_turn in
      Printf.printf "Number of turns %3d (~ %dx%d+%d)\n" t (t/ this#get_players_nb) this#get_players_nb this#get_subturn;
      List.iter show_one this#get_names
      ;

  (* first play *)
  method first_turn =
    
    turn       <- 0;
    subturn    <- this#get_turn mod this#get_players_c_nb;

    let c_player       = (List.hd this#get_players_order) in
    let c_doubles, c_s = (Base.split_according_to Base.is_double (this#dominos c_player)) in
    let idx            = Base.sort_and_index c_doubles in
    let idx_length     = List.length c_doubles in
    let rec no_max state nb =
      if nb < (idx_length -1) then
        no_max ( (List.nth c_doubles idx.(nb)) :: state) (nb+1)
      else
        state
    in
    let this_d = (List.nth c_doubles idx.(idx_length -1))
    in
      if this#is_verbose then
        Printf.printf "player <%s> begin the game\nHe give us a %s\n" c_player (Base.string_of_domino this_d);
        
      Hashtbl.replace this#get_distrib c_player   (no_max c_s 0);
      Hashtbl.add     this#get_distrib "sequence" [ this_d ]; 

      this#add_to_log_game this_d;
      game_state <- In_game;
      turn       <- this#get_turn +1;
      subturn    <- this#get_turn mod this#get_players_c_nb;

  (* any play after the first one 
     - can raise : Lost or Impossible
  *)
  method next_turn =
    if this#get_game_state = In_game then
      let c_player   = (List.nth this#get_players_order this#get_subturn)
      and sek        = (this#dominos "sequence") in
      let x1, x2     = Base.get_extremes sek in
      let rec till_domino _ =
        let my_doms = (this#dominos c_player) in
        let give_ = 
          (this#strategy c_player) 
          c_player this#get_subturn this#get_players_order my_doms x1 x2 sek this#get_log_game 
            (this#dominos_nb_list this#get_players_order) this#is_verbose
        in
          if give_ = (-1,-1) then
            let rest = (this#dominos "rest") in
              this#add_to_log_game give_;
              if this#is_verbose then
                Printf.printf "      pass\n";
              (* verification that no domino could be played *)
              ignore (Base.check_no_mark my_doms x1 x2);
              if (List.length rest) = 0 then
                raise Lost
              else
                let new_d, new_rest = Base.split_list rest 1 in
                  Hashtbl.replace this#get_distrib c_player (new_d @ my_doms);
                  Hashtbl.add     this#get_distrib "rest"   new_rest;
                  till_domino ()
          else
            give_
      in
      let give      = till_domino () in
      let n_give, x = (Base.re_ord give x1 x2) in
      let new_sek   = 
        if x = 1 then
          n_give :: sek
        else
          sek @ [ n_give ]
      and new_set = (Base.set_minus (this#dominos c_player) give)
      in
        if this#is_verbose then
          Printf.printf "      player <%s> give us a %s\n" c_player (Base.string_of_domino n_give);
          
        Hashtbl.replace this#get_distrib c_player   new_set;
        Hashtbl.add     this#get_distrib "sequence" new_sek;
              
        if (List.length new_set) = 0 then
          raise Win;

        this#add_to_log_game n_give;
        turn       <- this#get_turn +1;
        subturn    <- this#get_turn mod this#get_players_c_nb;
    else
      begin
        Printf.printf "you try to continue an ended game...\n";
        ()
      end
    ;

  (* all game  *)
  method full_game =
    this#init_game;
    this#first_turn;
    try
      while true do
        if this#is_verbose then
          Printf.printf "TURN %3d (%2d): %s\n" (this#get_turn) (this#get_subturn) (Base.string_of_dominos (this#dominos "sequence"));
        
        try
          this#next_turn;
        with 
            Lost ->
              (* re-order players *)
              let this_player = (List.nth this#get_players_order this#get_subturn) in
                if this#is_verbose then
                  Printf.printf "      player <%s> lost...\n      %s\n" this_player (Base.string_of_dominos (this#dominos this_player));
                
                let build_new_players_order state pname =
                  if pname = this_player then
                    state
                  else
                    state @ [pname]
                in
                  this#add_to_log_game (-2,-2);
                  players_order <- (List.fold_left build_new_players_order [] this#get_players_order);
                  players_c_nb  <- (this#get_players_c_nb -1);
                  (* subturn has to be re computed *)
                  if this#get_players_c_nb = 0 then
                    raise Lost;
                  subturn       <- this#get_turn mod this#get_players_c_nb;
                  ();
      done
    with 
        (* update players statistics ...*)
        Lost ->
          if this#is_verbose then
              Printf.printf "      all players have lost\n";
          stats#end_game "none";
          ();
      | Win ->
          (* message *)
          let this_player = (List.nth this#get_players_order this#get_subturn) in
            winner_name <- this_player;
            if this#is_verbose then
              Printf.printf "      player <%s> won...\n" this_player;
            stats#end_game this_player;
            ()
      | Base.Impossible ->
          let this_player = (List.nth this#get_players_order this#get_subturn) in
            Printf.printf "player <%s> tried to cheat, I stop the game...\n" this_player;
            ()
      ;

  (* batch games for statistics *)
  method batch_games n vmode =
    verbose <- vmode; 
    let rec one_game c =
      this#full_game;
      Printf.printf "%s" (if stats#is_dead_end then "." else "+");
      if c < n then
        one_game (c+1)
    in
      one_game 1;
      Printf.printf "\n";
      stats#show
      ;

  (* show players stats *)
  method show_players_stats =
    stats#show_players_stats
    ;

end;;

let process_interactive o p_list nb_games vmode =
  let this_config = new dominos_game o (Names( p_list)) in
    this_config#set_verbose vmode;
    let rec one_game c =
      if c<nb_games then
        begin
          Printf.printf "\nGAME NUMBER %d: " (1+c);
          this_config#full_game;
          this_config#show_players;
          one_game (c+1)
        end
    in
      one_game 0;
      this_config#show_players_stats
;;

let process_batch o p b vmode =
  let this_config = new dominos_game o (Nb( p))
  and start_time  = Sys.time () in
    this_config#batch_games b vmode;
    Printf.printf "Total CPU time used was %f seconds\n" ((Sys.time ()) -. start_time)
;;

let process_subsets o sz nb vmode =
  let this_config = new dominos_game o (Nb( 2))
  and start_time  = Sys.time () in
    this_config#batch_subsets sz nb vmode;
    Printf.printf "Total CPU time used was %f seconds\n" ((Sys.time ()) -. start_time)
;;

(* tests *)
let test_set _ =
  let garzol = new dominos_game 6 (Nb( 4)) in
(*
  garzol#init_game;;
  garzol#get_players_order;;
  garzol#first_turn;;
  garzol#dominos "sequence";;
  garzol#next_turn;;
  garzol#dominos "sequence";;
*) 


    garzol#full_game;
    let u= garzol#dominos_nb_list garzol#get_players_order in
      garzol#show_players;
    
    (* garzol#get_log_game; *)

(*
    garzol#batch_games 10;
    garzol#set_verbose On;
*)
    (* process_interactive 6 ["alpha"; "beta"; "cal (human)"] *)
  
    (* garzol#test_sequenciable 1 *)
  
;;

(*
  MAIN program
  inputs:
  - order of the set of dominos
  - number of players
  - [number of games]
*)
let main () =
  Printf.printf "DOMINOCAML version-%s/B:%s/S:%s\n" version Base.version Strategy.version;
  if !Sys.interactive then
      test_set ()
  else
    begin
      try
        let for_rand_seed = 
          begin
            Printf.printf "please press enter (seeding random generator)";
            read_line () 
          end
        and order_of_dominos, number_of_players, number_of_bgames, type_of_simulation, verbose_mode =
          (* verbose mode ... *)
          match Array.length Sys.argv with 
              2 ->
                6, 4, int_of_string Sys.argv.(1), "games", On
            | 3 -> 
                let ood = int_of_string Sys.argv.(1) in
                  ood, (ood+2)/2 , int_of_string Sys.argv.(2), "games", On
	    | 4 ->
	        int_of_string Sys.argv.(1), int_of_string Sys.argv.(2), int_of_string Sys.argv.(3), "games", On
            | 5 ->
                int_of_string Sys.argv.(1), int_of_string Sys.argv.(2), int_of_string Sys.argv.(3), Sys.argv.(4), On
            | 6 ->
                let vmode = if (String.uppercase Sys.argv.(5)) = "OFF" then Off else On in
                int_of_string Sys.argv.(1), int_of_string Sys.argv.(2), int_of_string Sys.argv.(3), Sys.argv.(4), vmode
	    | _ -> 
	        raise (Failure "int_of_string") (* Base.Impossible *)
        in
          Printf.printf "called with: dominocal %d %d %d %s\n" order_of_dominos number_of_players number_of_bgames type_of_simulation;
          match String.lowercase type_of_simulation with
              "games" | "game" ->
	        process_batch order_of_dominos number_of_players number_of_bgames verbose_mode;
            | "subsets" | "subset" ->
                process_subsets order_of_dominos number_of_players number_of_bgames verbose_mode;
            | _ -> raise Base.Impossible

      with 
          Failure "int_of_string" ->
            (* an interactive game is called *)
            let nb_args = (Array.length Sys.argv) in
            (* I get 
               - first element if it's an int (default=6) in order_of_dominos
               - first element if it's a string (default = none) in first_name
               - all but first and last 2 elements as a list of string (players names)
            *)
            let order_of_dominos , first_name =
              try 
                (int_of_string Sys.argv.(1)), []
              with Failure "int_of_string" ->
                6, [ Sys.argv.(1) ]
            and names = Base.array2list (Array.sub Sys.argv 2 (nb_args -3))
            (* I get              
               - last element (if it's an int) as nb of games (default = one)
               - last element (if it's a string) as last player name (default = none)
            *)
            and nb_games, last_name = 
              try
                (int_of_string Sys.argv.(nb_args -1)), []
              with Failure "int_of_string" ->
                1, [Sys.argv.(nb_args -1)]
            in
            let p_list, vmode = 
              let get_options part1 part2 =
                let upart2 = String.uppercase (List.hd part2) in
                if ( upart2 = "OFF") then
                  part1 , Off
                else
                  if (upart2 = "ON") then
                    part1 , On
                  else
                    (part1 @ part2), On
              in
              (* not the good test ... *)
              if last_name = [] then
                let nb_names = (List.length names) -1 in
                let tmp, last_arg = Base.split_list names nb_names in
                  get_options (first_name @ tmp) last_arg;
              else
                get_options (first_name @ names) last_name;
            in
              if last_name = [] then print_endline "garzol" else print_endline (List.hd last_name);
              Printf.printf "called with: %d arguments\n" nb_args;
              process_interactive order_of_dominos p_list nb_games vmode
            
        | Base.Impossible ->
            ignore (Printf.fprintf stdout "%s%s" 
                      "dominocaml [order-of-dominos [number-of-players]] number-of-batch-games\n" 
                      "dominocaml [order-of-dominos] player1 player2 player3 ... [[verbose-mode=ON|OFF] number-of-batch-games]\n");
    end;
 1
;;

main ();;
