(*
Thomas Moniot
Groupe 3.2 (M. Grandguillot)

IAP1

Projet individuel de programmation en OCaml

Le jeu de domino

Tables des mati�res
Question 1	(cr�ation du jeu initial) :				p2
Question 2	(mises � jour de la pioche et des mains) :		p5
Question 3	(d�tection des coups ill�gaux) :			p10
Question 4	(annulation d�un ou plusieurs coups) :			p15
Question 5	(joueur CPU, jouant de mani�re na�ve) :			p17
Question 6	(joueur S-CPU, d�veloppant une strat�gie) :		p19
*)
 
(* R�gles *)
(*
- chaque domino comporte deux nombres entre 0 et 6 (=> 28 dominos, dont 7 doubles)
- au d�but de la partie, on distribue 27/n dominos � chacun des n joueurs (le reste -> pioche)
- un joueur donn� ne voit que ses dominos
- au premier coup, le 1er joueur pose un domino sur la table, et en tire un nouveau dans la
pioche, au hasard
- � chaque fois, si le joueur suivant poss�de un domino dont l'un des nombres est identique
� celui de l'une des faces libres, il doit le jouer, et tirer un nouveau domino dans la pioche,
au hasard, sinon il passe son tour
- un joueur qui pose un double peut rejouer tout de suite (ou passer)
- celui qui se d�barrasse de ses dominos le premier a gagn�
- si la partie est bloqu�e, c'est le dernier � avoir jou� qui est d�clar� vainqueur
*)





(* ===================================================================== *)










(* QUESTION 1 *)
(*
= on d�finit l'ensemble des dominos
= on d�finit le nombre nb de joueurs, compris entre 2 et 27
= on affecte � chaque joueur un num�ro de joueur, compris entre 1 et n, et une main :
   - on d�finit un ensemble dominos_restant, initialement �gal � l'ensemble des dominos
   BOUCLE
   - la main du 1er joueur est constitu�e de 27/n dominos pris au hasard dans dominos_restant
   - dominos_restant est mis � jour en retirant les dominos qui sont tir�s
   FIN BOUCLE
= lorsque les n mains ont �t� distribu�es, on d�finit l'ensemble pioche �gal � dominos_restant
*)





(* Types *)

type domino = Simple of int*int | Double of int ;;

type joueur = {numero : int ; mutable main : domino list} ;;


(* Constantes *)

let dominos = [Double 0 ; Simple (0,1) ; Simple (0,2) ; Simple (0,3) ; Simple (0,4) ; Simple (0,5) ; Simple (0,6) ; Double 1 ; Simple (1,2) ; Simple (1,3) ; Simple (1,4) ; Simple (1,5) ; Simple (1,6) ; Double 2 ; Simple (2,3) ; Simple (2,4) ; Simple (2,5) ; Simple (2,6) ; Double 3 ; Simple (3,4) ; Simple (3,5) ; Simple (3,6) ; Double 4 ; Simple (4,5) ; Simple (4,6) ; Double 5 ; Simple (5,6) ; Double 6] ;;





(* Fonctions *)

(* interface tirer1_hasard :
type : 'a list -> ('a * 'a list)
arg : ldominos (une liste quelconque de dominos)
pr� : ldominos non vide
post : tirer1_hasard ldominos = un couple constitu� du domino tir� et de la
       liste de dominos restante, lorsqu'on a �t� le domino tir�
raises : erreur si ldominos = []
rem1 : utilise une fonction auxiliaire r�cursive qui compte le nombre k
       d'�l�ments restant � sauter dans la liste ldepart, et les ajoute �
       la liste des dominos non choisis
rem2 : le dernier cas de filtrage de aux_tirer1_hasard est impossible �
       atteindre, car le nombre k est pris al�atoirement entre 0 (inclus)
       et le nombre d'�l�ments de ldepart (exclus)
tests : [], [Double 0 ; Double 6]
*)

let tirer1_hasard ldominos =
   let rec aux_tirer1_hasard k ldepart non_choisis = match (k,ldepart) with
      | (0,x::fin) -> (x,non_choisis@fin)
      | (k,x::fin) -> aux_tirer1_hasard (k-1) fin (x::non_choisis)
      | _ -> failwith "aux_tirer1_hasard : cas impossible a atteindre"
   in
   if ldominos=[] then invalid_arg "tirer1_hasard : liste de dominos vide"
   else aux_tirer1_hasard (Random.int (List.length ldominos)) ldominos [] ;;





(* interface tirer :
type : int -> 'a list -> ('a list * 'a list)
arg : p (le nombre de dominos � tirer) ;
      ldominos (une liste quelconque de dominos)
contexte : la fonction tirer1_hasard
pr� : 0 <= p <= |ldominos|
post : tirer p ldominos = un couple constitu� d'une liste de p dominos
       tir�s al�atoirement et de la liste de dominos restante, lorsqu'on a
       �t� les dominos tir�s
raises : erreur si p < 0 ou p > |ldominos|
rem : utilise une fonction auxiliaire r�cursive qui compte le nombre k
      d'�l�ments restant � tirer, et calcule la liste des dominos choisis
      et celle des dominos non choisis
tests : 0 [Double 0], 3 [Double 0], 2 [Double 0 ; Double 3 ; Double 5]
*)

let tirer p ldominos =
   let rec aux_tirer k choisis non_choisis =
      if k=0 then (choisis,non_choisis)
      else let (x,lrestant) = tirer1_hasard non_choisis in
           aux_tirer (k-1) (x::choisis) lrestant
   in
      if p<0 || p>(List.length ldominos)
      then invalid_arg "tirer : nombre de dominos a tirer non compris entre
                        0 et le nombre d'elements de la liste"
      else aux_tirer p [] ldominos ;;





(* interface distribuer_mains :
type : int -> domino list -> (joueur list * domino list)
arg : n (le nombre de joueurs) ; ldominos (une liste quelconque de dominos)
contexte : la fonction tirer
pr� : 2 <= n <= 27
post : distribuer_mains n ldominos = un couple compos� de la liste des
       n joueurs (les mains �tant attribu�es � partir de la liste ldominos)
       et des dominos restants
raises : erreur si n < 2 ou n > 27
rem : utilise une fonction auxiliaire r�cursive qui compte le nombre k de
      joueurs restant � ajouter, et ajoute � chaque appel un joueur � la
      liste ljoueurs
tests : 28 [Double 0], 5 [Double 0]
*)

let distribuer_mains n ldominos =
   let rec aux_distribuer_mains k ljoueurs non_choisis =
      if k=0 then (ljoueurs,non_choisis)
      else let (choisis,lrestant) = tirer (27/n) non_choisis in
  aux_distribuer_mains (k-1) ({numero=k ; main=choisis}::ljoueurs) lrestant
   in
      if n<2 || n>27 then invalid_arg "distribuer_mains : nombre de joueurs
                                       non compris entre 2 et 27"
      else aux_distribuer_mains n [] ldominos ;;





(* Tests des fonctions *)

tirer1_hasard [] ;;

tirer1_hasard [Double 0 ; Double 6] ;;


tirer 0 [Double 0] ;;

tirer 3 [Double 0] ;;

tirer 2 [Double 0 ; Double 3 ; Double 5] ;;

distribuer_mains 28 [Double 0] ;;

distribuer_mains 5 [Double 0] ;;





(* Exemple de partie *)

let nb = 4 ;;

let (joueurs,pioche) = distribuer_mains nb dominos ;;






(* ===================================================================== *)










(* QUESTION 2 *)
(*
= on d�finit l'ensemble des dominos pos�s sur la table, initialement vide
= on d�finit les faces sur lesquelles on pourra jouer un domino, au d�part -1 et -1 (par convention)
= � chaque fois qu'un joueur pose un domino, on v�rifie si l'une des faces du domino jou�
correspond aux faces jouables (si les deux faces du domino jou� correspondent aux faces jouables,  on utilise la face pr�f�r�e pour les d�partager) :
   = si oui, on met � jour les ensembles joueurs (la main du joueur), pioche, table et faces_jouables
   = sinon, erreur
*)





(* Types *)

type faces = {mutable f1 : int ; mutable f2 : int} ;;








(* Fonctions *)

(* interface domino_jouable :
type : faces -> domino -> bool
arg : faces_jouables ; dom (le domino jou�)
post : domino_jouable faces_jouables dom teste si le domino peut �tre jou�
rem : si les valeurs des deux faces jouables sont -1 et �1
      (convention de d�part), alors tout domino peut �tre jou�
tests : {f1= -1 ; f2= -1} (Double 0)
        {f1=3 ; f2=6} (Double 0)
        {f1=3 ; f2=0} (Double 0)
*)

let domino_jouable faces_jouables dom = match (faces_jouables,dom) with
   | ({f1= -1 ; f2= -1},_) -> true
   | ({f1=a ; f2=b},Double x) -> a=x || b=x
   | ({f1=a ; f2=b},Simple (x,y)) -> a=x || b=x || a=y || b=y  ;;







(* interface trouver_joueur :
type : int -> joueur list -> (joueur * joueur list)
arg : n (le num�ro du joueur) ; ljoueurs (la liste des joueurs)
pr� : le joueur de num�ro n est suppos� exister dans la liste de joueurs
post : trouver_joueur n ljoueurs = le couple form� par le joueur de
       num�ro n et la liste des joueurs � laquelle on a retir� ce joueur
raises : erreur si le joueur de num�ro n n'est pas dans la liste
rem : utilise une fonction auxiliaire r�cursive
tests : 1 [{numero=1 ; main=[Double 0]} ; {numero=2 ; main=[]}]
        3 [{numero=1 ; main=[Double 0]} ; {numero=2 ; main=[]}]
*)

let trouver_joueur n ljoueurs =
   let rec aux_trouver_joueur ljoueurs lrestant = match ljoueurs with
      | [] ->
      failwith "trouver_joueur : ce joueur n'est pas dans la liste fournie"
      | ({numero=x} as j)::fin -> if x=n then (j,lrestant@fin)
                                  else aux_trouver_joueur fin (j::lrestant)
   in aux_trouver_joueur ljoueurs [] ;;






(* interface enlever_domino :
type : 'a -> 'a list -> 'a list
arg : d (domino � enlever) ; ld (liste de dominos)
pr� : le domino � enlever est suppos� exister dans la liste de dominos
post : enlever_domino d ld = la liste ld priv�e du domino d
raises : erreur si d n'appartient pas � ld
rem : utilise une fonction auxiliaire r�cursive
tests : (Double 0) [Simple (3,5) ; Double 0]
        (Double 0) [Double 1]
*)

let enlever_domino d ld =
   let rec aux_enlever_domino ld lrestant = match ld with
      | [] ->
      failwith "enlever_domino : ce domino n'est pas dans la liste fournie"
      | x::fin -> if x=d then lrestant@fin
                  else aux_enlever_domino fin (x::lrestant)
   in aux_enlever_domino ld [] ;;








(* interface actualiser_faces :
type : faces -> domino -> int -> faces
arg : f (les faces jouables) ; d (le domino jou�) ;
      face_preferee, la face sur laquelle on souhaite jouer
pr� : d est jouable par rapport � f
post : actualiser_faces f d face_preferee = les nouvelles faces jouables,
       lorsqu'on a jou� le domino
raises : erreur si le domino n'est pas jouable
rem : l'argument face_preferee est utilis� uniquement dans le cas o� les
      faces du domino jou� sont toutes les deux des faces jouables (si cet
      argument n'est pas coh�rent, la fonction jouera toujours le domino,
      par convention, sur la face n�2)
tests : {f1=6 ; f2=0} (Double 0) 99
        {f1=5 ; f2=6} (Double 0) 99
        {f1=0 ; f2=1} (Simple (0,1)) 0
*)

let actualiser_faces f d face_preferee = match (f,d) with
   | ({f1= -1 ; f2= -1},Double x) -> {f1=x ; f2=x}
   | ({f1= -1 ; f2= -1},Simple (x,y)) -> {f1=x ; f2=y}
   | ({f1=a ; f2=b},Double x) when a=x || b=x -> f
   | ({f1=a ; f2=b},Simple (x,y)) when (a=x && b=y) || (a=y && b=x) ->
                   if a=face_preferee then {f1=b ; f2=b} else {f1=a ; f2=a}
   | ({f1=a ; f2=b},Simple (x,y)) when a=x -> {f1=y ; f2=b}
   | ({f1=a ; f2=b},Simple (x,y)) when b=x -> {f1=a ; f2=y}
   | ({f1=a ; f2=b},Simple (x,y)) when a=y -> {f1=x ; f2=b}
   | ({f1=a ; f2=b},Simple (x,y)) when b=y -> {f1=a ; f2=x}
   | _ -> failwith "actualiser_faces : domino non jouable" ;;







(* interface jouer :
type : joueur list -> domino list -> domino list -> faces -> int ->
       domino -> int -> (joueur list * domino list * domino list * faces)
arg : joueurs ; pioche ; table ; faces_jouables ;
      nj (le num�ro du joueur qui joue) ; dom (le domino jou�) ;
      face_preferee
contexte : les fonctions domino_jouable, trouver_joueur, enlever_domino,
           tirer1_hasard et actualiser_faces
pr� : faces jouables en ad�quation avec la table,
      domino appartenant � la main du joueur
post : jouer joueurs pioche table faces_jouables nj dom face_preferee =
       la mise � jour des variables suivantes:
       liste des joueurs (mains), pioche, table et faces jouables
raises : erreur si le domino n'est pas jouable (faces)
         erreur si le domino n'appartient pas � la main du joueur
tests :
[{numero=1 ; main=[Double 0 ; Simple (2,6)]}] [Simple (1,2) ; Double 6] [Double 2] {f1=2 ; f2=2} 1 (Simple (2,6)) 99
[{numero=1 ; main=[Double 0 ; Simple (2,6)]}] [Simple (1,2) ; Double 6] [Double 2] {f1=2 ; f2=2} 1 (Double 0) 99
[{numero=1 ; main=[Double 0 ; Simple (2,6)]}] [Simple (1,2) ; Double 6] [Double 2] {f1=2 ; f2=2} 1 (Simple (3,5)) 99
*)

let jouer joueurs pioche table faces_jouables nj dom face_preferee =
   if not (domino_jouable faces_jouables dom)
   then failwith "jouer : impossible de jouer ce domino"
   else
      let (joueur_actuel,joueurs_restant) = trouver_joueur nj joueurs in
      let nouvelle_main = enlever_domino dom (joueur_actuel.main) in
      if pioche<>[] then        (* cas o� il reste des dominos � piocher *)
         let (dom_pioche,nouvelle_pioche) = tirer1_hasard pioche in
         ({numero=nj ; main=dom_pioche::nouvelle_main}::joueurs_restant,
         nouvelle_pioche, dom::table,
         actualiser_faces faces_jouables dom face_preferee)
      else ({numero=nj ; main=nouvelle_main}::joueurs_restant,
           [], dom::table,
           actualiser_faces faces_jouables dom face_preferee) ;;





(* Tests des fonctions *)

domino_jouable {f1= -1 ; f2= -1} (Double 0) ;;

domino_jouable {f1=3 ; f2=6} (Double 0) ;;

domino_jouable {f1=3 ; f2=0} (Double 0) ;;


trouver_joueur 1 [{numero=1 ; main=[Double 0]} ; {numero=2 ; main=[]}] ;;

trouver_joueur 3 [{numero=1 ; main=[Double 0]} ; {numero=2 ; main=[]}] ;;


enlever_domino (Double 0) [Simple (3,5) ; Double 0] ;;

enlever_domino (Double 0) [Double 1] ;;


actualiser_faces {f1=6 ; f2=0} (Double 0) 99 ;;

actualiser_faces {f1=5 ; f2=6} (Double 0) 99 ;;

actualiser_faces {f1=0 ; f2=1} (Simple (0,1)) 0 ;;


jouer [{numero=1 ; main=[Double 0 ; Simple (2,6)]}] [Simple (1,2) ; Double 6] [Double 2] {f1=2 ; f2=2} 1 (Simple (2,6)) 99 ;;

jouer [{numero=1 ; main=[Double 0 ; Simple (2,6)]}] [Simple (1,2) ; Double 6] [Double 2] {f1=2 ; f2=2} 1 (Double 0) 99 ;;

jouer [{numero=1 ; main=[Double 0 ; Simple (2,6)]}] [Simple (1,2) ; Double 6] [Double 2] {f1=2 ; f2=2} 1 (Simple (3,5)) 99 ;;





(* Exemple de partie *)

let table = [] ;;

let faces_jouables = {f1= -1 ; f2= -1} ;;


let nj = 1 ;;

let (dom,face_preferee) = (Double 0,99) ;;

let (joueurs,pioche,table,faces_jouables) = jouer joueurs pioche table faces_jouables nj dom face_preferee ;;


let nj = 2 ;;

let (dom,face_preferee) = (Double 6,99) ;;

let (joueurs,pioche,table,faces_jouables) = jouer joueurs pioche table faces_jouables nj dom face_preferee ;;


let nj = 3 ;;

let (dom,face_preferee) = (Simple (0,1),99) ;;

let (joueurs,pioche,table,faces_jouables) = jouer joueurs pioche table faces_jouables nj dom face_preferee ;;


let nj = 4 ;;

let (dom,face_preferee) = (Double 0,99) ;;

let (joueurs,pioche,table,faces_jouables) = jouer joueurs pioche table faces_jouables nj dom face_preferee ;;







(* ===================================================================== *)










(* QUESTION 3 *)
(*
= on cherche � d�tecter les coups ill�gaux (tricheries ou erreurs) :
   = stockage du num�ro dernier joueur � avoir jou�, et incr�mentation de ce nombre
      (modulo nb) � chaque fois qu'un joueur a valid� son tour
   = incorporation au type enregistrement joueur de deux nouveaux champs bool�ens :
      jeu_valide et peut_rejouer, qui conditionnent la validation du tour du joueur
= lorsqu'un joueur demande � jouer (sauf au tout d�but de la partie) :
   1) on v�rifie si le dernier joueur � avoir jou� a son champ jeu_valide affect� � true :
      = si c'est le cas, on l'affecte de nouveau � false, puis on passe � la seconde v�rification
      = sinon (cas ou le joueur veut passer son tour), on v�rifie que le joueur n'a aucun domino
         jouable :  si c'est le cas, on passe � la seconde v�rification ; sinon erreur : le joueur
         pr�c�dent doit jouer un domino
   2) on v�rifie si aucun tour n'a �t� saut� :
      = si le num�ro du nouveau joueur est �gal � celui du joueur pr�c�dent (le joueur veut
         rejouer),  on v�rifie si le joueur a son champ peut_rejouer affect� � true :
         - si oui, on le remet � false, et on fait rejouer le joueur
         - sinon erreur : le joueur ne peut pas rejouer
      = si le nouveau joueur est le successeur du joueur pr�c�dent, on veille � remettre le
         champ peut_rejouer du joueur pr�c�dent � false (le joueur pr�c�dent ne rejoue pas)
      = sinon erreur : ce n'est pas le tour de ce joueur
*)







(* Types *)

type joueur' = {numero : int ; mutable main : domino list ;
                mutable jeu_valide : bool ; mutable peut_rejouer : bool} ;;








 (* Fonctions *)

(* Fonction qui reste identique, m�me avec le nouveau type joueur' *)

(* interface trouver_joueur :
type : int -> joueur' list -> (joueur' * joueur' list)
rem : cf. question 2
*)

let trouver_joueur n ljoueurs =
   let rec aux_trouver_joueur ljoueurs lrestant = match ljoueurs with
      | [] ->
      failwith "trouver_joueur : ce joueur n'est pas dans la liste fournie"
      | ({numero=x} as j)::fin -> if x=n then (j,lrestant@fin)
                                  else aux_trouver_joueur fin (j::lrestant)
   in aux_trouver_joueur ljoueurs [] ;;








(* Fonction introduite pour effectuer les v�rifications sur la l�galit� des coups *)

(* interface peut_passer :
type : joueur' -> faces -> bool
arg : j (un joueur) ; faces_jouables
contexte : la fonction domino_jouable
post : peut_passer j faces_jouables teste si le joueur j peut passer son
       tour, compte tenu de sa main et des possibilit�s offertes par le jeu
       en cours
rem : utilise une fonction auxiliaire r�cursive qui parcourt la main du
      joueur tant qu'aucun domino n'est jouable
tests :
{numero=1 ; main=[Double 1 ; Simple (0,1)] ; jeu_valide=false ; peut_rejouer=false} {f1=0 ; f2=0}, {numero=1 ; main=[Double 0] ; jeu_valide=false ; peut_rejouer=false} {f1=1 ; f2=2}
*)

let peut_passer j faces_jouables =
   let rec aux_peut_passer lmain = match lmain with
      | [] -> true
      | d::fin ->
               not (domino_jouable faces_jouables d) && aux_peut_passer fin
   in aux_peut_passer j.main ;;








(* Fonctions dont le code change � cause du nouveau type joueur' *)

(* interface distribuer_mains' :
type : int -> domino list -> (joueur' list * domino list)
arg : n (le nombre de joueurs) ; ldominos (une liste quelconque de dominos)
contexte : la fonction tirer
pr� : 2 <= n <= 27
post : distribuer_mains' n ldominos = un couple compos� de la liste des
       n joueurs (les mains �tant attribu�es � partir de la liste ldominos)
       et des dominos restants
raises : erreur si n < 2 ou n > 27
rem1 : m�me effet que la fonction distribuer_mains,
       mais appliqu�e au type joueur'
rem2 : au d�part, les champs jeu_valide et peut_rejouer de chaque joueur
       sont affect�s � false
tests : 28 [Double 0], 5 [Double 0]
*)

let distribuer_mains' n ldominos =
   let rec aux_distribuer_mains' k ljoueurs non_choisis =
      if k=0 then (ljoueurs,non_choisis)
      else let (choisis,lrestant) = tirer (27/n) non_choisis in
           aux_distribuer_mains' (k-1) ({numero=k ; main=choisis ;
           jeu_valide=false ; peut_rejouer=false}::ljoueurs) lrestant
   in
      if n<2 || n>27 then invalid_arg "distribuer_mains' : nombre de
                                       joueurs non compris entre 2 et 27"
      else aux_distribuer_mains' n [] ldominos ;;





(* interface jouer' :
type : joueur' list -> domino list -> domino list -> faces -> int -> domino
  -> int -> int -> (joueur' list * domino list * domino list * faces * int)
arg : joueurs ; pioche ; table ; faces_jouables ;
      nj (le num�ro du joueur qui joue) ; dom (le domino jou�) ;
      face_preferee ; ndernier (le num�ro du dernier joueur � avoir jou�)
contexte : les fonctions domino_jouable, trouver_joueur, enlever_domino,
           tirer1_hasard et actualiser_faces, la fonction peut_passer
pr� : - faces jouables en ad�quation avec la table
      - domino appartenant � la main du joueur
post :
jouer' joueurs pioche table faces_jouables nj dom face_preferee ndernier =
       la mise � jour des variables suivantes :
       liste des joueurs (mains), pioche, table, faces jouables et ndernier
raises : - erreur si le domino n'est pas jouable (faces)
         - erreur si le domino n'appartient pas � la main du joueur
rem1 : m�me effet que la fonction jouer, mais appliqu�e au type joueur' et
     en tenant compte des v�rifications concernant la l�galit� du coup jou�
rem2 : si la valeur de ndernier est 0 (convention de d�part)
       alors n'importe quel joueur peut jouer
tests :
[{numero=1 ; main=[Double 0 ; Simple (2,6)] ; jeu_valide=false ; peut_rejouer=false}] [Simple (1,2) ; Double 6] [Double 2] {f1=2 ; f2=2} 1 (Simple (2,6)) 99 1
[{numero=1 ; main=[Double 0 ; Simple (2,6)] ; jeu_valide=false ; peut_rejouer=false}] [Simple (1,2) ; Double 6] [Double 2] {f1=2 ; f2=2} 1 (Double 0) 99 0
[{numero=1 ; main=[Double 0 ; Simple (2,6)] ; jeu_valide=false ; peut_rejouer=false}] [Simple (1,2) ; Double 6] [Double 2 ; Simple (2,3)] {f1=2 ; f2=3} 3 (Simple (3,5)) 99 0
*)

let jouer' joueurs pioche table faces_jouables nj dom face_preferee ndernier =
   let joueurs' = if ndernier=0 then joueurs     (* convention de d�part *)
   else 
      let (dernier_joueur,autres_joueurs) = trouver_joueur ndernier joueurs
      in
      if not (dernier_joueur.jeu_valide
             || peut_passer dernier_joueur faces_jouables)
                                                       (* v�rification 1 *)
      then failwith "jouer' : le joueur precedent doit jouer un domino"
      else
           if nj=ndernier && (not dernier_joueur.peut_rejouer)
                                                       (* v�rification 2 *)
           then failwith "le joueur ne peut pas rejouer"
           else if (nj=ndernier && dernier_joueur.peut_rejouer)
                   || ((ndernier=(List.length joueurs) && nj=1)
                   || (nj=ndernier+1))
                then {numero=ndernier ; main=dernier_joueur.main ;
                     jeu_valide=false ; peut_rejouer=false}::autres_joueurs
                else failwith "jouer' : c'est le tour d'un autre joueur"
   in
(* mises � jour *)
   if not (domino_jouable faces_jouables dom)
   then failwith "jouer' : impossible de jouer ce domino"
   else
      let (joueur_actuel,joueurs_restant) = trouver_joueur nj joueurs' in
      let nouvelle_main = enlever_domino dom (joueur_actuel.main) in
      let est_double = match dom with Double _ -> true | _ -> false in      
      if pioche<>[] then        (* cas o� il reste des dominos � piocher *)
         let (dom_pioche,nouvelle_pioche) = tirer1_hasard pioche in
         ({numero=nj ; main=dom_pioche::nouvelle_main ; jeu_valide=true ;
           peut_rejouer=est_double}::joueurs_restant,
         nouvelle_pioche,
         dom::table,
         actualiser_faces faces_jouables dom face_preferee,
         nj)
      else
         ({numero=nj ; main=nouvelle_main ; jeu_valide=true ;
           peut_rejouer=est_double}::joueurs_restant,
         [],
         dom::table,
         actualiser_faces faces_jouables dom face_preferee,
         nj) ;;





 (* Tests des fonctions *)

distribuer_mains' 28 [Double 0] ;;

distribuer_mains' 5 [Double 0] ;;


peut_passer {numero=1 ; main=[Double 1 ; Simple (0,1)] ; jeu_valide=false ; peut_rejouer=false} {f1=0 ; f2=0} ;;

peut_passer {numero=1 ; main=[Double 0] ; jeu_valide=false ; peut_rejouer=false} {f1=1 ; f2=2} ;;


jouer' [{numero=1 ; main=[Double 0 ; Simple (2,6)] ; jeu_valide=false ; peut_rejouer=false}] [Simple (1,2) ; Double 6] [Double 2] {f1=2 ; f2=2} 1 (Simple (2,6)) 99 1 ;;

jouer' [{numero=1 ; main=[Double 0 ; Simple (2,6)] ; jeu_valide=false ; peut_rejouer=false}] [Simple (1,2) ; Double 6] [Double 2] {f1=2 ; f2=2} 1 (Double 0) 99 0 ;;

jouer' [{numero=1 ; main=[Double 0 ; Simple (2,6)] ; jeu_valide=false ; peut_rejouer=false}] [Simple (1,2) ; Double 6] [Double 2 ; Simple (2,3)] {f1=2 ; f2=3} 3 (Simple (3,5)) 99 0 ;;



(* Exemple de partie *)

let nb = 4 ;; (* nombre de joueurs engag�s dans la partie *)

let (joueurs,pioche) = distribuer_mains' nb dominos ;;


let table = [] ;;

let faces_jouables = {f1= -1 ; f2= -1} ;;

let ndernier = 0 ;; (* num�ro du dernier joueur � avoir jou� *)


let (nj,dom,face_preferee) = (1,Double 0,99) ;;

let (joueurs,pioche,table,faces_jouables,ndernier) = jouer' joueurs pioche table faces_jouables nj dom face_preferee ndernier ;;






(* ===================================================================== *)









(* QUESTION 4 *)
(*
= utilisation d'une pile regroupant les diff�rentes variables qui d�finissent le jeu � un moment donn� :
   = lorsqu'on valide un coup (appel r�ussi de jouer'), on empile
   = lorsqu'on annule un coup, on d�pile
*)





 (* Fonctions *)

(* interface jouer'' :
type : joueur' list -> domino list -> domino list -> faces ->
       int -> domino -> int -> int ->
       (joueur' list * domino list * domino list * faces * int) Stack.t
       -> (joueur' list * domino list * domino list * faces * int *
          (joueur' list * domino list * domino list * faces * int) Stack.t)
arg : joueurs ; pioche ; table ; faces_jouables ;
      nj (le num�ro du joueur qui joue) ; dom (le domino jou�) ;
      face_preferee ; ndernier (le num�ro du dernier joueur � avoir jou�) ;
      pile
contexte : la fonction jouer'
post :
jouer'' joueurs pioche table faces_jouables nj dom face_preferee ndernier =
       la mise � jour des variables : liste des joueurs (mains), pioche,
       table, faces jouables et ndernier, et de la pile environnement
*)

let jouer'' joueurs pioche table faces_jouables nj dom face_preferee ndernier pile =
   let (lj,lp,lt,fj,nd) as environnement =
jouer' joueurs pioche table faces_jouables nj dom face_preferee ndernier in
   Stack.push environnement pile ;
   (lj,lp,lt,fj,nd,pile) ;;





(* interface annuler :
type : int -> (joueur' list * domino list * domino list * faces * int)
arg : k (nombre de coups � annuler)
pr� : 0 <= k < |pile|
post : annuler k = les varibles d�finissant le jeu avant que les k derniers
       coups n'aient �t� jou�s
raises : erreur si k < 0 ou |pile| = 1
rem : si k >= |pile|, la fonction d�pile jusqu'� l'environnement initial
     (non inclus)
*)

let rec annuler k =
   if k<0
   then invalid_arg "annuler : nombre de coups a annuler non positif"
   else if k=0 then Stack.top pile
        else if Stack.length pile=1
             then failwith "annuler : aucun coup a annuler"
             else let t = Stack.pop pile in annuler (k-1) ;;





(* Exemple de partie *)

let nb = 4 ;; (* nombre de joueurs engag�s dans la partie *)

let (joueurs,pioche) = distribuer_mains' nb dominos ;;


let table = [] ;;

let faces_jouables = {f1= -1 ; f2= -1} ;;

let ndernier = 0 ;; (* num�ro du dernier joueur � avoir jou� *)

(* environnement initial *)
let pile =  Stack.create() ;;

Stack.push (joueurs,pioche,table,faces_jouables,ndernier) pile ;;


let (nj,dom,face_preferee) = (1,Double 0,99) ;;

let (joueurs,pioche,table,faces_jouables,ndernier,pile) = jouer'' joueurs pioche table faces_jouables nj dom face_preferee ndernier pile ;;


let (joueurs,pioche,table,faces_jouables,ndernier) = annuler 2 ;;






(* ===================================================================== *)











(* QUESTION 5 *)
(*
cr�ation d'un joueur CPU, qui joue de fa�on na�ve :
= recherche automatique dans la main du joueur CPU d'un domino jouable
(le premier qui convient, d'o� la non prise en compte de la variable face_preferee)
= le joueur CPU joue s'il peut jouer, ou passe son tour
= s'il peut rejouer, le joueur CPU rejoue tout de suite
*)





(* Fonctions *)

(* interface premier_jouable :
type : domino list -> faces -> domino
arg : jmain (la main du joueur) ; faces_jouables
contexte : la fonction domino_jouable
pr� : un domino est jouable
post : premier_jouable jmain faces_jouables = le premier domino jouable
       apparaissant dans jmain
raises : erreur si aucun domino n'est jouable
tests : [Double 0] {f1=1 ; f2=2}, [Double 0 ; Double 1] {f1=1 ; f2=2}
*)

let rec premier_jouable jmain faces_jouables = match jmain with
   | [] -> failwith "premier_jouable : aucun domino n'est jouable"
   | d::fin -> if domino_jouable faces_jouables d then d
               else premier_jouable fin faces_jouables ;;




(* interface jouerCPU :
type : joueur' list -> domino list -> domino list ->
       faces -> int -> int ->
      (joueur' list * domino list * domino list * faces * int) Stack.t
       -> (joueur' list * domino list * domino list * faces * int *
          (joueur' list * domino list * domino list * faces * int) Stack.t)
arg : joueurs ; pioche ; table ; faces_jouables ;
      nj (le num�ro du joueur qui joue) ;
      ndernier (le num�ro du dernier joueur � avoir jou�) ; pile
contexte : les fonctions trouver_joueur, premier_jouable et jouer''
post : jouerCPU joueurs pioche table faces_jouables nj ndernier =
       la mise � jour des variables : liste des joueurs (mains), pioche,
       table, faces jouables et ndernier, et de la pile environnement
rem1 : jouerCPU joue le premier domino jouable, ou passe
rem2 : par d�faut, face_preferee est affect�e � 99 (pas de face pr�f�r�e)
rem3 : la fonction jouerCPU est r�cursive, car le joueur CPU tente de
       rejouer (par convention) d�s qu'il le peut
tests :
[{numero=1 ; main=[Double 0 ; Simple (2,6)] ; jeu_valide=false ; peut_rejouer=false}] [Simple (1,2) ; Double 6] [Double 2] {f1=2 ; f2=2} 1 0
[{numero=1 ; main=[Double 0 ; Simple (2,6)] ; jeu_valide=false ; peut_rejouer=false}] [Simple (1,2) ; Double 6] [Double 2] {f1=0 ; f2=2} 1 0
[{numero=1 ; main=[Double 0 ; Simple (2,6)] ; jeu_valide=false ; peut_rejouer=false}] [Simple (1,2) ; Double 6] [Double 2] {f1=1 ; f2=1} 1 0
*)

let rec jouerCPU joueurs pioche table faces_jouables nj ndernier pile =
   try
      let joueur_actuel = fst (trouver_joueur nj joueurs) in
      let dom = premier_jouable joueur_actuel.main faces_jouables in
      match dom with
         | Double _ ->
            let (joueurs',pioche',table',faces_jouables',ndernier',pile') =
        jouer'' joueurs pioche table faces_jouables nj dom 99 ndernier pile
     in jouerCPU joueurs' pioche' table' faces_jouables' nj ndernier' pile'
         | _ ->
        jouer'' joueurs pioche table faces_jouables nj dom 99 ndernier pile
   with
      Failure "premier_jouable : aucun domino n'est jouable" ->
                     (joueurs,pioche,table,faces_jouables,ndernier,pile) ;;





(* Tests des fonctions *)

premier_jouable [Double 0] {f1=1 ; f2=2} ;;

premier_jouable [Double 0 ; Double 1] {f1=1 ; f2=2} ;;


jouerCPU [{numero=1 ; main=[Double 0 ; Simple (2,6)] ; jeu_valide=false ; peut_rejouer=false}] [Simple (1,2) ; Double 6] [Double 2] {f1=2 ; f2=2} 1 0 pile ;;

jouerCPU [{numero=1 ; main=[Double 0 ; Simple (2,6)] ; jeu_valide=false ; peut_rejouer=false}] [Simple (1,2) ; Double 6] [Double 2] {f1=0 ; f2=2} 1 0 pile ;;

jouerCPU [{numero=1 ; main=[Double 0 ; Simple (2,6)] ; jeu_valide=false ; peut_rejouer=false}] [Simple (1,2) ; Double 6] [Double 2] {f1=1 ; f2=1} 1 0 pile ;;





(* ===================================================================== *)










(* QUESTION 6 *)
(*
cr�ation d'un joueur S-CPU, qui joue en d�veloppant une strat�gie :
= recherche automatique dans la main du joueur S-CPU du domino le plus judicieux � jouer :
  le domino est jou� afin d'essayer de bloquer les autres adversaires
  (d'o� la prise en compte de la variable face_preferee et des dominos d�j� pos�s sur la table)
= le joueur S-CPU joue s'il peut jouer, ou passe son tour
= s'il peut rejouer, le joueur S-CPU rejoue tout de suite
= le joueur S-CPU cherche � jouer en priorit� des doubles
*)




(* Fonctions *)

(* interface jouables :
type : domino list -> faces -> domino list
arg : jmain (la main du joueur) ; faces_jouables
contexte : la fonction domino_jouable
pr� : un domino est jouable
post : jouables jmain faces_jouables = la liste des dominos jouables
       contenus dans jmain
raises : erreur si aucun domino n'est jouable
rem : utilise une fonction auxiliaire r�cursive
tests : [Double 0] {f1=1 ; f2=2}
        [Double 0 ; Double 1 ; Simple (1,2)] {f1=1 ; f2=2}
*)

let jouables jmain faces_jouables =
   let rec aux_jouables lrestant ljouables = match lrestant with
      | [] -> if ljouables<>[] then ljouables
              else failwith "jouables : aucun domino n'est jouable"
      | d::fin -> if domino_jouable faces_jouables d
                  then aux_jouables fin (d::ljouables)
                  else aux_jouables fin ljouables
   in aux_jouables jmain [] ;;





(* interface trier_dominos :
type : domino list -> (domino list * domino list)
arg : ldom (liste de dominos)
post : trier_dominos ldom = le couple constitu� de la sous-liste des
       simples et de la sous-liste des doubles
tests : [], [Double 0 ; Simple (0,1) ; Simple (1,2)]
*)

let rec trier_dominos ldom = match ldom with
   | [] -> ([],[])
   | (Simple _ as dom)::fin ->
                            let (ls,ld) = trier_dominos fin in (dom::ls,ld)
   | (Double _ as dom)::fin ->
                         let (ls,ld) = trier_dominos fin in (ls,dom::ld) ;;





(* interface encore_jouables :
type : domino -> int -> domino list -> domino list -> faces -> int
arg : dom (domino jouable) ; f (face � jouer) ;
      jmain (main compl�te du joueur) ; table ; faces_jouables
contexte : les fonctions enlever_domino, actualiser_faces et jouables
post : encore_jouables dom f jmain table faces_jouables = le nombre de
       dominos qui pourront �tre jou�s par les autres joueurs, si le joueur
       de main jmain pose le domino dom sur la face f
tests : (Simple (0,1)) 1 [Double 0 ; Double 1 ; Simple (0,1) ; Simple (3,4) ; Double 6] [Simple (1,3) ; Simple (2,3) ; Double 2] {f1=1 ; f2=2}
*)

let encore_jouables dom f jmain table faces_jouables =
(* mises � jour dans le cas o� l'on joue le domino dom sur la face f *)
   let jmain' = enlever_domino dom jmain in
   let table' = dom::table in
   let faces_jouables' = actualiser_faces faces_jouables dom f in
(* d�compte des dominos pouvant alors �tre jou�s par les autres joueurs *)
   let a=faces_jouables'.f1 and b=faces_jouables'.f2 in
   let total_jouables = if a=b then 7 else (2*7)-1 in
   let deja_joues_table = List.length (jouables table' faces_jouables') in
   let jouables_joueur = List.length (jouables jmain' faces_jouables') in
   total_jouables - deja_joues_table - jouables_joueur ;;






(* interface scores :
type : domino list -> domino list -> domino list -> faces
       -> (domino * int * int) list
arg : ldom (liste de dominos) ; jmain (main compl�te du joueur) ; table ;
      faces_jouables
contexte : la fonction encore_jouables
post : scores ldom jmain table faces_jouables =
       la liste des triplets (domino,face jou�e,score associ�)
tests : [Double 2 ; Simple (1,2) ; Simple (1,6)] [Double 2 ; Simple (1,2) ; Simple (1,6) ; Simple (5,6)] [Simple (2,3) ; Simple (1,3) ; Simple (1,4) ; Simple (3,4) ; Simple (3,5) ; Simple (1,5)] {f1=2 ; f2=1}
*)

let rec scores ldom jmain table faces_jouables = match (ldom,faces_jouables) with
   | ([],_) -> []
   | ((Simple(x,y) as d)::fin,{f1=a ; f2=b}) ->
      if ((x=a)&&(y=b))||((x=b)&&(y=a))
      then (d,a,encore_jouables d a jmain table faces_jouables)
           ::(d,b,encore_jouables d b jmain table faces_jouables)
           ::(scores fin jmain table faces_jouables)
      else if (x=a)||(x=b)
           then (d,x,encore_jouables d x jmain table faces_jouables)
                ::(scores fin jmain table faces_jouables)
           else (d,y,encore_jouables d y jmain table faces_jouables)
                ::(scores fin jmain table faces_jouables)
   | ((Double x as d)::fin,{f1=a ; f2=b}) ->
      (d,x,encore_jouables d x jmain table faces_jouables)
      ::(scores fin jmain table faces_jouables) ;;





(* interface score_min :
type : ('a * 'b * 'c) list -> ('a * 'b)
arg : sc, liste des triplets (domino, face jou�e, score associ�)
pr� : sc suppos�e non vide
post : score_min sc = le couple constitu� du domino et de la face � jouer
       de score minimum
raises : erreur si sc=[]
rem : utilise une fonction auxiliaire r�cursive
tests : [], [(Simple (1,2),2,1) ; (Simple (1,2),1,6) ; (Simple (1,6),1,4)]
*)

let score_min sc =
   let rec aux_score_min lrestant couple_min imin = match lrestant with
      | [] -> couple_min
      | (d,f,x)::fin -> if x < imin then aux_score_min fin (d,f) x
                        else aux_score_min fin couple_min imin
   in
      if sc=[] then invalid_arg "score_min : liste vide"
      else let (d0,f0,x0) = List.hd sc in
                                   aux_score_min (List.tl sc) (d0,f0) x0 ;;





(* interface jouerSCPU :
type : joueur' list -> domino list -> domino list -> faces -> int -> int ->
       (joueur' list * domino list * domino list * faces * int) Stack.t
       -> (joueur' list * domino list * domino list * faces * int *
          (joueur' list * domino list * domino list * faces * int) Stack.t)
arg : joueurs ; pioche ; table ; faces_jouables ;
      nj (le num�ro du joueur qui joue) ;
      ndernier (le num�ro du dernier joueur � avoir jou�) ; pile
contexte : les fonctions trouver_joueur, jouables, trier_dominos, scores,
           score_min et jouer''
post : jouerSCPU joueurs pioche table faces_jouables nj ndernier =
       la mise � jour des variables : liste des joueurs (mains), pioche,
       table, faces jouables et ndernier, et de la pile environnement
rem1 : jouerSCPU joue le meilleur domino possible, ou passe
rem2 : la fonction jouerSCPU est r�cursive, car le joueur S-CPU tente de
       rejouer (par convention) d�s qu'il le peut
rem3 : jouerSCPU tente en priorit� de jouer des doubles
tests : [{numero=1 ; main=[Double 1 ; Simple (2,6)] ; jeu_valide=false ; peut_rejouer=false}] [Simple (1,2) ; Double 5] [Simple (1,3) ; Simple (3,6) ; Double 6 ; Simple (4,6) ; Simple (2,4)] {f1=1 ; f2=2} 1 0
*)

let rec jouerSCPU joueurs pioche table faces_jouables nj ndernier pile =
   try
      let joueur_actuel = fst (trouver_joueur nj joueurs) in
      let main_actuelle = joueur_actuel.main in
      let dom_possibles = jouables main_actuelle faces_jouables in
                                            (* tous les dominos jouables *)
      let dom_possibles' =  (* on tente en priorit� de jouer des doubles *)
         let (simples_possibles,doubles_possibles) =
         trier_dominos dom_possibles in
         if doubles_possibles<>[] then doubles_possibles
         else simples_possibles
      in
      let sc = scores dom_possibles' main_actuelle table faces_jouables in
      let (dom,face_preferee) = score_min sc in
      match dom with
         | Double _ ->
            let (joueurs',pioche',table',faces_jouables',ndernier',pile') =
            jouer'' joueurs pioche table faces_jouables nj dom
                    face_preferee ndernier pile
    in jouerSCPU joueurs' pioche' table' faces_jouables' nj ndernier' pile'
         | _ -> jouer'' joueurs pioche table faces_jouables nj dom
                        face_preferee ndernier pile
   with
      Failure "jouables : aucun domino n'est jouable" ->
                     (joueurs,pioche,table,faces_jouables,ndernier,pile) ;;





(* Tests des fonctions *)

jouables [Double 0] {f1=1 ; f2=2} ;;

jouables [Double 0 ; Double 1 ; Simple (1,2)] {f1=1 ; f2=2} ;;


encore_jouables (Simple (0,1)) 1 [Double 0 ; Double 1 ; Simple (0,1) ; Simple (3,4) ; Double 6] [Simple (1,3) ; Simple (2,3) ; Double 2] {f1=1 ; f2=2} ;;


trier_dominos [] ;;

trier_dominos [Double 0 ; Simple (0,1) ; Simple (1,2)] ;;


scores [Double 2 ; Simple (1,2) ; Simple (1,6)] [Double 2 ; Simple (1,2) ; Simple (1,6) ; Simple (5,6)] [Simple (2,3) ; Simple (1,3) ; Simple (1,4) ; Simple (3,4) ; Simple (3,5) ; Simple (1,5)] {f1=2 ; f2=1} ;;


score_min [] ;;

score_min [(Simple (1,2),2,1) ; (Simple (1,2),1,6) ; (Simple (1,6),1,4)] ;;


jouerSCPU [{numero=1 ; main=[Double 1 ; Simple (2,6)] ; jeu_valide=false ; peut_rejouer=false}] [Simple (1,2) ; Double 5] [Simple (1,3) ; Simple (3,6) ; Double 6 ; Simple (4,6) ; Simple (2,4)] {f1=1 ; f2=2} 1 0 pile ;;





