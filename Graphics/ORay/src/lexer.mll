(*
A GML program is written using a subset of the printable ASCII
character set, plus the space, tab, return, linefeed and vertical tab
characters. 
*)

{
open Parser

(*
A subset of the identifiers are used as predefined operators, which
may not be rebound. A list of the operators can be found in the
appendix.
*)
(* XXX *)
}

(*
The space, tab, return, linefeed and vertical tab characters are
called whitespace.
*)
let whitespace = [' ' '\010' '\013' '\009' '\012']

(*
Identifiers must start with an letter and can contain letters, digits,
dashes (`-'), and underscores (`_').
*)
let letter = ['a'-'z' 'A'-'Z']
let digit = ['0'-'9']
let identchar = letter | digit | ['-' '_']
let end_of_line = ['\010' '\011' '\013']
(* ASCII printable characters *)
let printable = ['!'-'~']
(* All whitespaces but newline *)
let blank = [ '\009' ' ']
(*
Strings are written enclosed in double quotes and may contain
any printable character other than the double quote.
*)
let stringchar = ['!' '#'-'~'] | blank

rule token = parse
    whitespace +
      { token lexbuf }
(*
Any occurrence of the character ``%'' not inside a string literal (see
below) starts a comment, which runs to the end of the current
line. Comments are treated as whitespace when tokenizing the input
file.
*)
  | '%' (printable|blank)* end_of_line
      { token lexbuf }
  | letter identchar*
      { IDENT (Lexing.lexeme lexbuf) }
(*
A binder is an identifier prefixed with a `/' character.
*)
  | '/' letter identchar*
      { let s = Lexing.lexeme lexbuf in
        BINDER (String.sub s 1 (String.length s - 1)) }
(* The characters %, [, ], {, } are special characters. *)
  | '['
      { LBRACKET }
  | ']'
      { RBRACKET }
  | '{'
      { LBRACE }
  | '}'
      { RBRACE }
(*
Strings are written enclosed in double quotes and may contain
any printable character other than the double quote. There are no
escape sequences.
*)
  | '"' stringchar * '"'
      { let s = Lexing.lexeme lexbuf in
        STRING (String.sub s 1 (String.length s - 2)) }
(*
Numbers are either integers or reals. The syntax of numbers is given
by the following grammar:
 Number 
    ::=
      Integer 
    | 
      Real 
  
 Integer 
    ::= 
      -opt DecimalNumber 
  
 Real 
    ::= 
      -opt DecimalNumber . DecimalNumber Exponentopt 
    | 
      -opt DecimalNumber Exponent 
  
 Exponent 
    ::= 
      e -opt DecimalNumber 
    | 
      E -opt DecimalNumber 

where a DecimalNumber is a sequence of one or more decimal digits.
*)
  | ['-']? digit+
      { INT (int_of_string (Lexing.lexeme lexbuf)) }
  | ['-']? digit+ ('.' digit+)? (['e' 'E'] digit+)?
      { FLOAT (float_of_string (Lexing.lexeme lexbuf)) }
  | eof
      { EOF }
