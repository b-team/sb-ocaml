%{
%}

%token <string> IDENT
%token <string> BINDER
%token <string> STRING
%token <int> INT
%token <float> FLOAT
%token LBRACKET
%token RBRACKET
%token LBRACE
%token RBRACE
%token EOF

%start program
%type <Program.t list> program

%%

program:
  tokenlist EOF
     { List.rev $1 }
;

tokenlist:
    tokenlist tokengroup
      { $2 :: $1 }
  | /* empty */
      { [] }
;

tokengroup:      
    token
      { $1 }
  | LBRACKET tokenlist RBRACKET
      { Program.Arr (List.rev $2) }
  | LBRACE tokenlist RBRACE
      { Program.Fun (List.rev $2) }
;

token:
    IDENT
      { Program.translate $1 }
  | BINDER
      { Program.Binder $1 }
  | INT
      { Program.Int $1 }
  | FLOAT
      { Program.Float $1 }
  | STRING
      { Program.String $1 }
;
