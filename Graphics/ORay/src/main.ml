
let _ =
  let p = Parser.program Lexer.token (Lexing.from_channel stdin) in
  if List.mem "-compile" (Array.to_list Sys.argv) then
    let chan = open_out "compiled.ml" in
    let str = Compile.f p in
    let _ = output_string chan str in
    let _ = close_out chan in
    if Unix.WEXITED 0 = Unix.system
	("cd support && " ^
	 "ocamlopt.opt -o ../compiled -unsafe " ^
	 "ppm.cmx math.cmx program.cmx matrix.cmx render.cmx " ^
	 "../compiled.ml && " ^
	 "cd .. && " ^
	 "./compiled " ^
	 if List.mem "-all" (Array.to_list Sys.argv)
	 then "-all"
	 else "")
    then ()
    else
      (prerr_string
	 ("compile failed: falling back\n" ^
	  "(if this message appears too often, there might be a bug in the run script)\n");
       Eval.f p (* fallback *))
  else
    Eval.f p
