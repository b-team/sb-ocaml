type saved

val checkpoint : unit -> saved
val start : saved -> unit
