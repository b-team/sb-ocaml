open Graphics

open GeoName

let round x =
  let rest = x -. (floor x) in
  if rest < 0.5 then truncate x
  else (truncate x) + 1

type flag_object = DRAW | NO_DRAW | ANIMATION

type renderer = NORMAL | ANTIALIAS | PS

let render = ref ANTIALIAS

module Draw =
  struct
    let draw_point (x,y) color name =
      begin
	GeoColor.set color;
	fill_circle x y 4;
	moveto (x+6) (y+6);
	draw_string name;
	GeoColor.set GeoColor.black
      end
    let draw_line (x1,y1) (x2,y2) color =
      begin
	GeoColor.set color;
        draw_poly_line [|(x1,y1);(x2,y2)|]
      end
    let draw_circle (x,y) r color =
      begin
	GeoColor.set color;
        draw_circle x y r
      end
  end

module DrawAntialias =
  struct
    let draw_point (x,y) color name =
      begin 
        GeoColor.set color;
	Antialias.point (x,y) color;
	moveto (x+6) (y+6);
	draw_string name;
        GeoColor.set GeoColor.black
      end
    let draw_line (x1,y1) (x2,y2) color =
      begin
        Antialias.line (x1,y1) (x2,y2) color
      end
    let draw_circle (x,y) r color =
      	begin
	  Antialias.circle (x,y) r color
        end
  end

module DrawPs =
  struct
    let draw_point (x,y) color name =
      begin
	GeoColor.setPs color;
        GraphicsPs.fill_circle x y 4;
        GraphicsPs.moveto (x+6) (y+6);
        GraphicsPs.draw_string name;
        GeoColor.setPs GeoColor.black
      end
    let draw_line (x1,y1) (x2,y2) color =
      begin
	GeoColor.setPs color;
        GraphicsPs.draw_poly_line [|(x1,y1);(x2, y2)|]
      end
    let draw_circle (x,y) r color =
      begin
	GeoColor.setPs color;
	GraphicsPs.draw_circle x y r
      end
  end

module type DrawGeneric =
  sig
    val draw_point : (int * int) -> GeoColor.t -> string -> unit
    val draw_line : (int * int) -> (int * int) -> GeoColor.t -> unit
    val draw_circle : (int * int) -> int -> GeoColor.t -> unit
  end

module DrawFunc =
  functor (Q: DrawGeneric) -> struct    
    class point (x_init,y_init) (name: string) c (flag: flag_object) =
    object(self)
      initializer self#draw ()
      val mutable x = x_init
      val mutable y = y_init
      val mutable s = name
      val mutable flag = flag
      val mutable color = c
      method get_x () = x
      method get_y () = y
      method set_x xval = x <- xval
      method set_y yval = y <- yval
      method get_name () = s
      method get_color () = color
      method draw () =
	if flag=NO_DRAW then ()
	else Q.draw_point (x,y) color name
      method redraw () = Q.draw_point (x,y) color name
    end
      
    class line (p1: point) (p2: point) (name: string) c (flag: flag_object) =
    object(self)
      initializer self#draw ()
      val mutable a = p1
      val mutable b = p2
      val mutable s = name
      val mutable flag = flag
      val mutable color = c
      method get_a () = a
      method get_b () = b
      method get_dx () = a#get_x () - b#get_x ()
      method get_dy () = a#get_y () - b#get_y ()
      method get_m () = (float)(self#get_dy ()) /. (float)(self#get_dx ())
      method get_p () = (float)(a#get_x () * b#get_y () - b#get_x () * a#get_y ()) /. (float)(self#get_dx ())
      method get_name () = s
      method get_color () = color
      method draw () =
	if flag=NO_DRAW then ()
	else
          begin
            let (x1,y1) = min (a#get_x (),a#get_y ()) (b#get_x (),b#get_y ()) in
            let (x2,y2) = max (a#get_x (),a#get_y ()) (b#get_x (),b#get_y ()) in
            let dx = x2 - x1 in
            let dy = y2 - y1 in
	      Q.draw_line (x2+dx,y2+dy) (x1-dx,y1-dy) color
          end
      method redraw () = 
	let (x1,y1) = min (a#get_x (),a#get_y ()) (b#get_x (),b#get_y ()) in
	let (x2,y2) = max (a#get_x (),a#get_y ()) (b#get_x (),b#get_y ()) in
	let dx = x2 - x1 in
	let dy = y2 - y1 in
	  Q.draw_line (x2+dx,y2+dy) (x1-dx,y1-dy) color
    end
      
    class segment (p1: point) (p2: point) (name: string) c (flag: flag_object) =
    object
      inherit line p1 p2 name c flag
      method draw () =
	if flag=NO_DRAW then ()
	else
          begin
            let (x1,y1) = (a#get_x (),a#get_y ()) in
            let (x2,y2) = (b#get_x (),b#get_y ()) in
              Q.draw_line (x2,y2) (x1,y1) color
          end
      method redraw () =
        begin
          let (x1,y1) = (a#get_x (),a#get_y ()) in
          let (x2,y2) = (b#get_x (),b#get_y ()) in
            Q.draw_line (x2,y2) (x1,y1) color
        end
    end
      
    class circle (p1: point) (p2: point) (name: string) c (flag: flag_object) =
    object(self)
      initializer self#draw ()
      val mutable center = p1;
      val mutable radius = round (sqrt (((float_of_int (p1#get_x() - p2#get_x())**2.)) +. (((float_of_int (p1#get_y() - p2#get_y())**2.)))))
      val mutable s = name
      val mutable flag = flag
      val mutable color = c
      method get_center () = center
      method get_radius () = radius
      method set_radius r = radius <- r 
      method get_name () = s
      method get_color () = color
      method draw () =
      if flag=NO_DRAW then ()
      else Q.draw_circle (center#get_x (),center#get_y ()) radius color
      method redraw () = Q.draw_circle (center#get_x (),center#get_y ()) radius color
    end
      
    class window (h_init,w_init) =
    object
      val mutable h = h_init
      val mutable w = w_init
      initializer
	begin
	  open_graph (" "^(string_of_int h)^"x"^(string_of_int w));
	  set_window_title GeoMessage.window_title;
	end
      method close () = close_graph ()
      method clear () = clear_graph ()
      method key () = read_key()
    end
      
    class figure (x,y) =
    object(self)
      inherit window (x,y) as w
      val mutable point_list = []
      val mutable line_list = []
      val mutable circle_list = []
      method add_point (p: point) = point_list <- p::point_list
      method add_line (l: line) = line_list <- l::line_list
      method add_segment (s: segment) = line_list <- s::line_list
      method add_circle (c: circle) = circle_list <- c::circle_list
      method purge =
	begin
	  (*point_list <- [];*)
	  line_list <- [];
	  circle_list <- []
	end
      method remove_point name =
	point_list <- List.filter (fun o -> (o#get_name ()) <> name) point_list
      method remove_line name =
	line_list <- List.filter (fun o -> (o#get_name ()) <> name) line_list
      method remove_circle name =
	circle_list <- List.filter (fun o -> (o#get_name ()) <> name) circle_list
	  
      method get_point name = List.find (fun o -> (o#get_name ()) = name) point_list
      method get_line name =
	let len = (String.length name)-1 in
          if name.[0]='(' && name.[len]=')' then
            begin
              let i = String.index name ',' in
              let a = String.sub name 1 (i-1) in
              let b = String.sub name (i+1) (len-i-1) in
              let rev_name = "("^b^","^a^")" in
		List.find (fun o -> (o#get_name ()) = name || (o#get_name ()) = rev_name) line_list
            end
          else 
            if name.[0]='[' && name.[len]=']' then
              begin
		let i = String.index name ',' in
		let a = String.sub name 1 (i-1) in
		let b = String.sub name (i+1) (len-i-1) in
		let rev_name = "["^b^","^a^"]" in
		  List.find (fun o -> (o#get_name ()) = name || (o#get_name ()) = rev_name) line_list
              end
            else List.find (fun o -> (o#get_name ()) = name) line_list
      method get_circle name = List.find (fun o -> (o#get_name ()) = name) circle_list
      method get_point_list () = point_list
      method get_line_list () = line_list
      method get_circle_list () = circle_list
      method move_on_right () = List.iter (fun p -> p#set_x (p#get_x() + 5)) point_list
      method move_on_left () = List.iter (fun p -> p#set_x (p#get_x() - 5)) point_list
      method move_on_top () = List.iter (fun p -> p#set_y (p#get_y() + 5)) point_list
      method move_on_bottom () = List.iter (fun p -> p#set_y (p#get_y() - 5)) point_list
      method zoom_down () = 
	begin
        List.iter (fun p -> p#set_x (round(float(p#get_x())/.2.));p#set_y (round(float(p#get_y())/.2.))) point_list;
          List.iter (fun c -> c#set_radius (round(float(c#get_radius())/.2.))) circle_list
	end
      method zoom_up () =
	begin
          List.iter (fun p -> p#set_x (round(float(p#get_x())*.2.));p#set_y (round(float(p#get_y())*.2.))) point_list;
          List.iter (fun c -> c#set_radius (round(float(c#get_radius())*.2.))) circle_list
	end
      method action () =
      let k = w#key() in
        match k with
          | '4' -> self#move_on_left ();w#clear();self#draw()
          | '6' -> self#move_on_right ();w#clear();self#draw()
          | '8' -> self#move_on_top ();w#clear();self#draw()
          | '2' -> self#move_on_bottom ();w#clear();self#draw()
          | '+' -> self#zoom_up ();w#clear();self#draw()
          | '-' -> self#zoom_down ();w#clear();self#draw()
          | _ -> ()
    method draw () =
      begin
        List.iter (fun o -> o#draw ()) line_list;
        List.iter (fun o -> o#draw ()) circle_list;
        List.iter (fun o -> o#draw ()) point_list;
      end
    method redraw () =
      begin
	w#clear();
        List.iter (fun o -> o#redraw()) point_list;
        List.iter (fun o -> o#redraw()) line_list;
        List.iter (fun o -> o#redraw()) circle_list
      end
    end
      
  end

module type GeoDrawModule =
  sig
    class point :
      int * int ->
      string ->
      GeoColor.t ->
      flag_object ->
      object
        val mutable color : GeoColor.t
        val mutable flag : flag_object
        val mutable s : string
        val mutable x : int
        val mutable y : int
        method draw : unit -> unit
        method get_name : unit -> string
	method get_color : unit -> GeoColor.t
        method get_x : unit -> int
        method get_y : unit -> int
        method redraw : unit -> unit
        method set_x : int -> unit
        method set_y : int -> unit
      end
    class line :
      point ->
      point ->
      string ->
      GeoColor.t ->
      flag_object ->
      object
        val mutable a : point
        val mutable b : point
        val mutable color : GeoColor.t
        val mutable flag : flag_object
        val mutable s : string
        method draw : unit -> unit
        method get_a : unit -> point
        method get_b : unit -> point
        method get_dx : unit -> int
        method get_dy : unit -> int
        method get_m : unit -> float
        method get_name : unit -> string
        method get_p : unit -> float
	method get_color : unit -> GeoColor.t
        method redraw : unit -> unit
      end
    class segment :
      point ->
      point ->
      string ->
      GeoColor.t ->
      flag_object ->
      object
        val mutable a : point
        val mutable b : point
        val mutable color : GeoColor.t
        val mutable flag : flag_object
        val mutable s : string
        method draw : unit -> unit
        method get_a : unit -> point
        method get_b : unit -> point
        method get_dx : unit -> int
        method get_dy : unit -> int
        method get_m : unit -> float
        method get_name : unit -> string
        method get_p : unit -> float
	method get_color : unit -> GeoColor.t
        method redraw : unit -> unit
      end
    class circle :
      point ->
      point ->
      string ->
      GeoColor.t ->
      flag_object ->
      object
        val mutable center : point
        val mutable color : GeoColor.t
        val mutable flag : flag_object
        val mutable radius : int
        val mutable s : string
        method draw : unit -> unit
        method get_center : unit -> point
        method get_name : unit -> string
        method get_radius : unit -> int
	method get_color : unit -> GeoColor.t
        method redraw : unit -> unit
        method set_radius : int -> unit
      end
    class window :
      int * int ->
      object
        val mutable h : int
        val mutable w : int
        method clear : unit -> unit
        method close : unit -> unit
        method key : unit -> char
      end
    class figure :
      int * int ->
      object
        val mutable circle_list : circle list
        val mutable h : int
        val mutable line_list : line list
        val mutable point_list : point list
        val mutable w : int
        method action : unit -> unit
        method add_circle : circle -> unit
        method add_line : line -> unit
        method add_point : point -> unit
        method add_segment : segment -> unit
        method clear : unit -> unit
        method close : unit -> unit
        method draw : unit -> unit
        method get_circle : string -> circle
        method get_circle_list : unit -> circle list
        method get_line : string -> line
        method get_line_list : unit -> line list
        method get_point : string -> point
        method get_point_list : unit -> point list
        method key : unit -> char
        method move_on_bottom : unit -> unit
        method move_on_left : unit -> unit
        method move_on_right : unit -> unit
        method move_on_top : unit -> unit
        method purge : unit
        method redraw : unit -> unit
        method remove_circle : string -> unit
        method remove_line : string -> unit
        method remove_point : string -> unit
        method zoom_down : unit -> unit
        method zoom_up : unit -> unit
      end
  end

(*module GeoDraw = DrawFunc (Drawing)

module GeoDrawAA = DrawFunc (DrawingAntialias)

module GeoDrawPs = DrawFunc (DrawingPs)
*)





