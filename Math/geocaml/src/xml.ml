(* $Id: xml.ml,v 1.1 2004/04/28 17:54:35 gmariano Exp $ *)

let encoding = " encoding=\"ISO-8859-1\""

let standalone = " standalone=\"no\""

let version = "<?xml version = \"1.0\""^encoding^standalone^"?>\n" 

let doctype = "<!DOCTYPE GeoCaml SYSTEM/PUBLIC \"geocaml.dtd\" >\n"

let name s =
  " name="^"\""^s^"\""

let coordinates (x,y) =
  let s = "\"("^string_of_int(x)^","^string_of_int(y)^")\"" in
  " coordinates="^s

let balise name attribut channel =
  output_string channel ("<"^name^attribut^"/>\n")

let open_balise name attribut channel =
  output_string channel ("<"^name^attribut^">\n")

let close_balise name channel =
  output_string channel ("</"^name^">\n")

let do_nothing (channel: out_channel) = ()

let indent level channel =
  for i=1 to level do output_string channel "\t" done

type figure = {point_list: string list ref;
               line_list: string list ref;
               circle_list: string list ref;}

let my_figure = {point_list=ref [];line_list=ref [];circle_list=ref []}

let is_a_declared_point s = List.exists (fun p -> s=p) !(my_figure.point_list)

let is_a_declared_line s = List.exists (fun l -> s=l) !(my_figure.line_list)

let is_a_declared_circle s = List.exists (fun c -> s=c) !(my_figure.circle_list)

let add_point name = my_figure.point_list := name::!(my_figure.point_list)

let add_line name = my_figure.line_list := name::!(my_figure.line_list)

let add_circle name = my_figure.circle_list := name::!(my_figure.circle_list)

(* point *)
let point_declaration s (x,y) level channel =
  add_point s;
  indent level channel;balise "point" ((name s)^(coordinates (x,y))) channel

let point s level channel =
  indent level channel;balise "point" (name s) channel

(* line *)
let line_declaration s (point1: out_channel -> unit) (point2: out_channel -> unit) level channel =
  add_line s;
  indent level channel;open_balise "line" (name s) channel;
  point1 channel;
  point2 channel;
  indent level channel;close_balise "line" channel

let line s level channel =
  indent level channel;balise "line" (name s) channel

(* circle *)
let circle_declaration s (point1: out_channel -> unit) (point2: out_channel -> unit) level channel =
  add_circle s;
  indent level channel;open_balise "circle" (name s) channel;
  point1 channel;
  point2 channel;
  indent level channel;close_balise "circle" channel

let circle s level channel =
  indent level channel;balise "circle" (name s) channel

(* inter_line_line *)
let inter_line_line s (line1: out_channel -> unit) (line2: out_channel -> unit) level channel =
  add_point s;
  indent level channel;open_balise "inter_line_line" (name s) channel;
  line1 channel;
  line2 channel;
  indent level channel;close_balise "inter_line_line" channel

(* point_on_line *)
let point_on_line s (line1: out_channel -> unit) level channel =
  add_point s;
  indent level channel;open_balise "point_on_line" (name s) channel;
  line1 channel;
  indent level channel;close_balise "point_on_line" channel

(* point_on_circle *)
let point_on_circle s (circle1: out_channel -> unit) level channel =
  add_point s;
  indent level channel;open_balise "point_on_circle" (name s) channel;
  circle1 channel;
  indent level channel;close_balise "point_on_circle" channel

(* middle *)
let middle s (point1: out_channel -> unit) (point2: out_channel -> unit) level channel =
  add_point s;
  indent level channel;open_balise "middle" (name s) channel;
  point1 channel;
  point2 channel;
  indent level channel;close_balise "middle" channel

(* inter_circle_line *)
let inter_circle_line s (circle1: out_channel -> unit) (line1: out_channel -> unit) level channel =
  add_point s;
  indent level channel;open_balise "inter_circle_line" (name s) channel;
  indent level channel;circle1 channel;
  indent level channel;line1 channel;
  indent level channel;close_balise "inter_circle_line" channel

(* inter_circle_circle *)
let inter_circle_circle s (circle1: out_channel -> unit) (circle2: out_channel -> unit) level channel =
  add_point s;
  indent level channel;open_balise "inter_circle_circle" (name s) channel;
  circle1 channel;
  circle2 channel;
  indent level channel;close_balise "inter_circle_circle" channel

(* segment *)
let segment s (point1: out_channel -> unit) (point2: out_channel -> unit) level channel =
  add_line s;
  indent level channel;open_balise "segment" (name s) channel;
  point1 channel;
  point2 channel;
  indent level channel;close_balise "segment" channel

(* perpendicular *)
let perpendicular s (line1: out_channel -> unit) (point1: out_channel -> unit) level channel =
  add_line s;
  indent level channel;open_balise "perpendicular" (name s) channel;
  line1 channel;
  point1 channel;
  indent level channel;close_balise "perpendicular" channel

(* perpendicular bissector *)
let perpendicular_bissector s (line1: out_channel -> unit) level channel =
  add_line s;
  indent level channel;open_balise "perpendicular_bissector" (name s) channel;
  line1 channel;
  indent level channel;close_balise "perpendicular_bissector" channel

(* parallel *)
let parallel s (line1: out_channel -> unit) (point1: out_channel -> unit) level channel =
  add_line s;
  indent level channel;open_balise "parallel" (name s) channel;
  line1 channel;
  point1 channel;
  indent level channel;close_balise "parallel" channel

(* bissecting *)
let bissecting s (point1: out_channel -> unit) (point2: out_channel -> unit) (point3: out_channel -> unit) level channel =
  add_line s;
  indent level channel;open_balise "bissecting" (name s) channel;
  point1 channel;
  point2 channel;
  point3 channel;
  indent level channel;close_balise "bissecting" channel

;;
