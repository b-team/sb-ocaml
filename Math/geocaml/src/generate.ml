(* $Id: generate.ml,v 1.1 2004/04/28 17:54:35 gmariano Exp $ *)

(* Generates names for geometric object *)
let name base =
  let i = ref (-1) in
  fun() -> (incr i ; base^"_"^(string_of_int !i))

let point_name = name "p"

let line_name = name "l"

let circle_name = name "c"

(* Generates coordinates for point *)

let xy (xtop,ytop) (xbottom,ybottom) =
  let width = abs (xtop - xbottom) in
  let height = abs (ytop - ybottom) in
  let (x,y) = (Random.int width,Random.int height) in ((min xtop xbottom)+x,(min ytop ybottom)+y)

;;
