
type t = int * int * int

let red = (203,20,34)

let black = (0,0,0)

let blue = (48,7,180)

let green = (27,145,82)

let orange = (238,132,28)

let yellow = (226,211,66)

let pink = (237,49,227)

let transp = (-1,-1,-1)

let set (r,g,b) =
  let c = Graphics.rgb r g b in
    if (r,g,b) = transp then Graphics.set_color Graphics.transp
    else Graphics.set_color c

let setPs (r,g,b) =
  let c = GraphicsPs.rgb r g b in
    if (r,g,b) = transp then GraphicsPs.set_color GraphicsPs.transp
    else GraphicsPs.set_color c

