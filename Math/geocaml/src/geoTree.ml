(* $Id: geoTree.ml,v 1.1 2004/04/28 17:54:35 gmariano Exp $ *)
open GeoType

open GeoName

let my_figure = {point_list=ref [];line_list=ref [];circle_list=ref []}

exception Error

let add_point p name col fig = fig.point_list := (name,col,p)::!(fig.point_list)

let rec point_depend_of abr name =
  match abr with
      Point s -> s = name
    | Inter_line_line (s,(l1,l2)) -> (s = name) || (line_depend_of l1 name) || (line_depend_of l2 name)
    | Point_on_line (s,l) -> (s = name) || (line_depend_of l name)
    | Point_on_circle (s,c) -> (s = name) || (circle_depend_of c name)
    | Middle (s,(p1,p2)) -> (s = name) || (point_depend_of p1 name) || (point_depend_of p2 name)
    | Inter_circle_line (s1,s2,(c,l)) -> (s1 = name) || (s2 = name) || (circle_depend_of c name) || (line_depend_of l name)
    | Inter_circle_circle (s1,s2,(c1,c2)) -> (s1 = name) || (s2 = name) || (circle_depend_of c1 name) || (circle_depend_of c2 name)
    | Construct_point p -> point_depend_of p name

and line_depend_of abr name =
  match abr with
      Line (s,(p1,p2)) -> (s = name) || (point_depend_of p1 name) || (point_depend_of p2 name)
    | Ruler l -> (line_depend_of l name)
    | Segment (s,(p1,p2)) -> (s = name) || (point_depend_of p1 name) || (point_depend_of p2 name)
    | Perpendicular (s,(l,p)) -> (s = name) || (line_depend_of l name) || (point_depend_of p name)
    | Perpendicular_segment (s,(l,p1,(p2,p3))) -> (s = name) || (line_depend_of l name) || (point_depend_of p1 name) || (point_depend_of p2 name) || (point_depend_of p3 name)
    | Perpendicular_bissector (s,l) -> (s = name) || (line_depend_of l name)
    | Parallel (s,(l,p)) -> (s = name) || (line_depend_of l name) || (point_depend_of p name)
    | Parallel_segment (s,(l,p1,(p2,p3))) -> (s = name) || (line_depend_of l name) || (point_depend_of p1 name) || (point_depend_of p2 name) || (point_depend_of p3 name)
    | Bissecting (s,(p1,p2,p3)) -> (s = name) || (point_depend_of p1 name) || (point_depend_of p2 name) || (point_depend_of p3 name)

and circle_depend_of abr name =
  match abr with
    | Circle (s,(p1,p2)) -> (s = name) || (point_depend_of p1 name) || (point_depend_of p2 name)
    | Compass c -> circle_depend_of c name

let suppr_obj name fig f = 
  begin
    fig.point_list := List.filter (fun (s,_,a) -> if point_depend_of a name then (f#remove_point s;false) else true) !(fig.point_list);
    fig.line_list := List.filter (fun (s,_,a) -> if line_depend_of a name then (f#remove_line s;false) else true) !(fig.line_list);
    fig.circle_list := List.filter (fun (s,_,a) -> if circle_depend_of a name then (f#remove_circle s;false) else true) !(fig.circle_list)
  end
    
let add_line l name col fig = fig.line_list := (name,col,l)::!(fig.line_list)

let add_circle c name col fig = fig.circle_list := (name,col,c)::!(fig.circle_list)

(*--------------------------------------------------------------------*)
let point name = Point name

let inter_line_line l1 l2 name = Inter_line_line (name,(l1,l2))

let inter_circle_line c l name1 name2 = Inter_circle_line (name1,name2,(c,l))

let inter_circle_circle c1 c2 name1 name2 = Inter_circle_circle (name1,name2,(c1,c2))

let point_on_line l name = Point_on_line (name,l)

let point_on_circle c name = Point_on_circle (name,c)

exception Get_point_error

let get_point_of o =
  match o with
  | Line (_,p) -> p
  | Segment (_,p) -> p
  | _ -> raise Get_point_error

let middle p1 p2 name = Middle (name,(p1,p2))

let middle2 seg name =
  let (a,b) = get_point_of seg in
  middle a b name

let line p1 p2 name = Line (name,(p1,p2))

let any_line name =
  let x = point (Generate.point_name ()) in
  let y = point (Generate.point_name ()) in
  line x y name

let segment p1 p2 name = Segment (name,(p1,p2))

let perpendicular l p name = Perpendicular (name,(l,p))

let perpendicular_seg l p (p1,p2) name = Perpendicular_segment (name,(l,p,(p1,p2)))

let parallel l p name = Parallel (name,(l,p))

let parallel_seg l p (p1,p2) name = Parallel_segment (name,(l,p,(p1,p2)))

let perpendicular_bissector l name = Perpendicular_bissector (name,l)

let bissecting p1 p2 p3 name = Bissecting (name,(p1,p2,p3))

let circle o p name = Circle (name,(o,p))

let any_circle name =
  let x = point (Generate.point_name ()) in
  let y = point (Generate.point_name ()) in
  circle x y name

let rec name_of_point p =
  match p with
  | Point s -> s
  | Inter_line_line (s,_) -> s
  | Point_on_line (s,_) -> s
  | Point_on_circle (s,_) -> s
  | Middle (s,_) -> s
  | Inter_circle_line (s,_,_) -> s
  | Inter_circle_circle (s,_,_) -> s
  | Construct_point p1 -> name_of_point p1

let rec expand_line l =
  match l with
  | Line (s,(p1,p2)) -> (p1,p2)
  | Ruler l1 -> expand_line l1
  | Segment (s,(p1,p2)) -> (p1,p2)
  | Perpendicular (s,(l1,p)) -> 
      let (p1,p2) = expand_line l1 in
      let c = Compass (if ((name_of_point p1)=(name_of_point p)) then circle p p2 (Generate.circle_name ())
                       else circle p p1 (Generate.circle_name ())) in
      let (name1,name2) = (Generate.point_name (),Generate.point_name ()) in 
      let a = Construct_point (inter_circle_line c l1 name1 name2) in
      let b = Construct_point (inter_circle_line c l1 name2 name1) in
      let c1 = Compass (circle a b (Generate.circle_name ())) in
      let c2 = Compass (circle b a (Generate.circle_name ())) in
      let (name3,name4) = (Generate.point_name (),Generate.point_name ()) in
      let e = inter_circle_circle c1 c2 name3 name4 in
      let f = inter_circle_circle c2 c1 name4 name3 in
        (e,f)
  | Perpendicular_segment (s,(l1,p,(_,_))) -> 
      let (p1,p2) = expand_line l1 in
      let c = Compass (if ((name_of_point p1)=(name_of_point p)) then circle p p2 (Generate.circle_name ())
                       else circle p p1 (Generate.circle_name ())) in
      let (name1,name2) = (Generate.point_name (),Generate.point_name ()) in 
      let a = Construct_point (inter_circle_line c l1 name1 name2) in
      let b = Construct_point (inter_circle_line c l1 name2 name1) in
      let c1 = Compass (circle a b (Generate.circle_name ())) in
      let c2 = Compass (circle b a (Generate.circle_name ())) in
      let (name3,name4) = (Generate.point_name (),Generate.point_name ()) in
      let e = inter_circle_circle c1 c2 name3 name4 in
      let f = inter_circle_circle c2 c1 name4 name3 in
        (e,f)
  | Perpendicular_bissector (s,l1) -> 
      let (p1,p2) = expand_line l1 in
      let c1 = Compass (circle p1 p2 (Generate.circle_name ())) in
      let c2 = Compass (circle p2 p1 (Generate.circle_name ())) in
      let (name1,name2) = (Generate.point_name (),Generate.point_name ()) in
      let a = inter_circle_circle c1 c2 name1 name2 in
      let b = inter_circle_circle c1 c2 name2 name1 in
        (a,b)
  | Parallel (s,(l1,p)) -> 
      let d = perpendicular l p (Generate.line_name ()) in
      let d2 = perpendicular d p s in
        expand_line d2
  | Parallel_segment (s,(l1,p,(_,_))) -> 
      let d = perpendicular l p (Generate.line_name ()) in
      let d2 = perpendicular d p s in
        expand_line d2
  | Bissecting (s,(p1,p2,p3)) -> raise Error

(* parcours de l arbre *)

module TreeToDrawing =
  functor (M: Drawing.DrawingObjModule) -> struct
    
    let rec draw_point p f col flag =
      match p with
	| Point s -> 
	    (try 
	       let x = f#get_point s in
		 if x#get_color () = GeoColor.transp then
		   (f#remove_point s;
		   M.point (x#get_x (),x#get_y ()) s col flag f)
		 else x 
	     with 
	       | Not_found -> 
		   M.point (Generate.xy (100,100) (540,380)) s col flag f)
	| Inter_line_line (s,(l1,l2)) -> 
	    (try 
	       let x = f#get_point s in
		 if x#get_color () = GeoColor.transp then
		   (f#remove_point s;raise Not_found)
		 else x 
	     with
	       | Not_found -> 
		   M.inter_ll 
		   (draw_line l1 f GeoColor.transp GeoGraphics.NO_DRAW) 
		   (draw_line l2 f GeoColor.transp GeoGraphics.NO_DRAW) s col flag f)
	| Inter_circle_line (s1,s2,(c,l)) -> 
	    (try 
	       let x = f#get_point s1 in
		 if x#get_color () = GeoColor.transp then
		   (f#remove_point s1;f#remove_point s2;raise Not_found)
		 else x 
	     with 
	       | Not_found -> 
		   let (x,_) = 
		     M.inter_lc 
		       (draw_line l f GeoColor.transp GeoGraphics.NO_DRAW) 
		       (draw_circle c f GeoColor.transp GeoGraphics.NO_DRAW) s1 s2 col flag f in x)
	| Inter_circle_circle (s1,s2,(c1,c2)) -> 
	    (try 
	       let x = f#get_point s1 in
		 if x#get_color () = GeoColor.transp then
		   (f#remove_point s1;f#remove_point s2;raise Not_found)
		 else x 
	     with
	       | Not_found -> let (x,_) = 
		   M.inter_cc 
		     (draw_circle c1 f GeoColor.transp GeoGraphics.NO_DRAW) 
		     (draw_circle c2 f GeoColor.transp GeoGraphics.NO_DRAW) s1 s2 col flag f in x)
	| Point_on_line (s,l) -> 
	    (try 
	       let x = f#get_point s in
		 if x#get_color () = GeoColor.transp then
		   (f#remove_point s;raise Not_found)
		 else x 
	     with
	       | Not_found -> M.point_on_line 
		   (draw_line l f GeoColor.transp GeoGraphics.NO_DRAW) s col flag f)
	| Point_on_circle (s,c) -> 
	    (try 
	       let x = f#get_point s in
		 if x#get_color () = GeoColor.transp then
		   (f#remove_point s;raise Not_found)
		 else x 
	     with
	       | Not_found -> M.point_on_circle 
		   (draw_circle c f GeoColor.transp GeoGraphics.NO_DRAW) s col flag f)
	| Middle (s,(p1,p2)) -> 
	    (try 
	       let x = f#get_point s in
		 if x#get_color () = GeoColor.transp then
		   (f#remove_point s;raise Not_found)
		 else x 
	     with
	       | Not_found -> M.middle 
		   (draw_point p1 f GeoColor.transp GeoGraphics.NO_DRAW) 
		   (draw_point p2 f GeoColor.transp GeoGraphics.NO_DRAW) s col flag f) 
	| Construct_point pp -> draw_point pp f GeoColor.transp GeoGraphics.NO_DRAW
	| _ -> raise Error
	    
    and draw_line l f col flag =
      match l with
	| Line (s,(p1,p2)) -> 
	    (try 
	       let x = f#get_line s in
		 if x#get_color () = GeoColor.transp then
		   (f#remove_line s;raise Not_found)
		 else x 
	     with
	       | Not_found -> M.line 
		   (draw_point p1 f GeoColor.transp GeoGraphics.NO_DRAW) 
		   (draw_point p2 f GeoColor.transp GeoGraphics.NO_DRAW) s col flag f)
	| Ruler ll -> draw_line ll f GeoColor.transp GeoGraphics.NO_DRAW
	| Segment (s,(p1,p2)) -> 
	    (try 
	       let x = f#get_line s in
		 if x#get_color () = GeoColor.transp then
		   (f#remove_line s;raise Not_found)
		 else x
	     with
               | Not_found -> M.segment 
		   (draw_point p1 f GeoColor.transp GeoGraphics.NO_DRAW) 
		   (draw_point p2 f GeoColor.transp GeoGraphics.NO_DRAW) s col flag f)
	| Perpendicular (s,(l,p)) -> 
	    (try 
	       let x = f#get_line s in
		 if x#get_color () = GeoColor.transp then
		   (f#remove_line s;raise Not_found)
		 else x
	     with
               | Not_found -> M.perpendicular 
		   (draw_line l f GeoColor.transp GeoGraphics.NO_DRAW) 
		   (draw_point p f GeoColor.transp GeoGraphics.NO_DRAW) s col flag f)
	| Perpendicular_segment (s,(l,p,(p1,p2))) -> 
	    (try 
	       let x = f#get_line s in
		 if x#get_color () = GeoColor.transp then
		   (f#remove_line s;raise Not_found)
		 else x
	     with
               | Not_found -> M.perpendicular_seg 
		   (draw_line l f GeoColor.transp GeoGraphics.NO_DRAW) 
		   (draw_point p f GeoColor.transp GeoGraphics.NO_DRAW) 
		   (draw_point p1 f GeoColor.transp GeoGraphics.NO_DRAW) 
		   (draw_point p2 f GeoColor.transp GeoGraphics.NO_DRAW) s col flag f)
	| Parallel (s,(l,p)) -> 
	    (try 
	       let x = f#get_line s in
		 if x#get_color () = GeoColor.transp then
		   (f#remove_line s;raise Not_found)
		 else x
	     with
               | Not_found -> M.parallel 
		   (draw_line l f GeoColor.transp GeoGraphics.NO_DRAW) 
		   (draw_point p f GeoColor.transp GeoGraphics.NO_DRAW) s col flag f)
	| Parallel_segment (s,(l,p,(p1,p2))) -> 
	    (try 
	       let x = f#get_line s in
		 if x#get_color () = GeoColor.transp then
		   (f#remove_line s;raise Not_found)
		 else x
	     with
               | Not_found -> M.parallel_seg 
		   (draw_line l f GeoColor.transp GeoGraphics.NO_DRAW) 
		   (draw_point p f GeoColor.transp GeoGraphics.NO_DRAW) 
		   (draw_point p1 f GeoColor.transp GeoGraphics.NO_DRAW) 
		   (draw_point p2 f GeoColor.transp GeoGraphics.NO_DRAW) s col flag f)
	| Bissecting (s,(p1,p2,p3)) -> 
	    (try
	       let x = f#get_line s in
		 if x#get_color () = GeoColor.transp then
		   (f#remove_line s;raise Not_found)
		 else x
	     with
               | Not_found -> M.angle_bissector 
		   (draw_point p1 f GeoColor.transp GeoGraphics.NO_DRAW) 
		   (draw_point p2 f GeoColor.transp GeoGraphics.NO_DRAW) 
		   (draw_point p3 f GeoColor.transp GeoGraphics.NO_DRAW) s col flag f)
	| Perpendicular_bissector (s,l) -> 
	    (try 
	       let x = f#get_line s in
		 if x#get_color () = GeoColor.transp then
		   (f#remove_line s;raise Not_found)
		 else x
	     with
               | Not_found -> M.perpendicular_bissector 
		   (draw_line l f GeoColor.transp GeoGraphics.NO_DRAW) s col flag f) 
	| _ -> raise Error
	    
    and draw_circle c f col flag =
      match c with
	| Circle (s,(o,p)) -> 
	    (try 
	       let x = f#get_circle s in
		 if x#get_color () = GeoColor.transp then
		   (f#remove_circle s;raise Not_found)
		 else x
	     with
               | Not_found -> M.circle 
		   (draw_point o f GeoColor.transp GeoGraphics.NO_DRAW) 
		   (draw_point p f GeoColor.transp GeoGraphics.NO_DRAW) s col flag f)
	| Compass cc -> draw_circle cc f GeoColor.transp GeoGraphics.NO_DRAW
	| _ -> raise Error
	    
    let draw_figure abstract_figure drawn_figure flag =
      List.iter 
	(fun (_,col,p) -> draw_point p drawn_figure col flag;()) 
	(List.rev !(abstract_figure.point_list));
      List.iter 
	(fun (_,col,l) -> draw_line l drawn_figure col flag;()) 
	(List.rev !(abstract_figure.line_list));
      List.iter 
	(fun (_,col,c) -> draw_circle c drawn_figure col flag;()) 
	(List.rev !(abstract_figure.circle_list))
	
  end

    (*
let anime_figure abstract_figure drawn_figure l_of_move_point l_of_trace_point =
*)

(* XML writer *)

let rec write_point p level =
  match p with
  | Point s -> (if Xml.is_a_declared_point s then (if level = 0 then Xml.do_nothing else Xml.point s level)
                else Xml.point_declaration s (Generate.xy (100,100) (540,380)) level)
  | Inter_line_line (s,(l1,l2)) -> (if Xml.is_a_declared_point s then (if level = 0 then Xml.do_nothing else Xml.point s level)
                                    else Xml.inter_line_line s (write_line l1 (level+1)) (write_line l2 (level+1)) level)
  | Inter_circle_line (s1,s2,(c,l)) -> (if Xml.is_a_declared_point s1 then (if level = 0 then Xml.do_nothing else Xml.point s1 level)
                                        else Xml.inter_circle_line s1 (write_circle c (level+1)) (write_line l (level+1)) level)
  | Inter_circle_circle (s1,s2,(c1,c2)) -> (if Xml.is_a_declared_point s1 then (if level = 0 then Xml.do_nothing else Xml.point s1 level)
                                            else Xml.inter_circle_circle s1 (write_circle c1 (level+1)) (write_circle c2 (level+1)) level)
  | Point_on_line (s,l) -> (if Xml.is_a_declared_point s then (if level = 0 then Xml.do_nothing else Xml.point s level)
                            else Xml.point_on_line s (write_line l (level+1)) level)
  | Point_on_circle (s,c) -> (if Xml.is_a_declared_point s then (if level = 0 then Xml.do_nothing else Xml.point s level)
                              else Xml.point_on_circle s (write_circle c (level+1)) level)
  | Middle (s,(p1,p2)) -> (if Xml.is_a_declared_point s then (if level = 0 then Xml.do_nothing else Xml.point s level)
                           else Xml.middle s (write_point p1 (level+1)) (write_point p2 (level+1)) level)
  | Construct_point p1 -> write_point p1 level

and write_line l level=
  match l with
  | Line (s,(p1,p2)) -> (if Xml.is_a_declared_line s then (if level = 0 then Xml.do_nothing else Xml.line s level)
                           else Xml.line_declaration s (write_point p1 (level+1)) (write_point p2 (level+1)) level)
  | Segment (s,(p1,p2)) -> (if Xml.is_a_declared_line s then (if level = 0 then Xml.do_nothing else Xml.line s level)
                           else Xml.segment s (write_point p1 (level+1)) (write_point p2 (level+1)) level)
  | Perpendicular (s,(l,p)) -> (if Xml.is_a_declared_line s then (if level = 0 then Xml.do_nothing else Xml.line s level)
                                else Xml.perpendicular s (write_line l (level+1)) (write_point p (level+1)) level)
  | Perpendicular_bissector (s,l) -> (if Xml.is_a_declared_line s then (if level = 0 then Xml.do_nothing else Xml.line s level)
                                      else Xml.perpendicular_bissector s (write_line l (level+1)) level)
  | Parallel (s,(l,p)) -> (if Xml.is_a_declared_line s then (if level = 0 then Xml.do_nothing else Xml.line s level)
                          else Xml.parallel s (write_line l (level+1)) (write_point p (level+1)) level)
  | Bissecting (s,(p1,p2,p3)) -> (if Xml.is_a_declared_line s then (if level = 0 then Xml.do_nothing else Xml.line s level)
                                  else Xml.bissecting s (write_point p1 (level+1)) (write_point p2 (level+1)) (write_point p3 (level+1)) level)
  | Ruler l1 -> write_line l1 level

and write_circle c level=
  match c with
  | Circle (s,(o,p)) -> (if Xml.is_a_declared_circle s then (if level = 0 then Xml.do_nothing else Xml.circle s level)
                         else Xml.circle_declaration s (write_point o (level+1)) (write_point p (level+1)) level)
  | Compass c1 -> write_circle c1 level

let write_figure abstract_figure channel =
  List.iter (fun (_,_,p) -> (write_point p 0) channel;()) (List.rev !(abstract_figure.point_list));
  List.iter (fun (_,_,l) -> (write_line l 0) channel;()) (List.rev !(abstract_figure.line_list));
  List.iter (fun (_,_,c) -> (write_circle c 0) channel;()) (List.rev !(abstract_figure.circle_list))


(* XML expansed writer*)
let rec write_point2 p col level =
  match p with
  | Point s -> ( let (is_here,colour) = Xmlexpansed.is_a_declared_point s in
                if is_here then (if level = 0 then Xmlexpansed.do_nothing else Xmlexpansed.point s colour level)
                else Xmlexpansed.point_declaration s (Generate.xy (0,0) (10,10)) col level)
  | Inter_line_line (s,(l1,l2)) -> ( let (is_here,colour) = Xmlexpansed.is_a_declared_point s in
                                     if is_here then (if level = 0 then Xmlexpansed.do_nothing else Xmlexpansed.point s colour level)
                                    else Xmlexpansed.inter_line_line s (write_line2 l1 "black" (level+1)) (write_line2 l2 "black" (level+1)) col level)
  | Inter_circle_line (s1,s2,(c,l)) -> (let (is_here,colour) = Xmlexpansed.is_a_declared_point s1 in
                                        if is_here then (if level = 0 then Xmlexpansed.do_nothing else Xmlexpansed.point s1 colour level)
                                        else Xmlexpansed.inter_circle_line s1 s2 (write_circle2 c "black" (level+1)) (write_line2 l "black" (level+1)) col level)
  | Inter_circle_circle (s1,s2,(c1,c2)) -> (let (is_here,colour) = Xmlexpansed.is_a_declared_point s1 in
                                            if is_here then (if level = 0 then Xmlexpansed.do_nothing else Xmlexpansed.point s1 colour level)
                                            else Xmlexpansed.inter_circle_circle s1 s2 (write_circle2 c1 "black" (level+1)) (write_circle2 c2 "black" (level+1)) col level)
  | Point_on_line (s,l) -> (let (is_here,colour) = Xmlexpansed.is_a_declared_point s in
                            if is_here then (if level = 0 then Xmlexpansed.do_nothing else Xmlexpansed.point s colour level)
                            else Xmlexpansed.point_on_line s (write_line2 l "black" (level+1)) col level)
  | Point_on_circle (s,c) -> (let (is_here,colour) = Xmlexpansed.is_a_declared_point s in
                              if is_here then (if level = 0 then Xmlexpansed.do_nothing else Xmlexpansed.point s colour level)
                              else Xmlexpansed.point_on_circle s (write_circle2 c "black" (level+1)) col level)
  | Middle (s,(p1,p2)) -> (let (is_here,colour) = Xmlexpansed.is_a_declared_point s in
                           if is_here then (if level = 0 then Xmlexpansed.do_nothing else Xmlexpansed.point s colour level)
                           else Xmlexpansed.middle s (write_point2 p1 "red" (level+1)) (write_point2 p2 "red" (level+1)) col level)

  | Construct_point p1 -> write_point2 p1 "white" level

and write_line2 l col level=
  match l with
  | Line (s,(p1,p2)) -> (let (is_here,colour) = Xmlexpansed.is_a_declared_line s in
                         if is_here then 
                           Xmlexpansed.line_declaration s (Xmlexpansed.do_nothing) (Xmlexpansed.do_nothing) (name_of_point p1) (name_of_point p2) colour level
                         else
                           Xmlexpansed.line_declaration s (write_point2 p1 col (level+1)) (write_point2 p2 col (level+1)) (name_of_point p1) (name_of_point p2) col level)
  | Segment (s,(p1,p2)) -> (let (is_here,colour) = Xmlexpansed.is_a_declared_line s in
                         if is_here then 
                           Xmlexpansed.segment s (Xmlexpansed.do_nothing) (Xmlexpansed.do_nothing) (name_of_point p1) (name_of_point p2) colour level
                         else
                           Xmlexpansed.segment s (write_point2 p1 col (level+1)) (write_point2 p2 col (level+1)) (name_of_point p1) (name_of_point p2) col level)
  | Perpendicular (s,_) -> write_line2 (Line (s,expand_line l)) col level
  | Perpendicular_segment (s,_) -> write_line2 (Segment (s,expand_line l)) col level
  | Perpendicular_bissector (s,_) -> write_line2 (Line (s,expand_line l)) col level
  | Parallel (s,_) -> write_line2 (Line (s,expand_line l)) col level
  | Parallel_segment (s,_) -> write_line2 (Segment (s,expand_line l)) col level
  | Bissecting (s,(p1,p2,p3)) -> raise Error
  | Ruler l1 -> write_line2 l1 "white" level

and write_circle2 c col level=
  match c with
  | Circle (s,(o,p)) -> (let (is_here,colour) = Xmlexpansed.is_a_declared_circle s in
                         if is_here then
                           Xmlexpansed.circle_declaration s (Xmlexpansed.do_nothing) (Xmlexpansed.do_nothing) (name_of_point o) (name_of_point p) colour level
                         else
                           Xmlexpansed.circle_declaration s (write_point2 o col (level+1)) (write_point2 p col (level+1)) (name_of_point o) (name_of_point p) col level)
  | Compass c1 -> write_circle2 c1 "white" level

let write_figure2 abstract_figure channel =
  List.iter (fun (_,_,p) -> (write_point2 p "red" 0) channel;()) (List.rev !(abstract_figure.point_list));
  List.iter (fun (_,_,l) -> (write_line2 l "black" 0) channel;()) (List.rev !(abstract_figure.line_list));
  List.iter (fun (_,_,c) -> (write_circle2 c "black" 0) channel;()) (List.rev !(abstract_figure.circle_list))
;;









