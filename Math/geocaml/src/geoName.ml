open GeoType

let get_point name fig =
  let (_,_,p) = List.find (fun (s,_,_) -> s=name) !(fig.point_list) in p

let get_line name fig =
  let len = (String.length name)-1 in
  if name.[0]='(' && name.[len]=')' then
    begin
      let i = String.index name ',' in
      let a = String.sub name 1 (i-1) in
      let b = String.sub name (i+1) (len-i-1) in
      let rev_name = "("^b^","^a^")" in
      let (_,_,l) = List.find (fun (s,_,_) -> s = name || s = rev_name) !(fig.line_list) in l
    end
  else 
    if name.[0]='[' && name.[len]=']' then
      begin
        let i = String.index name ',' in
        let a = String.sub name 1 (i-1) in
        let b = String.sub name (i+1) (len-i-1) in
        let rev_name = "["^b^","^a^"]" in
        let (_,_,l) = List.find (fun (s,_,_) -> s = name || s = rev_name) !(fig.line_list) in l
      end
    else let (_,_,l) = List.find (fun (s,_,_) -> s=name) !(fig.line_list) in l

let get_circle name fig =
  let (_,_,c) = List.find (fun (s,_,_) -> s=name) !(fig.circle_list) in c

