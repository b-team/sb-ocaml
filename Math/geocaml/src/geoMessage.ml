let version = "0.2"

let name = "GeOCaml"

let window_title = (name^" "^version)

let print_version () =
  begin
    print_endline window_title;
    flush stdout
  end

let prompt () =
  begin
    print_string ("\t "^name^" version "^version^"\n");
    print_string "# ";
    flush stdout
  end

