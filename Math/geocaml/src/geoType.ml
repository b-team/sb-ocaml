type point = Point of string
  | Inter_line_line of (string * (line * line))
  | Point_on_line of (string * line)
  | Point_on_circle of (string * circle)
  | Middle of (string * (point * point))
  | Inter_circle_line of (string * string * (circle * line))
  | Inter_circle_circle of (string * string * (circle * circle))
  | Construct_point of point 

and

line = Line of (string * (point * point))
       | Half_line of (string * (point * point)) 
  | Ruler of line
  | Segment of (string * (point * point))
  | Perpendicular of (string * (line * point))
  | Perpendicular_segment of (string * (line * point * (point * point)))
  | Perpendicular_bissector of (string * line)
  | Parallel of (string * (line * point))
  | Parallel_segment of (string * (line * point * (point * point)))
  | Bissecting of (string * (point * point * point))

and

circle = Circle of (string * (point * point))
  | Compass of circle

type figure = {point_list: (string * (int * int * int) * point) list ref;
               line_list: (string * (int * int * int) * line) list ref;
               circle_list: (string * (int * int * int) * circle) list ref;}


