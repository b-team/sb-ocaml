(* $Id: xmlexpansed.ml,v 1.1 2004/04/28 17:54:35 gmariano Exp $ *)

let encoding = " encoding=\"ISO-8859-1\""

let standalone = " standalone=\"no\""

let version = "<?xml version = \"1.0\""^encoding^standalone^"?>\n" 

let doctype = "<!DOCTYPE GeoCaml SYSTEM/PUBLIC \"geocaml.dtd\" >\n"

let name s =
  " name="^"\""^s^"\" "

let name1 s =
  " name1="^"\""^s^"\" "

let name2 s =
  " name2="^"\""^s^"\" "

let pointA s =
  " pointA="^"\""^s^"\" "

let pointB s =
  " pointB="^"\""^s^"\" "

let pointC s =
  " pointC="^"\""^s^"\" "

let color s =
  " color="^"\""^s^"\" "

let coordinates (x,y) =
  let s = "\"("^string_of_int(x)^","^string_of_int(y)^")\"" in
  " coordinates="^s

let balise name attribut channel =
  output_string channel ("<"^name^attribut^"/>\n")

let open_balise name attribut channel =
  output_string channel ("<"^name^attribut^">\n")

let close_balise name channel =
  output_string channel ("</"^name^">\n")

let do_nothing (channel: out_channel) = ()

let indent level channel =
  for i=1 to level do output_string channel "\t" done

type figure = {point_list: (string*string) list ref;
               line_list: (string*string) list ref;
               circle_list: (string*string) list ref;}

let my_figure = {point_list=ref [];line_list=ref [];circle_list=ref []}

let is_a_declared_point s = 
  try let (_,c) = List.find (fun p -> s=fst(p)) !(my_figure.point_list) in (true,c)
  with Not_found -> (false,"white")

let is_a_declared_line s = 
  try let (_,c) =  List.find (fun l -> s=fst(l)) !(my_figure.line_list) in (true,c)
  with Not_found -> (false,"white")

let is_a_declared_circle s = 
  try let (_,c) = List.find (fun c -> s=fst(c)) !(my_figure.circle_list) in (true,c)
  with Not_found -> (false,"white")

let add_point name col = my_figure.point_list := (name,col)::!(my_figure.point_list)

let add_line name col = my_figure.line_list := (name,col)::!(my_figure.line_list)

let add_circle name col = my_figure.circle_list := (name,col)::!(my_figure.circle_list)

(* point *)
let point_declaration s (x,y) col level channel =
  add_point s col;
  indent level channel;balise "point" ((name s)^(coordinates (x,y))^(color col)) channel

let point s col level channel =
  indent level channel;balise "point" ((name s)^(color col)) channel

(* line *)
let line_declaration s (point1: out_channel -> unit) (point2: out_channel -> unit) s1 s2 col level channel =
  add_line s col;
  indent level channel;open_balise "line" ((name s)^(pointA s1)^(pointB s2)^(color col)) channel;
  point1 channel;
  point2 channel;
  indent level channel;close_balise "line" channel

(* circle *)
let circle_declaration s (point1: out_channel -> unit) (point2: out_channel -> unit) s1 s2 col level channel =
  add_circle s col;
  indent level channel;open_balise "circle" ((name s)^(pointA s1)^(pointB s2)^(color col)) channel;
  point1 channel;
  point2 channel;
  indent level channel;close_balise "circle" channel

(* inter_line_line *)
let inter_line_line s (line1: out_channel -> unit) (line2: out_channel -> unit) col level channel =
  add_point s col;
  indent level channel;open_balise "inter_line_line" ((name s)^(color col)) channel;
  line1 channel;
  line2 channel;
  indent level channel;close_balise "inter_line_line" channel

(* point_on_line *)
let point_on_line s (line1: out_channel -> unit) col level channel =
  add_point s col;
  indent level channel;open_balise "point_on_line" ((name s)^(color col)) channel;
  line1 channel;
  indent level channel;close_balise "point_on_line" channel

(* point_on_circle *)
let point_on_circle s (circle1: out_channel -> unit) col level channel =
  add_point s col;
  indent level channel;open_balise "point_on_circle" ((name s)^(color col)) channel;
  circle1 channel;
  indent level channel;close_balise "point_on_circle" channel

(* middle *)
let middle s (point1: out_channel -> unit) (point2: out_channel -> unit) col level channel =
  add_point s col;
  indent level channel;open_balise "middle" ((name s)^(color col)) channel;
  point1 channel;
  point2 channel;
  indent level channel;close_balise "middle" channel

(* inter_circle_line *)
let inter_circle_line s1 s2 (circle1: out_channel -> unit) (line1: out_channel -> unit) col level channel =
  add_point s1 col;
  indent level channel;open_balise "inter_circle_line" ((name1 s1)^(name2 s2)^(color col)) channel;
  indent level channel;circle1 channel;
  indent level channel;line1 channel;
  indent level channel;close_balise "inter_circle_line" channel

(* inter_circle_circle *)
let inter_circle_circle s1 s2 (circle1: out_channel -> unit) (circle2: out_channel -> unit) col level channel =
  add_point s1 col;
  indent level channel;open_balise "inter_circle_circle" ((name1 s1)^(name2 s2)^(color col)) channel;
  circle1 channel;
  circle2 channel;
  indent level channel;close_balise "inter_circle_circle" channel

(* segment *)
let segment s (point1: out_channel -> unit) (point2: out_channel -> unit) s1 s2 col level channel =
  add_line s col;
  indent level channel;open_balise "segment" ((name s)^(pointA s1)^(pointB s2)^(color col)) channel;
  point1 channel;
  point2 channel;
  indent level channel;close_balise "segment" channel

(* bissecting *)
let bissecting s (point1: out_channel -> unit) (point2: out_channel -> unit) (point3: out_channel -> unit) col level channel =
  add_line s col;
  indent level channel;open_balise "bissecting" ((name s)^(color col)) channel;
  point1 channel;
  point2 channel;
  point3 channel;
  indent level channel;close_balise "bissecting" channel

;;
