(* $Id: abrlex.ml,v 1.1 2004/04/28 17:54:35 gmariano Exp $ *)

open GeoTree

type abr = POINT of (GeoType.point * string * (int * int * int)) 
	   | LINE of (GeoType.line * string * (int * int * int)) 
	   | CIRCLE of (GeoType.circle * string * (int * int * int)) 
	   | MULTIPLE of abr list 
	   | CONSTRUCT of abr
	   | SUPPR of abr

exception Wrong_number_declaration of (int * string)

exception Object_does_not_exist of string

exception Line_or_circle_expected of string

exception Intersection_with_point of string

exception Double_intersection_required of string

exception Simple_intersection_required of (string * string)

exception Line_found_point_expected of string

exception Circle_found_point_expected of string

exception Point_found_line_expected of string

exception Circle_found_line_expected of string

exception Segment_expected


let get_object s =
  try POINT (GeoName.get_point s my_figure,s,(0,0,0))
  with Not_found -> (try LINE (GeoName.get_line s my_figure,s,(0,0,0))
                     with Not_found -> (try CIRCLE (GeoName.get_circle s my_figure,s,(0,0,0))
                                        with Not_found -> raise (Object_does_not_exist s)))

let point name col = POINT (GeoTree.point name,name,col)

let multiple_point l col = MULTIPLE (List.map (fun f -> f col) l)

let point_on name s col =
  let o = get_object s in
  match o with 
  | POINT _ -> raise (Line_or_circle_expected s)
  | LINE (x,_,_) -> POINT (GeoTree.point_on_line x name,name,col)
  | CIRCLE (x,_,_) -> POINT (GeoTree.point_on_circle x name,name,col)

let intersection name s1 s2 col =
  let o1 = get_object s1 in
  let o2 = get_object s2 in
  match (o1,o2) with
  | (POINT _,_) -> raise (Intersection_with_point s1)
  | (_,POINT _) -> raise (Intersection_with_point s2)
  | (LINE (x,_,_),LINE (y,_,_)) -> POINT (GeoTree.inter_line_line x y name,name,col)
  | (CIRCLE _,_) -> raise (Double_intersection_required s1)
  | (_,CIRCLE _) -> raise (Double_intersection_required s2)

let double_intersection name1 name2 s1 s2 col =
  let o1 = get_object s1 in
  let o2 = get_object s2 in
  match (o1,o2) with
  | (POINT _,_) -> raise (Intersection_with_point s1)
  | (_,POINT _) -> raise (Intersection_with_point s2)
  | (LINE _,LINE _) -> raise (Simple_intersection_required (s1,s2))
  | (LINE (x,_,_),CIRCLE (y,_,_)) -> MULTIPLE [POINT (GeoTree.inter_circle_line y x name1 name2,name1,col);POINT (GeoTree.inter_circle_line y x name2 name1,name2,col)]
  | (CIRCLE (x,_,_),LINE (y,_,_)) -> MULTIPLE [POINT (GeoTree.inter_circle_line x y name1 name2,name1,col);POINT (GeoTree.inter_circle_line x y name2 name1,name2,col)]
  | (CIRCLE (x,_,_),CIRCLE (y,_,_)) -> MULTIPLE [POINT (GeoTree.inter_circle_circle x y name1 name2,name1,col);POINT (GeoTree.inter_circle_circle x y name2 name1,name2,col)]

let middle name s1 s2 col =
  let o1 = get_object s1 in
  let o2 = get_object s2 in
  match (o1,o2) with
  | (POINT (x,_,_),POINT (y,_,_)) -> POINT (GeoTree.middle x y name,name,col)
  | (LINE _,_) -> raise (Line_found_point_expected s1)
  | (_,LINE _) -> raise (Line_found_point_expected s2)
  | (CIRCLE _,_) -> raise (Circle_found_point_expected s1)
  | (_,CIRCLE _) -> raise (Circle_found_point_expected s2)

let middle2 name s col =
  let o = get_object s in
  match o with
  | LINE (GeoType.Segment (n,(p1,p2)),_,_) -> POINT (GeoTree.middle p1 p2 name,name,col)
  | _ -> raise Segment_expected

let any_line name col = LINE (GeoTree.any_line name,name,col)

let line name s1 s2 col =
  let o1 = get_object s1 in
  let o2 = get_object s2 in
  match (o1,o2) with
  | (POINT (x,_,_),POINT (y,_,_)) -> LINE (GeoTree.line x y name,name,col)
  | (LINE _,_) -> raise (Line_found_point_expected s1)
  | (_,LINE _) -> raise (Line_found_point_expected s2)
  | (CIRCLE _,_) -> raise (Circle_found_point_expected s1)
  | (_,CIRCLE _) -> raise (Circle_found_point_expected s2)

let segment name s1 s2 col =
  let o1 = get_object s1 in
  let o2 = get_object s2 in
  match (o1,o2) with
  | (POINT (x,_,_),POINT (y,_,_)) -> LINE (GeoTree.segment x y name,name,col)
  | (LINE _,_) -> raise (Line_found_point_expected s1)
  | (_,LINE _) -> raise (Line_found_point_expected s2)
  | (CIRCLE _,_) -> raise (Circle_found_point_expected s1)
  | (_,CIRCLE _) -> raise (Circle_found_point_expected s2)

let parallel name s1 s2 col =
  let o1 = get_object s1 in
  let o2 = get_object s2 in
  match (o1,o2) with
  | (LINE (x,_,_),POINT (y,_,_)) -> LINE (GeoTree.parallel x y name,name,col)
  | (POINT _,_) -> raise (Point_found_line_expected s1)
  | (LINE _,LINE _) -> raise (Line_found_point_expected s2)
  | (LINE _,CIRCLE _) -> raise (Circle_found_point_expected s2)
  | (CIRCLE _,_) -> raise (Circle_found_line_expected s1)

let parallel_seg name s1 s2 s3 s4 col =
  let o1 = get_object s1 in
  let o2 = get_object s2 in
  let o3 = get_object s3 in
  let o4 = get_object s4 in
  match (o1,o2,o3,o4) with
  | (LINE (x,_,_),POINT (y,_,_),POINT(a,_,_),POINT(b,_,_)) -> LINE (GeoTree.parallel_seg x y (a,b) name,name,col)
  | (POINT _,_,_,_) -> raise (Point_found_line_expected s1)
  | (LINE _,LINE _,_,_) -> raise (Line_found_point_expected s2)
  | (LINE _,CIRCLE _,_,_) -> raise (Circle_found_point_expected s2)
  | (CIRCLE _,_,_,_) -> raise (Circle_found_line_expected s1)
  | (LINE _,POINT _,LINE _,_) -> raise (Line_found_point_expected s3)
  | (LINE _,POINT _,CIRCLE _,_)  -> raise (Circle_found_point_expected s3)
  | (LINE _,POINT _,POINT _,LINE _) -> raise (Line_found_point_expected s4)
  | (LINE _,POINT _,POINT _,CIRCLE _) -> raise (Circle_found_point_expected s4)


let perpendicular name s1 s2 col =
  let o1 = get_object s1 in
  let o2 = get_object s2 in
  match (o1,o2) with
  | (LINE (x,_,_),POINT (y,_,_)) -> LINE (GeoTree.perpendicular x y name,name,col)
  | (POINT _,_) -> raise (Point_found_line_expected s1)
  | (LINE _,LINE _) -> raise (Line_found_point_expected s2)
  | (LINE _,CIRCLE _) -> raise (Circle_found_point_expected s2)
  | (CIRCLE _,_) -> raise (Circle_found_line_expected s1)

let perpendicular_seg name s1 s2 s3 s4 col =
  let o1 = get_object s1 in
  let o2 = get_object s2 in
  let o3 = get_object s3 in
  let o4 = get_object s4 in
  match (o1,o2,o3,o4) with
  | (LINE (x,_,_),POINT (y,_,_),POINT(a,_,_),POINT(b,_,_)) -> LINE (GeoTree.perpendicular_seg x y (a,b) name,name,col)
  | (POINT _,_,_,_) -> raise (Point_found_line_expected s1)
  | (LINE _,LINE _,_,_) -> raise (Line_found_point_expected s2)
  | (LINE _,CIRCLE _,_,_) -> raise (Circle_found_point_expected s2)
  | (CIRCLE _,_,_,_) -> raise (Circle_found_line_expected s1)
  | (LINE _,POINT _,LINE _,_) -> raise (Line_found_point_expected s3)
  | (LINE _,POINT _,CIRCLE _,_)  -> raise (Circle_found_point_expected s3)
  | (LINE _,POINT _,POINT _,LINE _) -> raise (Line_found_point_expected s4)
  | (LINE _,POINT _,POINT _,CIRCLE _) -> raise (Circle_found_point_expected s4)


let perpendicular_bissector name s col =
  let o = get_object s in
  match o with
  | LINE (GeoType.Segment seg,_,col) -> LINE (GeoTree.perpendicular_bissector (GeoType.Segment seg) name,name,col)
  | _ -> raise Segment_expected

let bissecting name s1 s2 s3 col =
  let o1 = get_object s1 in
  let o2 = get_object s2 in
  let o3 = get_object s3 in
  match (o1,o2,o3) with
  | (POINT (x,_,_),POINT (y,_,_),POINT (z,_,_)) -> LINE (GeoTree.bissecting x y z name,name,col)
  | (LINE _,_,_) -> raise (Line_found_point_expected s1)
  | (_,LINE _,_) -> raise (Line_found_point_expected s2)
  | (_,_,LINE _) -> raise (Line_found_point_expected s3)
  | (CIRCLE _,_,_) -> raise (Circle_found_point_expected s1)
  | (_,CIRCLE _,_) -> raise (Circle_found_point_expected s2)
  | (_,_,CIRCLE _) -> raise (Circle_found_point_expected s3)

let circle name s1 s2 col =
  let o1 = get_object s1 in
  let o2 = get_object s2 in
  match (o1,o2) with
  | (POINT (x,_,_),POINT (y,_,_)) -> CIRCLE (GeoTree.circle x y name,name,col)
  | (LINE _,_) -> raise (Line_found_point_expected s1)
  | (_,LINE _) -> raise (Line_found_point_expected s2)
  | (CIRCLE _,_) -> raise (Circle_found_point_expected s1)
  | (_,CIRCLE _) -> raise (Circle_found_point_expected s2)

let suppr name =
  let o = get_object name in SUPPR o

let treat_exception e =
  match e with
  | Object_does_not_exist s -> s^" n'est pas declar�\n"
  | Line_or_circle_expected s -> s^" est un point,la construction n�cessite une droite ou un cercle\n"
  | Intersection_with_point s -> s^" est un point,la construction n�cessite une droite\n"
  | Double_intersection_required s -> s^" est un cercle,l'intersection donne deux points\n"
  | Simple_intersection_required (s1,s2) -> s1^" et "^s2^"sont deux droites,l'intersection donne un point\n"
  | Line_found_point_expected s -> s^" est une droite,la construction n�cessite un point\n"
  | Circle_found_point_expected s -> s^" est un cercle,la construction n�cessite un point\n"
  | Point_found_line_expected s -> s^" est un point,la construction n�cessite une droite\n"
  | Circle_found_line_expected s -> s^" est un cercle,la construction n�cessite une droite\n"
  | _ -> "Syntaxe incorrecte!"

let rec add_abr_for_im abr fig f =
  match abr with
    | POINT (p,s,col) -> GeoTree.add_point p s col fig
    | LINE (l,s,col) -> GeoTree.add_line l s col fig
    | CIRCLE (c,s,col) -> GeoTree.add_circle c s col fig
    | MULTIPLE l -> List.iter (fun a -> add_abr_for_im a fig f) l
    | CONSTRUCT o -> (match o with
			| POINT (p,s,col) -> GeoTree.add_point (GeoType.Construct_point p) s col fig
			| LINE (l,s,col) -> GeoTree.add_line (GeoType.Ruler l) s col fig
			| CIRCLE (c,s,col) -> GeoTree.add_circle (GeoType.Compass c) s col fig
			| MULTIPLE l -> List.iter (fun a -> add_abr_for_im (CONSTRUCT a) fig f) l)
    | SUPPR o -> suppr_abr_for_im o fig f


and suppr_abr_for_im abr fig f =
  match abr with
    | POINT (p,s,_) -> (GeoTree.suppr_obj s fig f;f#remove_point s;f#redraw ())
    | LINE (l,s,_) -> (GeoTree.suppr_obj s fig f;f#remove_line s;f#redraw ())
    | CIRCLE (c,s,_) -> (GeoTree.suppr_obj s fig f;f#remove_circle s;f#redraw ())
    | _ -> ()

let rec add_abr abr fig=
  match abr with
    | POINT (p,s,col) -> GeoTree.add_point p s col fig
    | LINE (l,s,col) -> GeoTree.add_line l s col fig
    | CIRCLE (c,s,col) -> GeoTree.add_circle c s col fig
    | MULTIPLE l -> List.iter (fun a -> add_abr a fig) l
    | CONSTRUCT o -> (match o with
			| POINT (p,s,col) -> GeoTree.add_point (GeoType.Construct_point p) s col fig
			| LINE (l,s,col) -> GeoTree.add_line (GeoType.Ruler l) s col fig
			| CIRCLE (c,s,col) -> GeoTree.add_circle (GeoType.Compass c) s col fig
			| MULTIPLE l -> List.iter (fun a -> add_abr (CONSTRUCT a) fig) l)
    | SUPPR o -> ()


and suppr_abr abr fig f=
  match abr with
    | POINT (p,s,_) -> (GeoTree.suppr_obj s fig f)
    | LINE (l,s,_) -> (GeoTree.suppr_obj s fig f)
    | CIRCLE (c,s,_) -> (GeoTree.suppr_obj s fig f)
    | _ -> ()













