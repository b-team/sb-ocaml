
open Graphics

let round x =
  let rest = x -. (floor x) in
  if rest < 0.5 then truncate x
  else (truncate x) + 1

let abs_float v =
  if v < 0. then -.v else v

let intensity_of (r,g,b) i =
  let f x =
    let diff = 255 - x in
    let inc = i*.(float diff) in
      x+(round inc)
  in
  let r' = f r in
  let g' = f g in
  let b' = f b in
    (rgb r' g' b')

let put_pixel (x,y) color i =
  set_color (intensity_of color i);
  plot x y


let line (x1,y1) (x2,y2) col =
  let corr = 1.5 in
  let p1x = ref 0 and p2x = ref 0 in
  let p1y = ref 0 and p2y = ref 0 in
    begin
      (if x2 < x1 then (p1x:=x2;p2x:=x1;p1y:=y2;p2y:=y1)
       else (p1x:=x1;p2x:=x2;p1y:=y1;p2y:=y2));
      let dx = !p2x - !p1x and dy = !p2y - !p1y in
	if ((dy >= 0) && (dy <= dx)) then
	  begin
	    let d = ref (2*dy-dx) in
	    let incmajor = 2*dy in
	    let incminor = 2*(dy-dx) in
	    let two_v_dx = ref 0 in
	    let invdenom = 1. /. (2. *. sqrt(float_of_int (dx*dx+dy*dy))) in
	    let two_dx_invdenom = 2. *. (float_of_int dx) *. invdenom in
	      put_pixel (!p1x,!p1y) col 0.;
	      put_pixel (!p1x,!p1y+1) col (two_dx_invdenom /. corr);
	      put_pixel (!p1x,!p1y-1) col (two_dx_invdenom /. corr);
	      while p1x < p2x do
		begin
		  (if !d <= 0 then
		     begin
		       two_v_dx := !d + dx;
		       d := !d + incmajor;
		       p1x := !p1x + 1
		     end
		   else
		     begin
		       two_v_dx := !d - dx;
		       d := !d + incminor;
		       p1x := !p1x + 1;
		       p1y := !p1y + 1
		     end);
		  put_pixel (!p1x,!p1y) col (abs_float (float_of_int !two_v_dx)*.invdenom /. corr);
		  put_pixel (!p1x,!p1y+1) col (abs_float ((two_dx_invdenom -. ((float_of_int !two_v_dx) *. invdenom)) /. corr));
		  put_pixel (!p1x,!p1y-1) col (abs_float ((two_dx_invdenom +. ((float_of_int !two_v_dx) *. invdenom)) /. corr));
		end
	      done
	  end
	else if ((dy < 0) && (-dy <= dx)) then
	  begin
	    let d = ref (2*dy+dx) in
	    let incmajor = 2*dy in
	    let incminor = 2*(dy+dx) in
	    let two_v_dx = ref 0 in
	    let invdenom = 1. /. (2. *. sqrt(float_of_int (dx*dx+dy*dy))) in
	    let two_dx_invdenom = 2. *. (float_of_int dx) *. invdenom in
	      put_pixel (!p1x,!p1y) col 0.;
	      put_pixel (!p1x,!p1y+1) col (two_dx_invdenom /. corr);
	      put_pixel (!p1x,!p1y-1) col (two_dx_invdenom /. corr);
	      while p1x < p2x do
		begin
		  (if !d <= 0 then
		     begin
		       two_v_dx := !d + dx;
		       d := !d + incminor;
		       p1x := !p1x + 1;
		       p1y := !p1y - 1
		     end
		   else
		     begin
		       two_v_dx := !d - dx;
		       d := !d + incmajor;
		       p1x := !p1x + 1
		  end);
		  put_pixel (!p1x,!p1y) col (abs_float ((float_of_int !two_v_dx)*.invdenom /. corr));
		  put_pixel (!p1x,!p1y+1) col (abs_float ((two_dx_invdenom -. ((float_of_int !two_v_dx) *. invdenom)) /. corr));
		  put_pixel (!p1x,!p1y-1) col (abs_float ((two_dx_invdenom +. ((float_of_int !two_v_dx) *. invdenom)) /. corr));
		end
	      done
	  end
	else if ((dy > 0) && (dy > dx)) then
	  begin
	    let d = ref (2*dx-dy) in
	    let incmajor = 2*dx in
	    let incminor = 2*(dx-dy) in
	    let two_v_dy = ref 0 in
	    let invdenom = 1. /. (2. *. sqrt(float_of_int (dx*dx+dy*dy))) in
	    let two_dy_invdenom = 2. *. (float_of_int dy) *. invdenom in
	      put_pixel (!p1x,!p1y) col 0.;
	      put_pixel (!p1x+1,!p1y) col (two_dy_invdenom /. corr);
	      put_pixel (!p1x-1,!p1y) col (two_dy_invdenom /. corr);
	      while p1y < p2y do
		begin
		  (if !d <= 0 then
		     begin
		       two_v_dy := !d + dy;
		       d := !d + incmajor;
		       p1y := !p1y + 1
		     end
		   else
		     begin
		       two_v_dy := !d - dy;
		       d := !d + incminor;
		       p1x := !p1x + 1;
		       p1y := !p1y + 1
		  end);
		  put_pixel (!p1x,!p1y) col (abs_float ((float_of_int !two_v_dy)*.invdenom /. corr));
		  put_pixel (!p1x+1,!p1y) col (abs_float ((two_dy_invdenom -. ((float_of_int !two_v_dy) *. invdenom)) /. corr));
		  put_pixel (!p1x-1,!p1y) col (abs_float ((two_dy_invdenom +. ((float_of_int !two_v_dy) *. invdenom)) /. corr));
		end
	      done
	  end
	else if ((dy < 0) && (-dy > dx)) then
	  begin
	    let d = ref (2*dx+dy) in
	    let incmajor = 2*dx in
	    let incminor = 2*(dx+dy) in
	    let two_v_dy = ref 0 in
	    let invdenom = 1. /. (2. *. sqrt(float_of_int (dx*dx+dy*dy))) in
	    let two_dy_invdenom = 2. *. (float_of_int dy) *. invdenom in
	      put_pixel (!p1x,!p1y) col 0.;
	      put_pixel (!p1x+1,!p1y) col (two_dy_invdenom /. corr);
	      put_pixel (!p1x-1,!p1y) col (two_dy_invdenom /. corr);
	      while p1y > p2y do
		begin
		  (if !d <= 0 then
		     begin
		       two_v_dy := !d - dy;
		       d := !d + incmajor;
		       p1y := !p1y - 1
		     end
		   else
		     begin
		       two_v_dy := !d + dy;
		       d := !d + incminor;
		       p1x := !p1x + 1;
		       p1y := !p1y - 1
		  end);
		  put_pixel (!p1x,!p1y) col (abs_float ((float_of_int !two_v_dy)*.invdenom /. corr));
		  put_pixel (!p1x-1,!p1y) col (abs_float ((two_dy_invdenom -. ((float_of_int !two_v_dy) *. invdenom)) /. corr));
		  put_pixel (!p1x+1,!p1y) col (abs_float ((two_dy_invdenom +. ((float_of_int !two_v_dy) *. invdenom)) /. corr));
		end
	      done
	  end
	else ()
    end

let circle (centerx,centery) radius col =
  let x = ref 0 in
  let y = ref radius in
  let d = ref (1 - radius) in
  let sympixel x' y' i =
    put_pixel (x'+centerx,y'+centery) col i;
    put_pixel (y'+centerx,x'+centery) col i;
    put_pixel (y'+centerx,centery-x') col i;
    put_pixel (x'+centerx,centery-y') col i;
    put_pixel (centerx-x',centery-y') col i;
    put_pixel (centerx-y',centery-x') col i;
    put_pixel (centerx-y',x'+centery) col i;
    put_pixel (centerx-x',y'+centery) col i
  in
  let intensity x' y' radius =
    abs_float (((sqrt(float (x'*x'+y'*y'))) -. (float radius)) /. 1.5)
  in
    sympixel !x !y 0.;
    sympixel !x (!y+1) (intensity !x (!y+1) radius);
    sympixel !x (!y-1) (intensity !x (!y-1) radius);
    while !y > !x do
      begin
	(if !d < 0 then
	  begin
	    d := !d + 2 * !x + 3;
	    x := !x + 1
	  end
	else
	  begin
	    d := !d + (!x - !y) * 2 + 5;
	    x := !x + 1;
	    y := !y - 1
	  end);
	sympixel !x !y (intensity !x !y radius);
	sympixel !x (!y+1) (intensity !x (!y+1) radius);
	sympixel !x (!y-1) (intensity !x (!y-1) radius)
      end
    done

let point (x,y) (r,g,b) =
  circle (x,y) 3 (r,g,b);
  set_color (rgb r g b);
  fill_circle x y 3
;;
















