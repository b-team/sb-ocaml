%{

open Abrlex

%}

%token SOIT
%token SLASH
%token UN UNE LA LE
%token <int> NUM
%token VIRGULE
%token NORME
%token CROCHET_OUV CROCHET_FER 
%token PARENT_OUV PARENT_FER
%token DE EN
%token POINT DROITE CERCLE SEGMENT RAYON DIAMETRE
%token QUELCONQUE
%token LONGUEUR
%token INTERSECTION
%token MILIEU MEDIATRICE BISSECTRICE
%token PERPENDICULAIRE PARALLELE
%token ET A
//les geo-couleurs...
%token ROUGE VERT BLEU NOIR ORANGE JAUNE ROSE
%token CENTRE PASSANT PAR
%token <string> IDENT
%token <string> IDENT2
%token SUPPRIME EFFACE
%token EOL

%start start
%type <Abrlex.abr> start
%%

start:
  declaration {$1}
| suppression {$1}

suppression:
  SUPPRIME id_line EOL { suppr $2 }
| EFFACE id_line EOL { suppr $2 }
| SUPPRIME IDENT EOL { suppr $2 }
| EFFACE IDENT EOL { suppr $2 }

declaration:
  SOIT decl_point EOL { ($2 GeoColor.red) }
| SOIT decl_point couleur EOL { ($2 $3) }
| SLASH SOIT decl_point EOL { CONSTRUCT ($3 GeoColor.transp) }
| SOIT decl_line EOL { ($2 GeoColor.black) }
| SOIT decl_line couleur EOL { ($2 $3)}
| SLASH SOIT decl_line EOL { CONSTRUCT ($3 GeoColor.transp)}
| SOIT decl_line2 EOL { ($2 GeoColor.black) }
| SOIT decl_line2 couleur EOL { ($2 $3) }
| SLASH SOIT decl_line2 EOL { CONSTRUCT ($3 GeoColor.transp)}
| SOIT decl_circle EOL { ($2 GeoColor.black) }
| SOIT decl_circle couleur EOL { ($2 $3) }
| SLASH SOIT decl_circle decl_end { CONSTRUCT ($3 GeoColor.transp)}
;

decl_end:  
| EOL {}
|  {print_string "error decl_end" }

couleur:
  PARENT_OUV EN ROUGE PARENT_FER {GeoColor.red}
| PARENT_OUV EN VERT PARENT_FER {GeoColor.green}
| PARENT_OUV EN BLEU PARENT_FER {GeoColor.blue}
| PARENT_OUV EN ORANGE PARENT_FER {GeoColor.orange}
| PARENT_OUV EN JAUNE PARENT_FER {GeoColor.yellow}
| PARENT_OUV EN ROSE PARENT_FER {GeoColor.pink}
| PARENT_OUV EN NOIR PARENT_FER {GeoColor.black}


decl_point:
| IDENT UN POINT { point $1} 
| LE POINT IDENT { point $3} 
| IDENT UN POINT QUELCONQUE { point $1}
| IDENT UN POINT DE IDENT { point_on $1 $5}
| IDENT LE MILIEU DE IDENT { middle2 $1 $5}
| IDENT LE MILIEU DE IDENT ET IDENT { middle $1 $5 $7}
| point_intersection { $1 }
| list_of_point NUM POINT 
	{ if (List.length $1)==$2 
		then multiple_point $1 
		else raise (Wrong_number_declaration ($2,"points")) 
	}
| list_of_point NUM POINT QUELCONQUE 
	{ if (List.length $1)==$2 
		then multiple_point $1 
		else raise (Wrong_number_declaration ($2,"points")) 
	}
;

point_intersection:
  IDENT INTERSECTION DE id_line ET id_line 
    { intersection $1 $4 $6}
|   IDENT INTERSECTION DE IDENT ET IDENT 
    { intersection $1 $4 $6}
|   IDENT INTERSECTION DE IDENT ET id_line 
    { intersection $1 $4 $6}
|   IDENT INTERSECTION DE id_line ET IDENT 
    { intersection $1 $4 $6}
| IDENT ET IDENT INTERSECTION DE IDENT ET id_line 
    { double_intersection $1 $3 $6 $8}
| IDENT ET IDENT INTERSECTION DE id_line ET IDENT	      
    { double_intersection $1 $3 $6 $8}
| IDENT ET IDENT INTERSECTION DE IDENT ET IDENT	      
    { double_intersection $1 $3 $6 $8}


list_of_point:
  IDENT { [point $1] }
| IDENT VIRGULE list_of_point { (point $1)::$3 }
;

decl_line:
| IDENT UNE DROITE QUELCONQUE 
	{ any_line $1}
| IDENT LA DROITE PASSANT PAR IDENT ET IDENT 
	{ line $1 $6 $8}
| IDENT LE SEGMENT PASSANT PAR IDENT ET IDENT 
	{ segment $1 $6 $8}
| id_line LA DROITE PARALLELE A id_line PASSANT PAR IDENT 
	{ parallel $1 $6 $9}
| IDENT LA DROITE PARALLELE A id_line PASSANT PAR IDENT 
	{ parallel $1 $6 $9}
| id_line LE SEGMENT PARALLELE A id_line PASSANT PAR IDENT ET DE LONGUEUR NORME IDENT IDENT NORME 
	{ parallel_seg $1 $6 $9 $14 $15}
| IDENT LE SEGMENT PARALLELE A id_line PASSANT PAR IDENT ET DE LONGUEUR NORME IDENT IDENT NORME 
	{ parallel_seg $1 $6 $9 $14 $15}
| id_line LA DROITE PERPENDICULAIRE A id_line PASSANT PAR IDENT 
	{ perpendicular $1 $6 $9}
| IDENT LA DROITE PERPENDICULAIRE A id_line PASSANT PAR IDENT 
	{ perpendicular $1 $6 $9}
| id_line LE SEGMENT PERPENDICULAIRE A id_line PASSANT PAR IDENT ET DE LONGUEUR NORME IDENT IDENT NORME 
	{ perpendicular_seg $1 $6 $9 $14 $15}
| IDENT LE SEGMENT PERPENDICULAIRE A id_line PASSANT PAR IDENT ET DE LONGUEUR NORME IDENT IDENT NORME 
	{ perpendicular_seg $1 $6 $9 $14 $15}
| IDENT LA MEDIATRICE DE id_line 
	{ perpendicular_bissector $1 $5}
| id_line LA MEDIATRICE DE id_line 
	{ perpendicular_bissector $1 $5}
| IDENT LA BISSECTRICE DE PARENT_OUV IDENT VIRGULE IDENT VIRGULE IDENT PARENT_FER
	{ bissecting $1 $6 $8 $10}
;

decl_line2:
/* La droite (A,B) */
| LA DROITE PARENT_OUV IDENT VIRGULE IDENT PARENT_FER 
	{ line ("("^$4^","^$6^")") $4 $6}
/* La droite (AB) */
| LA DROITE PARENT_OUV IDENT IDENT PARENT_FER 
	{ line ("("^$4^","^$5^")") $4 $5}
/* Le segment [A,B] */
| LE SEGMENT CROCHET_OUV IDENT VIRGULE IDENT CROCHET_FER 
	{ segment ("["^$4^","^$6^"]") $4 $6}
/* Le segment [AB] */
| LE SEGMENT CROCHET_OUV IDENT  IDENT CROCHET_FER 
	{ segment ("["^$4^","^$5^"]") $4 $5}

;

id_segment: /* Notation de segment */
/* [A,B] */
| CROCHET_OUV IDENT VIRGULE IDENT CROCHET_FER 
	{ ("["^$2^","^$4^"]") }
/* [A B] */
| CROCHET_OUV IDENT IDENT CROCHET_FER 
	{ ("["^$2^","^$3^"]") }
/* [ ) */
| CROCHET_OUV IDENT IDENT PARENT_FER 
	{ ("["^$2^","^$3^")") }

id_line: /* Notation de droite */
| PARENT_OUV IDENT VIRGULE IDENT PARENT_FER 
	{ ("("^$2^","^$4^")") }
| PARENT_OUV IDENT  IDENT PARENT_FER 
	{ ("("^$2^","^$3^")") }
/* Admettons ;-) */
| id_segment {$1}

;

decl_circle:
|   IDENT LE CERCLE DE CENTRE IDENT PASSANT PAR IDENT 
    { circle $1 $6 $9}
|   IDENT LE CERCLE DE RAYON IDENT IDENT
    { circle $1 $6 $7} 







