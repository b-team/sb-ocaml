let round x =
  let rest = x -. (floor x) in
  if rest < 0.5 then truncate x
  else (truncate x) + 1

let solve (a,b,c) =
  if a = 0. then if b = 0. then [] else [-.c/.b]
  else
    let delta = b ** 2. -. 4. *. a *. c in
    if delta < 0. then []
    else if delta = 0. then [-.b/.2.*.a]
    else [(-.b +. sqrt(delta)) /. (2. *. a);(-.b -. sqrt(delta)) /. (2. *. a)]

let circle_with_circle (x0,y0,r0) (x1,y1,r1) =
  let n = (r1 ** 2. -. r0 ** 2. -. x1 ** 2. +. x0 ** 2. -. y1 ** 2. +. y0 ** 2.) /. ( 2. *. (y0 -. y1)) in
  let m = (x0 -. x1) /. (y0 -. y1) in
  let a = m ** 2. +. 1. in
  let b = 2. *. y0 *. m -. 2. *. n *. m -. 2. *. x0 in
  let c = x0 ** 2. +. y0 ** 2. +. n ** 2. -. r0 ** 2. -. 2. *. y0 *. n in
  List.map ( fun x -> (round(x),round(n -. x *. m)) ) (solve (a,b,c))

let circle_with_line (x0,y0,r0) (m,p) =
  let a = m ** 2. +. 1. in
  let b = 2. *. (m *. p -. m *. y0 -. x0) in
  let c = (p -. y0) ** 2. +. x0 ** 2. -. r0 ** 2. in
  List.map ( fun x -> (round(x),round(m *. x +. p)) ) (solve (a,b,c))

let line_with_line (m1,p1) (m2,p2) =
(round ((p1 -. p2)/.(m2 -. m1)),round ((p1*.m2 -. p2*.m1)/.(m2 -. m1)))
