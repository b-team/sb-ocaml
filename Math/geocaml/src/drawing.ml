(* $Id: drawing.ml,v 1.1 2004/04/28 17:54:35 gmariano Exp $ *)

open Graphics
open GeoGraphics

open GeoMath

module DrawingObjFunc =
  functor (M: GeoDrawModule)->
    struct

      module GeoDraw = M

      open GeoDraw

      let point (x,y) name col (flag: flag_object) (fig: figure) = 
	let p = new point (x,y) name col flag in
	  (fig#add_point p;p)
	  
      let point_on_line (l: line) name col (flag: flag_object) (fig: figure) =
	if l#get_dx () = 0 then
	  let (y1,y2) = ((l#get_a())#get_y (),(l#get_b())#get_y ()) in
	  let y = (min y1 y2) + Random.int (abs(y1-y2)) in
	    point ((l#get_a())#get_x (),y) name col flag fig
	else
	  let (x1,x2) = ((l#get_a())#get_x (),(l#get_b())#get_x ()) in
	  let x = (min x1 x2) + Random.int (abs(x1-x2)) in
	    point (x,int_of_float((l#get_m ()) *. float(x) +. (l#get_p ()))) name col flag fig
	      
      let point_on_circle (c: circle) name col (flag: flag_object) (fig: figure) =
	let (xcenter,ycenter) = (float((c#get_center())#get_x()),float((c#get_center())#get_y())) in
	let radius = float(c#get_radius()) in
	let x = xcenter -. radius +. Random.float (2.*.radius) in
	let a = 1. in
	let b = -2.*.ycenter in
	let c = ycenter**2. -. radius**2. +. (x-.xcenter)**2. in
	let l = List.map ( fun a -> (round(x),round(a)) ) (solve (a,b,c)) in
	  if (Random.int 2)=0 then point (List.hd l) name col flag fig
	  else point (List.hd (List.rev l)) name col flag fig
	    
      let line (a: point) (b: point) name col (flag: flag_object) (fig: figure) = 
	let l = new line a b name col flag in
	  (fig#add_line l;l)

      let segment (a: point) (b: point) name col (flag: flag_object) (fig: figure) =
	let s = new segment a b name col flag in
	  (fig#add_segment s;s)

      let circle (a: point) (b: point) name col (flag: flag_object) (fig: figure) = 
	let c = new circle a b name col flag in
	  (fig#add_circle c;c)

      let middle (a: point) (b: point) name col (flag: flag_object) (fig: figure) =
	point ((a#get_x () + b#get_x ())/2,(a#get_y () + b#get_y ())/2) name col flag fig
	  
      let perpendicular (l: line) (p: point) name col (flag: flag_object) (fig: figure) =
	let p2 = point (p#get_x () + l#get_dy (),p#get_y () - l#get_dx ()) (Generate.point_name ()) GeoColor.transp NO_DRAW fig in
	  line p p2 name col flag fig
	    
      let perpendicular_seg (l: line) (p: point) (p1: point) (p2: point) name col (flag: flag_object) (fig: figure) =
	let length1 = (sqrt (((float_of_int (p1#get_x() - p2#get_x())**2.)) +. (((float_of_int (p1#get_y() - p2#get_y())**2.))))) in
	let length2 = (sqrt (((float_of_int (l#get_dx ())**2.)) +. (((float_of_int (l#get_dy ())**2.))))) in
	let length = length1 /. length2 in
	let x = round ((float_of_int (l#get_dy ())) *. length) in
	let y = round ((float_of_int (l#get_dx ())) *. length) in
	let p3 = point (p#get_x () + x,p#get_y () - y) (Generate.point_name ()) GeoColor.transp NO_DRAW fig in
	  segment p p3 name col flag fig
	    
      let parallel (l: line) (p: point) name col (flag: flag_object) (fig: figure) =
	let p2 = point (p#get_x () - l#get_dx (),p#get_y () - l#get_dy ()) (Generate.point_name ()) GeoColor.transp NO_DRAW fig in
	  line p p2 name col flag fig
	    
      let parallel_seg (l: line) (p: point) (p1: point) (p2: point) name col (flag: flag_object) (fig: figure) =
	let length1 = (sqrt (((float_of_int (p1#get_x() - p2#get_x())**2.)) +. (((float_of_int (p1#get_y() - p2#get_y())**2.))))) in
	let length2 = (sqrt (((float_of_int (l#get_dx ())**2.)) +. (((float_of_int (l#get_dy ())**2.))))) in
	let length = length1 /. length2 in
	let x = round ((float_of_int (l#get_dx ())) *. length) in
	let y = round ((float_of_int (l#get_dy ())) *. length) in
	let p3 = point (p#get_x () - x,p#get_y () - y) (Generate.point_name ()) GeoColor.transp NO_DRAW fig in
	  segment p p3 name col flag fig
	    
      let perpendicular_bissector (l: line) name col (flag: flag_object) (fig: figure) =
	let a = l#get_a () in
	let b = l#get_b () in
	let i = middle a b (Generate.point_name ()) GeoColor.transp NO_DRAW fig in
	  perpendicular l i name col flag fig
	    
      let angle_bissector (b: point) (a: point) (c: point) name col (flag: flag_object) (fig: figure) =
	let d1 = sqrt (((float_of_int (a#get_x() - b#get_x())**2.)) +. (((float_of_int (a#get_y() - b#get_y())**2.)))) in
	let d2 = sqrt (((float_of_int (a#get_x() - c#get_x())**2.)) +. (((float_of_int (a#get_y() - c#get_y())**2.)))) in
	let d = d1 /. d2 in
	let (x,y) = (float(a#get_x()) +. float(c#get_x()-a#get_x())*.d,float(a#get_y()) +. float(c#get_y()-a#get_y())*.d) in
	let (x',y') = ((b#get_x() + round(x))/2,(b#get_y() + round(y))/2) in
	let p = point (x',y') (Generate.point_name ()) GeoColor.transp NO_DRAW fig in
	  line a p name col flag fig
	    
      let inter_ll (l1: line) (l2: line) name col (flag: flag_object) (fig: figure) =
	let (x,y) = 
	  if l1#get_dx () = 0 then
	    begin
              let a = l1#get_a () in
		(a#get_x () , round(l2#get_m () *. float_of_int(a#get_x ()) +. l2#get_p ()) )
	    end
	  else
	    if l2#get_dx () = 0 then
              begin
		let a = l2#get_a () in
		  (a#get_x () , round(l1#get_m () *. float_of_int(a#get_x ()) +. l1#get_p ()) )
              end
	    else
              line_with_line (l1#get_m (),l1#get_p ()) (l2#get_m (),l2#get_p ()) in
	  point (x,y) name col flag fig
	    
      let inter_lc (l: line) (c: circle) name1 name2 col (flag: flag_object) (fig: figure) =
	let o = c#get_center () in
	let r = c#get_radius () in
	let l =
	  if l#get_dx () = 0 then
	    let x = (l#get_a())#get_x() in
	    let a = 1. in
	    let b = -2. *. float_of_int(o#get_y()) in
	    let c = float_of_int(x - o#get_x()) ** 2. -. float_of_int(r) ** 2. +. float_of_int(o#get_y()) ** 2. in
	      List.map (fun y -> (x,round(y))) (solve (a,b,c))
	  else
	    let m = l#get_m () in
	    let p = l#get_p () in
	      circle_with_line (float(o#get_x ()),float(o#get_y ()),float(r)) (m,p)
	in
	let (x1,y1) = List.hd l and (x2,y2) = List.hd (List.rev l) in
	  if (x1,y1) = (x2,y2) then
	    let p = point (x1,y1) name1 col flag fig in (p,p)
	  else
	    let (p1,p2) = (point (x1,y1) name1 col flag fig,point (x2,y2) name2 col flag fig) in (p1,p2)
												   
      let inter_cc (c1: circle) (c2: circle) name1 name2 col (flag: flag_object) (fig: figure) =
	let o1 = c1#get_center () in
	let o2 = c2#get_center () in
	let r1 = c1#get_radius () in
	let r2 = c2#get_radius () in
	let l = circle_with_circle (float(o1#get_x()),float(o1#get_y())+.0.01,float(r1)) (float(o2#get_x()),float(o2#get_y()),float(r2)) in
	let (x1,y1) = List.hd l and (x2,y2) = List.hd (List.rev l) in
	  if (x1,y1) = (x2,y2) then
	    let p = point (x1,y1) name1 col flag fig in (p,p)
	  else
	    let (p1,p2) = (point (x1,y1) name1 col flag fig,point (x2,y2) name2 col flag fig) in (p1,p2)
	
    end

module type DrawingObjModule =
    sig

      module GeoDraw: GeoDrawModule

      val point :
        int * int ->
        string ->
        GeoColor.t -> flag_object -> GeoDraw.figure -> GeoDraw.point
      val point_on_line :
        GeoDraw.line ->
        string ->
        GeoColor.t -> flag_object -> GeoDraw.figure -> GeoDraw.point
      val point_on_circle :
        GeoDraw.circle ->
        string ->
        GeoColor.t -> flag_object -> GeoDraw.figure -> GeoDraw.point
      val line :
        GeoDraw.point ->
        GeoDraw.point ->
        string -> GeoColor.t -> flag_object -> GeoDraw.figure -> GeoDraw.line
      val segment :
        GeoDraw.point ->
        GeoDraw.point ->
        string ->
        GeoColor.t -> flag_object -> GeoDraw.figure -> GeoDraw.segment
      val circle :
        GeoDraw.point ->
        GeoDraw.point ->
        string ->
        GeoColor.t -> flag_object -> GeoDraw.figure -> GeoDraw.circle
      val middle :
        GeoDraw.point ->
        GeoDraw.point ->
        string ->
        GeoColor.t -> flag_object -> GeoDraw.figure -> GeoDraw.point
      val perpendicular :
        GeoDraw.line ->
        GeoDraw.point ->
        string -> GeoColor.t -> flag_object -> GeoDraw.figure -> GeoDraw.line
      val perpendicular_seg :
        GeoDraw.line ->
        GeoDraw.point ->
        GeoDraw.point ->
        GeoDraw.point ->
        string ->
        GeoColor.t -> flag_object -> GeoDraw.figure -> GeoDraw.segment
      val parallel :
        GeoDraw.line ->
        GeoDraw.point ->
        string -> GeoColor.t -> flag_object -> GeoDraw.figure -> GeoDraw.line
      val parallel_seg :
        GeoDraw.line ->
        GeoDraw.point ->
        GeoDraw.point ->
        GeoDraw.point ->
        string ->
        GeoColor.t -> flag_object -> GeoDraw.figure -> GeoDraw.segment
      val perpendicular_bissector :
        GeoDraw.line ->
        string -> GeoColor.t -> flag_object -> GeoDraw.figure -> GeoDraw.line
      val angle_bissector :
        GeoDraw.point ->
        GeoDraw.point ->
        GeoDraw.point ->
        string -> GeoColor.t -> flag_object -> GeoDraw.figure -> GeoDraw.line
      val inter_ll :
        GeoDraw.line ->
        GeoDraw.line ->
        string ->
        GeoColor.t -> flag_object -> GeoDraw.figure -> GeoDraw.point
      val inter_lc :
        GeoDraw.line ->
        GeoDraw.circle ->
        string ->
        string ->
        GeoColor.t ->
        flag_object -> GeoDraw.figure -> GeoDraw.point * GeoDraw.point
      val inter_cc :
        GeoDraw.circle ->
        GeoDraw.circle ->
        string ->
        string ->
        GeoColor.t ->
        flag_object -> GeoDraw.figure -> GeoDraw.point * GeoDraw.point
    end

(*
module DrawingObj = DrawingObjFunc (GeoDraw)

module DrawingObjAA = DrawingObjFunc (GeoDrawAA)

module DrawingObjPs = DrawingObjFunc (GeoDrawPs)
*)
;;










