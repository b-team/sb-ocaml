(* $Id: geoPs.ml,v 1.1 2004/04/28 17:54:35 gmariano Exp $ *)

(* Module pour la g�n�ration de postscript � partir du XML *)

(* la variable "sources" doit �tre encapsul�e *)
let make_postcript sources =
  let channel = open_out ((!sources)^".xml") in
    output_string channel Xml.version;
    (*
      output_string channel Xml.doctype;
    *)
    Xmlexpansed.open_balise "figure" "" channel;
    GeoTree.write_figure2 GeoTree.my_figure channel;
    Xmlexpansed.close_balise "figure" channel;
    close_out channel;
    (*MG
      Sys.command ("./postcript.sh "^(!sources)^".xml") ;
    *)
    let 
      s1 = "./postcript.sh "^(!sources)^".xml" 
    in 
      GeoSystem.call s1    ;()
      
