{

open Parser

exception Eof
exception Shutdown

} 

rule token = parse
  [' ' '\t' '\n'] {token lexbuf}
| '.' {EOL}
| ';' {EOL}
|"/*"      { comment lexbuf }
| ',' {VIRGULE}
| "/" {SLASH}
| "soit" {SOIT}
| "Soit" {SOIT}
| "soient" {SOIT}
| "Soient" {SOIT}
| "un" {UN}
| "une" {UNE}
| "en" {EN}
| "la" {LA}
| "le" {LE}
| "de" {DE}
| ['a'-'z']['0'-'9']* {IDENT (Lexing.lexeme lexbuf)}
| ['A'-'Z']['0'-'9']* {IDENT (Lexing.lexeme lexbuf)}
| ['A'-'Z']''' {IDENT (Lexing.lexeme lexbuf)}
| ['0'-'9']+ {NUM (int_of_string (Lexing.lexeme lexbuf))}
| '[' {CROCHET_OUV}
| ']' {CROCHET_FER}
| '(' {PARENT_OUV}
| ')' {PARENT_FER}
| '|' {NORME}
| "diametre" {DIAMETRE}
| "rayon" {RAYON}
| "point" {POINT}
| "points" {POINT}
| "droite" {DROITE}
| "segment" {SEGMENT}
| "cercle" {CERCLE}
| "longueur" {LONGUEUR}
| "quelconque" {QUELCONQUE}
| "quelconques" {QUELCONQUE}
| "intersection" {INTERSECTION}
| "intersections" {INTERSECTION}
| "milieu" {MILIEU}
| "m�diatrice" {MEDIATRICE}
| "mediatrice" {MEDIATRICE}
| "bissectrice" {BISSECTRICE}
| "perpendiculaire" {PERPENDICULAIRE}
| "parall�le" {PARALLELE}
| "parallele" {PARALLELE}

(* les geo-couleurs... *) 
| "rouge" {ROUGE}
| "vert" {VERT}
| "bleu" {BLEU}
| "noir" {NOIR}
| "orange" {ORANGE}
| "jaune" {JAUNE}
| "rose" {ROSE}
| "et" {ET}
| "�" {A}
| "a" {A}
| "passant" {PASSANT}
| "par" {PAR}
| "centre" {CENTRE}
| "efface" {EFFACE}
| "effacer" {EFFACE}
| "supprime" {SUPPRIME}
| "supprimer" {SUPPRIME}
| "quitter" {raise Shutdown}
| "quitte" {raise Shutdown}
| eof {raise Eof}
and comment = parse
    "*/"  { token lexbuf   }
| _     { comment lexbuf }


