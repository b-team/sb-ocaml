(* $Id: main.ml,v 1.3 2004/05/01 09:49:03 jnarboux Exp $ *)


type action = PSTRICKS | XML | DRAWING | EMPTY

type input_source = FILE | INTERACTIVE_MODE

let source = ref INTERACTIVE_MODE

let todo = ref DRAWING

let version = ref false
let author = ref false
let licence = ref false

let file = ref "geocaml"

let xwin = ref 640

let ywin = ref 480 

let wait f =
  while true do f#action() done

module GeoDraw = GeoGraphics.DrawFunc (GeoGraphics.Draw)

module GeoDrawPs = GeoGraphics.DrawFunc (GeoGraphics.DrawPs)

let open_figure () =
  match !GeoGraphics.render with
    | GeoGraphics.NORMAL | GeoGraphics.ANTIALIAS ->
	let f = new GeoDraw.figure (!xwin,!ywin) in f
    | GeoGraphics.PS ->
	GraphicsPs.open_ps (!file^".ps");
	let f = new GeoDrawPs.figure (!xwin,!ywin) in
	  f#close ();
	  GraphicsPs.open_graph "";
	  f

let close_figure () =
  match !GeoGraphics.render with
    | GeoGraphics.NORMAL | GeoGraphics.ANTIALIAS -> Graphics.close_graph ()
    | GeoGraphics.PS -> GraphicsPs.close_graph ()

let drawNormal f =
  let module GeoTreeDrawing = GeoTree.TreeToDrawing (Drawing.DrawingObjFunc (GeoDraw)) in
    GeoTreeDrawing.draw_figure GeoTree.my_figure f GeoGraphics.DRAW;
    f#draw ()
    
let drawAA f =
  let module GeoDrawAA = GeoGraphics.DrawFunc (GeoGraphics.DrawAntialias) in
  let module GeoTreeDrawing = GeoTree.TreeToDrawing (Drawing.DrawingObjFunc (GeoDrawAA)) in
    GeoTreeDrawing.draw_figure GeoTree.my_figure f GeoGraphics.DRAW;
    f#draw ()

let drawPs f =
  let module GeoDrawPs = GeoGraphics.DrawFunc (GeoGraphics.DrawPs) in
  let module GeoTreeDrawing = GeoTree.TreeToDrawing (Drawing.DrawingObjFunc (GeoDrawPs)) in
    GeoTreeDrawing.draw_figure GeoTree.my_figure f GeoGraphics.DRAW;
    f#draw ()
 
let draw f =
  match !GeoGraphics.render with
    | GeoGraphics.NORMAL -> drawNormal f
    | GeoGraphics.PS -> drawPs f
    | GeoGraphics.ANTIALIAS -> drawAA f

let analyse_file s =
  let ch_in = open_in s in
  (try
    let lexbuf = Lexing.from_channel ch_in in
    while true do
      let result = Parser.start Lexer.token lexbuf in 
      Abrlex.add_abr result GeoTree.my_figure;flush stdout
    done
   with Lexer.Eof -> (
     (*MG print_string "Parsing ok...\n"; *)
     flush stdout
   )
  )

let print_mess_error type_of_mess mess =
  begin
    print_string ("\t% "^type_of_mess^":\n");
    print_string ("\t% "^mess^"\n");
    print_string ("# ")
  end

let rec analyse_stdin f =
  try
    let lexbuf = Lexing.from_channel stdin in
      while true do
	let result = Parser.start Lexer.token lexbuf in 
	  begin
	    Abrlex.add_abr_for_im result GeoTree.my_figure f;
	    draw f;
	    print_string "# ";
	    flush stdout
	  end
      done
  with Lexer.Shutdown -> (print_string "Bye...\n";flush stdout;exit 0)
    | Lexer.Eof -> 
	begin
	  print_string "\n# ";
	  flush stdout;
	  let newstdin = Unix.openfile "/dev/tty" [Unix.O_RDWR] 0 in
	    (Unix.dup2 newstdin Unix.stdin;analyse_stdin f)
	end
    | Parsing.Parse_error -> (print_mess_error "Syntaxe incorrecte!" "Voir manuel";flush stdout;analyse_stdin f)
    | e -> (print_mess_error "Erreur !" (Abrlex.treat_exception e);flush stdout;analyse_stdin f)


    
let write_xml () =
  let channel = stdout in 
    output_string channel Xml.version;
    (*output_string channel Xml.doctype;*)
    Xml.open_balise "figure" "" channel;
    GeoTree.write_figure GeoTree.my_figure channel;
    Xml.close_balise "figure" channel;
    close_out channel
      
let options_list =
  [
   ("--version",Arg.Set version,"affichage du num�ro de version");
   ("-v", Arg.Set version, "affichage du num�ro de version");
   ("--auteur",  Arg.Set author, "Liste des auteurs");
   ("--licence",  Arg.Set licence, "Affiche les informations de licence");
   ("--hwin",
    Arg.Int (fun y -> ywin := y),
    "hauteur de la fen�tre graphique")
    ;
    ("--lwin",
     Arg.Int (fun x -> xwin := x),"largeur de la fen�tre graphique")
    ;
    ("--ps",
     Arg.Unit(fun () -> GeoGraphics.render := GeoGraphics.PS),"g�n�ration Postcript")
    ;
    ("--tex",
     Arg.Unit(fun () -> todo := PSTRICKS),"g�n�ration Pstricks")
    ;
    ("--xml",
     Arg.Unit(fun () -> todo := XML),"sauvegarde XML")
    ;
    ("--gr",
     Arg.Unit(fun () -> todo := DRAWING;GeoGraphics.render := GeoGraphics.NORMAL),"rendu graphique normal")
    ;
    ("--aa",
     Arg.Unit(fun () -> todo := DRAWING;GeoGraphics.render := GeoGraphics.ANTIALIAS),"rendu graphique antialis�")
  ]

let use_message = "Usage : "^Sys.argv.(0)^" [options] fichier source"

let treat_end s =
  begin
    source := FILE;
    file := s
  end

let main() =
  if !Sys.interactive then () else
  begin
    Random.self_init();
    Arg.parse options_list treat_end use_message;
    if !version then 
      GeoMessage.print_version ()
    else if !author then
      begin
	print_endline "GeoCaml was developed by :";
	print_endline "vos noms ici";
	print_endline "Please use http://gna.org/bugs/?group=geocaml to report bugs."
      end
    else if !licence then
      print_endline "GeoCaml is distributed under GNU General Public Licence Version 2"
    else
      begin
	(match !source with
	| FILE ->
	    begin
	      analyse_file !file
	    end
	| INTERACTIVE_MODE -> 
	    begin
	      GeoMessage.prompt ();
	      let f = open_figure () in
	      draw f;analyse_stdin f
	    end
	);
	match !todo with
	| DRAWING ->
	    let f =  open_figure () in
	    draw f;wait f;
	    close_figure ()
	| XML -> write_xml () (* GeoXml.write/write !? *)
	| PSTRICKS -> ()
	| EMPTY -> ()
      end
  end

let _ =
    Printexc.catch main ()

;;























