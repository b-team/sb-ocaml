(* Module point *)

let get_point name fig =
  let (_,p) = List.find (fun (s,_) -> s=name) !(fig.point_list) in p

