(*******************************************************)
(*                                                     *)
(*              Wolf: w-order logic frame              *)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

(** The Shrink *)

let rec process = fun f i o -> 
  begin 
    f (fun buf -> input i buf 0 (String.length buf)) 
      (fun buf len -> output o buf 0 len)
  end 

let compress = fun i o -> process Zlib.compress i o 

let uncompress = fun i o -> process Zlib.uncompress i o 
