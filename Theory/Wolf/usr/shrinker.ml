(*******************************************************)
(*                                                     *)
(*              Wolf: w-order logic frame              *)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

let usage = "usage: shrinker [-d] <files>\n" 

let files = ref ([] : string list)

let rec options = 
  [
    ("-h",Arg.Unit(fun () -> Arg.usage options usage; exit 0),"\t\tprint this help")
    ;
    ("-help",Arg.Unit(fun () -> Arg.usage options usage; exit 0),"")
    ;
    ("--help",Arg.Unit(fun () -> Arg.usage options usage; exit 0),"")
  ]

let main = fun () -> 
  let rec h = function 
    [] -> () 
  | f :: l -> 
      let g = f ^ ".zzz" in 
      let ic = open_in_bin f in 
      let oc = open_out_bin g 
      in Shrinking.compress ic oc; close_in ic; close_out oc; h l
  in h ! files 

let _ = Arg.parse options (fun x -> files := x :: !files) usage; main () 
