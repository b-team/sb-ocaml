type t =
    Var of string
  | Cst of Type.t Symb.t
  | Def of t Symb.t
  | App of t * t
  | Abs of (string -> t)
  | Brk of string * t
  | Cnt of (string -> t)
val subst : int -> (string -> t) -> t -> t
val bind : string -> t -> string -> t
val mem : string -> t -> bool
val base : t -> string list
val compare : t -> t -> int
val eval : int -> t -> t
val unify : ('a -> t) -> (t * t) list -> 'a -> t
val check :
  (string * Type.t) list * (string * Type.t) list * (Type.t * Type.t) list *
  (t * Type.t) list -> (Type.t * Type.t) list
