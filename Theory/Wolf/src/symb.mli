type 'a t = { name : string; contents : 'a; }
val symbol_of : string -> 'a -> 'a t
val string_of : 'a t -> string
val contents_of : 'a t -> 'a
val compare : ('a -> 'b -> int) -> 'a t -> 'b t -> int
