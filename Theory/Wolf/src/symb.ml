(*******************************************************)
(*                                                     *)
(*              Wolf: w-order logic frame              *)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

(** Symbol Library *)

(** 
  Symbols are [t ::= n,x]. 
*)
type 'a t = {name : string ; contents : 'a}

let symbol_of = fun n c -> {name = n ; contents = c}

let string_of = fun c -> c.name

let contents_of = fun c -> c.contents

let compare = fun f s s' -> 
  match String.compare (string_of s) (string_of s') with 
      0 -> f (contents_of s) (contents_of s')
    | n -> n
