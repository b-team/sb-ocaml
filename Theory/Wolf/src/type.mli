type t =
    Var of string
  | Cst of int Symb.t * t list
  | Def of t Symb.t
  | All of (string -> t)
val subst : (string -> t) -> t -> t
val bind : string -> t -> string -> t
val mem : string -> t -> bool
val base : t -> string list
val compare : t -> t -> int
val unify : 'a -> ('b -> t) -> (t * t) list -> 'b -> t
val var : string -> t
val bot : t
val arr : t * t -> t
val all : string -> t -> t
