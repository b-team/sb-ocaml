(*******************************************************)
(*                                                     *)
(*              Wolf: w-order logic frame              *)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

(** Tree Library *)

(** Values are: *)
type v = Term.t * Type.t

(** Sequents are: *)
type s = Right of v list * v * v list

(** Trees are: *)
type t = Leaf of s | Node of s * t list

(** The function [map g f t] returns the tree [g (t [x <- f(x)])]. *) 
let rec map = fun g f -> function 
    Leaf x -> g (Leaf (f x))
  | Node (x,l) -> g (Node ((f x),(List.map (fun x -> map g f x) l))) 

(** The function [subst f t] returns the tree. *)
let subst = fun f -> 
  let first = ref true in 
  let rec g = fun f -> function 
      Leaf x -> 
	if !first 
	then let _ = first := false in Node (x,(List.map (fun x -> Leaf x) (f x)))
	else Leaf x
    | Node (x,l) -> Node (x,(List.map (fun x -> g f x) l))
  in g f
