(*******************************************************)
(*                                                     *)
(*              Wolf: w-order logic frame              *)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

(** Xml sender *)

let name = ref "localhost"

let port = ref 1881 

let usage = "usage: sender --name " ^ !name ^" --port " ^ (string_of_int !port) ^ "\n" 

let rec options = 
  [
    ("-n",Arg.String(fun n -> name := n),"\t\tset name server")
    ;
    ("-name",Arg.String(fun n -> name := n),"")
    ;
    ("--name",Arg.String(fun n -> name := n),"")
    ;
    ("-p",Arg.Int(fun n -> port := n),"\t\tset port number")
    ;
    ("-port",Arg.Int(fun n -> port := n),"")
    ;
    ("--port",Arg.Int(fun n -> port := n),"")
    ;
    ("-h",Arg.Unit(fun () -> Arg.usage options usage; exit 0),"\t\tprint this help")
    ;
    ("-help",Arg.Unit(fun () -> Arg.usage options usage; exit 0),"")
    ;
    ("--help",Arg.Unit(fun () -> Arg.usage options usage; exit 0),"")
  ]

let main = fun () -> 
  let copy = fun i o -> Wolf.main (fun x -> x) i o in 
  let connect = fun father child -> 
    let adress = (Unix.gethostbyname !name).Unix.h_addr_list.(0) in 
    let socket = Unix.ADDR_INET(adress,!port) in 
    let ic,oc = Unix.open_connection socket in 
      match Unix.fork () with
	  0 -> if Unix.fork () = 0 then child oc; exit 0
	| id -> father ic; Unix.shutdown_connection ic; ignore (Unix.waitpid [] id) 
  in connect (fun i -> copy i stdout) (copy stdin)

let _ = Arg.parse options (fun x -> ()) usage; main () 
