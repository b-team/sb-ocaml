(*******************************************************)
(*                                                     *)
(*              Wolf: w-order logic frame              *)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

(** *)

let usage = "usage: prover\n" 

let rec options = 
  [
    ("-h",Arg.Unit(fun () -> Arg.usage options usage; exit 0),"\t\tprint this help")
    ;
    ("-help",Arg.Unit(fun () -> Arg.usage options usage; exit 0),"")
    ;
    ("--help",Arg.Unit(fun () -> Arg.usage options usage; exit 0),"")
  ]

let _ = 
  Arg.parse options (fun x -> ()) usage; 
  try Wolf.main (fun x -> x) stdin stdout 
  with Failure e -> output_string stderr e; exit 2
    | _ -> exit 1
