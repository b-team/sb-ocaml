(*******************************************************)
(*                                                     *)
(*              Wolf: w-order logic frame              *)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

(** Type Library *)

(** 
  Types are [t ::= x | c (t,..,t) | d = t | /\x t]. 
*)
type t = 
    Var of string
  | Cst of int Symb.t * t list
  | Def of t Symb.t
  | All of (string -> t)

(** 
  The function [subst f t] returns [t [x <- u]] if [f (x) = u].
*)
let rec subst = fun f -> function
    Var x -> f x
  | Cst (c,l) -> Cst (c,(List.map (fun x -> subst f x) l))
  | Def d -> Def (Symb.symbol_of (Symb.string_of d) (subst f (Symb.contents_of d)))
  | All t -> All (fun x -> subst f (t x))

(** 
  The function [bind x t] binds variable [x] in term [t]. 
*)
let bind = fun x t y -> subst (fun z -> if x = z then Var y else Var z) t 

(** 
  The function [mem x t] is true if [x] is a free variable of [t] and false else. 
*)
let rec mem = fun e -> function 
    Var x -> x = e 
  | Cst (c,l) -> List.exists (fun x -> mem e x) l
  | Def d -> mem e (Symb.contents_of d)
  | All t -> mem e (t "") 

(** 
  The function [base t] returns the free variables list of [t]. 
*)
let rec base = function 
    Var x -> if x = "" then [] else [x] 
  | Cst (c,l) -> List.concat (List.map (fun x -> base x) l)
  | Def d -> base (Symb.contents_of d)
  | All t -> base (t "") 

(** 
  The function [compare u v] returns 0 if [u = v], -1 if [u < v] and  1 if [u > v]. 
*)
let compare = 
  let rec f = fun n -> function 
      Var x -> 
	begin function 
	    Var x' -> String.compare x x'
	  | _  -> -1
	end 
    | Cst (c,l) -> 
	begin function 
	    Var x -> 1 
	  | Cst (c',l') -> 
	      begin match Symb.compare Pervasives.compare c c' with 
		  0 -> g n l l'
		| m -> m
	      end 
	  | _ -> -1
	end 
    | Def d -> 
	begin function 
	    Var x -> 1
	  | Cst (c,l) -> 1
	  | Def d' -> Symb.compare (f n) d d'
	  | _ -> -1
	end 
    | All t -> 
	begin function 
	    All t' -> f (n+1) (t (string_of_int n)) (t' (string_of_int n))
	  | _ -> 1
	end 
  and g = fun n -> function 
      [] -> 
	begin function 
	    [] -> 0 
	  | _ -> assert false
	end 
    | t :: l -> 
	begin function 
	    [] -> assert false 
	  | t' :: l' -> 
	      begin match f n t t' with 
		| 0 -> g n l l'
		| m -> m
	      end 
	end 
  in f 0

(** 
  The function [unify sigma epsilon] returns the most general unificator 
  that satisfy all type equations in [epsilon]. 
*)
let unify = fun sigma -> 
  let rec f = fun n sigma -> function 
      [] -> sigma 
    | (Var x,Var x') :: epsilon -> 
	if x = x' 
	then f n sigma epsilon 
	else 
	  let g = fun y -> if x = y then Var x' else Var y 
	  in f n (fun z -> subst g (sigma z)) 
	       (List.map (fun t -> ((subst g (fst t)),(subst g (snd t)))) epsilon) 
    | (Var x,t) :: epsilon 
    | (t,Var x) :: epsilon -> 
	if mem x t 
	then failwith "unification failed by occur-check\n" 
	else 
	  let g = fun y -> if x = y then t else Var y
	  in f n (fun z -> subst g (sigma z))
	      (List.map (fun t -> ((subst g (fst t)),(subst g (snd t)))) epsilon)
    | (Cst (c,l),Cst (c',l')) :: epsilon -> 
	if (Symb.compare Pervasives.compare c c') = 0
	then f n sigma ((List.combine l l') @ epsilon)
	else failwith "unification failed by clash\n" 
    | (Def d,t) :: epsilon 
    | (t,Def d) :: epsilon -> f n sigma (((Symb.contents_of d),t) :: epsilon)
    | (All t,u) :: epsilon 
    | (u,All t) :: epsilon -> f (n+1) sigma (((t (string_of_int n)),u) :: epsilon) 
  in f 0

let var = fun x -> Var x

let bot = Cst ((Symb.symbol_of "" 0),[])

let arr = fun (x,y) -> Cst ((Symb.symbol_of "" 0),[x;y])

let all = fun x t -> All (bind x t)
