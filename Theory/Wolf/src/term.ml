(*******************************************************)
(*                                                     *)
(*              Wolf: w-order logic frame              *)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

(** Term Library *)

(** 
  Terms are [t ::= x | c | d = t | t t | \x t | [x] t | �x t]. 
*)
type t = 
    Var of string
  | Cst of Type.t Symb.t
  | Def of t Symb.t
  | App of t * t
  | Abs of (string -> t)
  | Brk of string * t
  | Cnt  of (string -> t)

(** 
  The function [subst n f t] returns [t [x <- u]] if [n = 0], 
  [t [[x]v <- [x](v)u]] if [n = 1] and 
  [t [[x]v <- [x](u)v]] if [n = 2] where [f (x) = u]. 
*)
let rec subst = fun n f t -> match n with 
    0 -> 
      begin 
	match t with 
	    Var x -> f x
	  | Cst c -> Cst c
	  | Def d -> Def (Symb.symbol_of (Symb.string_of d) (subst n f (Symb.contents_of d)))
	  | App (u,v) -> App ((subst n f u),(subst n f v))
	  | Abs u -> Abs (fun x -> subst n f (u x)) 
	  | Brk (x,u) -> Brk (x,(subst n f u))
	  | Cnt u -> Cnt (fun x -> subst n f (u x)) 
      end
  | 1 ->
      begin 
	match t with 
	    Var x -> Var x
	  | Cst c -> Cst c
	  | Def d -> Def (Symb.symbol_of (Symb.string_of d) (subst n f (Symb.contents_of d)))
	  | App (u,v) -> App ((subst n f u),(subst n f v))
	  | Abs u -> Abs (fun x -> subst n f (u x)) 
	  | Brk (x,u) -> 
	      begin
		try Brk (x,App((subst n f u),(f x)))
		with _ -> Brk (x,(subst n f u))
	      end
	  | Cnt u -> Cnt (fun x -> subst n f (u x)) 
      end 
  | 2 -> 
      begin
	match t with 
	    Var x -> Var x
	  | Cst c -> Cst c
	  | Def d -> Def (Symb.symbol_of (Symb.string_of d) (subst n f (Symb.contents_of d)))
	  | App (u,v) -> App ((subst n f u),(subst n f v))
	  | Abs u -> Abs (fun x -> subst n f (u x)) 
	  | Brk (x,u) -> 
	      begin
		try Brk (x,App((f x),(subst n f u)))
		with _ -> Brk (x,(subst n f u))
	      end
	  | Cnt u -> Cnt (fun x -> subst n f (u x)) 
      end
  | _ -> assert false


(** 
  The function [bind x t] binds the variable [x] in the term [t]. 
*)
let bind = fun x t y -> subst 0 (fun z -> if x = z then Var y else Var z) t

(** 
  The function [mem x t] is true if [x] is a free variable of [t] and false else. 
*)
let rec mem = fun e -> function 
    Var x -> x = e 
  | Cst c -> false
  | Def d -> mem e (Symb.contents_of d)
  | App (u,v) -> (mem e u) || (mem e v) 
  | Abs t -> mem e (t "") 
  | Brk (x,t) -> if x = e then true else mem e t
  | Cnt t -> mem e (t "") 

(** 
  The function [base t] returns the free variables list of [t]. 
*)
let rec base = function 
    Var x -> if x = "" then [] else [x] 
  | Cst c -> []
  | Def d -> base (Symb.contents_of d)
  | App (u,v) -> ((base u) @ (base v)) 
  | Abs t -> base (t "") 
  | Brk (x,t) -> if x = "" then base t else x :: (base t)
  | Cnt t -> base (t "") 

(** 
  The function [compare u v] returns 0 if [u = v], -1 if [u < v] and 1 if [u > v]. 
*)
let compare = 
  let rec f = fun n -> function 
      Var x -> 
	begin function 
	    Var x' -> String.compare x x'
	  | _ -> -1
	end 
    | Cst c -> 
	begin function 
	    Var x -> 1
	  | Cst c' -> Symb.compare Type.compare c c'
	  | _ -> -1
	end 
    | Def d -> 
	begin function 
	    Var x -> 1 
	  | Cst c -> 1 
	  | Def d' -> Symb.compare (f n) d d'
	  | _ -> -1
	end 
    | App (u,v) -> 
	begin function 
	    Var x -> 1 
	  | Cst c -> 1 
	  | Def d -> 1
	  | App (u',v') -> 
	      begin match f n u u' with 
		  0 -> f n v v'
		| n -> n
	      end 
	  | _ -> -1
	end 
    | Abs t -> 
	begin function 
	    Var x -> 1
	  | Cst c -> 1
	  | Def d -> 1
	  | App (u,v) -> 1 
	  | Abs t' -> f (n+1) (t (string_of_int n)) (t' (string_of_int n))
	  | _ -> -1
	end 
    | Brk (x,t) -> 
	begin function 
	    Var x -> 1
	  | Cst c -> 1
	  | Def d -> 1
	  | App (u,v) -> 1
	  | Abs t -> 1 
	  | Brk (x',t') -> 
	      begin match String.compare x x' with 
		  0 -> f n t t'
		| n -> n
	      end 
	  | _ -> -1
	end 
    | Cnt t -> 
	begin function 
	    Cnt t' -> f (n+1) (t (string_of_int n)) (t' (string_of_int n))
	  | _ -> 1
	end 
  in f 0 

(** 
  The function [eval n t] returns the term [t] normal form 
  in call-by-name reduction if n = 0 
  in call-by-value if n = 1
  in another call-by-value if n = 2. 
*)
let rec eval = fun n -> 
  let rec f = fun m n -> function 
    Var x -> Var x 
  | Cst c -> Cst c
  | Def d -> Def (Symb.symbol_of (Symb.string_of d) (eval n (Symb.contents_of d)))
  | Brk (x,t) -> 
      begin match t with 
	  Cnt u -> f m n (u x)
	| _ -> Brk (x,(f m n t))
      end 
  | Abs t -> let k = string_of_int m in 
      begin match (t k) with 
	  App (u,Var k) -> 
	    if List.mem k (base u) 
	    then Abs (fun x -> f m n (t x))
	    else f m n u
	| _ -> Abs (fun x -> f m n (t x))
      end 
  | Cnt t -> let k = string_of_int m in 
      begin match (t k) with 
	  Brk (k,u) -> 
	    if List.mem k (base u) 
	    then Cnt (fun x -> f m n (t x))
	    else f m n u
	| _ -> Cnt (fun x -> f m n (t x))
      end 
  | App (u,v) as t -> 
      let g = fun m a b c d -> 
	f (m+1) a 
	(subst b (fun x -> if x = (string_of_int m) then d else Var x) (c (string_of_int m))) 
      in
	begin match n with 
	    0 -> 
	      begin match t with 
		  App (Abs u,v) -> g m n 0 u v
		| App (Cnt u,v) -> g m n 1 u v 
		| App (v,Cnt u) -> g m n 2 u v 
		| App (u,v) -> App ((f m n u),(f m n v))
		| _ -> assert false
	      end
	  | 1 -> 
	      begin match t with 
		  App (Cnt u,v) -> g m n 1 u v 
		| App (v,Cnt u) -> g m n 2 u v 
		| App (Abs u,v) -> g m n 0 u v 
		| App (u,v) -> App ((f m n u),(f m n v))
		| _ -> assert false
	      end
	  | 2 -> 
	      begin match t with 
		  App (v,Cnt u) -> g m n 2 u v 
		| App (Abs u,v) -> g m n 0 u v 
		| App (Cnt u,v) -> g m n 1 u v 
		| App (u,v) -> App ((f m n u),(f m n v))
		| _ -> assert false
	      end
	  | _ -> assert false
	end
  in f 0 n
	  
(** 
  The function [unify sigma epsilon] returns the most general unificator 
  that satisfy all term equations in [epsilon]. 
*)
let rec unify = fun sigma -> 
  let rec f = fun n sigma -> function 
      [] -> sigma 
    | (Var x,Var x') :: epsilon -> 
	if x = x' 
	then f n sigma epsilon 
	else 
	  let g = fun y -> if x = y then Var x' else Var y 
	  in f n (fun z -> subst 0 g (sigma z)) 
	       (List.map (fun t -> ((subst 0 g (fst t)),(subst 0 g (snd t)))) epsilon) 
    | (Var x,t) :: epsilon 
    | (t,Var x) :: epsilon -> 
	if mem x t 
	then failwith "unification failed by occur-check\n"   
	else 
	  let g = fun y -> if x = y then t else Var y 
	  in f n (fun z -> subst 0 g (sigma z)) 
	       (List.map (fun t -> ((subst 0 g (fst t)),(subst 0 g (snd t)))) epsilon) 
    | (Cst c,Cst c') :: epsilon -> 
	if c = c' then unify sigma epsilon
	else failwith "unification failed by clash\n"
    | (Def d,t) :: epsilon 
    | (t,Def d) :: epsilon -> f n sigma (((Symb.contents_of d),t) :: epsilon)
    | (App (u,v),App (u',v')) :: epsilon -> f n sigma ((u,u') :: (v,v') :: epsilon) 
    | (Abs u,Cnt v) :: epsilon 
    | (Cnt v,Abs u) :: epsilon -> failwith "unification failed by clash\n" 
    | (Abs t,u) :: epsilon 
    | (u,Abs t) :: epsilon -> f (n+1) sigma (((t (string_of_int n)),u) :: epsilon) 
    | (Cnt t,Cnt u) :: epsilon -> 
	f (n+1) sigma (((t (string_of_int n)),(u (string_of_int n))) :: epsilon) 
    | _ -> failwith "unification failed by clash\n" 
  in f 0 sigma 

(** 
  The function [check (gamma,delta,epsilon,lambda)] 
  defines the type checking for all terms in [lambda]. 
*)
let check = 
  let rec f = fun m n -> function 
      (gamma,delta,epsilon,(Var x,a) :: lambda) -> 
	if List.mem_assoc x gamma 
	then f m n (gamma,delta,((a,(List.assoc x gamma))::epsilon),lambda) 
	else assert false
    | (gamma,delta,epsilon,(Cst c,a) :: lambda) -> 
	f m n (gamma,delta,((a,(Symb.contents_of c))::epsilon),lambda) 
    | (gamma,delta,epsilon,(Def d,a) :: lambda) -> 
	f m n (gamma,delta,epsilon,(Symb.contents_of d,a) :: lambda) 
    | (gamma,delta,epsilon,(App (u,v),a) :: lambda) -> 
	let b = Type.var (string_of_int m) in 
	let c = Type.var (string_of_int (m+1)) 
	in f (m+2) n (gamma,delta,((b,(Type.arr (c,a)))::epsilon),((u,b)::(v,c)::lambda)) 
    | (gamma,delta,epsilon,(Abs t,a)::lambda) -> 
	let b = Type.var (string_of_int m) in 
	let c = Type.var (string_of_int (m+1)) in 
	let x = string_of_int n 
	in f (m+2) (n+1) 
	     (((x,b)::gamma),delta,((a,(Type.arr (b,c)))::epsilon),(((t x),c)::lambda)) 
    | (gamma,delta,epsilon,(Cnt t,a) :: lambda) -> 
	let b = Type.var (string_of_int m) in 
	let x = string_of_int n 
	in f (m+1) (n+1) 
	     (gamma,((x,a)::delta),(b,Type.bot)::epsilon,((t x),b)::lambda) 
    | (gamma,delta,epsilon,(Brk (x,t),a)::lambda) -> 
	let b = Type.var (string_of_int m) in 
	  if List.mem_assoc x delta 
	  then f (m+1) n 
	    (gamma,delta,(b,(List.assoc x delta))::(a,Type.bot)::epsilon,((t,b)::lambda))
	  else assert false
    | (gamma,delta,epsilon,[])-> epsilon 
  in f 0 0
