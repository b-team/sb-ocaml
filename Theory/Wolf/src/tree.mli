type v = Term.t * Type.t
type s = Right of v list * v * v list
type t = Leaf of s | Node of s * t list
val map : (t -> t) -> (s -> s) -> t -> t
val subst : (s -> s list) -> t -> t
