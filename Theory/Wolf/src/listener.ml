(*******************************************************)
(*                                                     *)
(*              Wolf: w-order logic frame              *)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

(** Xml listener *)

let name = ref ""

let service = fun s -> (fun i o -> Wolf.main (fun x -> x) i o)

let port = ref 1881 

let usage = "usage: listener --port " ^ (string_of_int !port) ^ "\n" 

let rec options = 
  [
    ("-n",Arg.String(fun n -> name := n),"\t\tset name service")
    ;
    ("-name",Arg.String(fun n -> name := n),"")
    ;
    ("--name",Arg.String(fun n -> name := n),"")
    ;
    ("-p",Arg.Int(fun n -> port := n),"\t\tset port number")
    ;
    ("-port",Arg.Int(fun n -> port := n),"")
    ;
    ("--port",Arg.Int(fun n -> port := n),"")
    ;
    ("-h",Arg.Unit(fun () -> Arg.usage options usage; exit 0),"\t\tprint this help")
    ;
    ("-help",Arg.Unit(fun () -> Arg.usage options usage; exit 0),"")
    ;
    ("--help",Arg.Unit(fun () -> Arg.usage options usage; exit 0),"")
  ]

let main = fun () -> 
    let establish = fun f -> 
      let adress = (Unix.gethostbyname(Unix.gethostname ())).Unix.h_addr_list.(0) in 
      let socket = (Unix.ADDR_INET(adress,!port)) in 
      let server = fun g -> Unix.establish_server g socket 
      in server f
    in establish (service !name)

let _ = Arg.parse options (fun x -> ()) usage; main () 
