
module type GeneSig = 
sig
  type t

  val mutate : t -> t
  val crossover : t -> t -> t * t
  val to_string : t -> string

end


module Gene = 
struct 
  type optype = 
      | Plus 
      | Times
	  
  type opnode = {left : peano; right : peano; operation : optype}
  and nodeinfo = {nodes : int; height : int}
  and peano = 
      | Operation of nodeinfo * opnode
      | Constant of int

  type t = peano


  let rec string_of_peano expr = 
    match expr with
      | Operation(info, op) ->
	  "(" ^ (string_of_peano op.left) 
	  ^ " " ^ (string_of_op op.operation) ^ " " 
	  ^ (string_of_peano op.right) ^ ")"
      | Constant(i) -> string_of_int i
  and string_of_op op = 
    match op with
      | Plus -> "+"
      | Times -> "*"
	  
 let rec node_number expr = 
    match expr with
      | Operation(info, op) -> info.nodes
      | Constant(i) -> 1
	  
  let height expr = 
    match expr with
      | Operation(info, op) -> info.height
      | Constant(i) -> 0
	  
  let rec eval_peano expr = 
    match expr with
      | Operation(info, op) ->
	  (eval_op op.operation) (eval_peano op.left) (eval_peano op.right)
      | Constant(i) -> i
  and eval_op op = 
    match op with
      | Plus -> (fun i j -> i + j)
      | Times -> (fun i j -> i * j)
	  
	  
  let random_location expr = 
    match expr with
      | Constant(_) -> 0
      | Operation(info, op) -> Random.int info.nodes
	  

  let rec get_location number tree =
    assert( (print_endline ("get_location : " ^ 
			      (string_of_int number) ^ " " ^ 
			      (string_of_peano tree))); 
	    true);  
    match tree with
      | Constant(_) -> if number = 0 then tree else invalid_arg "get_location"
      | Operation(info, op) ->
	  let left_nodes = node_number op.left in
	    if number < left_nodes then get_location number op.left
	    else if number < info.nodes - 1 then get_location (number - left_nodes) op.right
	    else if number = info.nodes - 1 then tree
	    else invalid_arg "get_location"
	      

  let rec replace_location number tree new_subtree = 
    assert( (print_endline ("replace_location : " ^ 
			      (string_of_int number) ^ " " ^ 
			      (string_of_peano tree) ^ " with " ^ 
			      (string_of_peano new_subtree))); 
	    true);  
    match tree with
      | Constant(_) -> if number = 0 then new_subtree else invalid_arg "replace_location"
      | Operation(info, op) ->
	  let left_nodes = node_number op.left in
	    if number < left_nodes then 
	      let replaced_left =  replace_location number op.left new_subtree in
		Operation( {nodes = (node_number replaced_left) + 
			       (node_number op.right) + 1;
			    height = 1 + 
			       (max (height replaced_left) (height op.right))},
			   {op with left = replaced_left}
			 )
	    else if number < info.nodes - 1 
	    then 
	      let replaced_right = 
		replace_location (number - left_nodes) op.right new_subtree 
	      in
		Operation( {nodes = (node_number replaced_right) + 
			       (node_number op.left) + 1;
			    height = 1 + 
			       (max (height replaced_right) (height op.left))},
			   {op with right = replaced_right}
			 )	      
	    else if number = info.nodes - 1 
	    then new_subtree
	    else invalid_arg "replace_location"
	      
	      
  let int_of_peano expr = 
    match expr with
      | Constant(_) -> 0
      | Operation(info, op) ->
	  match op.operation with
	    | Plus -> 1
	    | Times -> 2


  let rec peano_of_int i =
    match i with
      | 0 -> Constant(1 + (Random.int 9))
      | 1 | 2 -> 
	  begin 
	    let newop = match i with 1 -> Plus | 2 -> Times | _ -> invalid_arg "peano_of_int" in
	      Operation( {nodes = 3; height = 1},
			 { left = peano_of_int 0;
			   right = peano_of_int 0;
			   operation = newop
			 }
		       )
	  end
      | _ ->  invalid_arg "peano_of_int"     
	  

  let mutate expr = 
    let loc = random_location expr in
      (*  let subexpr = get_location loc expr in *)
    let new_subexpr = peano_of_int (Random.int 3) in
      replace_location loc expr new_subexpr
	

  let crossover expr1 expr2 = 
    let loc1 = random_location expr1 in
    let loc2 = random_location expr2 in
    let subexpr1 = get_location loc1 expr1 in
    let subexpr2 = get_location loc2 expr2 in
      (replace_location loc1 expr1 subexpr2,
       replace_location loc2 expr2 subexpr1)

  (* Less is better *)
  let relative_fitness expected expr = 
    let accuracy = abs ((eval_peano expr) - expected) in
    let nodes = node_number expr in
    let height = height expr in
      (accuracy , nodes, height)
	

  let to_string = string_of_peano 

end


module type GenePoolSig =
  functor (Genes : GeneSig) ->  
sig
  type 'a t

  val empty : int -> (Genes.t -> 'a) -> 'a t
  val insert : Genes.t -> 'a t -> 'a t
  val kill_unfit : 'a t -> 'a t
  val choose : 'a t -> Genes.t
  val get_fittest : 'a t -> Genes.t
  val get_unfittest : 'a t -> Genes.t
  val cardinal : 'a t -> int

end


module GenePool : GenePoolSig =
  functor (Genes : GeneSig) -> 
struct

(* Memoization for some optimisation *)
  type 'a t = 
      { pool : ( Genes.t * 'a ) list;
	max_size : int;
	fitness : Genes.t -> 'a }

  let empty size fitness = 
    { pool = []; max_size = size; fitness = fitness }
    

  let rec insert gene world = 
    let gene_val = world.fitness gene in

    let compare (gene1, fit1) (gene2, fit2) = 
      Pervasives.compare fit1 fit2
    in

    let rec aux_insert g l = 
      match l with
	| [] -> [g]
	| h :: t ->
	    if compare g h > 0
	    then h :: (aux_insert g t)
	    else g :: l
    in
      {world with pool = aux_insert (gene, gene_val) world.pool}
	

(* We want a tail-recursive version because it might use big memory *)
  let kill_unfit world = 
    let rec trim left l result =
      if left <= 0 then result
      else match l with
	  | [] -> result
	  | h::t -> trim (left - 1) t (h :: result)
    in
    let reverted_pool = trim world.max_size world.pool [] in
      {world with pool = List.rev reverted_pool}

  let choose world =
    let n = List.length world.pool in
    let i = Random.int n in
      fst (List.nth world.pool i)

  let get_fittest world = 
    if world.pool = [] then raise Not_found
    else fst (List.hd world.pool)

  let get_unfittest world = 
    get_fittest {world with pool = List.rev world.pool}

  let get_unfittest world = 

    let rec get_last l = 
      match l with
	| [] -> raise Not_found
	| [h] -> h
	| h::t -> get_last t
    in
      fst (get_last world.pool)

  let cardinal world = List.length world.pool

end


module ExprPool = GenePool(Gene)


let genetic_algorithm poolsize crossover_rate mutation_rate number = 
  let chosen_fitness = Gene.relative_fitness number in
  let nb_mutations = 1 + (int_of_float ((float_of_int poolsize) *. mutation_rate)) in
  let nb_crossovers = 1 + (int_of_float ((float_of_int poolsize) *. crossover_rate)) in

  let init_genepool = ExprPool.insert (Gene.Constant(1)) (ExprPool.empty poolsize chosen_fitness) in

  let rec cross_them basepool newpool left = 
    if left <= 0 then newpool
    else 
      let expr1 = ExprPool.choose basepool in
      let expr2 = ExprPool.choose basepool in
      let (expr3_1, expr3_2) = Gene.crossover expr1 expr2 in
	cross_them 
	  basepool 
	  (ExprPool.insert expr3_2 (ExprPool.insert expr3_1 newpool))
	  (left - 1)
  in

  let rec mutate_them basepool newpool left = 
    if left <= 0 then newpool
    else 
      let expr1 = ExprPool.choose basepool in
      let expr2 = Gene.mutate expr1 in
	mutate_them basepool (ExprPool.insert expr2 newpool) (left - 1)
  in

  let rec aux_ga genepool = 
    let fittest = ExprPool.get_fittest genepool in
(*    let unfittest = ExprPool.get_unfittest genepool in *)
      print_endline ( "### " ^ (Gene.to_string fittest) ^ ": " ^ (string_of_int (Gene.eval_peano fittest)));
(*      print_endline ( "��� " ^ (Gene.to_string unfittest) ^ ": " ^ (string_of_int (Gene.eval_peano unfittest))); *)
      let mutated = mutate_them genepool genepool nb_mutations in
      let crossed = cross_them genepool mutated nb_crossovers in
      let dead_unfit = ExprPool.kill_unfit crossed in
	aux_ga dead_unfit
  in

    aux_ga init_genepool



let usage_message=
  Sys.executable_name ^ "<n>"
  ^ "Tries to find a smallest tree expression whose value is close to, or better, equal, to <n>"


let poolsize = ref 100
let crossover_rate = ref 0.25
let mutation_rate = ref 0.1


let anonymous_fun str = 
  let n = int_of_string str in
  let _ = genetic_algorithm !poolsize !crossover_rate !mutation_rate n in
    ()


let args = 
  [("--poolsize",
    Arg.Set_int poolsize,
    "Sets the size of the genome pool"
   );
   ("--crossovers",
    Arg.Set_float crossover_rate,
    "Sets the rate of crossovers for each generation"
   );
   ("--mutations",
    Arg.Set_float mutation_rate,
    "Set the rate of mutations for each generation"
   )
  ]


let _ = 
  Random.self_init ();
  Arg.parse args anonymous_fun usage_message;
  exit 0


