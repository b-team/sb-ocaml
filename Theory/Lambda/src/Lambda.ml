(*******************************************************)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

(** The lambda terms abstract syntax. *)
module Make = 

functor (Misc : 
sig 
  val set_of : 'a list -> 'a list
end)

->

functor (Type : 
sig 
  type t
  val fresh : unit -> 'a
  val var : 'a -> t
  val arr : t -> t -> t
  val close : t -> 'a list -> t
  val base : t -> 'a list
  val subst : ('a -> t) -> t -> t
  val unify : ('a -> t) -> (t * t) list -> 'a -> t
end) 

-> 

struct

(** 
  The lambda terms abstract grammar is [t ::= x | t t | \x t]. 
*)
type t = 
    Var of string
  | App of t * t
  | Abs of (string -> t)

(** 
  The reference [count] gets the number of term variables. 
*)
let count = ref (-1) 

(** 
  The function [reset ()] sets [count] to its initial value. 
*)
let reset = fun () -> count := (-1) 

(** 
  The function [fresh ()] provides a fresh name for a new term variable. 
*)
let fresh = fun () -> incr count ; string_of_int !count 

(** 
  The function [subst f t] provides the term where the function [f] 
  is applied to all free variables of [t]. 
*)
let rec subst = fun f -> function 
    Var x -> f x 
  | App (u,v) -> App ((subst f u),(subst f v)) 
  | Abs t -> Abs (fun x -> subst f (t x)) 

(** 
  The function [bind x t] binds the variable [x] in the term [t]. 
*)
let bind = fun x t -> 
  let f = fun x y z -> if x = z then Var y else Var z 
  in (fun y -> subst (f x y) t) 

(** 
  The function [mem x t] is true if [x] is a free variable of [t] and false else. 
*)
let rec mem = fun e -> function 
    Var x -> x = e 
  | App (u,v) -> (mem e u) || (mem e v) 
  | Abs t -> mem e (t "") 

(** 
  The function [base t] returns the free variables set of [t]. 
*)
let rec base = function 
    Var x -> if x = "" then [] else [x] 
  | App (u,v) -> Misc.set_of ((base u) @ (base v)) 
  | Abs t -> base (t "") 

(** 
  The function [compare u v] returns 0 if [u] and [v] are equal, 
  -1 if [u] is smaller then [v] and 1 if [u] is greater than [v]. 
*)
let rec compare = fun w w' -> match (w,w') with 
    (Var x,Var x') -> String.compare x x'
  | (Var x,t) -> -1
  | (t,Var x) -> 1
  | (App (u,v),App (u',v')) -> 
      let n = compare v v'
      in if n = 0 then compare u u' else n
  | (App (u,v),t) -> -1
  | (t,App (u,v)) -> 1
  | (Abs t,Abs t') -> let x = fresh () in compare (t x) (t' x) 

(** 
  The function [norm b t] returns the term [t] normal form 
  in call-by-name reduction if b is true and in call-by-value else. 
*)
let rec norm = fun b -> function 
    Var x -> Var x 
  | App (Abs u,v) -> 
      let x = fresh () in 
      let f = fun y -> if x = y then (if b then v else norm b v) else Var y 
      in norm b (subst f (u x)) 
  | App (u,v) -> App (norm b u,norm b v)
  | Abs t -> Abs (fun x -> norm b (t x)) 

(** 
  The function [unify sigma epsilon] returns the most general unificator 
  that satisfy all term equations in [epsilon]. 
*)
let rec unify = fun sigma -> function 
    [] -> sigma 
  | (Var x,Var x') :: epsilon -> 
      if x = x' 
      then unify sigma epsilon 
      else 
	let f y = if x = y then Var x' else Var y 
	in unify (fun z -> subst f (sigma z)) 
	     (List.map (fun t -> ((subst f (fst t)),(subst f (snd t)))) epsilon) 
  | (Var x,t) :: epsilon 
  | (t,Var x) :: epsilon -> 
      if mem x t 
      then  
	let f y = if x = y then t else Var y 
	in unify (fun z -> subst f (sigma z)) 
	     (List.map (fun t -> ((subst f (fst t)),(subst f (snd t)))) epsilon) 
      else failwith "unification failed by occur-check\n" 
  | (App (u,v),App (u',v')) :: epsilon -> unify sigma ((u,u') :: (v,v') :: epsilon) 
  | (Abs t,u) :: epsilon 
  | (u,Abs t) :: epsilon -> 
      let x = fresh () 
      in unify sigma (((t x),u) :: epsilon) 

(** 
  The function [check (gamma,delta,epsilon,lambda)] 
  defines the type checking for all terms in [lambda]. 
*)
let rec check = function 
    (gamma,epsilon,(Var x,a) :: lambda) -> 
      if List.mem_assoc x gamma 
      then check (gamma,((a,(List.assoc x gamma)) :: epsilon),lambda) 
      else failwith "unpossible\n"
  | (gamma,epsilon,(App (u,v),a) :: lambda) -> 
      let b = Type.var (Type.fresh ()) in 
      let c = Type.var (Type.fresh ()) 
      in check (gamma,((b,(Type.arr c a)) :: epsilon),((u,b) :: (v,c) :: lambda)) 
  | (gamma,epsilon,(Abs t,a) :: lambda) -> 
      let b = Type.var (Type.fresh ()) in 
      let c = Type.var (Type.fresh ()) in 
      let x = fresh () 
      in check (((x,b) :: gamma),((a,(Type.arr b c)) :: epsilon),(((t x),c) :: lambda)) 
  | (gamma,epsilon,lambda)-> epsilon

(** 
  The function [infer t] provides the close term [t] main type. 
*)
let infer = fun t -> 
  let a = Type.var (Type.fresh ()) in 
  let epsilon = check ([],[],[(t,a)]) in 
  let sigma = Type.unify (fun x -> Type.var x) epsilon in 
  let ty = Type.subst sigma a 
  in Type.close ty (Type.base ty)

end
