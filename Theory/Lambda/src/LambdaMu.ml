(*******************************************************)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

(** The lambda-mu terms abstract syntax for simple type system. *)
module Make = 

functor (Misc : 
sig 
  val set_of : 'a list -> 'a list
end)

->

functor (Type : 
sig 
  type t
  val fresh : unit -> 'a
  val var : 'a -> t
  val arr : t -> t -> t
  val close : t -> 'a list -> t
  val base : t -> 'a list
  val subst : ('a -> t) -> t -> t
  val unify : ('a -> t) -> (t * t) list -> 'a -> t
end) 

-> 

struct

(** 
  The lambda-mu terms abstract grammar is [t ::= x | t t | \x t | �x c] and [c ::= [x] t]. 
*)
type t = 
    Var of string
  | App of t * t
  | Abs of (string -> t)
  | Mu  of (string -> c)
and c = string * t 
and e = 
    Lab of string 
  | Fun of string * t 
  | Arg of e * t 

(** 
  The function [fill t e] returns a command c where the term [t] fills the hole 
  inside the environment [e]. 
*)
let rec fill = fun t -> function 
    Lab x -> (x,t) 
  | Fun (x,u) -> (x,App(u,t)) 
  | Arg (e,u) -> fill (App (t,u)) e

(** 
  The reference [count] gets the number of term variables. 
*)
let count = ref (-1) 

(** 
  The function [reset ()] sets [count] to its initial value. 
*)
let reset = fun () -> count := (-1) 

(** 
  The function [fresh ()] provides a fresh name for a new term variable. 
*)
let fresh = fun () -> incr count ; string_of_int !count 

(** 
  The function [subst n f t] returns - assuming that f(x) = u -  
  [[u/x] t] if n = 0, 
  [[[x](v)u/[x]v] t] else if n = 1 
  [[[x](u)v/[x]v] t] and else. 
*)
let rec subst = fun n f -> function 
    Var x -> if n = 0 then f x else Var x
  | App (u,v) -> App ((subst n f u),(subst n f v)) 
  | Abs t -> Abs (fun x -> subst n f (t x)) 
  | Mu c -> Mu (fun x -> subst' n f (c x)) 
and subst' = fun n f -> function 
    (x,t) -> if n = 0 then (x,subst n f t) else if n = 1
    then begin try (x,App (t,f x)) with _ -> (x,subst n f t) end 
    else begin try (x,App (f x,t)) with _ -> (x,subst n f t) end  

(** 
  The function [bind x t] binds the variable [x] in the term [t]. 
*)
let bind = fun x t -> 
  let f = fun x y z -> if x = z then Var y else Var z 
  in (fun y -> subst 0 (f x y) t) 

(** 
  The function [mem x t] is true if [x] is a free variable of [t] and false else. 
*)
let rec mem = fun e -> function 
    Var x -> x = e 
  | App (u,v) -> (mem e u) || (mem e v) 
  | Abs t -> mem e (t "") 
  | Mu c -> mem' e (c "") 
and mem' = fun e -> function (x,t) -> if x = e then true else mem e t 

(** 
  The function [base t] returns the free variables set of [t]. 
*)
let rec base = function 
    Var x -> if x = "" then [] else [x] 
  | App (u,v) -> Misc.set_of ((base u) @ (base v)) 
  | Abs t -> base (t "") 
  | Mu c -> base' (c "") 
and base' = function (x,t) -> if x = "" then (base t) else Misc.set_of (x :: (base t))

(** 
  The function [compare u v] returns 0 if [u] and [v] are equal, 
  -1 if [u] is smaller then [v] and 1 if [u] is greater than [v]. 
*)
let rec compare = fun w w' -> match (w,w') with 
    (Var x,Var x') -> String.compare x x'
  | (Var x,t) -> -1
  | (t,Var x) -> 1
  | (App (u,v),App (u',v')) -> 
      let n = compare v v'
      in if n = 0 then compare u u' else n
  | (App (u,v),t) -> -1
  | (t,App (u,v)) -> 1
  | (Abs t,Abs t') -> let x = fresh () in compare (t x) (t' x) 
  | (Abs t,u) -> -1
  | (u,Abs t) -> 1
  | (Mu c,Mu c') -> let x = fresh () in compare' (c x) (c' x) 
and compare' = fun c c' -> match (c,c') with 
    ((x,t),(x',t')) -> 
      let n = String.compare x x' 
      in if n = 0 then compare t t' else n

(** 
  The function [norm b t] returns the term [t] normal form 
  in call-by-name reduction if b is true and in call-by-value else. 
*)
let rec norm = fun b -> function 
    Var x -> Var x 
  | App (t, Mu c) -> 
      let f = fun x y -> if x = y then t else failwith "no" 
      in Mu (fun x -> norm' b (subst' 2 (f x) (c x))) 
  | App (Abs u,v) -> 
      let x = fresh () in 
      let f = fun y -> if x = y then (if b then v else norm b v) else Var y 
      in norm b (subst 0 f (u x)) 
  | App (Mu c, t) -> 
      let f = fun x y -> if x = y then t else failwith "no" 
      in Mu (fun x -> norm' b (subst' 1 (f x) (c x)))
  | App (u,v) -> App (norm b u,norm b v)
  | Abs t -> Abs (fun x -> norm b (t x)) 
  | Mu c -> Mu (fun x -> norm' b (c x)) 
and norm' = fun b -> function 
    (x,Mu c) -> norm' b (c x)
  | (x,t) -> (x,norm b t)

(** 
  The function [unify sigma epsilon] returns the most general unificator 
  that satisfy all term equations in [epsilon]. 
*)
let rec unify = fun sigma -> function 
    [] -> sigma 
  | (Var x,Var x') :: epsilon -> 
      if x = x' 
      then unify sigma epsilon 
      else 
	let f y = if x = y then Var x' else Var y 
	in unify (fun z -> subst 0 f (sigma z)) 
	     (List.map (fun t -> ((subst 0 f (fst t)),(subst 0 f (snd t)))) epsilon) 
  | (Var x,t) :: epsilon 
  | (t,Var x) :: epsilon -> 
      if mem x t 
      then  
	let f y = if x = y then t else Var y 
	in unify (fun z -> subst 0 f (sigma z)) 
	     (List.map (fun t -> ((subst 0 f (fst t)),(subst 0 f (snd t)))) epsilon) 
      else failwith "unification failed by occur-check\n" 
  | (App (u,v),App (u',v')) :: epsilon -> unify sigma ((u,u') :: (v,v') :: epsilon) 
  | (Abs u,Mu v) :: epsilon 
  | (Mu v,Abs u) :: epsilon -> failwith "unification failed by clash\n" 
  | (Abs t,u) :: epsilon 
  | (u,Abs t) :: epsilon -> 
      let x = fresh () 
      in unify sigma (((t x),u) :: epsilon) 
  | (Mu c,Mu c') :: epsilon -> 
      let x = fresh () 
      in unify' sigma epsilon ((c x),(c' x))
  | _ -> failwith "unification failed by clash\n" 
and unify' = fun sigma epsilon -> function
    ((x,t),(x',t')) -> 
      if x = x' 
      then unify sigma ((t,t') :: epsilon) 
      else failwith "unification failed by clash\n" 

(** 
  The function [check (gamma,delta,epsilon,lambda)] 
  defines the type checking for all terms in [lambda]. 
*)
let rec check = function 
    (gamma,delta,epsilon,(Var x,a) :: lambda) -> 
      if List.mem_assoc x gamma 
      then check (gamma,delta,((a,(List.assoc x gamma)) :: epsilon),lambda) 
      else failwith "unpossible\n"
  | (gamma,delta,epsilon,(App (u,v),a) :: lambda) -> 
      let b = Type.var (Type.fresh ()) in 
      let c = Type.var (Type.fresh ()) 
      in check (gamma,delta,((b,(Type.arr c a)) :: epsilon),((u,b) :: (v,c) :: lambda)) 
  | (gamma,delta,epsilon,(Abs t,a) :: lambda) -> 
      let b = Type.var (Type.fresh ()) in 
      let c = Type.var (Type.fresh ()) in 
      let x = fresh () 
      in check (((x,b) :: gamma),delta,((a,(Type.arr b c)) :: epsilon),(((t x),c) :: lambda)) 
  | (gamma,delta,epsilon,(Mu c,a) :: lambda) -> 
      let x = fresh () 
      in check' (gamma,((x,a)::delta),epsilon,lambda,(c x)) 
  | (gamma,delta,epsilon,[])-> epsilon
and check' = function 
    (gamma,delta,epsilon,lambda,(x,t)) -> 
      if List.mem_assoc x delta 
      then check (gamma,delta,epsilon,((t,(List.assoc x delta))::lambda))
      else failwith "unpossible too\n" 

(** 
  The function [infer t] provides the close term [t] main type. 
*)
let infer = fun t -> 
  let a = Type.var (Type.fresh ()) in 
  let epsilon = check ([],[],[],[(t,a)]) in 
  let sigma = Type.unify (fun x -> Type.var x) epsilon in 
  let ty = Type.subst sigma a 
  in Type.close ty (Type.base ty)

end
